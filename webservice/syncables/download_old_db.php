<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	// global functions are located in /includes/functions.php
	// 	-- function getColumnIndex($cols, $search_value);
	//	-- function getValueFromIndex($vals, $index);
	//	-- function getValuePlaceHolders($vals);
	//	-- function clearValuesOfQuotes($values);
	// 	-- function getColumnToValueString($columns, $values);
	
	## -------------------- DOWNLOAD OLD DATABASE -------------------- ##
	// BMC 08.16.2016
	//	-- here i'll pull down all the data from the member id from the
	//		server to the tablet starting with the association codes
	// BMC 09.09.2016
	//	-- i'm going to have to implement the use of grouping becasue the
	//		json array is just too large to be read into the tablet all at once
	// BMC 09.12.2016
	//	-- i'm using an offset and limit for the tables in the old database
	//		we'll start off by sending the 0 offset and send the max record
	//		count+1 back to the user as the new offset.  the user can
	//		send in the limit if need by to alter the webservice depending
	//		on the memory, but we'll default the limit to 500 if nothing is
	//		sent.  
	$group 	= (isset($REQUEST['group']) 	? $REQUEST['group'] 	: "");
	$offset = (isset($_REQUEST['offset']) 	? $REQUEST['offset'] 	: "0");
	$limit	= (isset($REQUEST['limit']) 	? $REQUEST['limit'] 	: "500");
	
	switch($group) {
		case "ASSOCIATION_CODES": 
			## -------------------- codes_disposal -------------------- ##
			$response["codes_disposal"] = array();
			$sql = "SELECT *
					FROM codes_disposal
					ORDER BY disposal ASC";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_disposal"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_disposal";
				return json_encode($response);
			}
			
			## -------------------- codes_scur_score -------------------- ##
			$response["codes_scur_score"] = array();
			$sql = "SELECT *
					FROM codes_scur_score";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_scur_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_scur_score";
				return json_encode($response);
			}
			
			## -------------------- codes_color_score -------------------- ##
			$response["codes_color_score"] = array();
			$sql = "SELECT *
					FROM codes_color_score
					ORDER BY color_score_id ASC";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_color_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_color_score";
				return json_encode($response);
			}
			
			## -------------------- codes_calving_ease -------------------- ##
			$response["codes_calving_ease"] = array();
			$sql = "SELECT *
					FROM codes_calving_ease
                    ORDER BY calving_ease_id ASC";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_calving_ease"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_calving_ease";
				return json_encode($response);
			}
			
			## -------------------- codes_management -------------------- ##
			$response["codes_management"] = array();
			$sql = "SELECT *
					FROM codes_management
                    ORDER BY management ASC";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_management"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_management";
				return json_encode($response);
			}
			
			## -------------------- codes_navel_score -------------------- ##
			$response["codes_navel_score"] = array();
			$sql = "SELECT *
					FROM codes_navel_score
                    ORDER BY navel_score_id ASC";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_navel_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_navel_score";
				return json_encode($response);
			}
			
			## -------------------- codes_sheath_score -------------------- ##
			$response["codes_sheath_score"] = array();
			$sql = "SELECT *
					FROM codes_sheath_score
                    ORDER BY sheath_score_id ASC";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_sheath_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_sheath_score";
				return json_encode($response);
			}
			
			## -------------------- codes_temper_score -------------------- ##
			$response["codes_temper_score"] = array();
			$sql = "SELECT *
					FROM codes_temper_score
                    ORDER BY temper_score_id ASC";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
	
					array_push($response["codes_temper_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_temper_score";
				return json_encode($response);
			}
			
			## -------------------- codes_twin_code -------------------- ##
			$response["codes_twin_code"] = array();
			$sql = "SELECT *
					FROM codes_twin_code
                    ORDER BY twin_code ASC";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_twin_code"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_twin_code";
				return json_encode($response);
			}
			
			## -------------------- codes_udder_score_suspension -------------------- ##
			$response["codes_udder_score_suspension"] = array();
			$sql = "SELECT *
					FROM codes_udder_score_suspension
                    ORDER BY suspension_id ASC";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_udder_score_suspension"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_udder_score_suspension";
				return json_encode($response);
			}
			
			## -------------------- codes_udder_score_teat_size -------------------- ##
			$response["codes_udder_score_teat_size"] = array();
			$sql = "SELECT *
					FROM codes_udder_score_teat_size
                    ORDER BY teat_size_id ASC";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_udder_score_teat_size"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_udder_score_teat_size";
				return json_encode($response);
			}
			
			## -------------------- codes_breed -------------------- ##
			$response["codes_breed"] = array();
			$sql = "SELECT *
					FROM codes_breed";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_breed"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_breed";
				return json_encode($response);
			}
			
			## -------------------- codes_body_condition -------------------- ##
			$response["codes_body_condition"] = array();
			$sql = "SELECT *
					FROM codes_body_condition
                    ORDER BY body_condition_score ASC";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_body_condition"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_body_condition";
				return json_encode($response);
			}

            ## -------------------- codes_marking_location -------------------- ##
            $response["codes_marking_location"] = array();
            $sql = "SELECT *
					FROM codes_marking_location
                    ORDER BY marking_location ASC";
            $rs = $this->PDO->recordSetQuery($sql);
            if($rs) {
                while(!$rs->EOF) {
                    array_push($response["codes_marking_location"], $rs->fields);
                    $rs->MoveNext();
                }
            } else {
                $response["success"] = false;
                $response["message"] = "failed to sync data at codes_marking_location";
                return json_encode($response);
            }
			break;
		case "tbl_animal":
			## -------------------- tbl_animal -------------------- ##
			$response["tbl_animal"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM tbl_animal a 
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND a.animal_record_status = 'A'
						
						UNION
		
						SELECT a.*
						FROM tbl_animal a
							INNER JOIN tbl_semen_inventory s ON a.animal_registration = s.animal_registration
							LEFT JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
								AND NOT o.superceded_flag
								AND o.is_primary
						WHERE s.member_id = '".$member_id."'
							AND o.owner_id <> '".$member_id."'
											
						UNION
							
						SELECT a.*
						FROM tbl_animal a
							INNER JOIN tbl_embryo_inventory e ON a.animal_registration = e.dam_registration
							LEFT JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
								AND NOT o.superceded_flag
								AND o.is_primary
						WHERE e.member_id = '".$member_id."'
								AND o.owner_id <> '".$member_id."') x
					ORDER BY x.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal";
				return json_encode($response);
			}
			break;
		case "tbl_animal_data_birth":
			## -------------------- tbl_animal_data_birth -------------------- ##
			$response["tbl_animal_data_birth"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM tbl_animal_data_birth a
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') x
					ORDER BY x.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_data_birth"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_data_birth";
				return json_encode($response);
			}
			break;
		case "tbl_animal_data_wean":
			## -------------------- tbl_animal_data_wean -------------------- ##
			$response["tbl_animal_data_wean"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_data_wean a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_data_wean"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_data_wean";
				return json_encode($response);
			}
			break;
		case "tbl_animal_data_year":
			## -------------------- tbl_animal_data_year -------------------- ##
			$response["tbl_animal_data_year"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_data_year a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_data_year"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
	
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_data_year";
				return json_encode($response);
			}
			break;
		case "tbl_animal_data_ultrasound":
			## -------------------- tbl_animal_data_ultrasound -------------------- ##
			$response["tbl_animal_data_ultrasound"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_data_ultrasound a
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration 
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_data_ultrasound"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_data_ultrasound";
				return json_encode($response);
			}
			break;
		case "tbl_animal_data_carcass":
			## -------------------- tbl_animal_data_carcass -------------------- ##
			$response["tbl_animal_data_carcass"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_data_carcass a
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary) z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_data_carcass"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_data_carcass";
				return json_encode($response);
			}
			break;
		case "tbl_animal_health":
			## -------------------- tbl_animal_health -------------------- ##
			$response["tbl_animal_health"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_health a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_health"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_health";
				return json_encode($response);
			}
			break;
		case "tbl_animal_notes":
			## -------------------- tbl_animal_notes -------------------- ##
			$response["tbl_animal_notes"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_notes a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_notes"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_notes";
				return json_encode($response);
			}
			break;
		case "tbl_animal_data_misc":
			## -------------------- tbl_animal_data_misc -------------------- ##
			$response["tbl_animal_data_misc"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_data_misc a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_data_misc"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_data_misc";
				return json_encode($response);
			}
			break;
		case "tbl_animal_heat":
			## -------------------- tbl_animal_heat -------------------- ##
			$response["tbl_animal_heat"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_heat a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_heat"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_heat";
				return json_encode($response);
			}
			break;
		case "tbl_animal_sync":
			## -------------------- tbl_animal_sync -------------------- ##
			$response["tbl_animal_sync"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_sync a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_sync"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_sync";
				return json_encode($response);
			}
			break;
		case "tbl_animal_data_other_weight":
			## -------------------- tbl_animal_data_other_weight -------------------- ##
			$response["tbl_animal_data_other_weight"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_data_other_weight a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_data_other_weight"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_data_other_weight";
				return json_encode($response);
			}
			break;
		case "tbl_animal_data_mature":
			## -------------------- tbl_animal_data_mature -------------------- ##
			$response["tbl_animal_data_mature"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_data_mature a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_data_mature"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_data_mature";
				return json_encode($response);
			}
			break;
		case "tbl_animal_photo":
			## -------------------- tbl_animal_photo -------------------- ##
			$response["tbl_animal_photo"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_photo a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_photo"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_photo";
				return json_encode($response);
			}
			break;
		case "tbl_animal_location":
			## -------------------- tbl_animal_location -------------------- ##
			$response["tbl_animal_location"] = array();
			$sql = "SELECT z.* 
	
					FROM (SELECT a.*
						FROM tbl_animal_location a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_location"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_location";
				return json_encode($response);
			}
			break;
		case "tbl_animal_pen_location":
			## -------------------- tbl_animal_pen_location -------------------- ##
			$response["tbl_animal_pen_location"] = array();
			$sql = "SELECT z.* 
					FROM (SELECT a.*
						FROM tbl_animal_pen_location a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_pen_location"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_pen_location";
				return json_encode($response);
			}
			break;
		case "tbl_animal_assessment":
			## -------------------- tbl_animal_assessment -------------------- ##
			$response["tbl_animal_assessment"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_animal_assessment a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.animal_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_animal_assessment"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_animal_assessment";
				return json_encode($response);
			}
			break;
		case "tbl_breeding_ai":
			## -------------------- tbl_breeding_ai -------------------- ##
			$response["tbl_breeding_ai"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_breeding_ai a 
							INNER JOIN tbl_animal x ON a.cow_registration = x.animal_registration
								OR a.bull_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.bull_registration
								OR o.animal_registration = a.cow_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.breeding_ai_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_breeding_ai"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_breeding_ai";
				return json_encode($response);
			}
			break;
		case "tbl_breeding_agreement":
			## -------------------- tbl_breeding_agreement -------------------- ##
			$response["tbl_breeding_agreement"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_breeding_agreement a 
							INNER JOIN tbl_animal x ON a.cow_registration = x.animal_registration
								OR a.bull_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.bull_registration
								OR o.animal_registration = a.cow_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.breeding_agreement_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_breeding_agreement"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_breeding_agreement";
				return json_encode($response);
			}
			break;
		case "tbl_breeding_et":
			## -------------------- tbl_breeding_et -------------------- ##
			$response["tbl_breeding_et"] = array();
			$sql = "SELECT z.* 
					FROM (SELECT a.*
						FROM tbl_breeding_et a 
							INNER JOIN tbl_animal x ON a.cow_registration = x.animal_registration
								OR a.bull_registration = x.animal_registration
								OR a.recip_registration = x.animal_registration
							INNER JOIN tbl_ownership o ON o.animal_registration = a.bull_registration
								OR o.animal_registration = a.cow_registration
								OR o.animal_registration = a.recip_registration
						WHERE o.owner_id = '".$member_id."'
							AND NOT o.superceded_flag
							AND o.is_primary
							AND x.animal_record_status = 'A') z
					ORDER BY z.breeding_et_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_breeding_et"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_breeding_et";
				return json_encode($response);
			}
			break;
		case "tbl_semen_inventory":
			## -------------------- tbl_semen_inventory -------------------- ##
			$response["tbl_semen_inventory"] = array();
			$sql = "SELECT z.* 
					FROM (SELECT a.*
						FROM tbl_semen_inventory a 
						WHERE a.member_id = '".$member_id."') z
					ORDER BY z.pk_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_semen_inventory"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_semen_inventory";
				return json_encode($response);
			}
			break;
		case "tbl_embryo_inventory":
			## -------------------- tbl_embryo_inventory -------------------- ##
			$response["tbl_embryo_inventory"] = array();
			$sql = "SELECT z.* 
					FROM (SELECT a.*
						FROM tbl_embryo_inventory a 
						WHERE a.member_id = '".$member_id."') z
					ORDER BY z.pk_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_embryo_inventory"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_embryo_inventory";
				return json_encode($response);
			}
			break;
		case "tbl_member":
			## -------------------- tbl_member -------------------- ##
			$response["tbl_member"] = array();
			$sql = "SELECT z.* 
					FROM (SELECT a.*
						FROM tbl_member a 
						WHERE a.member_id = '".$member_id."') z
					ORDER BY z.member_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_member"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_member";
				return json_encode($response);
			}
			break;
		case "tbl_member_transfers":
			## -------------------- tbl_member_transfers -------------------- ##
	
			$response["tbl_member_transfers"] = array();
			$sql = "SELECT z.* 
					FROM (SELECT a.*
						FROM tbl_member_transfers a 
						WHERE a.member_id = '".$member_id."') z
					ORDER BY z.pk_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_member_transfers"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_member_transfers";
				return json_encode($response);
			}
			break;
		case "tbl_pastures":
			## -------------------- tbl_pastures -------------------- ##
			$response["tbl_pastures"] = array();
			$sql = "SELECT z.* 
					FROM (SELECT a.*
						FROM tbl_pastures a 
						WHERE a.member_id = '".$member_id."') z
					ORDER BY z.pk_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_pastures"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_pastures";
				return json_encode($response);
			}
			break;
		case "tbl_pens":
			## -------------------- tbl_pens -------------------- ##
			$response["tbl_pens"] = array();
			$sql = "SELECT z.* 
					FROM (SELECT a.*
						FROM tbl_pens a 
						WHERE a.member_id = '".$member_id."') z
					ORDER BY z.pen_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_pens"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_pens";
				return json_encode($response);
			}
			break;
		case "tbl_todo":
			## -------------------- tbl_todo -------------------- ##
			$response["tbl_todo"] = array();
			$sql = "SELECT z.* 
					FROM (SELECT a.*
						FROM tbl_todo a 
						WHERE a.member_id = '".$member_id."') z
					ORDER BY z.todo_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_todo"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_todo";
				return json_encode($response);
			}
			break;
		case "tbl_vaccinations":
			## -------------------- tbl_vaccinations -------------------- ##
			$response["tbl_vaccinations"] = array();
			$sql = "SELECT z.* 
					FROM (SELECT a.*
						FROM tbl_vaccinations a 
						WHERE a.member_id = '".$member_id."') z
					ORDER BY z.pk_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
	
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_vaccinations"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_vaccinations";
				return json_encode($response);
			}
			break;
		case "tbl_work_recieved":
			## -------------------- tbl_work_recieved -------------------- ##
			$response["tbl_work_recieved"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_work_recieved a 
	
						WHERE a.member_id = '".$member_id."'
							AND (CURRENT_DATE - DATE(a.date_received)) < 365) z
					ORDER BY z.work_received_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_work_recieved"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_work_recieved";
				return json_encode($response);
			}
			break;
		case "tbl_invoice_items":
			## -------------------- tbl_invoice_items -------------------- ##
			$response["tbl_invoice_items"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_invoice_items a 
						WHERE a.member_id = '".$member_id."'
							AND (CURRENT_DATE - DATE(a.date_received)) < 365) z
					ORDER BY z.invoice_item_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_invoice_items"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_invoice_items";
				return json_encode($response);
			}
			break;
		case "vw_member_customers":
			## -------------------- vw_member_customers -------------------- ##
			$response["vw_member_customers"] = array();
			$sql = "SELECT z.*
					FROM (SELECT * ";
					if($provider == "AGVA_CATTLE") {
						// BMC 09.09.2016
						//	-- fix for USA Gelbvieh
						$sql .= " , '' AS member_dba, 
									'' AS herd_code, 
									'' AS hold_brand_location, 
									'' tattoo_location ";
					}
				$sql .= " FROM vw_member_customers 
						WHERE owner_id='".$member_id."'
					
						UNION
						
							(SELECT m.member_id AS customer_id, m.member_name AS customer_name, b.address_line1, b.address_line2, b.address_city, b.address_state,
								  b.address_postal_code, upper(isnull(b.address_country, 'UNITED STATES') ::text) AS address_country, b.address_email,
								  count(*) AS num_purch, max(o.ownership_date) AS date_purch, o.old_owner_id AS owner_id, b.latitude, b.longitude,
								  b.avg_latitude, b.avg_longitude, b.address_id, m.member_dba, m.herd_code, m.hold_brand_location, m.phn_location AS tattoo_location
							  FROM tbl_member m
								  JOIN tbl_address b ON m.general_correspondance_address = b.address_id
								  JOIN tbl_ownership o ON m.member_id = o.owner_id
								  JOIN tbl_animal a ON o.animal_registration::text = a.animal_registration::text
							  WHERE o.owner_id = '".$member_id."'
							  GROUP BY m.member_id, m.member_name, b.address_line1, b.address_line2, b.address_city, b.address_state, b.address_postal_code,
								  upper(isnull(b.address_country, 'UNITED STATES') ::text), b.address_email, o.old_owner_id, b.latitude, b.longitude,
								  b.avg_latitude, b.avg_longitude, b.address_id, m.member_dba, m.herd_code, m.hold_brand_location, m.phn_location)
						
						UNION
						
							(SELECT m.member_id AS customer_id, m.member_name AS customer_name, b.address_line1, b.address_line2, b.address_city, b.address_state,
								  b.address_postal_code, upper(isnull(b.address_country, 'UNITED STATES') ::text) AS address_country, b.address_email,
								  count(*) AS num_purch, max(o.ownership_date) AS date_purch, o.old_owner_id AS owner_id, b.latitude, b.longitude,
								  b.avg_latitude, b.avg_longitude, b.address_id, m.member_dba, m.herd_code, m.hold_brand_location, m.phn_location AS tattoo_location
							  FROM tbl_member m
								  JOIN tbl_address b ON m.general_correspondance_address = b.address_id
								  JOIN tbl_ownership o ON m.member_id = o.owner_id
								  JOIN tbl_animal a ON o.animal_registration::text = a.animal_registration::text
							  WHERE o.owner_id::varchar IN (SELECT t.customer_id
																FROM tbl_member_transfers t
																WHERE t.member_id = '".$member_id."'
																GROUP BY t.customer_id)
							  GROUP BY m.member_id, m.member_name, b.address_line1, b.address_line2, b.address_city, b.address_state, b.address_postal_code,
								  upper(isnull(b.address_country, 'UNITED STATES') ::text), b.address_email, o.old_owner_id, b.latitude, b.longitude,
								  b.avg_latitude, b.avg_longitude, b.address_id, m.member_dba, m.herd_code, m.hold_brand_location, m.phn_location)) z
					ORDER BY z.customer_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["vw_member_customers"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at vw_member_customers";
				return json_encode($response);
			}
			break;
		case "vw_animal_epd":
			## -------------------- vw_animal_epd -------------------- ##
			// TODO BMC 09.08.2016
			//	-- get all the epds and not just the current year/season's epds
			$response["vw_animal_epd"] = array();
			// get current year and season
			$year 	= date("Y");
			$season = (date('n',time()) < 4 ? 'spring' : (date('n',time()) > 9 ? 'spring' : 'fall'));
			// set epd table
			
			$my_seasons = array('fall', 'spring');
			if ($season == 'fall') {
				$season_index = 0;	
			} else {
				$season_index = 1;
			}
			
			$sql_msg = "";
			$my_years = $year;
			while ($my_years >= 2013) {
				$EPD_TABLE = "tbl_epd_".$my_years."_".$my_seasons[$season_index];
	
				// get epd data
				$sql = "SELECT z.*
						FROM (SELECT e.* 
							FROM ".$EPD_TABLE." e
								INNER JOIN tbl_ownership o ON o.animal_registration = e.animal_registration
								INNER JOIN tbl_animal a ON a.animal_registration = o.animal_registration
							WHERE o.owner_id = '".$member_id."'
								AND NOT o.superceded_flag
								AND o.is_primary
								AND a.animal_record_status = 'A'
							
							UNION
							
							(SELECT e.* 
							FROM ".$EPD_TABLE." e
								INNER JOIN tbl_semen_inventory s ON e.animal_registration = s.animal_registration
							WHERE s.member_id = '".$member_id."')
							
							UNION
							
							(SELECT e.* 
							FROM ".$EPD_TABLE." e
								INNER JOIN tbl_embryo_inventory ei ON e.animal_registration = ei.dam_registration
							WHERE ei.member_id = '".$member_id."')) z
						ORDER BY z.animal_registration ASC
						LIMIT ".$limit." OFFSET ".$offset;
					$sql_msg .= "\n" . $sql;	
				
				$rs = $this->PDO->recordSetQuery($sql);
				
				if($rs) {
					$response["offset"] = $offset + $limit + 1;
					$response["record_count"] = $rs->RecordCount();
					if (!$rs->EOF) {
						while(!$rs->EOF) {
							array_push($response["vw_animal_epd"], $rs->fields);
							$rs->MoveNext();
						}
						break; //exit loop
					} 
				}
				//$response["message2"] = $sql_msg;
				
				// change seasons
				if ($season_index < 1) {
					$season_index++;	
				} else {
					# decrement years
					$my_years--;
					$season_index = 0;	
				}	
			}
			break;
		case "tbl_ownership":
			## -------------------- tbl_ownership -------------------- ##
			$response["tbl_ownership"] = array();
			$sql = "SELECT z.*
					FROM (SELECT a.*
						FROM tbl_ownership a 
							INNER JOIN tbl_animal x ON a.animal_registration = x.animal_registration
						WHERE a.owner_id = '".$member_id."'
							AND x.animal_record_status = 'A') z
					ORDER BY z.animal_registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_ownership"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_ownership";
				return json_encode($response);
			}
			break;
		case "reports":	// using the name of the app table name
			## -------------------- tbl_member_reports_android -------------------- ##
			$response["reports"] = array();
			$sql = "SELECT z.*
					FROM (SELECT *
						FROM tbl_member_reports_android
						WHERE member_id = '".$member_id."') z
					ORDER BY z.pk_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["reports"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at reports";
				return json_encode($response);
			}
			break;
		case "customer":
			## -------------------- customer -------------------- ##
			$response["customer"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM customer
						WHERE member_id = '".$member_id."') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["customer"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at customer";
				return json_encode($response);
			}
			break;
		case "customer_ownership":
			## -------------------- customer_ownership -------------------- ##
			$response["customer_ownership"] = array();
			$sql = "SELECT x.*
					FROM (SELECT o.*
						FROM customer_ownership o
							INNER JOIN customer c ON o.customer_id = c.customer_id
						WHERE c.member_id = '".$member_id."') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["customer_ownership"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at customer_ownership";
				return json_encode($response);
			}
			break;
		case "tbl_multisire":
			## -------------------- tbl_multisire -------------------- ##
			$response["tbl_multisire"] = array();
			$sql = "SELECT x.*
					FROM (SELECT m.*
						FROM tbl_multisire m
						WHERE m.member_id = '".$member_id."') x
					ORDER BY x.multisire_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_multisire"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_multisire";
				return json_encode($response);
			}
			break;
		case "tbl_multisire_component":
			## -------------------- tbl_multisire_component -------------------- ##
			$response["tbl_multisire_component"] = array();
			$sql = "SELECT x.*
					FROM (SELECT c.*
						FROM tbl_multisire_component c
							INNER JOIN tbl_multisire m ON c.multisire_id = m.multisire_id
						WHERE m.member_id = '".$member_id."') x
					ORDER BY x.multisire_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tbl_multisire_component"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_multisire_component";
				return json_encode($response);
			}
			break;
        case "eid_sequences":
            ## -------------------- eid_sequences -------------------- ##
            $response["eid_sequences"] = array();
            $sql = "SELECT x.*
					FROM (SELECT e.*
						FROM eid_sequences e
						WHERE e.member_id = '".$member_id."') x
					ORDER BY x.eid ASC
					LIMIT ".$limit." OFFSET ".$offset;
            $rs = $this->PDO->recordSetQuery($sql);
            if($rs) {
                $response["offset"] = $offset + $limit + 1;
                $response["record_count"] = $rs->RecordCount();
                while(!$rs->EOF) {
                    array_push($response["eid_sequences"], $rs->fields);
                    $rs->MoveNext();
                }
            } else {
                $response["success"] = false;
                $response["message"] = "failed to sync data at eid_sequences";
                return json_encode($response);
            }
            break;
		default:
			$response["success"] = false;
			$response["message"] = "failed to sync data at group: ".$group;
			return json_encode($response);
			break;
	}
?>
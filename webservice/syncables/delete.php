    <?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	// global functions are located in /includes/functions.php
	// 	-- function getColumnIndex($cols, $search_value);
	//	-- function getValueFromIndex($vals, $index);
	//	-- function getValuePlaceHolders($vals);
	//	-- function clearValuesOfQuotes($values);
	// 	-- function getColumnToValueString($columns, $values);
	
	## -------------------- DELETE -------------------- ##
	// BMC 10.04.2016
	//	-- i'll assemble the query based on the table selected, taking into account
	//		any special cases that occur as well as the general action which will be
	//		put into the default case
	switch($table_string) {
		default:
			break;
	}
	
	// BMC 10.05.2016
	//	-- check each query to see if they contain the temporary registration, if so
	//		then we'll need to alter the data so things can go in properly
    if (strpos($sql_query, 'TEMP_REG_DB') !== false) {
        $sql_query		= modifySqlQueryWithActualReg($columns_array, $values_array, $sql_query, $PDO);
    }
    if (strpos($where_clause, 'TEMP_REG_DB') !== false) {
        $where_clause	= modifyWhereClauseWithActualReg($where_clause, $PDO);
    }
	
	// handle the general query here
	$sql = "DELETE FROM ".$table_string;
	if(!is_null($where_clause) && trim($where_clause) != "") {
		$sql .= " WHERE ".trim($where_clause);
	}
	
	// the delete command needs no parameters, but as an extra measure of security we will
	// make sure that a where clause is always present
	if(is_null($where_clause) 
	|| trim($where_clause) == "" 
	|| trim($where_clause) == "1") {
		$API = new SyncAdapter($PDO);
		$message 	= "<p><strong>Command:</strong> DELETE</p><p><strong>SQL:</strong> ".$sql."</p>";
		$email 		= "brandon@digitalbeef.com";
		$subject 	= "Failed API Attempt";
		$API->sendMail($message, $subject, $email);
		
		// do not execute the query
		$response["success"] 	= false;
		$response["message"] 	= "failed to push syncable query";
		$response["sql_query"] 	= $sql . " [" . implode(", ", $params) . "]";
		$json = json_encode($response);
	} else {
		// write query to the database
		if($PDO->executeQuery($sql)) {
			// successfully input the query
			$response["success"] 	= true;
			$response["message"] 	= "syncable query pushed successfully";
			$response["sql_query"] 	= $sql . " [" . implode(", ", $params) . "]";
			$json = json_encode($response);
		} else {
			// if the query fails then try to do the old method before 
			// completely giving up on it
			include_once(SITE_ROOT . "/webservice/syncables/query.php");
		}
	}
    ?>
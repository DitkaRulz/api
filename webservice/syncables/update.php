    <?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	// global functions are located in /includes/functions.php
	// 	-- function getColumnIndex($cols, $search_value);
	//	-- function getValueFromIndex($vals, $index);
	//	-- function getValuePlaceHolders($vals);
	//	-- function clearValuesOfQuotes($values);
	// 	-- function getColumnToValueString($columns, $values);
	
	// BMC 10.14.2016
	//	-- NOTE -- We can only update the columns/values for the SET part.
	//				the where clause is just a string.
    // BMC 06.01.2017
    //  -- currently the TEMP_REGISTRATION updates aren't being converted to the actual
    //      correct query, because the WHERE clause isn't changing the temp reg to the actual
    //      registration.  we need to fix this.
	
	## -------------------- UPDATE -------------------- ##
	// BMC 10.04.2016
	//	-- i'll assemble the query based on the table selected, taking into account
	//		any special cases that occur as well as the general action which will be
	//		put into the default case
	switch($table_string) {
		case "tbl_animal_data_birth":
			// functions that look like this:
			//	UPDATE tbl_animal_data_birth SET bw_ratio = '0.0' WHERE animal_registration = 'DB17395'
			// 	we need to get rid of the 0.0 values
			$value = getValueFromIndex($values_array, getColumnIndex($columns_array, "bw_ratio"));
			if($value == "0.0") {
				// change the 0.0 to a 0, as the bw_ratio is not a double percision on
				// the old database, it's a BIGINT.
				$values_array = setValueAtIndex(getColumnIndex($columns_array, "bw_ratio"), "0", $values_array);
			}
			break;
		case "animal_location":
			if (strpos($where_clause, "move_out IS NULL OR move_out = 'null'") !== false) {
				$where_clause = str_replace("move_out IS NULL OR move_out = 'null'", "move_out IS NULL", $where_clause);
			}

            if (strpos($where_clause, "OR move_out = ''") !== false) {
                $where_clause = str_replace("OR move_out = ''", "OR move_out IS NULL", $where_clause);
            }

            if (strpos($where_clause, "OR move_out = 'null'") !== false) {
                $where_clause = str_replace("OR move_out = 'null'", "OR move_out IS NULL", $where_clause);
            }
			break;
		case "tbl_animal_location":
			if (strpos($where_clause, "move_out_date IS NULL OR move_out_date = 'null'") !== false) {
				$where_clause = str_replace("move_out_date IS NULL OR move_out_date = 'null'", "move_out_date IS NULL", $where_clause);
			}

            if (strpos($where_clause, "OR move_out_date = ''") !== false) {
                $where_clause = str_replace("OR move_out_date = ''", "OR move_out_date IS NULL", $where_clause);
            }

            if (strpos($where_clause, "OR move_out_date = 'null'") !== false) {
                $where_clause = str_replace("OR move_out_date = 'null'", "OR move_out_date IS NULL", $where_clause);
            }
			break;
		case "tbl_animal_pen_location":
			//UPDATE tbl_animal_pen_location SET update_stamp = ?, expired = ?, 
			// move_out_date = ?, update_user = ? WHERE animal_registration = 'TEMP_REG_DBC00076913' 
			// AND (move_out_date IS NULL OR move_out_date = 'null')
			if (strpos($where_clause, "move_out_date IS NULL OR move_out_date = 'null'") !== false) {
				$where_clause = str_replace("move_out_date IS NULL OR move_out_date = 'null'", "move_out_date IS NULL", $where_clause);
			}
			break;
		case "pen_location":
			// UPDATE pen_location SET move_out = ? WHERE registration = 'CF15836' AND (move_out IS NULL OR move_out = 'null')
			// the move_out appears twice so we need a clean way to get to every instance of the move_out
			if (strpos($where_clause, "move_out IS NULL OR move_out = 'null'") !== false) {
				$where_clause = str_replace("move_out IS NULL OR move_out = 'null'", "move_out IS NULL", $where_clause);
			}
			break;
		case "tbl_breeding_agreement":
			// UPDATE tbl_breeding_agreement 
			// SET update_stamp = '2016-09-22', palpation_date = '2016-09-22', update_user = '4245', pasture_id = '', palpation_result = '3' 
			// WHERE breeding_agreement_id = '722204' AND cow_registration = 'AMGV1184486' AND bull_registration = 'AMGV1279164'
			$value = getValueFromIndex($values_array, getColumnIndex($columns_array, "pasture_id"));
			if($value == "") {
				$values_array = setValueAtIndex(getColumnIndex($columns_array, "pasture_id"), "0", $values_array);
			}
			break;
		default:
			break;
	}

    /*
            $sql_query 			= (isset($REQUEST['sql_query']) 		? $REQUEST['sql_query'] 		: "");
            $command 			= (isset($REQUEST['command']) 			? $REQUEST['command'] 			: NULL);
            $table_string 		= (isset($REQUEST['table_string']) 		? $REQUEST['table_string'] 		: NULL);
            $columns_array 		= (isset($REQUEST['columns_array']) 	? $REQUEST['columns_array'] 	: NULL);
            $values_array 		= (isset($REQUEST['values_array']) 		? $REQUEST['values_array'] 		: NULL);
            $where_clause 		= (isset($REQUEST['where_clause']) 		? $REQUEST['where_clause'] 		: NULL);
     */

	// BMC 10.05.2016
	//	-- check each query to see if they contain the temporary registration, if so
	//		then we'll need to alter the data so things can go in properly
	if (strpos($sql_query, 'TEMP_REG_DB') !== false) {
		$sql_query		= modifySqlQueryWithActualReg($columns_array, $values_array, $sql_query, $PDO);
	}
    if (strpos($values_array, 'TEMP_REG_DB') !== false) {
        $values_array 	= modifyValuesWithActualReg($columns_array, $values_array, $PDO);
    }
    if (strpos($where_clause, 'TEMP_REG_DB') !== false) {
        $where_clause	= modifyWhereClauseWithActualReg($where_clause, $PDO);
    }
	
	// handle the general query here
	$sql = "UPDATE ".$table_string;
	$sql .= " SET ".getColumnToValueString($columns_array, $values_array);
	if(!is_null($where_clause) && trim($where_clause) != "") {
		$sql .= " WHERE ".trim($where_clause);
	}
	
	// set the parameters
	//	we'll need to clean the values of single quotes as we're going to use the pdo params method
	// 	to store the data.
	$params = clearValuesOfQuotes(explode(",", $values_array));
	
	// write query to the database
	if($PDO->executeQuery($sql, $params)) {
		// successfully input the query
		$response["success"] 	= true;
		$response["message"] 	= "syncable query pushed successfully";
		$response["sql_query"] 	= $sql . " [" . implode(", ", $params) . "]";
		$json = json_encode($response);
	} else {	
		// if the query fails then try to do the old method before 
		// completely giving up on it
		include_once(SITE_ROOT . "/webservice/syncables/query.php");
	}
    ?>
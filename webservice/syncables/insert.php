    <?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FLAGS -------------------- ##
	$isFeedback 		= false;
	$isAnimalTable		= false;
	$isCustomerOwnership= false;	
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	// global functions are located in /includes/functions.php
	// 	-- function getColumnIndex($cols, $search_value);
	//	-- function getValueFromIndex($vals, $index);
	//	-- function getValuePlaceHolders($vals);
	//	-- function clearValuesOfQuotes($values);
	// 	-- function getColumnToValueString($columns, $values);
	
	## -------------------- INSERT -------------------- ##
	// BMC 10.04.2016
	//	-- i'll assemble the query based on the table selected, taking into account
	//		any special cases that occur as well as the general action which will be
	//		put into the default case
	switch($table_string) {
		case "animal":
		case "tbl_animal":
			$isAnimalTable = true;
			// BMC 10.05.2016
			//	-- the user will be inserting a new animal record and we will need to know how to
			//		handle those animals based on the animal registrations
			//	-- first we will need to get the animals registration and determine if it's a temporary
			//		registration or not.
			if($PDO->DB_TYPE === "NEW") {
				$registration = getValueFromIndex($values_array, getColumnIndex($columns_array, "registration"));
			} else if($PDO->DB_TYPE === "OLD") {
				$registration = getValueFromIndex($values_array, getColumnIndex($columns_array, "animal_registration"));
			} else {
				$registration = "";
			}
			
			// check if the animal registration is temporary
			$response["has_temp_registration"] = false;
			if(strpos($registration, 'TEMP_REG_DB') !== false) {
				// this is a temporary registration so we need to handle it properly 
				// and determine if it's a commercial or a registered animal.
				// commercial animals have a "C" and registered animals have an "R"
				if(strpos($registration, 'TEMP_REG_DBC') !== false) {
					$isCommercial = true;
				} else if(strpos($registration, 'TEMP_REG_DBR') !== false) {
					$isCommercial = false;
				} else {
					// default to true so we don't ruin any data for the associations
					$isCommercial = true;
				}
				// now that we've determine if the animal is commerical or registered
				// we can determine the process for getting the actual registration prefix
				$updateRegFirstTime = true;	// we only want to tell the app to update the registrations on the first time
											// otherwise leave it alone, the work is already done.
				$tempReg 			= $registration;
				$actualReg 			= "";
				// check if we've seen this temporary registration before
				$sql_chk = "SELECT actual_registration
							FROM temp_registration_check
							WHERE temp_registration = ?";
				$params = array($tempReg);
				$rs = $PDO->recordSetQuery($sql_chk, $params);
				if($rs) {
					if(!$rs->EOF) {
						// we have seen this temporary registration before 
						// so we just need to get the actual registration now
						$rs->MoveFirst();
						$actualReg = strtoupper(trim($rs->fields['actual_registration']));
						$updateRegFirstTime = false;
					} else {
						// BMC 10.21.2016
						//	-- determine the SQL which will get the proper registration
						// note, $isCommercial is defined above
						include_once(SITE_ROOT . "/webservice/syncables/determine_registration.php");
						
						$rs = $PDO->recordSetQuery($sql_chk);
						if($rs) {
							if(!$rs->EOF) {
								$rs->MoveFirst();
								$actualReg = strtoupper(trim($rs->fields['actual_registration']));
								
								// now insert the temp and actual registrations into the table
								$sql_chk = "INSERT INTO temp_registration_check
												(temp_registration, actual_registration)
											VALUES
												(?, ?)";
								$params = array($tempReg, $actualReg);
								$PDO->executeQuery($sql_chk, $params);
							} else {
								$response["success"] = false;
								$response["message"] = "failed to handle temp registration: ERROR 3";
								$json = json_encode($response);
							}
							$rs->Close();
						} else {
							$response["success"] = false;
							$response["message"] = "failed to handle temp registration: ERROR 2";
							$json = json_encode($response);
						}
					}
					$rs->Close();
				} else {
					$response["success"] = false;
					$response["message"] = "failed to handle temp registration: ERROR 1";
					$json = json_encode($response);
				}
				
				// here we need to update the temporary registration with the new registration
				// and fix our values string so that it goes in properly
				if($actualReg != "") {
					// BMC 10.05.2016
					//	-- set the value at the index with the new registration
					if($PDO->DB_TYPE === "NEW") {
						$values_array = setValueAtIndex(getColumnIndex($columns_array, "registration"), $actualReg, $values_array);
					} else if($PDO->DB_TYPE === "OLD") {
						$values_array = setValueAtIndex(getColumnIndex($columns_array, "animal_registration"), $actualReg, $values_array);
					}
					
					// update the international id which is a simple string replace
					$international_id = getValueFromIndex($values_array, getColumnIndex($columns_array, "international_id"));	// get the international id value
					$international_id = str_replace($tempReg, $actualReg, $international_id);	// replace the temp registration part with the actual registration part
					$values_array = setValueAtIndex(getColumnIndex($columns_array, "international_id"), $international_id, $values_array);	// replace the international id value in the array
					
					// update the raw sql query with the new registration
					$sql_query = str_replace($tempReg, $actualReg, $sql_query);
				}
				
				// now tell the app that we have an actual registration to replace
				// for the temp registrations.  send back the actual registration
				// to update all instances of this within the app
				// we only want to update the registrations on the app the first time
				// that this comes through, every other query we run server side with 
				// the changes.  the app should already have been updated
				if($updateRegFirstTime) {
					$response["has_temp_registration"] 	= true;
					$response["temp_registration"] 		= $tempReg;
					$response["actual_registration"] 	= $actualReg;
				}
				
				// now insert the data and return the encoded json back
				$sql = "INSERT INTO ".$table_string." (".$columns_array.") VALUES (".getValuePlaceHolders($values_array).")";
				
				// set the parameters
				//	we'll need to clean the values of single quotes as we're going to use the pdo params method
				// 	to store the data.
				$params = clearValuesOfQuotes(explode(",", $values_array));
				
				// write query to the database
				if($PDO->executeQuery($sql, $params)) {
					// successfully pushed the syncable query
					$response["success"] 	= true;
					$response["message"] 	= "syncable query pushed successfully";
					$response["sql_query"] 	= $sql_query;
					$json = json_encode($response);
				} else {
					// if the query fails then try to do the old method before 
					// completely giving up on it
					include_once(SITE_ROOT . "/webservice/syncables/query.php");
				}
			} else {
				// this is not a temporary registration so let's just proceed like normal
				// handle the general query here
				$sql = "INSERT INTO ".$table_string." (".$columns_array.") VALUES (".getValuePlaceHolders($values_array).")";
				
				// set the parameters
				//	we'll need to clean the values of single quotes as we're going to use the pdo params method
				// 	to store the data.
				$params = clearValuesOfQuotes(explode(",", $values_array));
				
				// write query to the database
				if($PDO->executeQuery($sql, $params)) {
					// successfully pushed the syncable query
					$response["success"] 	= true;
					$response["message"] 	= "syncable query pushed successfully";
					$response["sql_query"] 	= $sql_query;
					$json = json_encode($response);
				} else {
					// if the query fails then try to do the old method before 
					// completely giving up on it
					include_once(SITE_ROOT . "/webservice/syncables/query.php");
				}
			}
			break;
		case "feedback":
		case "tbl_feedback":
			$isFeedback = true;
			// BMC 10.05.2016
			//	-- handle user feedback that has been entered into the system.  
			//	-- we'll format the feedback to a readable form and then mail it
			//		to the support email so we can receive it as it's sent.
			if($PDO->DB_TYPE === "NEW") {
				$message  = "<p><strong>From:</strong> ".getValueFromIndex($values_array, getColumnIndex($columns_array, "member_id"))."</p>";
				$message .= "<p><strong>Provider:</strong> ".$provider."</p>";
				$message .= "<p><strong>Message:</strong> ".getValueFromIndex($values_array, getColumnIndex($columns_array, "message"))."</p>";
				$message .= "<p><strong>Date:</strong> ".getValueFromIndex($values_array, getColumnIndex($columns_array, "submission_date"))."</p>";
			} else if($PDO->DB_TYPE === "OLD") {
				$message  = "<p><strong>From:</strong> ".getValueFromIndex($values_array, getColumnIndex($columns_array, "create_user"))."</p>";
				$message .= "<p><strong>Provider:</strong> ".$provider."</p>";
				$message .= "<p><strong>Message:</strong> ".getValueFromIndex($values_array, getColumnIndex($columns_array, "feedback"))."</p>";
				$message .= "<p><strong>Date:</strong> ".getValueFromIndex($values_array, getColumnIndex($columns_array, "submission_date"))."</p>";
			}
			
			// create instance of sync adapter class
			$API = new SyncAdapter($PDO);
			// attempt to send the email
			if ($API->sendMail($message)) {
				$response["success"] = true;
				$response["message"] = "feedback submitted successfully";
				$json = json_encode($response);
			} else {
				// if the query fails then try to do the old method before 
				// completely giving up on it
				include_once(SITE_ROOT . "/webservice/syncables/query.php");
			}
			break;
		case "tbl_breeding_agreement":
			// BMC 10.14.2016
			//	-- the pasture_id needs to be an integer and not a blank string
			// INSERT INTO tbl_breeding_agreement(update_stamp, cow_registration, bull_registration, palpation_date, bull_owner_id, 
			// create_user, create_stamp, begin_date, cow_owner_id, update_user, pasture_id, palpation_result, breeding_agreement_id) 
			// VALUES ('2016-09-19', 'AMGV1316599', 'DUMMY', '2016-09-19', '4245', '4245', '2016-09-19', '2016-06-06', '4245', 
			// '4245', '', '4', '1')
			$value = getValueFromIndex($values_array, getColumnIndex($columns_array, "pasture_id"));
			if($value == "") {
				$values_array = setValueAtIndex(getColumnIndex($columns_array, "pasture_id"), "0", $values_array);
			}
			break;
		case "tbl_animal_health":
			// BMC 10.17.2016
			//	-- the withdrawal_period needs to be an integer and not a blank string
			// INSERT INTO tbl_animal_health(update_stamp, application_method, serial_number, lot_number, create_stamp, 
			// date_administered, withdrawal_period, animal_registration, animal_health_id, application_location, 
			// expiration_date, administered_by, create_user, product_name, update_user, dosage) VALUES 
			// ('2016-10-17', 'Subcutaneous', '', '', '2016-10-17', '2016-10-17', '', 'AF11477', '12405', 'Neck Region', 
			// '2016-03-22', '', '3', 'Vira Shield 6+VL5', '3', '5cc/mL')
			$value = getValueFromIndex($values_array, getColumnIndex($columns_array, "withdrawal_period"));
			if($value == "") {
				$values_array = setValueAtIndex(getColumnIndex($columns_array, "withdrawal_period"), "0", $values_array);
			}
			break;
		case "customer_ownership":
			// BMC 10.18.2016
			//	-- we should check to see if our customer is an actual member within the association/database.
			//	-- if this customer is a member then we will transfer the animals to that member instead of
			//		the customer ownership table.
			//	-- if we transfer to a member's ownership then we need to write the query here and disable it
			//		below, by setting the flag isCustomerOwnership to true. if we're going to let it go to
			//		the customer ownership table then we set the flag isCustomerOwnership to false.
			// "customer_ownership";
        	// "customer_id";
        	// "registration";
        	// "date_owned";
        	// "amount";
        	// "previous_owner";
			$customer_id = getValueFromIndex($values_array, getColumnIndex($columns_array, "customer_id"));
			
			if($PDO->DB_TYPE === "NEW") {
				$sql = "SELECT member_id
						FROM member
						WHERE member_id = ?";
				$params = array($customer_id);
				$rs = $PDO->recordSetQuery($sql, $params);
				if($rs) {
					if(!$rs->EOF) {
						// the customer is an actual member so we have to
						// send the animals to the member table
						$isCustomerOwnership = true;
						
						// first check if the temp registration exists, and replace if it so
						if (strpos($sql_query, 'TEMP_REG_DB') !== false) {
							$values_array 	= modifyValuesWithActualReg($columns_array, $values_array, $PDO);
							$sql_query		= modifySqlQueryWithActualReg($columns_array, $values_array, $sql_query, $PDO);
						}
						
						// get the values
						$registration = getValueFromIndex($values_array, getColumnIndex($columns_array, "registration"));
						$date_owned = getValueFromIndex($values_array, getColumnIndex($columns_array, "date_owned"));
						$amount = getValueFromIndex($values_array, getColumnIndex($columns_array, "amount"));
						$previous_owner = getValueFromIndex($values_array, getColumnIndex($columns_array, "previous_owner"));
						
						// handle the general query here
						$sql = "INSERT INTO ownership 
									(registration, member_id, amount, date_owned, previous_owner) 
								VALUES 
									(?, ?, ?, ?, ?)";
						$params = array($registration, $customer_id, $amount, $date_owned, $previous_owner);
						
						// write query to the database
						if($PDO->executeQuery($sql, $params)) {
							// successfully pushed the syncable query
							$response["success"] 	= true;
							$response["message"] 	= "syncable query pushed successfully";
							$response["sql_query"] 	= $sql_query;
							$json = json_encode($response);
						} else {
							// if the query fails then try to do the old method before 
							// completely giving up on it
							include_once(SITE_ROOT . "/webservice/syncables/query.php");
						}
					} else {
						// the customer is not a member so we can transfer
						// the animals normally to the customer table.  
						// no need to change anything else
						$isCustomerOwnership = false;
					}
					$rs->Close();
				} else {
					$response["success"] = false;
					$response["message"] = "failed to handle customer ownership: ERROR 01";
					$json = json_encode($response);
				}
			} else if($PDO->DB_TYPE === "OLD") {
				// we need to quickly check the customer id is either a membership/member_id
				// this will put it into the member_id form.
				$API = new SyncAdapter($PDO);
				switch($provider) {
					case "AAKA_CATTLE":
					case "ASA_CATTLE":
                    case "NALRS_CATTLE":
						$customer_id = $this->getMemberIdFromMembershipId($customer_id);
						break;
					default:
						break;
				}
				
				$sql = "SELECT member_id
						FROM tbl_member
						WHERE member_id = ?";
				$params = array($customer_id);
				$rs = $PDO->recordSetQuery($sql, $params);
				if($rs) {
					if(!$rs->EOF) {
						// the customer is an actual member so we have to
						// send the animals to the member table
						$isCustomerOwnership = true;
						
						// first check if the temp registration exists, and replace if it so
						if (strpos($sql_query, 'TEMP_REG_DB') !== false) {
							$values_array 	= modifyValuesWithActualReg($columns_array, $values_array, $PDO);
							$sql_query		= modifySqlQueryWithActualReg($columns_array, $values_array, $sql_query, $PDO);
						}
						
						// get the values
						$registration = getValueFromIndex($values_array, getColumnIndex($columns_array, "registration"));
						$date_owned = getValueFromIndex($values_array, getColumnIndex($columns_array, "date_owned"));
						$amount = getValueFromIndex($values_array, getColumnIndex($columns_array, "amount"));
						$previous_owner = getValueFromIndex($values_array, getColumnIndex($columns_array, "previous_owner"));
						
						// handle the general query here
						$sql = "INSERT INTO tbl_ownership 
									(animal_registration, owner_id, ownership_amount, ownership_date, old_owner_id, is_primary,
									is_transfer, create_user, create_stamp, update_user, update_stamp) 
								VALUES 
									(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
						$params = array($registration, $customer_id, $amount, $date_owned, $previous_owner, true, true,
										$previous_owner, $date_owned, $previous_owner, $date_owned);
						
						// write query to the database
						if($PDO->executeQuery($sql, $params)) {
							// successfully pushed the syncable query
							$response["success"] 	= true;
							$response["message"] 	= "syncable query pushed successfully";
							$response["sql_query"] 	= $sql_query;
							$json = json_encode($response);
						} else {
							// if the query fails then try to do the old method before 
							// completely giving up on it
							include_once(SITE_ROOT . "/webservice/syncables/query.php");
						}
					} else {
						// the customer is not a member so we can transfer
						// the animals normally to the customer table.  
						// no need to change anything else
						$isCustomerOwnership = false;
					}
					$rs->Close();
				} else {
					$response["success"] = false;
					$response["message"] = "failed to handle customer ownership: ERROR 01";
					$json = json_encode($response);
				}
			}
			break;
		default:
			break;
	}
	
	if($isFeedback) {
		// BMC 10.14.2016
		//	-- if this is feedback then we don't have to do anything
		//	-- everything else gets run below.
	} else if($isAnimalTable) { 
		// BMC 10.14.2016
		//	-- the animal table is held a bit differently and will do the 
		//		temporary registration check so we don't have to worry
		//		about handling it every time.
	}  else if($isCustomerOwnership) {
		// BMC 10.18.2016
		//	-- if we changed the customer_ownership insert to go to the member's ownership table
		//		then we can skip the insert query below, because we do it above.
		//	-- if we ended up transferring the animal to the customer_ownership because the customer
		//		was not an actual member then we insert it normally below
	} else {
		// BMC 10.05.2016
		//	-- check each query to see if they contain the temporary registration, if so
		//		then we'll need to alter the data so things can go in properly
        if (strpos($sql_query, 'TEMP_REG_DB') !== false) {
            $sql_query		= modifySqlQueryWithActualReg($columns_array, $values_array, $sql_query, $PDO);
        }
        if (strpos($values_array, 'TEMP_REG_DB') !== false) {
            $values_array 	= modifyValuesWithActualReg($columns_array, $values_array, $PDO);
        }
	
		// handle the general query here
		// 	note that the table_string should hold the table name.
		//	the columns_array will already be comma delimited so we can just put that right in
		//	the values_array will need to be converted to ?s.
		$sql = "INSERT INTO ".$table_string." (".$columns_array.") VALUES (".getValuePlaceHolders($values_array).")";
		
		// set the parameters
		//	we'll need to clean the values of single quotes as we're going to use the pdo params method
		// 	to store the data.
		$params = clearValuesOfQuotes(explode(",", $values_array));
		
		// write query to the database
		if($PDO->executeQuery($sql, $params)) {
			// successfully pushed the syncable query
			$response["success"] 	= true;
			$response["message"] 	= "syncable query pushed successfully";
			$response["sql_query"] 	= $sql . " [" . implode(", ", $params) . "]";
			$json = json_encode($response);
		} else {
			// if the query fails then try to do the old method before 
			// completely giving up on it
			include_once(SITE_ROOT . "/webservice/syncables/query.php");
		}
	}
    ?>
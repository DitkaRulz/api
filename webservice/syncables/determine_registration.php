<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	// global functions are located in /includes/functions.php
	// 	-- function getColumnIndex($cols, $search_value);
	//	-- function getValueFromIndex($vals, $index);
	//	-- function getValuePlaceHolders($vals);
	//	-- function clearValuesOfQuotes($values);
	// 	-- function getColumnToValueString($columns, $values);
	
	## -------------------- GLOBAL VARIABLES -------------------- ##
	$sql_check = "";
	// $isCommercial is defined in insert.php
	
	## -------------------- DETERMINE REGISTRATIONS -------------------- ##
	// we have not seen this temporary registration before
	// so we must first get the actual registration and 
	// insert the record into the table
	if($PDO->DB_TYPE === "NEW") {
		// BMC 10.21.2016
		//	-- get the dam and sire registration to determine the prefix
		$dam_registration = getValueFromIndex($values_array, getColumnIndex($columns_array, "dam"));
		$sire_registration = getValueFromIndex($values_array, getColumnIndex($columns_array, "sire"));
		
		// determine sire registration if it's unresolved
		if(strpos($sire_registration, 'UNRESOLVED') !== false
		|| strpos($sire_registration, 'UNKNOWN') !== false
		|| strpos($sire_registration, 'DUMMY') !== false
		|| trim($sire_registration) === "" || trim($sire_registration) === "null") {
			$isCommercial = true;
		}
		
		// determine the provider
		switch($provider) {
			case "TEST_DB":	# TEST-CATTLE
				if($isCommercial) {
					// grab the commercial prefix if there is a difference
					$sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
				} else {
					// grab the registered prefix if there is a difference
					$sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
				}
				break;
            case "CWCF_CATTLE":	# COWCALF
                // BMC 10.21.2016
                //	-- cowcalf should only have DB as the prefix for both commercial and registered
                if($isCommercial) {
                    $sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
                } else {
                    $sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
                }
                break;
            case "ABHA_CATTLE":	# AMERICAN BLACK HEREFORD
                // BMC 11.09.2016
                //	-- determine the possible prefix for the new animals
                // 	--	Possible **Suffix: 	X:  commercial/performance
				if($isCommercial) {
					// grab the commercial prefix if there is a difference
					$sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text || 'X'::character varying::text) AS actual_registration";
				} else {
					// grab the registered prefix if there is a difference
					$sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
				}
				break;
			default:
				if($isCommercial) {
					// grab the commercial prefix if there is a difference
					$sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
				} else {
					// grab the registered prefix if there is a difference
					$sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
				}
				break;
		}
	} else if($PDO->DB_TYPE === "OLD") {
		// BMC 10.21.2016
		//	-- get the dam and sire registration to determine the prefix
		$dam_registration = getValueFromIndex($values_array, getColumnIndex($columns_array, "animal_dam"));
		$sire_registration = getValueFromIndex($values_array, getColumnIndex($columns_array, "animal_sire"));
		
		// determine sire registration if it's unresolved
		if(strpos($sire_registration, 'UNRESOLVED') !== false
		|| strpos($sire_registration, 'UNKNOWN') !== false
		|| strpos($sire_registration, 'DUMMY') !== false
		|| trim($sire_registration) === "" || trim($sire_registration) === "null") {
			$isCommercial = true;
		}
		
		// determine the provider
		switch($provider) {
			case "AAKA_CATTLE":	# AKAUSHI
				// BMC 09.01.2016
				//	-- determine the possible prefix for the new animals
				// 	--	Possible Prefix: 	AF: full blood
				//							AP: pure blood
				//							X: 	commercial
				//							1-3T: terminal
				if($isCommercial) {
					$sql_chk = "SELECT ('X'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				} else {
					if(strpos($sire_registration, 'AF') !== false
					|| strpos($sire_registration, 'AP') !== false) {
						if(strpos($dam_registration, 'AF') !== false
						|| strpos($dam_registration, 'AP') !== false) {
							$sql_chk = "SELECT ('AF'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
						} else if(strpos($dam_registration, '1T') !== false) {
							$sql_chk = "SELECT ('2T'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
						} else if(strpos($dam_registration, '2T') !== false) {
							$sql_chk = "SELECT ('3T'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
						} else if(strpos($dam_registration, '3T') !== false) {
							$sql_chk = "SELECT ('AF'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
						} else {
							$sql_chk = "SELECT ('1T'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
						}
					} else {
						// bad sire, use commercial
						$sql_chk = "SELECT ('X'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
					}
				}
				break;
			case "ACRS_CATTLE":	# CHIANINA
				// BMC 09.01.2016
				//	-- possible prefixs:	P:	performance
				//							<blank>: registered
				if($isCommercial) {
					$sql_chk = "SELECT ('P'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				} else {
					$sql_chk = "SELECT (''::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				}
				break;
			case "AGVA_CATTLE":	# USA GELBVIEH
				// TODO BMC 09.01.2016
				//	-- possible prefixs:	AMCX:	smart-select commercial
				//							AMGV:	registered gelbvieh
				//							AMAN:	american angus based
				if($isCommercial) {
					$sql_chk = "SELECT ('AMCX'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				} else {
					$sql_chk = "SELECT ('AMGV'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				}
				break;
			case "CDGV_CATTLE":	# CAN GELBVIEH
				// BMC 09.01.2016
				//	-- possible prefixs:	CDGV:	canadian gelbvieh
				if($isCommercial) {
					$sql_chk = "SELECT ('CDGV'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				} else {
					$sql_chk = "SELECT ('CDGV'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				}
				break;
			case "NALRS_CATTLE": # USA LIMOUSIN
				// TODO BMC 09.01.2016
				//	-- possible prefixs:	UR:	performance	(classification)
				//							NF: full blood	(classification)
				//							NP:	pure blood	(classification)
				//							NX:	commercial	(classification)
				//							registered: (classification).(sex =='C' ? 'F' : 'M')
				$sex = getValueFromIndex($values_array, getColumnIndex($columns_array, "animal_sex"));
				$sex = ($sex == "C" ? "F" : "M");
				
				if($isCommercial) {
					$sql_chk = "SELECT ('NX".$sex."'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				} else {
					$sql_chk = "SELECT ('UR".$sex."'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				}
				break;
			case "CLRS_CATTLE":	# CAN LIMOUSIN
				// TODO BMC 09.01.2016
				//	-- possible prefixs:	UR:	performance	(classification)
				//							NF: full blood	(classification)
				//							NP:	pure blood	(classification)
				//							NX:	commercial	(classification)
				//							CP:
				//							registered: (classification).(sex =='C' ? 'F' : 'M')
				$sex = getValueFromIndex($values_array, getColumnIndex($columns_array, "animal_sex"));
				$sex = ($sex == "C" ? "F" : "M");
				if($isCommercial) {
					$sql_chk = "SELECT ('NX".$sex."'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				} else {
					$sql_chk = "SELECT ('UR".$sex."'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				}
				break;
			case "AMARS_CATTLE": # MAINE-ANJOU
				// BMC 09.01.2016
				//	-- possible prefixs:	P:	performance or commercial
				//							<blank>: registered
				if($isCommercial) {
					$sql_chk = "SELECT ('P'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				} else {
					$sql_chk = "SELECT (''::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				}
				break;
			case "ASA_CATTLE": # USA SHORTHORN
				// BMC 09.01.2016
				//	-- possible prefixs:	DAM && SIRE == <blank> then <blank>
				//							DAM == *AR || SH || AR || DR then AR
				//							DAM == SH && SIRE == RA ==> DR
				//							U:	unregistered/performance
				if($isCommercial) {
					$sql_chk = "SELECT ('U'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				} else {
					if(is_numeric($sire_registration) && is_numeric($dam_registration)) {
						// purebred
						$sql_chk = "SELECT (''::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
					} else {
						if((strpos($dam_registration, 'SH') !== false
							|| strpos($dam_registration, 'AR') !== false)
							&& strpos($sire_registration, 'RA') !== false) {
							$sql_chk = "SELECT ('DR'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
						} else if(strpos($dam_registration, '*AR') !== false
							|| strpos($dam_registration, 'SH') !== false
							|| strpos($dam_registration, 'AR') !== false
							|| strpos($dam_registration, 'DR') !== false) {
							$sql_chk = "SELECT ('AR'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
						} else {
							$sql_chk = "SELECT ('U'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
						}
					}
				}
				break;
			default:	# COWCALF (default)
				if($isCommercial) {
					$sql_chk = "SELECT ('DB'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				} else {
					$sql_chk = "SELECT ('DB'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
				}
			break;
		}
	}
?>
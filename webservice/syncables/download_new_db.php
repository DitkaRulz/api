<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	// global functions are located in /includes/functions.php
	// 	-- function getColumnIndex($cols, $search_value);
	//	-- function getValueFromIndex($vals, $index);
	//	-- function getValuePlaceHolders($vals);
	//	-- function clearValuesOfQuotes($values);
	// 	-- function getColumnToValueString($columns, $values);
	
	## -------------------- DOWNLOAD NEW DATABASE -------------------- ##
	// BMC 08.16.2016
	//	-- merged the association codes within the general full sync process.
	//		association codes get pulled first before the member data
	//	-- added in missing tables of member data as well (alphabetical order)
	// BMC 09.23.2016
	//	-- i'm using an offset and limit for the tables in the old database
	//		we'll start off by sending the 0 offset and send the max record
	//		count+1 back to the user as the new offset.  the user can
	//		send in the limit if need by to alter the webservice depending
	//		on the memory, but we'll default the limit to 500 if nothing is
	//		sent.  
	$group 	= (isset($REQUEST['group']) 	? $REQUEST['group'] 	: "");
	$offset = (isset($REQUEST['offset']) 	? $REQUEST['offset'] 	: "0");
	$limit	= (isset($REQUEST['limit']) 	? $REQUEST['limit'] 	: "500");
		
	switch($group) {
		case "ASSOCIATION_CODES": 
			## -------------------- codes_status -------------------- ##
			$response["codes_status"] = array();
			$sql = "SELECT *
					FROM codes_status";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_status"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_status";
				return json_encode($response);
			}
			
			## -------------------- codes_scur_score -------------------- ##
			$response["codes_scur_score"] = array();
			$sql = "SELECT *
					FROM codes_scur_score";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_scur_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_scur_score";
				return json_encode($response);
			}
			
			## -------------------- codes_color_score -------------------- ##
			$response["codes_color_score"] = array();
			$sql = "SELECT *
					FROM codes_color_score";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_color_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_color_score";
				return json_encode($response);
			}
			
			## -------------------- codes_calving_ease -------------------- ##
			$response["codes_calving_ease"] = array();
			$sql = "SELECT *
					FROM codes_calving_ease";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_calving_ease"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_calving_ease";
				return json_encode($response);
			}
			
			## -------------------- codes_management_code -------------------- ##
			$response["codes_management_code"] = array();
			$sql = "SELECT *
					FROM codes_management_code";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_management_code"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_management_code";
				return json_encode($response);
			}
			
			## -------------------- codes_navel_score -------------------- ##
			$response["codes_navel_score"] = array();
			$sql = "SELECT *
					FROM codes_navel_score";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_navel_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_navel_score";
				return json_encode($response);
			}
			
			## -------------------- codes_sheath_score -------------------- ##
			$response["codes_sheath_score"] = array();
			$sql = "SELECT *
					FROM codes_sheath_score";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_sheath_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_sheath_score";
				return json_encode($response);
			}
			
			## -------------------- codes_temper_score -------------------- ##
			$response["codes_temper_score"] = array();
			$sql = "SELECT *
					FROM codes_temper_score";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_temper_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_temper_score";
				return json_encode($response);
			}
			
			## -------------------- codes_birth_type -------------------- ##
			$response["codes_birth_type"] = array();
			$sql = "SELECT *
					FROM codes_birth_type";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_birth_type"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_birth_type";



				return json_encode($response);
			}
			
			## -------------------- codes_udder_suspension_score -------------------- ##
			$response["codes_udder_suspension_score"] = array();
			$sql = "SELECT *
					FROM codes_udder_suspension_score";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_udder_suspension_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_udder_suspension_score";
				return json_encode($response);
			}
			
			## -------------------- codes_udder_teat_score -------------------- ##
			$response["codes_udder_teat_score"] = array();
			$sql = "SELECT *
					FROM codes_udder_teat_score";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_udder_teat_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_udder_teat_score";
				return json_encode($response);
			}
			
			## -------------------- codes_breed_codes -------------------- ##
			$response["codes_breed_codes"] = array();
			$sql = "SELECT *
					FROM codes_breed_codes";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_breed_codes"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_breed_codes";
				return json_encode($response);
			}
			
			## -------------------- codes_body_condition_score -------------------- ##
			$response["codes_body_condition_score"] = array();
			$sql = "SELECT *
					FROM codes_body_condition_score";
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["codes_body_condition_score"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at codes_body_condition_score";
				return json_encode($response);
			}

            ## -------------------- codes_tattoo_locations -------------------- ##
            $response["codes_tattoo_locations"] = array();
            $sql = "SELECT *
					FROM codes_tattoo_locations";
            $rs = $this->PDO->recordSetQuery($sql);
            if($rs) {
                while(!$rs->EOF) {
                    array_push($response["codes_tattoo_locations"], $rs->fields);
                    $rs->MoveNext();
                }
            } else {
                $response["success"] = false;
                $response["message"] = "failed to sync data at codes_tattoo_locations";
                return json_encode($response);
            }
			break;
		case "ai_certificates":
			## -------------------- ai_certificates -------------------- ##
			$response["ai_certificates"] = array();
            switch($provider) {
                case "TEST_DB":	# TEST-CATTLE
                case "CWCF_CATTLE":	# COWCALF
                case "GREEN_CATTLE":# GREENACRES CATTLE
                case "ABHA_CATTLE":	# AMERICAN BLACK HEREFORD ASSOCIATION
                    $sql = "SELECT x.*
                            FROM (SELECT *
                                FROM ai_certificates
                                WHERE member_id = '".$member_id."') x
                            ORDER BY x._id ASC
                            LIMIT ".$limit." OFFSET ".$offset;
                    break;
                case "TEST_SWINE":	# TEST-SWINE
                case "CPS_SWINE":	# CERTIFIED PEDIGREE SWINE
                case "NSR_SWINE":	# NATIONAL SWINE REGISTRY
                case "BKS_SWINE":	# BERKSHIRE (NATIONAL SWINE REGISTRY)
                    $sql = "SELECT x.*
                            FROM (SELECT *
                                FROM ai_certificates
                                WHERE sire_owner = '".$member_id."'
                                    OR dam_owner = '".$member_id."') x
                            ORDER BY x.certificate_id ASC
                            LIMIT ".$limit." OFFSET ".$offset;
                    break;
                default:
                    $sql = "SELECT x.*
                            FROM (SELECT *
                                FROM ai_certificates
                                WHERE sire_owner = '".$member_id."'
                                    OR dam_owner = '".$member_id."') x
                            ORDER BY x.certificate_id ASC
                            LIMIT ".$limit." OFFSET ".$offset;
                    break;
            }
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["ai_certificates"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at ai_certificates";
				return json_encode($response);
			}
			break; 
		case "animal":
			## -------------------- animal -------------------- ##
			$response["animal"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal a
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT a.is_deleted
                            AND a.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal a
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT a.is_deleted
                            AND a.status = '0'
							
						UNION
		
						SELECT a.*
						FROM animal a
							INNER JOIN semen_inventory s ON a.registration = s.registration
							LEFT JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE s.member_id = '".$member_id."'
							AND NOT a.is_deleted
							AND o.member_id <> '".$member_id."'
											
						UNION
							
						SELECT a.*
						FROM animal a
							INNER JOIN embryo_inventory e ON a.registration = e.registration
							LEFT JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE e.member_id = '".$member_id."'
							AND NOT a.is_deleted
							AND o.member_id <> '".$member_id."') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal";
				return json_encode($response);
			}
			break;
		case "animal_assessment":
			## -------------------- animal_assessment -------------------- ##
			$response["animal_assessment"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_assessment a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_assessment a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.assessment_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_assessment"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_assessment";
				return json_encode($response);
			}
			break;
		case "animal_birth":
			## -------------------- animal_birth -------------------- ##
			$response["animal_birth"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_birth a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_birth a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_birth"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_birth";
				return json_encode($response);
			}
			break;
		case "animal_breed":
			## -------------------- animal_breed -------------------- ##
			$response["animal_breed"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_breed a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_breed a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_breed"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_breed";
				return json_encode($response);
			}
			break;
		case "animal_carcass":
			## -------------------- animal_carcass -------------------- ##
			$response["animal_carcass"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_carcass a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_carcass a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_carcass"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_carcass";
				return json_encode($response);
			}
			break;
		case "animal_comments":
			## -------------------- animal_comments -------------------- ##
			$response["animal_comments"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_comments a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_comments a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_comments"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_comments";
				return json_encode($response);
			}
			break;
		case "animal_mature":
			## -------------------- animal_mature -------------------- ##
            // TODO BMC 11.05.2016
            //  -- temporarily removed this for now until it's fixed on the app.
            //  -- replace the 6563 with the actual member_id when things are fixed
            // BMC 11.08.2016
            //  -- i've replaced back the memberId
			$response["animal_mature"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_mature a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_mature a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_mature"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_mature";
				return json_encode($response);
			}
			break;
		case "animal_data":
			## -------------------- animal_data -------------------- ##
			$response["animal_data"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_data a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_data a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_data"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_data";
				return json_encode($response);
			}
			break;
		case "animal_enrolled":
			## -------------------- animal_enrolled -------------------- ##
			$response["animal_enrolled"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_enrolled a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_enrolled a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_enrolled"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_enrolled";
				return json_encode($response);
			}
			break;
		case "animal_epd":
			## -------------------- animal_epd -------------------- ##
			$response["animal_epd"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_epd a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_epd a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
							
						UNION
		
						SELECT a.*
						FROM animal_epd a
						INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN semen_inventory s ON a.registration = s.registration
							LEFT JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE s.member_id = '".$member_id."'
							AND o.member_id <> '".$member_id."'
							AND NOT x.is_deleted
											
						UNION
							
						SELECT a.*
						FROM animal_epd a
						INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN embryo_inventory e ON a.registration = e.registration
							LEFT JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE e.member_id = '".$member_id."'
							AND o.member_id <> '".$member_id."'
							AND NOT x.is_deleted) x
					ORDER BY x.epd_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_epd"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_epd";
				return json_encode($response);
			}
			break;
		case "animal_estrous":
			## -------------------- animal_estrous -------------------- ##
			$response["animal_estrous"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_estrous a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_estrous a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_estrous"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_estrous";
				return json_encode($response);
			}
			break;
		case "animal_health":
			## -------------------- animal_health -------------------- ##
			$response["animal_health"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_health a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_health a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_health"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_health";
				return json_encode($response);
			}
			break;
        case "animal_horns":
            ## -------------------- animal_horns -------------------- ##
            $response["animal_horns"] = array();
            $sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_horns a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_horns a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
            $rs = $this->PDO->recordSetQuery($sql);
            if($rs) {
                $response["offset"] = $offset + $limit + 1;
                $response["record_count"] = $rs->RecordCount();
                while(!$rs->EOF) {
                    array_push($response["animal_horns"], $rs->fields);
                    $rs->MoveNext();
                }
            } else {
                $response["success"] = false;
                $response["message"] = "failed to sync data at animal_horns";
                return json_encode($response);
            }
            break;
        case "animal_inbreeding":
            ## -------------------- animal_inbreeding -------------------- ##
            $response["animal_inbreeding"] = array();
            $sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_inbreeding a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_inbreeding a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
            $rs = $this->PDO->recordSetQuery($sql);
            if($rs) {
                $response["offset"] = $offset + $limit + 1;
                $response["record_count"] = $rs->RecordCount();
                while(!$rs->EOF) {
                    array_push($response["animal_inbreeding"], $rs->fields);
                    $rs->MoveNext();
                }
            } else {
                $response["success"] = false;
                $response["message"] = "failed to sync data at animal_inbreeding";
                return json_encode($response);
            }
            break;
		case "animal_legacy":
			## -------------------- animal_legacy -------------------- ##
			$response["animal_legacy"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_legacy a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_legacy a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_legacy"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_legacy";
				return json_encode($response);
			}
			break;
		case "animal_location":
			## -------------------- animal_location -------------------- ##
			$response["animal_location"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_location a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_location a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.location_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_location"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_location";
				return json_encode($response);
			}
			break;
		case "pen_location":
			## -------------------- pen_location -------------------- ##
			$response["pen_location"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM pen_location a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM pen_location a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["pen_location"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at pen_location";
				return json_encode($response);
			}
			break;
		case "animal_measurements":
			## -------------------- animal_measurements -------------------- ##
			$response["animal_measurements"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_measurements a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_measurements a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id

								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.measurement_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_measurements"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_measurements";
				return json_encode($response);
			}
			break;
		case "animal_pedigree":
			## -------------------- animal_pedigree -------------------- ##
			$response["animal_pedigree"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_pedigree a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_pedigree a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.pedigree_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_pedigree"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_pedigree";
				return json_encode($response);
			}
			break;
		case "animal_synchronization":
			## -------------------- animal_synchronization -------------------- ##
			$response["animal_synchronization"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_synchronization a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_synchronization a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_synchronization"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_synchronization";
				return json_encode($response);
			}
			break;
		case "animal_ultrasound":
			## -------------------- animal_ultrasound -------------------- ##
			$response["animal_ultrasound"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_ultrasound a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_ultrasound a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_ultrasound"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_ultrasound";
				return json_encode($response);
			}
			break;
		case "animal_wean":
			## -------------------- animal_wean -------------------- ##
			$response["animal_wean"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_wean a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_wean a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_wean"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_wean";
				return json_encode($response);
			}
			break;
		case "animal_year":
			## -------------------- animal_year -------------------- ##
			$response["animal_year"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_year a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM animal_year a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_year"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_year";
				return json_encode($response);
			}
			break;
		case "contact":
			## -------------------- contact -------------------- ##
			// NOTE: this will also get the contact information for the members customers
            // TODO BMC 11.08.2016
            //  -- this needs to be updated
			$response["contact"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM contact
						WHERE member_id = '".$member_id."'
						
						UNION
						
						SELECT *
						FROM contact
						WHERE member_id IN (SELECT m.member_id
											FROM member m
												INNER JOIN transfer t ON m.member_id = t.customer_id
											WHERE t.member_id = '".$member_id."'
											GROUP BY m.member_Id)
							AND is_primary = 't') x
					ORDER BY x.contact_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["contact"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at contact";
				return json_encode($response);
			}
			break;
		case "dna_parentage":
			## -------------------- dna_parentage -------------------- ##
			$response["dna_parentage"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM dna_parentage a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM dna_parentage a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["dna_parentage"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at dna_parentage";
				return json_encode($response);
			}
			break;
		case "dna_requests":
			## -------------------- dna_requests -------------------- ##
			$response["dna_requests"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM dna_requests a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM dna_requests a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.dna_request_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["dna_requests"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at dna_requests";
				return json_encode($response);
			}
			break;
		case "dna_snp":
			## -------------------- dna_snp -------------------- ##
			$response["dna_snp"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a._id,
							a.sample_id,
							a.test,
							a.date_banked,
							a.barcode,
							a.registration,
							a.sample_status
						FROM dna_snp a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a._id,
							a.sample_id,
							a.test,
							a.date_banked,
							a.barcode,
							a.registration,
							a.sample_status
						FROM dna_snp a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["dna_snp"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at dna_snp";
				return json_encode($response);
			}
			break;
        case "dna_str":
            ## -------------------- dna_str -------------------- ##
            $response["dna_str"] = array();
            $sql = "SELECT x.*
					FROM (SELECT a._id,
							a.registration,
							a.case_number,
							a.sample_id,
							a.dna_genotyped,
							a.sire_qualified,
							a.dam_qualified
						FROM dna_str a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a._id,
							a.registration,
							a.case_number,
							a.sample_id,
							a.dna_genotyped,
							a.sire_qualified,
							a.dam_qualified
						FROM dna_str a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
            $rs = $this->PDO->recordSetQuery($sql);
            if($rs) {
                $response["offset"] = $offset + $limit + 1;
                $response["record_count"] = $rs->RecordCount();
                while(!$rs->EOF) {
                    array_push($response["dna_str"], $rs->fields);
                    $rs->MoveNext();
                }
            } else {
                $response["success"] = false;
                $response["message"] = "failed to sync data at dna_str";
                return json_encode($response);
            }
            break;
		case "dna_tests":
			## -------------------- dna_tests -------------------- ##
			$response["dna_tests"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM dna_tests a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN ownership o ON a.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0'
						
						UNION
		
						SELECT a.*
						FROM dna_tests a
							INNER JOIN animal x ON a.registration = x.registration
							INNER JOIN partnership p ON a.registration = p.registration
							INNER JOIN partnership_members m ON p.partnership_id = m.partnership_id
								AND m.is_accepted AND m.end_date IS NULL
						WHERE m.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x.dna_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["dna_tests"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at dna_tests";
				return json_encode($response);
			}
			break;
		case "embryo_inventory":
			## -------------------- embryo_inventory -------------------- ##
			$response["embryo_inventory"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM embryo_inventory
						WHERE member_id = '".$member_id."') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["embryo_inventory"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at embryo_inventory";
				return json_encode($response);
			}
			break;
		case "health_manager":
			## -------------------- health_manager -------------------- ##
			$response["health_manager"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM health_manager
						WHERE member_id = '".$member_id."') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["health_manager"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at health_manager";
				return json_encode($response);
			}
			break;
		case "semen_inventory":
			## -------------------- semen_inventory -------------------- ##
			$response["semen_inventory"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM semen_inventory
						WHERE member_id = '".$member_id."') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["semen_inventory"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at semen_inventory";
				return json_encode($response);
			}
			break;
		case "litter":
			## -------------------- litter -------------------- ##
			$response["litter"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM litter
						WHERE member_id = '".$member_id."') x
					ORDER BY x.litter_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["litter"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at litter";
				return json_encode($response);
			}
			break;
		case "member":
			## -------------------- member -------------------- ##
			// note: this will get the members customers
			$response["member"] = array();
			$sql = "SELECT x.*
					FROM (SELECT member_id,
							member_name,
							membership_type,
							ranch_name,
							premise_id,
							website,
							member_since,
							birth_date,
							active_flag,
							herd_code
						FROM member
						WHERE member_id IN (SELECT m.member_id
											FROM member m
												INNER JOIN transfer t ON m.member_id = t.customer_id
											WHERE t.member_id = '".$member_id."'
											GROUP BY m.member_Id)) x
					ORDER BY x.member_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["member"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at member";
				return json_encode($response);
			}
			break;
		case "member_breed_associations":
			## -------------------- member_breed_associations -------------------- ##
			$response["member_breed_associations"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM member_breed_associations
						WHERE member_id = '".$member_id."') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["member_breed_associations"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at member_breed_associations";
				return json_encode($response);
			}
			break;
		case "multisire":
			## -------------------- multisire -------------------- ##
			$response["multisire"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM multisire
						WHERE member_id = '".$member_id."') x
					ORDER BY x.multisire_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["multisire"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at multisire";
				return json_encode($response);
			}
			break;
		case "multisire_bulls":
			## -------------------- multisire_bulls -------------------- ##
			$response["multisire_bulls"] = array();
			$sql = "SELECT x.*
					FROM (SELECT b.*
						FROM multisire m
							INNER JOIN multisire_bulls b ON m.multisire_id = b.multisire_id
						WHERE m.member_id = '".$member_id."') x
					ORDER BY x.multisire_bull_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["multisire_bulls"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at multisire_bulls";
				return json_encode($response);
			}
			break;
		case "ownership":
			## -------------------- ownership -------------------- ##
			$response["ownership"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM ownership
						WHERE member_id = '".$member_id."') x
					ORDER BY x.registration ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["ownership"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at ownership";
				return json_encode($response);
			}
			break;
		case "partnership":
			## -------------------- partnership -------------------- ##
			$response["partnership"] = array();
			$sql = "SELECT x.*
					FROM (SELECT p.*
						FROM partnership_members m
							INNER JOIN partnership p ON m.partnership_id = p.partnership_id
						WHERE m.member_id = '".$member_id."') x
					ORDER BY x.partnership_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["partnership"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at partnership";
				return json_encode($response);
			}
			break;
		case "partnership_members":
			## -------------------- partnership_members -------------------- ##
			$response["partnership_members"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM partnership_members
						WHERE member_id = '".$member_id."') x
					ORDER BY x.partnership_member_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
	
					array_push($response["partnership_members"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at partnership_members";
				return json_encode($response);
			}
			break;
		case "pasture":
			## -------------------- pasture -------------------- ##
			$response["pasture"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM pasture
						WHERE member_id = '".$member_id."') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["pasture"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at pasture";
				return json_encode($response);
			}
			break;
		case "pen":
			## -------------------- pen -------------------- ##
			$response["pen"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM pen
						WHERE member_id = '".$member_id."') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["pen"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at pen";
				return json_encode($response);
			}
			break;
		case "transfer":
			## -------------------- transfer -------------------- ##
			$response["transfer"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM transfer
						WHERE member_id = '".$member_id."') x
					ORDER BY x.transfer_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["transfer"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at transfer";
				return json_encode($response);
			}
			break;
		case "work_order":
			## -------------------- work_order -------------------- ##
			$response["work_order"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM work_order
						WHERE member_id = '".$member_id."') x
					ORDER BY x.work_order ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["work_order"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at work_order";
				return json_encode($response);
			}
			break;
		case "work_order_item":
			## -------------------- work_order_item -------------------- ##
			$response["work_order_item"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM work_order_item
						WHERE member_id = '".$member_id."') x
					ORDER BY x.item_id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["work_order_item"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at work_order_item";
				return json_encode($response);
			}
			break;
		case "animal_breeding":
			## -------------------- animal_breeding -------------------- ##
			$response["animal_breeding"] = array();
			$sql = "SELECT x.*
					FROM (SELECT a.*
						FROM animal_breeding a
							INNER JOIN animal x ON a.sire = x.registration
								OR a.dam = x.registration
							INNER JOIN ownership o ON x.registration = o.registration
								AND NOT o.superceded
						WHERE o.member_id = '".$member_id."'
							AND NOT x.is_deleted
                            AND x.status = '0') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["animal_breeding"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at animal_breeding";
				return json_encode($response);
			}
			break;
		case "tasks":
			## -------------------- tasks -------------------- ##
			$response["tasks"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM tasks
						WHERE member_id = '".$member_id."') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["tasks"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tasks";
				return json_encode($response);
			}
			break;
		case "reports":
			## -------------------- reports -------------------- ##
			$response["reports"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM reports
						WHERE member_id = '".$member_id."') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["reports"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at reports";
				return json_encode($response);
			}
			break;
		case "customer":
			## -------------------- customer -------------------- ##
			$response["customer"] = array();
			$sql = "SELECT x.*
					FROM (SELECT *
						FROM customer
						WHERE member_id = '".$member_id."') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["customer"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at customer";
				return json_encode($response);
			}
			break;
		case "customer_ownership":
			## -------------------- customer_ownership -------------------- ##
			$response["customer_ownership"] = array();
			$sql = "SELECT x.*
					FROM (SELECT o.*
						FROM customer_ownership o
							INNER JOIN customer c ON o.customer_id = c.customer_id
						WHERE c.member_id = '".$member_id."') x
					ORDER BY x._id ASC
					LIMIT ".$limit." OFFSET ".$offset;
			$rs = $this->PDO->recordSetQuery($sql);
			if($rs) {
				$response["offset"] = $offset + $limit + 1;
				$response["record_count"] = $rs->RecordCount();
				while(!$rs->EOF) {
					array_push($response["customer_ownership"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at customer_ownership";
				return json_encode($response);
			}
			break;
        case "eid_sequences":
            ## -------------------- eid_sequences -------------------- ##
            $response["eid_sequences"] = array();
            $sql = "SELECT x.*
					FROM (SELECT e.*
						FROM eid_sequences e
						WHERE e.member_id = '".$member_id."') x
					ORDER BY x.eid ASC
					LIMIT ".$limit." OFFSET ".$offset;
            $rs = $this->PDO->recordSetQuery($sql);
            if($rs) {
                $response["offset"] = $offset + $limit + 1;
                $response["record_count"] = $rs->RecordCount();
                while(!$rs->EOF) {
                    array_push($response["eid_sequences"], $rs->fields);
                    $rs->MoveNext();
                }
            } else {
                $response["success"] = false;
                $response["message"] = "failed to sync data at eid_sequences";
                return json_encode($response);
            }
            break;
		default:
			$response["success"] = false;
			$response["message"] = "failed to sync data at group: ".$group;
			return json_encode($response);
			break;
	}
?>
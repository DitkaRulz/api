    <?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	// global functions are located in /includes/functions.php
	// 	-- function getColumnIndex($cols, $search_value);
	//	-- function getValueFromIndex($vals, $index);
	//	-- function getValuePlaceHolders($vals);
	//	-- function clearValuesOfQuotes($values);
	// 	-- function getColumnToValueString($columns, $values);
	
	## -------------------- QUERY -------------------- ##
	// use the older syncing method which will sync by the query
	if($query_timestamp != "") {
		// BMC 07.07.2016
		//	-- SQLite to PostGreSQL conversion
		//		replace all instances of [= 'null'] with [IS NULL]
		$sql_query = str_replace("= 'null'", "IS NULL", $sql_query);
		
		// BMC 09.01.2016
		//	-- we will deal with adding a new animal to the database.
		//		coming from the app we will be given some temporary registration id
		//		and we need to change this temporary registration id so that we get an
		//		actual registration to be entered into the system.
		//	-- to do this i will search for the keyword of the temporary registration
		//		then i will replace it with the new registration.  
		//	-- i'll store the temporary id to make sure i don't generate a new registration
		//		for each query that comes in, but only use the new registration where ever this
		//		temporary regisstration is found.
		//	-- $sql_query = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS registration";
		
		// check if there is a temporary registration
		$response["has_temp_registration"] = false;
		$start = strpos($sql_query, 'TEMP_REG_DB');
		if ($start !== false) {
			$updateRegFirstTime = true;	// we only want to tell the app to update the registrations on the first time
										// otherwise leave it alone, the work is already done.
			$tempReg 			= substr($sql_query, $start, 20);	// substr(string, start, length);
			$actualReg 			= "";
			// check if we've seen this temporary registration before
			$sql_chk = "SELECT actual_registration
						FROM temp_registration_check
						WHERE temp_registration = ?";
			$params = array($tempReg);
			$rs = $PDO->recordSetQuery($sql_chk, $params);
			if($rs) {
				if(!$rs->EOF) {
					// we have seen this temporary registration before 
					// so we just need to get the actual registration and 
					// we can continue on
					$rs->MoveFirst();
					$actualReg = strtoupper(trim($rs->fields['actual_registration']));
					$updateRegFirstTime = false;
				} else {
					// we have not seen this temporary registration before
					// so we must first get the actual registration and 
					// insert the record into the table
					if($PDO->DB_TYPE === "NEW") {
                        // BMC 10.21.2016
                        //	-- get the dam and sire registration to determine the prefix
                        $dam_registration = getValueFromIndex($values_array, getColumnIndex($columns_array, "dam"));
                        $sire_registration = getValueFromIndex($values_array, getColumnIndex($columns_array, "sire"));

                        // determine sire registration if it's unresolved
                        if(strpos($sire_registration, 'UNRESOLVED') !== false
                            || strpos($sire_registration, 'UNKNOWN') !== false
                            || strpos($sire_registration, 'DUMMY') !== false
                            || trim($sire_registration) === "" || trim($sire_registration) === "null") {
                            $isCommercial = true;
                        }

                        // determine the provider
                        switch($provider) {
                            case "TEST_DB":	# TEST-CATTLE
                                if($isCommercial) {
                                    // grab the commercial prefix if there is a difference
                                    $sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
                                } else {
                                    // grab the registered prefix if there is a difference
                                    $sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
                                }
                                break;
                            case "CWCF_CATTLE":	# COWCALF
                                // BMC 10.21.2016
                                //	-- cowcalf should only have DB as the prefix for both commercial and registered
                                if($isCommercial) {
                                    $sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
                                } else {
                                    $sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
                                }
                                break;
                            case "ABHA_CATTLE":	# AMERICAN BLACK HEREFORD
                                // BMC 11.09.2016
                                //	-- determine the possible prefix for the new animals
                                // 	--	Possible **Suffix: 	X:  commercial/performance
                                if($isCommercial) {
                                    // grab the commercial prefix if there is a difference
                                    $sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text || 'X'::character varying::text) AS actual_registration";
                                } else {
                                    // grab the registered prefix if there is a difference
                                    $sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
                                }
                                break;
                            default:
                                if($isCommercial) {
                                    // grab the commercial prefix if there is a difference
                                    $sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
                                } else {
                                    // grab the registered prefix if there is a difference
                                    $sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
                                }
                                break;
                        }
					} else if($PDO->DB_TYPE === "OLD") {
                        // BMC 10.21.2016
                        //	-- get the dam and sire registration to determine the prefix
                        $dam_registration = getValueFromIndex($values_array, getColumnIndex($columns_array, "animal_dam"));
                        $sire_registration = getValueFromIndex($values_array, getColumnIndex($columns_array, "animal_sire"));

                        // determine sire registration if it's unresolved
                        if(strpos($sire_registration, 'UNRESOLVED') !== false
                            || strpos($sire_registration, 'UNKNOWN') !== false
                            || strpos($sire_registration, 'DUMMY') !== false
                            || trim($sire_registration) === "" || trim($sire_registration) === "null") {
                            $isCommercial = true;
                        }

                        // determine the provider
                        switch($provider) {
                            case "AAKA_CATTLE":	# AKAUSHI
                                // BMC 09.01.2016
                                //	-- determine the possible prefix for the new animals
                                // 	--	Possible Prefix: 	AF: full blood
                                //							AP: pure blood
                                //							X: 	commercial
                                //							1-3T: terminal
                                if($isCommercial) {
                                    $sql_chk = "SELECT ('X'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                } else {
                                    if(strpos($sire_registration, 'AF') !== false
                                        || strpos($sire_registration, 'AP') !== false) {
                                        if(strpos($dam_registration, 'AF') !== false
                                            || strpos($dam_registration, 'AP') !== false) {
                                            $sql_chk = "SELECT ('AF'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                        } else if(strpos($dam_registration, '1T') !== false) {
                                            $sql_chk = "SELECT ('2T'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                        } else if(strpos($dam_registration, '2T') !== false) {
                                            $sql_chk = "SELECT ('3T'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                        } else if(strpos($dam_registration, '3T') !== false) {
                                            $sql_chk = "SELECT ('AF'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                        } else {
                                            $sql_chk = "SELECT ('1T'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                        }
                                    } else {
                                        // bad sire, use commercial
                                        $sql_chk = "SELECT ('X'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                    }
                                }
                                break;
                            case "ACRS_CATTLE":	# CHIANINA
                                // BMC 09.01.2016
                                //	-- possible prefixs:	P:	performance
                                //							<blank>: registered
                                if($isCommercial) {
                                    $sql_chk = "SELECT ('P'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                } else {
                                    $sql_chk = "SELECT (''::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                }
                                break;
                            case "AGVA_CATTLE":	# USA GELBVIEH
                                // TODO BMC 09.01.2016
                                //	-- possible prefixs:	AMCX:	smart-select commercial
                                //							AMGV:	registered gelbvieh
                                //							AMAN:	american angus based
                                if($isCommercial) {
                                    $sql_chk = "SELECT ('AMCX'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                } else {
                                    $sql_chk = "SELECT ('AMGV'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                }
                                break;
                            case "CDGV_CATTLE":	# CAN GELBVIEH
                                // BMC 09.01.2016
                                //	-- possible prefixs:	CDGV:	canadian gelbvieh
                                if($isCommercial) {
                                    $sql_chk = "SELECT ('CDGV'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                } else {
                                    $sql_chk = "SELECT ('CDGV'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                }
                                break;
                            case "NALRS_CATTLE": # USA LIMOUSIN
                                // TODO BMC 09.01.2016
                                //	-- possible prefixs:	UR:	performance	(classification)
                                //							NF: full blood	(classification)
                                //							NP:	pure blood	(classification)
                                //							NX:	commercial	(classification)
                                //							registered: (classification).(sex =='C' ? 'F' : 'M')
                                $sex = getValueFromIndex($values_array, getColumnIndex($columns_array, "animal_sex"));
                                $sex = ($sex == "C" ? "F" : "M");

                                if($isCommercial) {
                                    $sql_chk = "SELECT ('NX".$sex."'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                } else {
                                    $sql_chk = "SELECT ('UR".$sex."'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                }
                                break;
                            case "CLRS_CATTLE":	# CAN LIMOUSIN
                                // TODO BMC 09.01.2016
                                //	-- possible prefixs:	UR:	performance	(classification)
                                //							NF: full blood	(classification)
                                //							NP:	pure blood	(classification)
                                //							NX:	commercial	(classification)
                                //							CP:
                                //							registered: (classification).(sex =='C' ? 'F' : 'M')
                                $sex = getValueFromIndex($values_array, getColumnIndex($columns_array, "animal_sex"));
                                $sex = ($sex == "C" ? "F" : "M");
                                if($isCommercial) {
                                    $sql_chk = "SELECT ('NX".$sex."'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                } else {
                                    $sql_chk = "SELECT ('UR".$sex."'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                }
                                break;
                            case "AMARS_CATTLE": # MAINE-ANJOU
                                // BMC 09.01.2016
                                //	-- possible prefixs:	P:	performance or commercial
                                //							<blank>: registered
                                if($isCommercial) {
                                    $sql_chk = "SELECT ('P'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                } else {
                                    $sql_chk = "SELECT (''::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                }
                                break;
                            case "ASA_CATTLE": # USA SHORTHORN
                                // BMC 09.01.2016
                                //	-- possible prefixs:	DAM && SIRE == <blank> then <blank>
                                //							DAM == *AR || SH || AR || DR then AR
                                //							DAM == SH && SIRE == RA ==> DR
                                //							U:	unregistered/performance
                                if($isCommercial) {
                                    $sql_chk = "SELECT ('U'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                } else {
                                    if(is_numeric($sire_registration) && is_numeric($dam_registration)) {
                                        // purebred
                                        $sql_chk = "SELECT (''::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                    } else {
                                        if((strpos($dam_registration, 'SH') !== false
                                                || strpos($dam_registration, 'AR') !== false)
                                            && strpos($sire_registration, 'RA') !== false) {
                                            $sql_chk = "SELECT ('DR'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                        } else if(strpos($dam_registration, '*AR') !== false
                                            || strpos($dam_registration, 'SH') !== false
                                            || strpos($dam_registration, 'AR') !== false
                                            || strpos($dam_registration, 'DR') !== false) {
                                            $sql_chk = "SELECT ('AR'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                        } else {
                                            $sql_chk = "SELECT ('U'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                        }
                                    }
                                }
                                break;
                            default:	# COWCALF (default)
                                if($isCommercial) {
                                    $sql_chk = "SELECT ('DB'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                } else {
                                    $sql_chk = "SELECT ('DB'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
                                }
                                break;
                        }
					}
					
					$rs = $PDO->recordSetQuery($sql_chk);
					if($rs) {
						if(!$rs->EOF) {
							$rs->MoveFirst();
							$actualReg = strtoupper(trim($rs->fields['actual_registration']));
							
							// now insert the temp and actual registrations into the table
							$sql_chk = "INSERT INTO temp_registration_check
											(temp_registration, actual_registration)
										VALUES
											(?, ?)";
							$params = array($tempReg, $actualReg);
							$PDO->executeQuery($sql_chk, $params);
						} else {
							// send mail to notify of failed API query
							$API = new SyncAdapter($PDO);
							$message 	= "<p><strong>Command:</strong> QUERY</p><p><strong>SQL:</strong> ".$sql_query."</p>";
							$email 		= "brandon@digitalbeef.com";
							$subject 	= "Failed API Attempt";
							$API->sendMail($message, $subject, $email);
				
							$response["success"] = false;
							$response["message"] = "failed to handle temp registration: ERROR 3";
							$json = json_encode($response);
						}
						$rs->Close();
					} else {
						$API = new SyncAdapter($PDO);
						$message 	= "<p><strong>Command:</strong> QUERY</p><p><strong>SQL:</strong> ".$sql_query."</p>";
						$email 		= "brandon@digitalbeef.com";
						$subject 	= "Failed API Attempt";
						$API->sendMail($message, $subject, $email);
							
						$response["success"] = false;
						$response["message"] = "failed to handle temp registration: ERROR 2";
						$json = json_encode($response);
					}
				}
				$rs->Close();
			} else {
				$API = new SyncAdapter($PDO);
				$message 	= "<p><strong>Command:</strong> QUERY</p><p><strong>SQL:</strong> ".$sql_query."</p>";
				$email 		= "brandon@digitalbeef.com";
				$subject 	= "Failed API Attempt";
				$API->sendMail($message, $subject, $email);
							
				$response["success"] = false;
				$response["message"] = "failed to handle temp registration: ERROR 1";
				$json = json_encode($response);
			}
			
			// update the sql statement with the new registration
			if($actualReg != "") {
				// this will run normally below
				$sql_query = str_replace($tempReg, $actualReg, $sql_query);
			}
			
			// now tell the app that we have an actual registration to replace
			// for the temp registrations.  send back the actual registration
			// to update all instances of this within the app
			// we only want to update the registrations on the app the first time
			// that this comes through, every other query we run server side with 
			// the changes.  the app should already have been updated
			if($updateRegFirstTime) {
				$response["has_temp_registration"] 	= true;
				$response["temp_registration"] 		= $tempReg;
				$response["actual_registration"] 	= $actualReg;
			}
		}
		
		// BMC 09.12.2016
		//	-- handle feedback submission
		//	-- if we see that the query is a specific insert query for feedback
		//		then we must handle it so that it doesn't get put into the database
		// check if there is a temporary registration
		if($PDO->DB_TYPE === "NEW") {
			$start = strpos($sql_query, 'INSERT INTO feedback');
		} else if($PDO->DB_TYPE === "OLD") {
			$start = strpos($sql_query, 'INSERT INTO tbl_feedback');
		}
		
		// use php mailer to send an email to the support account
		if ($start !== false) {
			## ------------------ PREPARE MAILERS ------------------ ##
			// BMC 10.05.2016
			//	-- handle user feedback that has been entered into the system.  
			//	-- we'll format the feedback to a readable form and then mail it
			//		to the support email so we can receive it as it's sent.
			$message  = "<p>".$sql_query."</p>";
			
			// create instance of sync adapter class
			$API = new SyncAdapter($PDO);
			// attempt to send the email
			if ($API->sendMail($message)) {
				$response["success"] = true;
				$response["message"] = "feedback submitted successfully";
				$json = json_encode($response);
			} else {
				$API = new SyncAdapter($PDO);
				$message 	= "<p><strong>Command:</strong> QUERY</p><p><strong>SQL:</strong> ".$sql_query."</p>";
				$email 		= "brandon@digitalbeef.com";
				$subject 	= "Failed API Attempt";
				$API->sendMail($message, $subject, $email);
							
				$response["success"] = false;
				$response["message"] = "failed to send feedback";
				$json = json_encode($response);
			}
		} else {
			// BMC 10.12.2016
			//	-- we'll attempt to find a resolution before completely failing.  
			//	-- the resolution will attempt to modify the query and send back the
			//		$sql_query variable so we'll run it one last time.  If it runs then
			//		everything is fine.  if it fails then we'll have to com back and fix it
			//		at a later date.
			include_once(SITE_ROOT . "/webservice/syncables/attempt_resolution.php");
			
			if($PDO->executeQuery($sql_query)) {
				$response["success"] = true;
				$response["message"] = "syncable query pushed successfully";
				$json = json_encode($response);
			} else {
			    // BMC 02.23.2017
                //  -- attempt to find the sql query in the logs when it fails
			    $Log = new Logs();
			    $Log->writeToErrorLog($sql_query);

				$API = new SyncAdapter($PDO);
				$message 	= "<p><strong>Command:</strong> QUERY</p><p><strong>SQL:</strong> ".$sql_query."</p>";
				$email 		= "brandon@digitalbeef.com";
				$subject 	= "Failed API Attempt";
				$API->sendMail($message, $subject, $email);
							
				$response["success"] = false;
				$response["message"] = "failed to run query: ".$sql_query;
				$json = json_encode($response);
			}
		}
	}
    ?>
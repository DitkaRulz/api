<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	// global functions are located in /includes/functions.php
	// 	-- function getColumnIndex($cols, $search_value);
	//	-- function getValueFromIndex($vals, $index);
	//	-- function getValuePlaceHolders($vals);
	//	-- function clearValuesOfQuotes($values);
	// 	-- function getColumnToValueString($columns, $values);
	
	## -------------------- ATTEMPT RESOLUTION -------------------- ##
	// BMC 12.16.2016
	//	-- i've finally got a good fix/solution for the move_out = '' and move_out = 'null' issue
    //      for both the old and new databases.
    //  -- the solution is here, as a last resort fix, but it's also in the update.php area
	if($PDO->DB_TYPE === "NEW") {
        if (strpos($sql_query, "UPDATE animal_location") !== false && strpos($where_clause, "OR move_out = ''") !== false) {
            $where_clause = str_replace("OR move_out = ''", "OR move_out IS NULL", $where_clause);
        }

        if (strpos($sql_query, "UPDATE animal_location") !== false && strpos($where_clause, "OR move_out = 'null'") !== false) {
            $where_clause = str_replace("OR move_out = 'null'", "OR move_out IS NULL", $where_clause);
        }
	} else if($PDO->DB_TYPE === "OLD") {
        if (strpos($sql_query, "UPDATE tbl_animal_location") !== false && strpos($where_clause, "OR move_out_date = ''") !== false) {
            $where_clause = str_replace("OR move_out_date = ''", "OR move_out_date IS NULL", $where_clause);
        }

        if (strpos($sql_query, "UPDATE tbl_animal_location") !== false && strpos($where_clause, "OR move_out_date = 'null'") !== false) {
            $where_clause = str_replace("OR move_out_date = 'null'", "OR move_out_date IS NULL", $where_clause);
        }
    }
?>
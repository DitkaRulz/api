    <?php
    // this is needed in case a one-time script has already defined it.
    if (!defined('SITE_ROOT')) {
        define('SITE_ROOT', dirname(dirname(__FILE__)));
    }

    include_once(SITE_ROOT . "/includes/includes.php");

    $uploadKey = (isset($_REQUEST['upload_key']) ? md5($_REQUEST['upload_key']) : NULL);
    if(!is_null($uploadKey)) {
        if ($uploadKey === md5("DIGITALBEEF_UPLOAD_6563")) {
            $remoteDir = (isset($_REQUEST['remote_dir']) ? $_REQUEST['remote_dir'] : NULL);
            $provider = (isset($_REQUEST['provider']) ? $_REQUEST['provider'] : NULL);

            // set up the provider
            $PDO = new Connect($provider);  // used for the provider URL

            $file_path = "/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/" . $remoteDir;
            if (!file_exists($file_path)) {
                mkdir($file_path, 0755, true);
            }

            $file_path = $file_path . "/" . basename($_FILES['uploaded_file']['name']);
            if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path)) {
                echo "success";
            } else{
                die("ERROR: failed to move uploaded file");
            }
        } else {
            die("ERROR: unable to authenticate key");
        }
    } else {
        die("ERROR: upload key is null");
    }
    ?>
    <?php
    // BMC 12.07.2016
    //  -- this web service was created strictly for the greenacres group to be able to get the latest image from the server
    //      and to pull that image url to the app to display side by side with the video stream from the web cam
    # ------------------------------------------------------------------------------------------------------------ #
    if (!defined('SITE_ROOT')) {
        define('SITE_ROOT', dirname(dirname(__FILE__)));
    }

    require_once(SITE_ROOT . "/includes/includes.php");
    # ------------------------------------------------------------------------------------------------------------ #


    ## -------------------- GLOBAL VARIABLES -------------------- ##
    $provider       = (isset($_REQUEST['provider'])         ? strtoupper(trim($_REQUEST['provider']))       : "");
    $registration   = (isset($_REQUEST['registration'])     ? strtoupper(trim($_REQUEST['registration']))   : "");
    $memberId       = (isset($_REQUEST['member_id'])        ? $_REQUEST['member_id']                        : "");
    $webserviceFlag = (isset($_REQUEST['flag'])             ? $_REQUEST['flag']                             : "");

    $PDO = new Connect($provider);  // used for the provider URL
    
    switch($webserviceFlag) {
        case "GET_IMAGE":
            $response["success"] = false;
            $response["message"] = "processing digitalbeef api webservice...";
            $response["file_url"] = "";

            // BMC 12.07.2016
            //  -- depending on the group we're using this for the member_id may be fully uppercase or fully lowercase
            //  -- first check the normal, then check full uppercase, then check full lowercase

            // TODO BMC 12.12.2016
            //  -- it's highly likely that we'll run into an issue with the background tasks not getting the correct
            //      previous image as we're setting the current image almost at the same time as we get the previous.
            //      this means that we'll probably have to get the $files[1] rather than the $files[0], but only if
            //      $files[1] exists.  Otherwise we'll get $files[0].  Let's see what happens first!

            // create / load the image directory
            $target_dir = "img/cattle/" . $memberId;

            // get all the files in the directory for this animal.
            $files = glob("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/" . $target_dir . "/" . $registration . "_*.jpg");
            rsort($files);   // sort in descending order

            $file_url = str_replace("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/", "http://" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/", $files[0]);

            if(trim($file_url) == "") {
                // check fully uppercase
                $memberId = strtoupper($memberId);
                $target_dir = "img/cattle/" . $memberId;

                // get all the files in the directory for this animal.
                $files = glob("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/" . $target_dir . "/" . $registration . "_*.jpg");
                rsort($files);   // sort in descending order

                $file_url = str_replace("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/", "http://" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/", $files[0]);

                if(trim($file_url) == "") {
                    // check fully lowercase
                    $memberId = strtolower($memberId);
                    $target_dir = "img/cattle/" . $memberId;

                    // get all the files in the directory for this animal.
                    $files = glob("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/" . $target_dir . "/" . $registration . "_*.jpg");
                    rsort($files);   // sort in descending order

                    $file_url = str_replace("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/", "http://" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/", $files[0]);

                    if(trim($file_url) == "") {
                        $response["success"] = false;
                        $response["message"] = "failed to determine the file url";
                        $response["file_url"] = "provider: ".$provider.", reg: ".$registration.", memberId: ".$memberId;
                        die(json_encode($response));
                    } else {
                        $response["success"] = true;
                        $response["message"] = "file url found successfully";
                        $response["file_url"] = trim($file_url);
                        echo json_encode($response);
                    }
                } else {
                    $response["success"] = true;
                    $response["message"] = "file url found successfully";
                    $response["file_url"] = trim($file_url);
                    echo json_encode($response);
                }
            } else {
                $response["success"] = true;
                $response["message"] = "file url found successfully";
                $response["file_url"] = trim($file_url);
                echo json_encode($response);
            }
            break;
    }
    ?>

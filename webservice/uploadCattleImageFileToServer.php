    <?php
    // this is needed in case a one-time script has already defined it.
    if (!defined('SITE_ROOT')) {
        define('SITE_ROOT', dirname(dirname(__FILE__)));
    }

    include_once(SITE_ROOT . "/includes/includes.php");

    $Log = new Logs();

    $accessKey = (isset($_REQUEST["access_key"]) ? $_REQUEST["access_key"] : NULL);

    // check access key
    if(!is_null($accessKey)
        && $accessKey == "DIGITALBEEF_UPLOAD_6563_8456") {
        // get the other request variables
        $provider           = (isset($_REQUEST["provider"])     ? $_REQUEST["provider"]     : NULL);
        $remoteDirectory    = (isset($_REQUEST["remote_dir"])   ? $_REQUEST["remote_dir"]   : NULL);
        $memberId           = (isset($_REQUEST["member_id"])    ? $_REQUEST["member_id"]    : NULL);

        $PDO = new Connect($provider);

        // get the target directory
        $target_dir = "/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html" . $remoteDirectory;

        // create directory if it does not exist, then give it proper permissions
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0755, true);
        }

        // check file
        $uploadOk = true;

        // check file size (5mb = 5000000)
        // cannot be larger than 5mb
        if ($_FILES["fileToUpload"]["size"] > 5000000) {
            $uploadOk = false;
            $Log->writeToErrorLog("uploadCattleImageFileToServer.php --> upload file size exceeded 5 Mb");
        }

        // get the target file name
        $target_file = $_FILES["fileToUpload"]["name"];

        // get the target file extension
        $temp = explode(".", $target_file);
        $extension = end($temp);

        // allow .png, .jpg, .jpeg, and .gif
        if($extension != "png"
            && $extension != "jpg"
            && $extension != "jpeg"
            && $extension != "gif") {
            // other file types are not allowed
            $uploadOk = false;
            $Log->writeToErrorLog("uploadCattleImageFileToServer.php --> invalid file extension: ".$extension);
        }

        // set directory and file together for full path
        $target_file = $target_dir."/".$target_file;

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            // the file is an image file type, so it's good
        } else {
            // the file is NOT an image file type, so it will fail
            $uploadOk = false;
            $Log->writeToErrorLog("uploadCattleImageFileToServer.php --> the file is NOT an image file type: ".$_FILES["fileToUpload"]["name"]);
        }

        // if all is good, save the file!
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk) {
            // check to make sure this file does not exist, if it does, delete it then add the new file
            if(file_exists($target_file)) {
                unlink($target_file); //remove the file
            }

            if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                // set file permissions
                chmod($target_file, 0755);
            } else {
                $Log->writeToErrorLog("uploadCattleImageFileToServer.php --> failed to move the upload file: move_uploaded_file()");
            }
        } else {
            $Log->writeToErrorLog("uploadCattleImageFileToServer.php --> failed to pass the upload requirements...");
        }
    } else {
        $Log->writeToErrorLog("uploadCattleImageFileToServer.php --> invalid access key...");
    }
    ?>
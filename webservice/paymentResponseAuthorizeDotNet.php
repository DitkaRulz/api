<?php
	include_once("../includes/includes.php");			
	
	// This is the new relay page that is beign passed in /modules/account/ajax/authorizeDotNetPaymentForm.php for the x_relay_url input variable
	
	// ********** GLOBAL VARIABLES ********** //
	$x_response_code 	= $_REQUEST['x_response_code'];		// response code (format below)
	$x_cust_id  		= $_REQUEST['x_cust_id'];			// member id
	$x_auth_code  		= $_REQUEST['x_auth_code'];
	$x_invoice_num  	= $_REQUEST['x_invoice_num'];		// invoice id
	$x_amount  			= $_REQUEST['x_amount'];			// amount charged
	$x_MD5_Hash  		= $_REQUEST['x_MD5_Hash'];			// md5 hash

	/*
	Value: The overall status of the transaction.
	Format:
	1—Approved
	2—Declined
	3—Error
	4—Held for Review
	*/
	/********************** the following variables are not needed by me (that i know of) ******************************/
	/*
	$x_response_reason_code = $_REQUEST['x_response_reason_code'];
	$x_response_reason_text = $_REQUEST['x_response_reason_text'];
	$x_avs_code  = $_REQUEST['x_avs_code'];
	$x_trans_id  = $_REQUEST['x_trans_id'];
	$x_description  = $_REQUEST['x_description'];
	$x_method  = $_REQUEST['x_method'];
	$x_type  = $_REQUEST['x_type'];
	$x_account_number  = $_REQUEST['x_account_number'];
	$x_card_type  = $_REQUEST['x_card_type'];
	$x_split_tender_id  = $_REQUEST['x_split_tender_id'];
	$x_prepaid_requested_amount = $_REQUEST['x_prepaid_requested_amount'];
	$x_prepaid_balance_on_card = $_REQUEST['x_prepaid_balance_on_card'];
	$x_first_name  = $_REQUEST['x_first_name'];
	$x_last_name  = $_REQUEST['x_last_name'];
	$x_company  = $_REQUEST['x_company'];
	$x_address  = $_REQUEST['x_address'];
	$x_city  = $_REQUEST['x_city'];
	$x_state  = $_REQUEST['x_state'];
	$x_zip  = $_REQUEST['x_zip'];
	$x_country  = $_REQUEST['x_country'];
	$x_phone  = $_REQUEST['x_phone'];
	$x_fax  = $_REQUEST['x_fax'];
	$x_email  = $_REQUEST['x_email'];
	$x_ship_to_first_name = $_REQUEST['x_ship_to_first_name'];
	$x_ship_to_last_name = $_REQUEST['x_ship_to_last_name'];
	$x_ship_to_company = $_REQUEST['x_ship_to_company'];
	$x_ship_to_address  = $_REQUEST['x_ship_to_address'];
	$x_ship_to_city  = $_REQUEST['x_ship_to_city'];
	$x_ship_to_state  = $_REQUEST['x_ship_to_state'];
	$x_ship_to_zip  = $_REQUEST['x_ship_to_zip'];
	$x_ship_to_country  = $_REQUEST['x_ship_to_country'];
	$x_tax  = $_REQUEST['x_tax'];
	$x_duty  = $_REQUEST['x_duty'];
	$x_freight  = $_REQUEST['x_freight'];
	$x_tax_exempt  = $_REQUEST['x_tax_exempt'];
	$x_po_num  = $_REQUEST['x_po_num'];
	$x_cvv2_resp_code  = $_REQUEST['x_cvv2_resp_code'];
	$x_cavv_response = $_REQUEST['x_cavv_response'];
	*/
	
	if($x_cust_id != "") {
		$arr = array();
		$arr[] = $x_response_code;
		$arr[] = $x_auth_code;
		$arr[] = $x_invoice_num;
		$arr[] = $x_amount;
		$arr[] = $x_MD5_Hash;
		$arr[] = $rsAuthorize->fields['transaction_id'];
		
		$sql = "UPDATE transaction 
				SET x_response_code = ?, 
					x_auth_code = ?, 
					invoice = ?, 
					amount = ?, 
					fp_hash = ? 
				WHERE transaction_id = ?";
		executeQuery($sql, $arr);
		
		if($x_response_code == '1') {
			// Now record the payment subscription fee!!!
			// 	This function is located in _SubscriptionFunctions.php
			// 	$x_amount is the payment amount/ or fee
			// 	I do not need to send over the work order
			paySubscriptionFee($x_cust_id, $x_amount); 
			
			// create return url
			$home_url = getAssociationURL()."/home.php?member_id=".$x_cust_id;
			?>
			<table border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr>
					<td width="100%">
						<h2>DigitalBeef, LLC - Payment Receipt</h2>
						<img src="<?php echo getAssociationURL().getAssociationLogoUrl(); ?>" />
						<p>
							Thank you for your business!
							<br>We have successfully received your payment of <strong>$<?php echo $x_amount; ?></strong> on <strong><?php echo date("m d, Y"); ?></strong>
						</p>
						<p>
							<h3>Authorize.net - Online Payment</h3>
							<strong>Invoice ID:</strong>&nbsp;&nbsp;<?php echo $x_invoice_num; ?> 
							<br><strong>Received By:</strong>&nbsp;&nbsp;<?php echo $x_cust_id; ?> 
							<br><strong>Date Received:</strong>&nbsp;&nbsp;<?php echo date("m d, Y"); ?> 
							<br><strong>Amount Received:</strong>&nbsp;&nbsp;<?php echo $x_amount; ?> 
						</p>
						<p>Please print out this page for your records.</p>
						<p>A more detailed receipt of your payment has been sent to your email, please allow up to 30 minutes to receive the receipt from Authorize.net.</p>
						<p style="font-size:16px; font-weight:600; text-align:center; width:100%;">
							<a href="<?php echo $home_url; ?>" target="_self">Return To DigitalBeef - <?php echo getAssociationName(); ?></a>
						</p>
					</td>
				</tr>
			</table>
			<?php
		}	
	}
?>
<?php
	// this is needed in case a one-time script has already defined it.
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	include_once(SITE_ROOT . "/includes/includes.php");

	/* GLOBAL RESPONSE */
	$response["success"] 	= false;
	$response["status"]		= "PROCESSING";	// Status Options: PROCESSING, COMPLETE, ERROR
	$response["message"] 	= "Processing mobile_webservice.php...";
	$response["array"] 		= array();

	// BMC 01.06.2016 -- Unlike the other associations the cowcalf will pull all data from the tbl_member table.
	$sql = "SELECT *
			FROM member";
	$rs = recordSetQuery($sql);
	if($rs) {
		if(!$rs->EOF) {
			$rs->MoveFirst();
			while(!$rs->EOF) {
				$member             				= array();
				$member["member_id"]				= $rs->fields['member_id'];
				$member["member_password"]			= $rs->fields['member_password'];
				$member["member_name"]				= $rs->fields['member_name'];
				$member["membership_type"]			= $rs->fields['membership_type'];
				$member["ranch_name"]				= $rs->fields['ranch_name'];
				$member["premise_id"]				= $rs->fields['premise_id'];
				$member["website"]					= $rs->fields['website'];
				$member["member_since"]				= $rs->fields['member_since'];
				$member["membership_begin"]			= $rs->fields['membership_begin'];
				$member["membership_end"]			= $rs->fields['membership_end'];
				$member["subscription_end_date"]	= $rs->fields['subscription_end_date'];
				$member["birth_date"]				= $rs->fields['birth_date'];
				$member["active_flag"]				= $rs->fields['active_flag'];
				$member["mobile_app_active_flag"]	= $rs->fields['mobile_app_active_flag'];
				$member["is_staff"]					= $rs->fields['is_staff'];
				$member["is_admin"]					= $rs->fields['is_admin'];
				$member["legacy_password"]			= $rs->fields['legacy_password'];
				$member["herd_code"]				= $rs->fields['herd_code'];
	
				array_push($response["array"], $member);
				
				$response["success"] 	= true;
				$response["status"]		= "PROCESSING";
				
				$rs->MoveNext();
			}
		} else {
			// SQL statement failed
			$response["success"] 	= false;
			$response["status"]		= "ERROR";
			$response["message"] 	= "Failed running sql statement: [".$sql."]  (mobile_webservice.php)";
			die(json_encode($response));
		}
		$rs->Close();
	}
	else {
		// SQL statement failed
		$response["success"] 	= false;
		$response["status"]		= "ERROR";
		$response["message"] 	= "Failed running sql statement: [".$sql."]  (mobile_webservice.php)";
		die(json_encode($response));
	}
	
	$response["success"] 	= true;
	$response["status"]		= "COMPLETE";
	$response["message"] 	= "array has been created successfully. (mobile_webservice.php)";
	echo json_encode($response);
?>
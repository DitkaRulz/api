<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);

	## -------------------- RESPONSE SETUP -------------------- ##
	$response["success"] 	= false;
	$response["message"] 	= "processing get_association_rules_and_codes.php...";


	## -------------------- GET CODES ---------------------- ##	
	// STATUS CODES
	$response["status_codes"] = array();
	$sql = "SELECT code,
				text
			FROM codes_status
			ORDER BY code";
	$rs = $PDO->recordSetQuery($sql);
	if($rs) {
		if(!$rs->EOF) {
			$rs->MoveFirst();
			while(!$rs->EOF) {
				$status_codes 			= array();
				$status_codes["code"] 	= $rs->fields['code'];
				$status_codes["text"] 	= $rs->fields['text'];
				
				array_push($response["status_codes"], $status_codes);
				
				$response["success"] 	= true;
			
				$rs->MoveNext();
			}
		}
		
		$rs->Close();
	} else {
		// SQL statement failed
		$response["success"] 	= false;
		$response["message"] 	= "Failed running sql statement: [".$sql."]  (get_association_rules_and_codes.php)";
		die(json_encode($response));
	}
	
	// SCUR CODES
	$response["scur_codes"] = array();
	$sql = "SELECT code,
				text
			FROM codes_scur_score
			ORDER BY code";
	$rs = $PDO->recordSetQuery($sql);
	if($rs) {
		if(!$rs->EOF) {
			$rs->MoveFirst();
			while(!$rs->EOF) {
				$scur_codes 			= array();
				$scur_codes["code"] 	= $rs->fields['code'];
				$scur_codes["text"] 	= $rs->fields['text'];
				
				array_push($response["scur_codes"], $scur_codes);
				
				$response["success"] 	= true;
			
				$rs->MoveNext();
			}
		}
		
		$rs->Close();
	} else {
		// SQL statement failed
		$response["success"] 	= false;
		$response["message"] 	= "Failed running sql statement: [".$sql."]  (get_association_rules_and_codes.php)";
		die(json_encode($response));
	}
	
	// CALVING EASE CODES
	$response["calving_ease_codes"] = array();
	$sql = "SELECT code,
				text
			FROM codes_calving_ease
			ORDER BY code";
	$rs = $PDO->recordSetQuery($sql);
	if($rs) {
		if(!$rs->EOF) {
			$rs->MoveFirst();
			while(!$rs->EOF) {
				$calving_ease_codes 			= array();
				$calving_ease_codes["code"] 	= $rs->fields['code'];
				$calving_ease_codes["text"] 	= $rs->fields['text'];
				
				array_push($response["calving_ease_codes"], $calving_ease_codes);
				
				$response["success"] 	= true;
			
				$rs->MoveNext();
			}
		}
		
		$rs->Close();
	} else {
		// SQL statement failed
		$response["success"] 	= false;
		$response["message"] 	= "Failed running sql statement: [".$sql."]  (get_association_rules_and_codes.php)";
		die(json_encode($response));
	}

	$response["message"] 	= "Successfully received all association rules and codes. (get_association_rules_and_codes.php)";
	echo json_encode($response);
?>
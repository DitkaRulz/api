    <?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);
	
	## -------------------- RESPONSE SETUP -------------------- ##
	$response["success"] 			= false;
	$response["message"] 			= "processing pdf_reports_webservice.php...";
	$response["array"] 				= array();

	## -------------------- REQUEST VARIABLES -------------------- ##
	$reports_flag 	= (isset($_REQUEST['reports_flag']) ? $_REQUEST['reports_flag'] : "");
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	function url_exists($url){
		// will check if the url exists
		// returns true if ti does exist or false if it does not
   		$headers = get_headers($url);
		
   		return stripos($headers[0],"200 OK") ? true : false;
	}
	
	if($reports_flag == "PDF_SEARCH") {
		// we will look for the pdf that the user has previously generated
		// given the file_name and member_id.  if the pdf is found we will
		// return the pdf url.  if the pdf is not found then we will return
		// a false success.
		$member_id 	= (isset($_REQUEST['member_id']) ? $_REQUEST['member_id'] : "");
		$file_name 	= (isset($_REQUEST['file_name']) ? $_REQUEST['file_name'] : "");
		
		if($member_id == "" || $file_name == "") {
			$response["success"] = false;
			$response["message"] = "could not determine member_id: ".$member_id.", or file_name: ".$file_name;
			echo json_encode($response);
		} else {
			// search through the file structure
			if($PDO->DB_TYPE == "NEW") {
				// new databases keep everything in the exports directory under the member id folder
				// so the url is easy to determine.
				$pdf_url = $PDO->FILE_DIR . $member_id . "/" . $file_name;
				
				// check if the file exists
				if(url_exists($pdf_url)) {
					// return the pdf url to the user
					$response["success"] = true;
					$response["message"] = "pdf exists";
					$response["pdf_url"] = $pdf_url;
					echo json_encode($response);
				} else {
					$response["success"] 				= false;
					$response["message"] 				= "pdf url does not exist";	
					$response["pdf_url"] = $pdf_url;
					die(json_encode($response));
				}
			} else if($PDO->DB_TYPE == "OLD") {
				// old databases keep everything in the exports directory under the herd_reports folder
				// so the url is easy to determine.
				$pdf_url = $PDO->FILE_DIR . $file_name;
				
				// check if the file exists
				if(url_exists($pdf_url)) {
					// return the pdf url to the user
					$response["success"] = true;
					$response["message"] = "pdf exists";
					$response["pdf_url"] = $pdf_url;
					echo json_encode($response);
				} else {
					$response["success"] 				= false;
					$response["message"] 				= "pdf url does not exist";	
					$response["pdf_url"] = $pdf_url;
					die(json_encode($response));
				}
			}
		}
	} else if ($reports_flag == "PDF_GENERATION") {
		// we will now generate the report for the member
		// based on the report type they give.
		$member_id 		= (isset($_REQUEST['member_id']) 	? $_REQUEST['member_id'] 	: "");
		$report_type 	= (isset($_REQUEST['report_type']) 	? $_REQUEST['report_type'] 	: "");
		
		// BMC 09.11.2016
		//	-- we'll determine what process to take next when we have called the actual pdf generation
		// 	-- get all possible input request variables
		//	-- our return will be the "pdf_url" of the full pdf location so that the app can
		//		run an intent to get the file from the direct url
		$year 			= (isset($_REQUEST['year']) 		? $_REQUEST['year'] 		: "");
		$start_date 	= (isset($_REQUEST['start_date']) 	? $_REQUEST['start_date'] 	: "");
		$end_date 		= (isset($_REQUEST['end_date']) 	? $_REQUEST['end_date'] 	: "");
		$pasture_id 	= (isset($_REQUEST['pasture_id']) 	? $_REQUEST['pasture_id'] 	: "");
		$curr_date 		= (isset($_REQUEST['curr_date']) 	? $_REQUEST['curr_date'] 	: "");
		
		// determine which report to generate
		// each report will return the proper response json array
		switch($report_type) {
			case "ACTIVE_CALVES":
                $year 			= (isset($_REQUEST['year']) 		? $_REQUEST['year'] 		: date("Y"));
				$active_year    = (isset($_REQUEST['active_year'])  ? $_REQUEST['active_year'] 	: date("Y"));
				include_once (SITE_ROOT . "/webservice/pdf_generation/active_calves_summary.php");
				break;
			case "CALF":
                include_once (SITE_ROOT . "/webservice/pdf_generation/calving_summary.php");
				break;
			case "WEAN":
				$start_date = (isset($_REQUEST['start_date']) 	? $_REQUEST['start_date'] 	: date("Y-m-d"));
				$end_date 	= (isset($_REQUEST['end_date']) 	? $_REQUEST['end_date'] 	: date("Y-m-d"));
                include_once (SITE_ROOT . "/webservice/pdf_generation/weaning_summary.php");
				break;
			case "YEAR":
				$start_date = (isset($_REQUEST['start_date']) 	? $_REQUEST['start_date'] 	: date("Y-m-d"));
				$end_date 	= (isset($_REQUEST['end_date']) 	? $_REQUEST['end_date'] 	: date("Y-m-d"));
                include_once (SITE_ROOT . "/webservice/pdf_generation/yearling_summary.php");
				break;
			case "PREG_SUM":
                include_once (SITE_ROOT . "/webservice/pdf_generation/pregnancy_summary.php");
				break;
			case "PREG_REP":
                include_once (SITE_ROOT . "/webservice/pdf_generation/daily_pregnancy_report.php");
				break;
			case "SYNC":
                include_once (SITE_ROOT . "/webservice/pdf_generation/synchronization_summary.php");
				break;
			case "HEAT":
				$start_date = (isset($_REQUEST['start_date']) 	? $_REQUEST['start_date'] 	: date("Y-m-d"));
				$end_date 	= (isset($_REQUEST['end_date']) 	? $_REQUEST['end_date'] 	: date("Y-m-d"));
                include_once (SITE_ROOT . "/webservice/pdf_generation/heat_summary.php");
				break;
			case "BREED_AI":
                include_once (SITE_ROOT . "/webservice/pdf_generation/ai_breeding_summary.php");
				break;
			case "BREED_ET":
                include_once (SITE_ROOT . "/webservice/pdf_generation/et_breeding_summary.php");
				break;
			case "HEALTH":
                include_once (SITE_ROOT . "/webservice/pdf_generation/health_report.php");
				break;
			case "NOTES":
                include_once (SITE_ROOT . "/webservice/pdf_generation/notes_comments_report.php");
				break;
			case "PASTURE_STATS":
				$pasture_id = (isset($_REQUEST['pasture_id']) ? $_REQUEST['pasture_id'] : 0);
                include_once (SITE_ROOT . "/webservice/pdf_generation/pasture_statistics.php");
				break;
			case "WORK_REPORT":
				$curr_date = (isset($_REQUEST['curr_date']) ? $_REQUEST['curr_date'] : date("Y-m-d"));
                include_once (SITE_ROOT . "/webservice/pdf_generation/daily_work_report.php");
				break;
			case "ESTIMATED_CALVING":
				$curr_date	= (isset($_REQUEST['curr_date']) 	? $_REQUEST['curr_date'] 	: date("Y-m-d"));
				$pasture_id = (isset($_REQUEST['pasture_id']) 	? $_REQUEST['pasture_id'] 	: 0);
                include_once (SITE_ROOT . "/webservice/pdf_generation/estimated_calving_report.php");
				break;
			default:
				// unable to determine the report type
				$response["success"] = false;
				$response["message"] = "failed to generate pdf report";
				die(json_encode($response));
				break;
		}
	} else if ($reports_flag == "REPORTS_SYNC") {
		// here we will get all the reports by the member so that
		// the member will have an up to date report history
		// keep in mind the table names on the actual server and
		// on the app will be different
		$member_id 	= (isset($_REQUEST['member_id']) ? $_REQUEST['member_id'] : "");
		
		if($PDO->DB_TYPE == "NEW") {
			$response["reports"] = array();
			$sql = "SELECT *
					FROM reports
					WHERE member_id = ?";
			$params = array($member_id);
			$rs = $PDO->recordSetQuery($sql, $params);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["reports"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at reports";
				die(json_encode($response));
			}
		} else if($PDO->DB_TYPE == "OLD") {
			$response["reports"] = array();
			$sql = "SELECT *
					FROM tbl_member_reports_android
					WHERE member_id = ?";
			$params = array($member_id);
			$rs = $PDO->recordSetQuery($sql, $params);
			if($rs) {
				while(!$rs->EOF) {
					array_push($response["reports"], $rs->fields);
					$rs->MoveNext();
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to sync data at tbl_member_reports_android";
				die(json_encode($response));
			}
		}
		
		$response["success"] = true;
		$response["message"] = "reports sync complete";
		echo json_encode($response);
	}
    <?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);
	$API = new SyncAdapter($PDO);

	## -------------------- REQUEST VARIABLES -------------------- ##
	$sync_flag 	= (isset($_REQUEST['sync_flag']) ? $_REQUEST['sync_flag'] : NULL);
	
	## -------------------- 1. LOGIN -------------------- ##
	if($sync_flag == "LOGIN") {
		// BMC 10.04.2016
		//	-- we're moving all the sync adapter function to the
		//		SyncAdapter class.
		echo $API->Login($_REQUEST, $provider);
	## -------------------- 2. PULL -------------------- ##
	} else if($sync_flag == "PULL") {
		echo $API->SyncPull($_REQUEST);
		
	## -------------------- 3. SYNC COMPLETE -------------------- ##
	} else if($sync_flag == "SYNC_COMPLETE") {
		echo $API->SyncCompleted($_REQUEST);
		
	## -------------------- 4. PUSH -------------------- ##
	} else if($sync_flag == "PUSH") {
		echo $API->SyncPush($_REQUEST, $provider);
		
	## -------------------- 5. SYNC FULL -------------------- ##
	} else if($sync_flag == "SYNC_FULL") {
		echo $API->SyncFull($_REQUEST, $provider);
		
	## -------------------- 6. WOOCOMMERCE LOGIN -------------------- ##
	} else if($sync_flag == "LOGIN_WOOCOMMERCE") {
		// BMC 10.04.2016
		//	-- login with specific information for woo-commerce
		echo $API->LoginWooCommerce($_REQUEST, $provider);
		 
	## -------------------- 7. WOOCOMMERCE PUSH -------------------- ##
	} else if($sync_flag == "WOOCOMMERCE_FREE_TRIAL_PUSH") {
		// BMC 10.11.2016
		//	-- woocommerce will get information from the free trial form and
		//		that data needs to be pushed back to the database using request
		//		variables
		echo $API->WooCommerceFreeTrialPush($_REQUEST, $provider);
		
	## -------------------- 8. WOOCOMMERCE UPDATE -------------------- ##
	} else if($sync_flag == "WOOCOMMERCE_UPDATE") {
		// BMC 10.13.2016
		//	-- woo-commerce will need a method to update subscription data
		//	-- this method will allow them to send over the member_id as well
		//		as some item to update
		echo $API->WooCommerceUpdate($_REQUEST);
		
	## -------------------- 9. GET CUSTOMER DATA -------------------- ##
	} else if($sync_flag == "GET_CUSTOMER_DATA") {
		// BMC 10.17.2016
		//	-- the user is requesting information on a customer
		//		so we will attempt to get that data and send back
		//		any relevant customer information we can find.
		echo $API->GetCustomerData($_REQUEST, $provider);
	## -------------------- 10. WOOCOMMERCE DELETE MEMBER -------------------- ##
	} else if($sync_flag == "WOOCOMMERCE_DELETE_MEMBER") {
		// BMC 10.25.2016
		//	-- this will allow the woo commerce people to delete a member
		//		given the member_id
		echo $API->WooCommerceDeleteMember($_REQUEST);
    ## -------------------- 11. CHECK LOGIN EXISTS -------------------- ##
    } else if($sync_flag == "CHECK_LOGIN_EXISTS") {
        // BMC 12.06.2016
        //  -- check if the login exists for a pre-existing member_id that
        //      is part of an association.  will not work for commercial
        //      cowcalf.
        echo $API->CheckLoginExists($_REQUEST, $provider);
    } else {
        $Log = new Logs();
        $Log->writeToErrorLog($sync_flag." :: ".print_r($_REQUEST, true)." -- failed to connect to digitalbeef api");

		$response["success"] = false;
		$response["message"] = "failed to connect to digitalbeef api";
		die(json_encode($response));
	}
    ?>
<?php
    // BMC 12.07.2016
    //  -- this web service was created strictly for the greenacres group to be able to get the latest image from the server
    //      and to pull that image url to the app to display side by side with the video stream from the web cam
    # ------------------------------------------------------------------------------------------------------------ #
    if (!defined('SITE_ROOT')) {
        define('SITE_ROOT', dirname(dirname(__FILE__)));
    }

    require_once(SITE_ROOT . "/includes/includes.php");
    # ------------------------------------------------------------------------------------------------------------ #

    $TEMP_IP_ADDRESS = "192.171.118.58:80"; //"192.168.1.28";  // webservice connections must be a local ip, internal ip

    ## -------------------- GLOBAL VARIABLES -------------------- ##
    $provider       = (isset($_REQUEST['provider'])         ? strtoupper(trim($_REQUEST['provider']))       : "");
    $registration   = (isset($_REQUEST['registration'])     ? strtoupper(trim($_REQUEST['registration']))   : "");
    $memberId       = (isset($_REQUEST['member_id'])        ? $_REQUEST['member_id']                        : "");
    $webserviceFlag = (isset($_REQUEST['flag'])             ? $_REQUEST['flag']                             : "");
    $ipAddress      = (isset($_REQUEST['ip_address'])       ? $_REQUEST['ip_address']                       : $TEMP_IP_ADDRESS);

    $PDO = new Connect($provider);  // used for the provider URL
    
    switch($webserviceFlag) {
        case "SET_IMAGE":
            if($registration == "") {
                $registration = NULL;
            }

            // create the directory
            $full_dir = "/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/";
            $target_dir = "img/cattle/".$memberId;
            if (!file_exists($full_dir.$target_dir)) {
                mkdir($full_dir.$target_dir, 0755, true);
            }

            // build the image url
            $img_url = 'http://'.$ipAddress.'/axis-cgi/jpg/image.cgi?resolution=640x480&camera=1';

            // save the image
            if(!is_null($registration)) {
                $filename = $registration."_".date("Y_m_d").".jpg";
                $target_file = $target_dir ."/".$filename;
                $full_target_file = $full_dir.$target_file;

                // check if file exists
                if(file_exists($full_target_file)) {
                    unlink($full_target_file);  // removes the files
                }

                // save the image
                file_put_contents($full_target_file, getCurlImage($img_url));

                if(file_exists($full_target_file)) {
                    // create / load the image directory
                    $target_dir = "img/cattle/" . $memberId;

                    // get all the files in the directory for this animal.
                    $files = glob("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/" . $target_dir . "/" . $registration . "_".date("Y_m_d").".jpg");
                    rsort($files);   // sort in descending order

                    $file_url = str_replace("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/", "http://" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/", $files[0]);

                    if(trim($file_url) == "") {
                        // check fully uppercase
                        $memberId = strtoupper($memberId);
                        $target_dir = "img/cattle/" . $memberId;

                        // get all the files in the directory for this animal.
                        $files = glob("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/" . $target_dir . "/" . $registration . "_".date("Y_m_d").".jpg");
                        rsort($files);   // sort in descending order

                        $file_url = str_replace("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/", "http://" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/", $files[0]);

                        if(trim($file_url) == "") {
                            // check fully lowercase
                            $memberId = strtolower($memberId);
                            $target_dir = "img/cattle/" . $memberId;

                            // get all the files in the directory for this animal.
                            $files = glob("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/" . $target_dir . "/" . $registration . "_".date("Y_m_d").".jpg");
                            rsort($files);   // sort in descending order

                            $file_url = str_replace("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/", "http://" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/", $files[0]);

                            if(trim($file_url) == "") {
                                $response["success"] = false;
                                $response["message"] = "failed to determine the file url";
                                $response["file_url"] = "provider: ".$provider.", reg: ".$registration.", memberId: ".$memberId;
                                die(json_encode($response));
                            } else {
                                $response["success"] = true;
                                $response["message"] = "file url found successfully";
                                $response["file_url"] = trim($file_url);
                                echo json_encode($response);
                            }
                        } else {
                            $response["success"] = true;
                            $response["message"] = "file url found successfully";
                            $response["file_url"] = trim($file_url);
                            echo json_encode($response);
                        }
                    } else {
                        $response["success"] = true;
                        $response["message"] = "file url found successfully";
                        $response["file_url"] = trim($file_url);
                        echo json_encode($response);
                    }
                } else {
                    die("ERROR: 1");
                }
            } else {
                die("ERROR: 2");
            }
            break;
        case "GET_IMAGE":
            $response["success"] = false;
            $response["message"] = "processing digitalbeef api webservice...";
            $response["file_url"] = "";

            // BMC 12.07.2016
            //  -- depending on the group we're using this for the member_id may be fully uppercase or fully lowercase
            //  -- first check the normal, then check full uppercase, then check full lowercase

            // TODO BMC 12.12.2016
            //  -- it's highly likely that we'll run into an issue with the background tasks not getting the correct
            //      previous image as we're setting the current image almost at the same time as we get the previous.
            //      this means that we'll probably have to get the $files[1] rather than the $files[0], but only if
            //      $files[1] exists.  Otherwise we'll get $files[0].  Let's see what happens first!

            // create / load the image directory
            $target_dir = "img/cattle/" . $memberId;

            // get all the files in the directory for this animal.
            $files = glob("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/" . $target_dir . "/" . $registration . "_*.jpg");
            rsort($files);   // sort in descending order

            $file_url = str_replace("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/", "http://" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/", $files[0]);

            if(trim($file_url) == "") {
                // check fully uppercase
                $memberId = strtoupper($memberId);
                $target_dir = "img/cattle/" . $memberId;

                // get all the files in the directory for this animal.
                $files = glob("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/" . $target_dir . "/" . $registration . "_*.jpg");
                rsort($files);   // sort in descending order

                $file_url = str_replace("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/", "http://" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/", $files[0]);

                if(trim($file_url) == "") {
                    // check fully lowercase
                    $memberId = strtolower($memberId);
                    $target_dir = "img/cattle/" . $memberId;

                    // get all the files in the directory for this animal.
                    $files = glob("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/" . $target_dir . "/" . $registration . "_*.jpg");
                    rsort($files);   // sort in descending order

                    $file_url = str_replace("/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html/", "http://" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/", $files[0]);

                    if(trim($file_url) == "") {
                        $response["success"] = false;
                        $response["message"] = "failed to determine the file url";
                        $response["file_url"] = "provider: ".$provider.", reg: ".$registration.", memberId: ".$memberId;
                        die(json_encode($response));
                    } else {
                        $response["success"] = true;
                        $response["message"] = "file url found successfully";
                        $response["file_url"] = trim($file_url);
                        echo json_encode($response);
                    }
                } else {
                    $response["success"] = true;
                    $response["message"] = "file url found successfully";
                    $response["file_url"] = trim($file_url);
                    echo json_encode($response);
                }
            } else {
                $response["success"] = true;
                $response["message"] = "file url found successfully";
                $response["file_url"] = trim($file_url);
                echo json_encode($response);
            }
            break;
    }
?>

    <?php
    // this is needed in case a one-time script has already defined it.
    if (!defined('SITE_ROOT')) {
        define('SITE_ROOT', dirname(dirname(__FILE__)));
    }

    include_once(SITE_ROOT . "/includes/includes.php");

    $Log = new Logs();

    $accessKey = (isset($_REQUEST["access_key"]) ? $_REQUEST["access_key"] : NULL);

    // check access key
    if(!is_null($accessKey)
        && $accessKey == "DIGITALBEEF_UPLOAD_6563_8456") {
        // get the other request variables
        $provider           = (isset($_REQUEST["provider"])     ? $_REQUEST["provider"]     : NULL);
        $remoteDirectory    = (isset($_REQUEST["remote_dir"])   ? $_REQUEST["remote_dir"]   : NULL);
        $memberId           = (isset($_REQUEST["member_id"])    ? $_REQUEST["member_id"]    : NULL);

        $PDO = new Connect($provider);

        // get the target directory
        $target_dir = "/home/digitalbeef/domains/" . str_replace("http://", "", $PDO->PROVIDER_URL) . "/public_html" . $remoteDirectory;

        // create directory if it does not exist, then give it proper permissions
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0755, true);
        }

        // check file
        $uploadOk = true;

        // check file size (5mb = 2500000)
        // cannot be larger than 2.5mb
        if ($_FILES["fileToUpload"]["size"] > 2500000) {
            $uploadOk = false;
            $Log->writeToErrorLog("uploadGeneralFileToServer.php --> upload file size exceeded 2.5 Mb");
        }

        // get the target file name
        $target_file = $_FILES["fileToUpload"]["name"];

        // get the target file extension
        $temp = explode(".", $target_file);
        $extension = end($temp);

        // BMC 02.22.2017
        //  -- we will only exclude files that are going to be dangerous to the server
        //      all other files should be allowed to come through
        if($extension == "exe"
            || $extension == "js"
            || $extension == "php"
            || $extension == "html"
            || $extension == "css"
            || $extension == "batch"
            || $extension == "asp") {
            // these file type are not allowed!!!
            $uploadOk = false;
            $Log->writeToErrorLog("uploadGeneralFileToServer.php --> invalid file extension: ".$extension);
        }

        // set directory and file together for full path
        $target_file = $target_dir."/".$target_file;

        // if all is good, save the file!
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk) {
            // check to make sure this file does not exist, if it does, delete it then add the new file
            if(file_exists($target_file)) {
                unlink($target_file); //remove the file
            }

            if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                // set file permissions
                chmod($target_file, 0755);
            } else {
                $Log->writeToErrorLog("uploadGeneralFileToServer.php --> failed to move the upload file: move_uploaded_file()");
            }
        } else {
            $Log->writeToErrorLog("uploadGeneralFileToServer.php --> failed to pass the upload requirements...");
        }
    } else {
        $Log->writeToErrorLog("uploadGeneralFileToServer.php --> invalid access key...");
    }
    ?>
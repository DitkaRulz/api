<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}

    header('Access-Control-Allow-Origin: *');
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	function formatSex($sex) {
		switch(strtoupper(trim($sex))) {
			case "C": return "Cow";
			case "B": return "Bull";
			case "S": return "Steer";
			default: return "N/A";
		}
	}
	
	function formatDate($date) {
		// BMC 02.01.2016
		//	This function will take in a date, most likely formatted 
		//	like YYYY-MM-DD and change the format to MM DD, YYYY
		//	as an example, Jan 01, 2016
		// BMC 02.03.2016
		//	If the date comes in as blank or null then this will display
		//	Dec 31, 1969!!! So we need to account for these input and
		//	display the proper output.  For now, we'll display nothing, just
		//	a blank string.
		if(trim($date) == "" 
			|| trim($date) == "null") {
			return "";
		} else {
			$date = strtotime($date);
			return date("M d, Y", $date);
		}
	}
	
	function determineWorkDone($rs) {
		$rows = 1;
		// 	I receive the record set at the specific row
		// 	Then I find out which work was done, and from that I can
		// 	return the relevant information
		$data = "";
		if($rs->fields['birth_date'] != "") {
			$data .= "Calf Recorded.  Weight: ".trim(strtoupper($rs->fields['birth_weight']))." lbs.  Sex: ".formatSex(trim(strtoupper($rs->fields['sex'])));
		} 
		if ($rs->fields['wean_date'] != "") {
			if($data != "") {
				$data .= "\n";
				$rows++;
			}
			$data .= "Weaning recorded.  Weight: ".trim(strtoupper($rs->fields['wean_weight']))." lbs";
		} 
		if ($rs->fields['year_date'] != "") {
			if($data != "") {
				$data .= "\n";
				$rows++;
			}
			$data .= "Yearling recorded.  Weight: ".trim(strtoupper($rs->fields['year_weight']))." lbs";
		} 
		if ($rs->fields['has_vaccinations'] == "YES") {
			if($data != "") {
				$data .= "\n";
				$rows++;
			}
			$data .= "Vaccination data recorded";
		} 
		if ($rs->fields['additional_date'] != "") {
			if($data != "") {
				$data .= "\n";
				$rows++;
			}
			$data .= "Additional weight.  Weight: ".trim(strtoupper($rs->fields['additional_weight']))." lbs";
		} 
		if ($rs->fields['palpation_date'] != "") {
			if($data != "") {
				$data .= "\n";
				$rows++;
			}
			$data .= "Pregnancy recorded.  Palpation: ".formatPalpationResult(trim(strtoupper($rs->fields['palpation_result'])));
		} 
		if ($rs->fields['ns_palpation_date'] != "") {
			if($data != "") {
				$data .= "\n";
				$rows++;
			}
			$data .= "Pregnancy recorded.  Palpation: ".formatPalpationResult(trim(strtoupper($rs->fields['ns_palpation_result'])));
		} 
		if ($rs->fields['ai_palpation_date'] != "") {
			if($data != "") {
				$data .= "\n";
			}
			$data .= "Pregnancy recorded.  Palpation: ".formatPalpationResult(trim(strtoupper($rs->fields['ai_palpation_result'])));
		} 
		if ($rs->fields['et_palpation_date'] != "") {
			if($data != "") {
				$data .= "\n";
				$rows++;
			}
			$data .= "Pregnancy recorded.  Palpation: ".formatPalpationResult(trim(strtoupper($rs->fields['et_palpation_result'])));
		}
		if ($rs->fields['sync_date'] != "") {
			if($data != "") {
				$data .= "\n";
				$rows++;
			}
			$data .= "Synchronization data recorded";
		}
		
		if($data == "") {
			$data = "N/A";
		}
		
		$dataArray = array($data, $rows);
		
		return $dataArray;
	}
	
	function getHeight($rows) {
		switch($rows) {
			case 1:	return 5;
			case 2: return 8;
			case 3: return 11;
			case 4: return 14;
			case 5: return 17;
			case 6: return 20;
			default:	return 20;
		}
	}
	
	function formatPalpationResult($result) {
		switch($result) {
			case "OPEN":
            case "0":
                return "Open";
            case "1":
                return "0-30 Days";
            case "2":
                return "31-60 Days";
            case "3":
                return "61-90 Days";
            case "4":
                return "91-120 Days";
            case "5":
                return "121-150 Days";
            case "6":
                return "151-180 Days";
            case "7":
                return "181-210 Days";
            case "8":
                return "211-240 Days";
            case "9":
                return "241-270 Days";
            default:
                return "N/A";
		}
	}
	
	## -------------------- GENERATE PDF -------------------- ##
	$response["pdf_url"] = "";
	
	$MemberName = "";
	$YearDate	= "";
	$TotalCount	= "";
	
	//** ------------------------------------ BUILD REPORT HERE ----------------------------------- **//
	$report_title = "Daily Work Report ".$year;

	// GET DATA
	//	I will be loading all the logs that have their date worked on the incomming work date.
	//	Then I will determine what kind of work they include and display that to the user.
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT a.registration,
					a.private_herd_number, 
					a.sex,
					a.eid,
					b.birth_date, b.weight AS birth_weight,
					w.wean_date, w.weight AS wean_weight,
					y.year_date, y.weight AS year_weight,
					aw.measurement_date AS additional_date, aw.weight AS additional_weight,
					(CASE WHEN h.date_administered::VARCHAR = '' THEN 'NO' ELSE 'YES' END) AS has_vaccinations,
					br.palpation_date, br.palpation_result,
					s.sync_date, s.comment AS sync_comment,
					p.pasture_name,
					'' AS ns_palpation_date,
					'' AS ai_palpation_date,
					'' AS et_palpation_date
				FROM animal a
					INNER JOIN ownership o ON a.registration = o.registration
					LEFT JOIN animal_location l ON a.registration = l.registration
						AND (l.move_in <= DATE(?)
							AND (l.move_out >= DATE(?)
								OR l.move_out IS NULL))
					LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
					LEFT JOIN animal_birth b ON a.registration = b.registration
						AND b.birth_date = DATE(?)
					LEFT JOIN animal_wean w ON a.registration = w.registration
						AND w.wean_date = DATE(?)
					LEFT JOIN animal_year y ON a.registration = y.registration
						AND y.year_date = DATE(?)
                    LEFT JOIN ( SELECT w.registration,
    				                w.measurement_date,
                                    w.weight
                                FROM animal_measurements w
                	                INNER JOIN (SELECT registration,
                    			                    MAX(measurement_date) AS measurement_date
                              	                FROM animal_measurements
                                                WHERE measurement_date = DATE(?)
                              	                GROUP BY registration) x ON w.registration = x.registration
                                								                    AND w.measurement_date = x.measurement_date
                                WHERE w.measurement_date = DATE(?)) aw ON a.registration = aw.registration
					LEFT JOIN animal_health h ON a.registration = h.registration
						AND h.date_administered = DATE(?)
					LEFT JOIN animal_breeding br ON a.registration = br.dam
						AND br.palpation_date = DATE(?)
					LEFT JOIN animal_synchronization s ON a.registration = s.registration
						AND s.sync_date = DATE(?)
				WHERE o.member_id = ?
					AND o.date_owned <= DATE(?)
					AND (b.birth_date = DATE(?)
						OR w.wean_date = DATE(?)
						OR y.year_date = DATE(?)
						OR aw.measurement_date = DATE(?)
						OR h.date_administered = DATE(?)
						OR br.palpation_date = DATE(?)
						OR s.sync_date = DATE(?))
				GROUP BY a.registration,
					a.private_herd_number, 
					a.sex,
					a.eid,
					b.birth_date, 
					b.weight,
					w.wean_date, 
					w.weight,
					y.year_date, 
					y.weight,
					aw.measurement_date, 
					aw.weight,
					h.date_administered,
					br.palpation_date, 
					br.palpation_result,
					s.sync_date, 
					s.comment,
					p.pasture_name
				ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
		$params = array($curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$member_id,
            $curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date);
	} else if($PDO->DB_TYPE == "OLD") {
		$sql = "SELECT a.animal_registration AS registration,
					a.animal_private_herd_id AS private_herd_number,
					a.animal_sex AS sex,
					a.eid,
					b.birth_date, b.birth_weight,
					w.wean_date, w.wean_weight,
					y.year_date, y.year_weight,
					aw.measurement_date AS additional_date, aw.weight AS additional_weight,
					(CASE WHEN h.date_administered::VARCHAR = '' THEN 'NO' ELSE 'YES' END) AS has_vaccinations,
					ns.palpation_date AS ns_palpation_date, ns.palpation_result AS ns_palpation_result,
					ai.palpation_date AS ai_palpation_date, ai.palpation_result AS ai_palpation_result,
					et.palpation_date AS et_palpation_date, et.palpation_result AS et_palpation_result,
					s.sync_date, s.comment AS sync_comment,
					p.pasture_id AS pasture_name,
					'' AS palpation_date
				FROM tbl_animal a
					INNER JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
					LEFT JOIN tbl_animal_location l ON a.animal_registration = l.animal_registration
						AND (l.move_in_date <= DATE(?)
							AND (l.move_out_date >= DATE(?)
								OR l.move_out_date IS NULL))
					LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::VARCHAR
					LEFT JOIN tbl_animal_data_birth b ON a.animal_registration = b.animal_registration
						AND b.birth_date = DATE(?)
					LEFT JOIN tbl_animal_data_wean w ON a.animal_registration = w.animal_registration
						AND w.wean_date = DATE(?)
					LEFT JOIN tbl_animal_data_year y ON a.animal_registration = y.animal_registration
						AND y.year_date = DATE(?)
                    LEFT JOIN ( SELECT w.animal_registration,
    				                w.measurement_date,
                                    w.weight
                                FROM tbl_animal_data_other_weight w
                	                INNER JOIN (SELECT animal_registration,
                    			                    MAX(measurement_date) AS measurement_date
                              	                FROM tbl_animal_data_other_weight
                                                WHERE measurement_date = DATE(?)
                              	                GROUP BY animal_registration) x ON w.animal_registration = x.animal_registration
                                								                    AND w.measurement_date = x.measurement_date
                                WHERE w.measurement_date = DATE(?)) aw ON a.animal_registration = aw.animal_registration
					LEFT JOIN tbl_animal_health h ON a.animal_registration = h.animal_registration
						AND h.date_administered = DATE(?)
					LEFT JOIN tbl_breeding_agreement ns ON a.animal_registration = ns.cow_registration
						AND ns.palpation_date = DATE(?)
					LEFT JOIN tbl_breeding_ai ai ON a.animal_registration = ai.cow_registration
						AND ai.palpation_date = DATE(?)
					LEFT JOIN tbl_breeding_et et ON a.animal_registration = et.cow_registration
						AND et.palpation_date = DATE(?)
					LEFT JOIN tbl_animal_sync s ON a.animal_registration = s.animal_registration
						AND s.sync_date = DATE(?)
				WHERE o.owner_id = ?
					AND o.ownership_date <= DATE(?)
					AND (b.birth_date = DATE(?)
						OR w.wean_date = DATE(?)
						OR y.year_date = DATE(?)
						OR aw.measurement_date = DATE(?)
						OR h.date_administered = DATE(?)
						OR ns.palpation_date = DATE(?)
						OR ai.palpation_date = DATE(?)
						OR et.palpation_date = DATE(?)
						OR s.sync_date = DATE(?))
				GROUP BY a.animal_registration,
					a.animal_private_herd_id,
					a.animal_sex,
					a.eid,
					b.birth_date, b.birth_weight,
					w.wean_date, w.wean_weight,
					y.year_date, y.year_weight,
					aw.measurement_date, aw.weight,
					h.date_administered,
					ns.palpation_date, ns.palpation_result,
					ai.palpation_date, ai.palpation_result,
					et.palpation_date, et.palpation_result,
					s.sync_date, s.comment,
					p.pasture_id
				ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
        $params = array($curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,
            $curr_date,$curr_date,$member_id,
            $curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date,$curr_date);
	}
	$rs = $PDO->recordSetQuery($sql, $params);
	if($rs) {
		// BUILD PDF
		if($rs->Recordcount() > 0) {
			// ********** CREATE THE PDF ********** //
			$pdf = new PDF(); 
			//$pdf->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
			
			// Build PDF
			$requested_font	= 'Times';
			$pdf->AliasNbPages();
			$pdf->SetTopMargin(10);
			$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
			$pdf->ZeroCount();
			$pdf->SetFont('Times','B', 10);
			
			$i=0;	// not sure about this
			
			// set the year
			$YearDate = $year;
			
			// get the cow count
			$TotalCount = $rs->Recordcount();
			
			// Get the member name
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT member_name 
						FROM member 
						WHERE member_id = '".$member_id."'";
			} else if($PDO->DB_TYPE == "OLD") { 
				$sql = "SELECT member_name 
						FROM tbl_member 
						WHERE member_id='".$member_id."'";
			}
			
			$rsMember = $PDO->recordSetQuery($sql);
			if($rsMember) {
				if(!$rsMember->EOF) {
					$rsMember->MoveFirst();
					
					$MemberName = $rsMember->fields['member_name'];
				} else {
					$MemberName = $member_id;
				}
				
				$rsMember->Close();
			} else {
				$MemberName = $member_id;
			}

            // BMC 11.07.2016
            //  -- get the member details
            $memberDetails = "";
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT c.address_line_1,
                            c.address_line_2,
                            c.city,
                            c.state,
                            c.zip_code,
                            c.phone_number
                        FROM member m
                            LEFT JOIN contact c ON m.member_id = c.member_id
                                AND c.is_primary
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line_1'];
                        $address_line_2 = $rsContact->fields['address_line_2'];
                        $city = $rsContact->fields['city'];
                        $state = $rsContact->fields['state'];
                        $zip_code = $rsContact->fields['zip_code'];
                        //$phone_number = $rsContact->fields['phone_number'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                        //$phone_number = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT a.address_line1,
                            a.address_line2,
                            a.address_city,
                            a.address_state,
                            a.address_postal_code
                        FROM tbl_member m
                            LEFT JOIN tbl_address a ON m.general_correspondance_address = a.address_id
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line1'];
                        $address_line_2 = $rsContact->fields['address_line2'];
                        $city = $rsContact->fields['address_city'];
                        $state = $rsContact->fields['address_state'];
                        $zip_code = $rsContact->fields['address_postal_code'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else {
                $memberDetails = "";
            }
			
			// build the title
			$pdf->Image(SITE_ROOT . '/images/digital_beef_logo_no_text_tm.png', 10, 10, 35);
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,5,"Digital Beef, LLC", '', 1, 'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(188,12,$memberDetails, '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,7.5,"Daily Work Report for ".formatDate($YearDate), '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(188,7.5, "Total Worked: ".$TotalCount, '', 1, 'C');
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(188, 8, $MemberName."\n".$member_id, '', 1, 'C');
			$pdf->Cell(188, 6, "", '', 1, 'C');
			
			// Headers
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(1,5,"",'R',0,'C');	
			$pdf->Cell(20,5,"PHN",'LRBT',0,'C');
			$pdf->Cell(20,5,"Registration",'LRBT',0,'C');	
			$pdf->Cell(20,5,"Pasture",'LRBT',0,'C');
            $pdf->Cell(25,5,"EID",'LRBT',0,'C');
			$pdf->Cell(100,5,"Work Log",'LRBT',1,'C');
			
			$rs->MoveFirst();
			$previousAnimal = "";
			$currentAnimal  = "";
			$workData 		= "";
			while(!$rs->EOF) {
				// Determine what work was done
				// Send the current row as a parameter
				$workData = determineWorkDone($rs);
				// BMC 09.16.2016
				//	-- we'll get back an array of data here, index 0 is the actual 
				//		text of the work log and index 1 is the number of rows
				//		that we need as a multiplier
				
				// We want to structure the animal report such that 
				// each animal is listed only once, but its work gets 
				// posted in succession, in the comments.
				$pdf->SetFont('Arial','',7);
				$pdf->Cell(1,getHeight($workData[1]),"",'R',0,'C');	
				$pdf->Cell(20,getHeight($workData[1]),$rs->fields['private_herd_number'],'LRT',0,'C');
				$pdf->Cell(20,getHeight($workData[1]),$rs->fields['registration'],'LRT',0,'C');	
				$pdf->Cell(20,getHeight($workData[1]),$rs->fields['pasture_name'],'LRT',0,'C');
                $pdf->Cell(25,getHeight($workData[1]),$rs->fields['eid'],'LRT',0,'C');
				$pdf->Cell(100,getHeight($workData[1]),$workData[0],'LRT',1,'L');
				
				$rs->MoveNext();
			}
			
			$rs->Close();
			
			// Close up the bottom
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(1,5,"",'',0,'C');	
			$pdf->Cell(20,5,"",'T',0,'C');	
			$pdf->Cell(20,5,"",'T',0,'C');
			$pdf->Cell(20,5,"",'T',0,'C');
            $pdf->Cell(25,5,"",'T',0,'C');
			$pdf->Cell(100,5,"",'T',0,'C');
		
			// create export directory, name, and url
			$exportDir = getExportDirectory($provider, $member_id);
			$timestamp = time();
			
			$exportName = "daily_work_report_app_".$member_id."_".$curr_date.".pdf";
			$exportDir = $exportDir . "daily_work_report_app_".$member_id."_".$curr_date.".pdf";
			
			if($PDO->DB_TYPE == "NEW") {
				// for the new databases we have to look in the members folder for the 
				// correct pdf directory
				$exportUrl 	= $PDO->FILE_DIR . $member_id . "/" . $exportName;
			} else if($PDO->DB_TYPE == "OLD") {
				$exportUrl 	= $PDO->FILE_DIR . $exportName;
			}
			
			// export the file
			$pdf->Output($exportDir);
			
			// If file name already exists, then update it!
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT * 
						FROM reports 
						WHERE report_title = '".$report_title."' 
							AND report_file_name ='".$exportName."'
							AND member_id = '".$member_id."'";
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT * 
						FROM tbl_member_reports_android 
						WHERE report_type = '".$report_title."' 
							AND report_file_name='".$exportName."'
							AND create_user = '".$member_id."'";
			}
			$rsCheckDuplicate = $PDO->recordSetQuery($sql);
			if($rsCheckDuplicate) {
				if(!$rsCheckDuplicate->EOF) {
					$rsCheckDuplicate->MoveFirst();
					// Update
					if($PDO->DB_TYPE == "NEW") {
						$sql = "UPDATE reports 
								SET report_date = '".date('Y-m-d h:i:s')."'
								WHERE member_id = '".$member_id."'
									AND report_title = '".$report_title."'
									AND report_file_name = '".$exportName."'
									AND report_format = 'P'";
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "UPDATE tbl_member_reports_android 
								SET create_stamp='".date('Y-m-d h:i:s')."'
								WHERE member_id='".$member_id."'
									AND report_type='".$report_title."'
									AND report_file_name='".$exportName."'
									AND report_format='P'
									AND create_user='".$member_id."'";
					}
					$PDO->executeQuery($sql);
				} else {
					// INSERT
					if($PDO->DB_TYPE == "NEW") {
						$sql = "INSERT INTO reports 
									(member_id, report_title, report_file_name, report_format, report_date) 
								VALUES 
									('".$member_id."','".$report_title."','".$exportName."', 'P', 
									'".date('Y-m-d h:i:s')."')";
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "INSERT INTO tbl_member_reports_android 
									(member_id, report_type, report_file_name, report_format, create_user, 
									create_stamp) 
								VALUES 
									('".$member_id."','".$report_title."','".$exportName."', 'P', '".$member_id."', 
									'".date('Y-m-d h:i:s')."')";
					}
					$PDO->executeQuery($sql);
				}
			}
		
			// send back the file name and the pdf url as the result
			$response["success"] = true;
			$response["pdf_url"] = $exportUrl;	// send back the url
			$response["message"] = "successfully generated pdf";
			echo json_encode($response);
		} else {
			$response["success"] = false;
			$response["message"] = "you have no daily work available";
			die(json_encode($response));
		}
	} else {
		$response["success"] = false;
		$response["message"] = "sql error";
		die(json_encode($response));
	}
<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}

    header('Access-Control-Allow-Origin: *');
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	function formatSex($sex) {
		switch(strtoupper(trim($sex))) {
			case "C": return "Cow";
			case "B": return "Bull";
			case "S": return "Steer";
			default: return "N/A";
		}
	}
	
	function formatDate($date) {
		// BMC 02.01.2016
		//	This function will take in a date, most likely formatted 
		//	like YYYY-MM-DD and change the format to MM DD, YYYY
		//	as an example, Jan 01, 2016
		// BMC 02.03.2016
		//	If the date comes in as blank or null then this will display
		//	Dec 31, 1969!!! So we need to account for these input and
		//	display the proper output.  For now, we'll display nothing, just
		//	a blank string.
		
		if(trim($date) == "" 
			|| trim($date) == "null") {
			return "";
		} else {
			$date = strtotime($date);
			return date("M d, Y", $date);
		}
	}
	
	function date283DaysFromDate($date) {
		// This function will add 283 days to the date provided
		if($date == "") {
			return "";
		} else {
			$date = strtotime("+283 days", strtotime($date));
			return date("m/d/Y", $date);
		}
	}
	
	function compareDatesEarliest($date) {
		global $BeginCalvingSeason;
		
		if($date == "") {
			return $BeginCalvingSeason;
		} else {
			$date = strtotime($date);
			if($BeginCalvingSeason == "") {
				return $date;
			} else if($date <= $BeginCalvingSeason) { 
				return $date;
			} else {
				return $BeginCalvingSeason;
			}
		}
	}
	
	function compareDatesLatest($date) {
		global $EndCalvingSeason;
		
		if($date == "") {
			return $EndCalvingSeason;
		} else {
			$date = strtotime($date);
			if($EndCalvingSeason == "") {
				return $date;
			} else if($date >= $EndCalvingSeason) { 
				return $date;
			} else {
				return $EndCalvingSeason;
			}
		}
	}
	
	function isDateAfterCurrDate($date) {
		// We use this function to check if the date is expired
		// meaning that it is after the current date.  if so then make it red!
		$date = strtotime($date);
		$curr = strtotime("now");
		
		if($date >= $curr) {
			return true;
		} else {
			return false;
		}
	}
	
	function isFirstDateAfterSecondDate($firstDate, $secondDate) {
		// this function will check if the first date is after the second date
		// if the first date is after the second date then it returns true
		// if it's before the second date OR EQUAL TO it then it returns false.
		$firstDate = strtotime($firstDate);
		$secondDate = strtotime($secondDate);
		
		if($firstDate > $secondDate) {	// after means greater than
			return true;
		} else {
			return false;
		}
	}
	
	function getEstimatedCalvingDates($animal_registration) {
		//	Get the Estimated calving dates
		global $BeginCalvingSeason;
		global $EndCalvingSeason;
		global $PDO;
		
		// Get Breeding Agreement
		$BreedingAgr = false;
		$agr_begin_date = "";
		$agr_end_date = "";
		// BMC 03.28.2016
		//	Modified this AND (CURRENT_DATE - agr.end_date) <= (283+18) to 577
		if($PDO->DB_TYPE == "NEW") {
			$sql = "SELECT br.begin_date, 
						br.end_date
					FROM animal a
						INNER JOIN animal_breeding br ON a.registration = br.dam
					WHERE a.registration = '".$animal_registration."'
						AND (CURRENT_DATE - br.end_date) <= 577
						AND (CURRENT_DATE - br.begin_date) < 577
						AND br.type = 'NS'
					 ORDER BY br.begin_date DESC
                     LIMIT 1";	// earliest begin date goes first
		} else if($PDO->DB_TYPE == "OLD") {
			$sql = "SELECT agr.begin_date, 
						agr.end_date
					FROM tbl_animal a
						INNER JOIN tbl_breeding_agreement agr ON a.animal_registration = agr.cow_registration
					WHERE a.animal_registration = '".$animal_registration."'
						AND (CURRENT_DATE - agr.end_date) <= 577
						AND (CURRENT_DATE - DATE(agr.create_stamp)) < 577
					 ORDER BY agr.begin_date DESC
                     LIMIT 1";	// earliest begin date goes first

		}
		$rsAgr = $PDO->recordSetQuery($sql);
		if($rsAgr) {
			if(!$rsAgr->EOF) {
				$BreedingAgr = true;
				$rsAgr->MoveFirst();	// go to earliest begin date
				$agr_begin_date = $rsAgr->fields['begin_date'];
				
				$rsAgr->MoveLast();		// go to latest end date
				$agr_end_date = $rsAgr->fields['end_date'];
			}
		}
		
		// Get Breeding AI
		$BreedingAi = false;
		$ai_date = "";
		if($PDO->DB_TYPE == "NEW") {
			$sql = "SELECT ai.ai_date
					FROM animal a
						INNER JOIN animal_breeding ai ON a.registration = ai.dam
					WHERE a.registration = '".$animal_registration."'
						AND (CURRENT_DATE - ai.ai_date) <= 577
						AND type = 'AI'
                    ORDER BY ai.ai_date DESC
                    LIMIT 1";	// earliest begin date goes first
		} else if($PDO->DB_TYPE == "OLD") {
			$sql = "SELECT ai.ai_date
					FROM tbl_animal a
						INNER JOIN tbl_breeding_ai ai ON a.animal_registration = ai.cow_registration
					WHERE a.animal_registration = '".$animal_registration."'
						AND (CURRENT_DATE - ai.ai_date) <= 577
						AND (CURRENT_DATE - DATE(ai.create_stamp)) < 577
                    ORDER BY ai.ai_date DESC
                    LIMIT 1";	// earliest begin date goes first

		}
		$rsAi = $PDO->recordSetQuery($sql);
		if($rsAi) {
			if(!$rsAi->EOF) {
				$BreedingAi = true;
				$ai_date = $rsAi->fields['ai_date'];
			}
		}
		
		// Get Breeding ET
		$BreedingEt = false;
		$et_transfer_date = "";
		if($PDO->DB_TYPE == "NEW") {
			$sql = "SELECT et.transfer_date
					FROM animal a
						INNER JOIN animal_breeding et ON a.registration = et.dam
					WHERE a.registration = '".$animal_registration."'
						AND (CURRENT_DATE - et.transfer_date) <= 577
						AND type = 'ET'
                    ORDER BY et.transfer_date DESC
                    LIMIT 1";	// earliest begin date goes first
		} else if($PDO->DB_TYPE == "OLD") {
			$sql = "SELECT et.transfer_date
				FROM tbl_animal a
					INNER JOIN tbl_breeding_et et ON a.animal_registration = et.cow_registration
				WHERE a.animal_registration = '".$animal_registration."'
					AND (CURRENT_DATE - et.transfer_date) <= 577
					AND (CURRENT_DATE - DATE(et.create_stamp)) < 577
                    ORDER BY et.transfer_date DESC
                    LIMIT 1";	// earliest begin date goes first

		}
		$rsEt = $PDO->recordSetQuery($sql);
		if($rsEt) {
			if(!$rsEt->EOF) {
				$BreedingAi = true;
				$et_transfer_date = $rsEt->fields['transfer_date'];
			}
		}
	
		// Determine estimated birthing...
		$estimatedBirthingRange = "";
		if($BreedingAgr && !$BreedingAi && !$BreedingEt) {
			// AGR Only
			if(isDateAfterCurrDate($agr_end_date)) {
				$estimatedBirthingRange = formatDisplayDate(date283DaysFromDate($agr_begin_date))." to *".formatDisplayDate(date283DaysFromDate($agr_end_date));
			} else {
				$estimatedBirthingRange = formatDisplayDate(date283DaysFromDate($agr_begin_date))." to ".formatDisplayDate(date283DaysFromDate($agr_end_date));
			}
			
			// Find range
			$BeginCalvingSeason = compareDatesEarliest(date283DaysFromDate($agr_begin_date));
			$EndCalvingSeason = compareDatesLatest(date283DaysFromDate($agr_end_date));
		} else if(!$BreedingAgr && $BreedingAi && !$BreedingEt) {
			// AI Only
			if(isDateAfterCurrDate($ai_date)) {
				$estimatedBirthingRange = "*".formatDisplayDate(date283DaysFromDate($ai_date));
			} else {
				$estimatedBirthingRange = formatDisplayDate(date283DaysFromDate($ai_date));
			}
			
			// Find range
			$BeginCalvingSeason = compareDatesEarliest(date283DaysFromDate($ai_date));
			$EndCalvingSeason = compareDatesLatest(date283DaysFromDate($ai_date));
		} else if(!$BreedingAgr && !$BreedingAi && $BreedingEt) {
			// ET Only
			if(isDateAfterCurrDate($et_transfer_date)) {
				$estimatedBirthingRange = "*".formatDisplayDate(date283DaysFromDate($et_transfer_date));
			} else {
				$estimatedBirthingRange = formatDisplayDate(date283DaysFromDate($et_transfer_date));
			}
			
			// Find range
			$BeginCalvingSeason = compareDatesEarliest(date283DaysFromDate($et_transfer_date));
			$EndCalvingSeason = compareDatesLatest(date283DaysFromDate($et_transfer_date));
		} else if($BreedingAgr && $BreedingAi && !$BreedingEt) {
			// AGR + AI
			$estimatedBeginDate	= "";
			$estimatedEndDate 	= "";
			
			// check if the AI date is AFTER the end date
			// if it is after the end date then we'll use
			// the AI date as the end date
			if(isFirstDateAfterSecondDate($ai_date, $agr_end_date)) {
				$estimatedEndDate = $ai_date;		// ai date is last date
			} else {
				$estimatedEndDate = $agr_end_date;	// end date from AGR is latest date
			}
			
			// check if the ai date is after the begin date.
			// if it's after the begin date then use the begin
			// date as the first start date, if it's before
			// the begin date then use the ai date as the first estimated begin date
			if(isFirstDateAfterSecondDate($ai_date, $agr_begin_date)) {
				$estimatedBeginDate = $agr_begin_date;	// agr begin date is the earliest date
			} else {
				$estimatedBeginDate = $ai_date;			// ai date is the earliest date
			}
			
			// get estimated birthing range
			if(isDateAfterCurrDate($estimatedEndDate)) {
				$estimatedBirthingRange = formatDisplayDate(date283DaysFromDate($estimatedBeginDate))." to *".formatDisplayDate(date283DaysFromDate($estimatedEndDate));
			} else {
				$estimatedBirthingRange = formatDisplayDate(date283DaysFromDate($estimatedBeginDate))." to ".formatDisplayDate(date283DaysFromDate($estimatedEndDate));
			}
			
			// Find range
			$BeginCalvingSeason = compareDatesEarliest(date283DaysFromDate($estimatedBeginDate));
			$EndCalvingSeason = compareDatesLatest(date283DaysFromDate($estimatedEndDate));
			
		} else if($BreedingAgr && !$BreedingAi && $BreedingEt) {
			// AGR + ET
			$estimatedBeginDate	= "";
			$estimatedEndDate 	= "";
			
			// check if the ET transfer date is AFTER the end date
			// if it is after the end date then we'll use
			// the ET transfer date as the end date
			if(isFirstDateAfterSecondDate($et_transfer_date, $agr_end_date)) {
				$estimatedEndDate = $et_transfer_date;	// ET transfer date is last date
			} else {
				$estimatedEndDate = $agr_end_date;		// end date from AGR is latest date
			}
			
			// check if the ET transfer date is after the begin date.
			// if it's after the begin date then use the begin
			// date as the first start date, if it's before
			// the begin date then use the ET transfer date as the first estimated begin date
			if(isFirstDateAfterSecondDate($et_transfer_date, $agr_begin_date)) {
				$estimatedBeginDate = $agr_begin_date;		// agr begin date is the earliest date
			} else {
				$estimatedBeginDate = $et_transfer_date;	// ET transfer date is the earliest date
			}
			
			// get estimated birthing range
			if(isDateAfterCurrDate($estimatedEndDate)) {
				$estimatedBirthingRange = formatDisplayDate(date283DaysFromDate($estimatedBeginDate))." to *".formatDisplayDate(date283DaysFromDate($estimatedEndDate));
			} else {
				$estimatedBirthingRange = formatDisplayDate(date283DaysFromDate($estimatedBeginDate))." to ".formatDisplayDate(date283DaysFromDate($estimatedEndDate));
			}
			
			// Find range
			$BeginCalvingSeason = compareDatesEarliest(date283DaysFromDate($estimatedBeginDate));
			$EndCalvingSeason = compareDatesLatest(date283DaysFromDate($estimatedEndDate));
		} else if(!$BreedingAgr && $BreedingAi && $BreedingEt) {
			// AI + ET
			$estimatedBeginDate	= "";
			$estimatedEndDate 	= "";
			
			// check if the ET transfer date is AFTER the AI date
			// if it is after the AI date then we'll use
			// the ET transfer date as the end date
			if(isFirstDateAfterSecondDate($et_transfer_date, $ai_date)) {
				$estimatedEndDate = $et_transfer_date;	// ET transfer date is last date
			} else {
				$estimatedEndDate = $ai_date;			// end date from AGR is latest date
			}
			
			// check if the ET transfer date is after the AI date.
			// if it's after the AI date then use the AI date
			// as the first start date, if it's before
			// the AI date then use the ET transfer date as the first estimated begin date
			if(isFirstDateAfterSecondDate($et_transfer_date, $ai_date)) {
				$estimatedBeginDate = $ai_date;				// agr begin date is the earliest date
			} else {
				$estimatedBeginDate = $et_transfer_date;	// ET transfer date is the earliest date
			}
			
			// get estimated birthing range
			if(isDateAfterCurrDate($estimatedEndDate)) {
				$estimatedBirthingRange = formatDisplayDate(date283DaysFromDate($estimatedBeginDate))." to *".formatDisplayDate(date283DaysFromDate($estimatedEndDate));
			} else {
				$estimatedBirthingRange = formatDisplayDate(date283DaysFromDate($estimatedBeginDate))." to ".formatDisplayDate(date283DaysFromDate($estimatedEndDate));
			}
			
			// Find range
			$BeginCalvingSeason = compareDatesEarliest(date283DaysFromDate($estimatedBeginDate));
			$EndCalvingSeason = compareDatesLatest(date283DaysFromDate($estimatedEndDate));
			
		} else {
			// AGR + AI + ET
			$estimatedBeginDate	= "";
			$estimatedEndDate 	= "";
			// check if the ET transfer date is AFTER the end date
			// if it is after the end date then we'll use
			// the ET transfer date as the end date
			if(isFirstDateAfterSecondDate($et_transfer_date, $agr_end_date)) {
				if(isFirstDateAfterSecondDate($et_transfer_date, $ai_date)) {
					$estimatedEndDate = $et_transfer_date;	// ET transfer date is latest date
				} else {
					$estimatedEndDate = $ai_date;			// Ai date is latest date
				}
			} else {
				if(isFirstDateAfterSecondDate($agr_end_date, $ai_date)) {
					$estimatedEndDate = $agr_end_date;		// AGR end date is latest date
				} else {
					$estimatedEndDate = $ai_date;			// AI date is latest date
				}
			}
			
			// check if the ET transfer date is after the begin date.
			// if it's after the begin date then use the begin
			// date as the first start date, if it's before
			// the begin date then use the ET transfer date as the first estimated begin date
			if(isFirstDateAfterSecondDate($et_transfer_date, $agr_begin_date)) {
				if(isFirstDateAfterSecondDate($agr_begin_date, $ai_date)) {
					$estimatedEndDate = $ai_date;				// AI date is the earliest date
				} else {
					$estimatedEndDate = $agr_begin_date;		// AGR begin date is the earliest date
				}
			} else {
				if(isFirstDateAfterSecondDate($et_transfer_date, $ai_date)) {
					$estimatedEndDate = $ai_date;				// AI date is the earliest date
				} else {
					$estimatedEndDate = $et_transfer_date;		// ET transfer date is the earliest date
				}
			}
			
			// get estimated birthing range
			if(isDateAfterCurrDate($estimatedEndDate)) {
				$estimatedBirthingRange = formatDisplayDate(date283DaysFromDate($estimatedBeginDate))." to *".formatDisplayDate(date283DaysFromDate($estimatedEndDate));
			} else {
				$estimatedBirthingRange = formatDisplayDate(date283DaysFromDate($estimatedBeginDate))." to ".formatDisplayDate(date283DaysFromDate($estimatedEndDate));
			}
			
			// Find range
			$BeginCalvingSeason = compareDatesEarliest(date283DaysFromDate($estimatedBeginDate));
			$EndCalvingSeason = compareDatesLatest(date283DaysFromDate($estimatedEndDate));
		}
		
		// Correct for empty dates.
		$estimatedBirthingRange = trim($estimatedBirthingRange);
		if($estimatedBirthingRange == "to" 
		|| $estimatedBirthingRange == "to *" 
		|| $estimatedBirthingRange == "*") {
			return "N/A";	
		} else {
			return $estimatedBirthingRange;
		}
	}
	
	function getCalf($animal_registration) {
		// Get's the animal calf
		global $PDO;
		
		$calf = "";
		if($PDO->DB_TYPE == "NEW") {
			$sql = "SELECT b.registration
					FROM animal_breeding br
						INNER JOIN animal a ON br.dam = a.dam
							AND br.sire = a.sire
						INNER JOIN animal_birth b ON a.registration = b.registration
					WHERE br.dam = '".$animal_registration."'
						AND ((br.type = 'NS'
                        		AND (CURRENT_DATE - DATE(br.begin_date)) < 577
								AND ((DATE(b.birth_date) >= (((283-14) * interval '1 day') + DATE(br.begin_date)))
								AND (DATE(b.birth_date) <= (((283+14) * interval '1 day') + DATE(br.end_date)))))
                            OR (br.type = 'AI'
                            	AND (CURRENT_DATE - DATE(br.ai_date)) < 577
								AND (DATE(b.birth_date) >= ((283-18-14) * interval '1 day') + DATE(br.ai_date)
								AND DATE(b.birth_date) <= ((283+18+14) * interval '1 day') + DATE(br.ai_date)))
                            OR (br.type = 'ET'
                            	AND (CURRENT_DATE - DATE(br.transfer_date)) < 577
								AND (DATE(b.birth_date) >= ((283-18-14) * interval '1 day') + DATE(br.transfer_date)
								AND DATE(b.birth_date) <= ((283+18+14) * interval '1 day') + DATE(br.transfer_date))))";
		} else if($PDO->DB_TYPE == "OLD") {
			$sql = "SELECT b.animal_registration AS registration
					FROM tbl_breeding_agreement agr
						INNER JOIN tbl_animal a ON agr.cow_registration = a.animal_dam
							AND agr.bull_registration = a.animal_sire
						INNER JOIN tbl_animal_data_birth b ON a.animal_registration = b.animal_registration
					WHERE agr.cow_registration = '".$animal_registration."'
						AND (CURRENT_DATE - DATE(agr.create_stamp)) < 577
						AND ((DATE(b.birth_date) >= (((283-14) * interval '1 day') + DATE(agr.begin_date)))
							AND (DATE(b.birth_date) <= (((283+14) * interval '1 day') + DATE(agr.end_date))))
						 
					UNION
					
					SELECT b.animal_registration AS registration
					FROM tbl_breeding_ai ai
						INNER JOIN tbl_animal a ON ai.cow_registration = a.animal_dam
							AND ai.bull_registration = a.animal_sire
						INNER JOIN tbl_animal_data_birth b ON a.animal_registration = b.animal_registration
					WHERE ai.cow_registration = '".$animal_registration."'
						AND (CURRENT_DATE - DATE(ai.create_stamp)) < 577
						AND (DATE(b.birth_date) >= ((283-18-14) * interval '1 day') + DATE(ai.ai_date)
							AND DATE(b.birth_date) <= ((283+18+14) * interval '1 day') + DATE(ai.ai_date))
						
					UNION

					SELECT b.animal_registration AS registration
					FROM tbl_breeding_et et
						INNER JOIN tbl_animal a ON et.cow_registration = a.animal_dam
							AND et.bull_registration = a.animal_sire
						INNER JOIN tbl_animal_data_birth b ON a.animal_registration = b.animal_registration
					WHERE et.cow_registration = '".$animal_registration."'
						AND (CURRENT_DATE - DATE(et.create_stamp)) < 577
						AND (DATE(b.birth_date) >= ((283-18-14) * interval '1 day') + DATE(et.transfer_date)
							AND DATE(b.birth_date) <= ((283+18+14) * interval '1 day') + DATE(et.transfer_date))";

		}
		$rs = $PDO->recordSetQuery($sql);
		if($rs) {
			if(!$rs->EOF) {
				$rs->MoveFirst();
				// have already calved
				$calf = $rs->fields['registration'];
			} else {
				$calf = "";
			}
			$rs->Close();
		}
		
		return $calf;
	}
	
	function formatDisplayDate($date) {
		if($date == "" || $date == "null") {
			return "";
		} else {
			$date = strtotime($date);
			return date("M d, Y", $date);
		}
	}
	
	## -------------------- GENERATE PDF -------------------- ##
	$response["pdf_url"] = "";
	
	$MemberName = "";
	$YearDate	= "";
	$BeginCalvingSeason = "";
	$EndCalvingSeason = "";

	// BMC 04.11.2016
	//	Add the animal record status to determine dead or alive calves.
	//	Split the sql up into two groups, where the animal record status is alive, A, or dead, D.
	$report_title = "Estimated Calving Report ".$year;

	//** ------------------------------------ BUILD REPORT HERE ----------------------------------- **// 
	// Get all breeding age females in the pasture owned by this member.
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT a.registration, 
					a.private_herd_number
				FROM animal a
					INNER JOIN ownership o ON a.registration = o.registration
					INNER JOIN animal_location l ON a.registration = l.registration
					INNER JOIN animal_birth b ON a.registration = b.registration
				WHERE o.member_id = '".$member_id."'
					AND NOT o.superceded
                    AND a.sex = 'C'
					AND a.status = '0'
                    AND NOT a.is_deleted
					AND l.pasture_id = '".$pasture_id."'
					AND l.move_out IS NULL
					AND (CURRENT_DATE - b.birth_date) > 365
                GROUP BY a.registration, 
					a.private_herd_number
				ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
	} else if($PDO->DB_TYPE == "OLD") {
		$sql = "SELECT a.animal_registration AS registration, 
					a.animal_private_herd_id AS private_herd_number
				FROM tbl_animal a
					INNER JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
					INNER JOIN tbl_animal_location l ON a.animal_registration = l.animal_registration
					INNER JOIN tbl_animal_data_birth b ON a.animal_registration = b.animal_registration
				WHERE o.owner_id = '".$member_id."'
					AND NOT o.superceded_flag
                    AND a.animal_sex = 'C'
					AND a.animal_record_status = 'A'
					AND l.pasture_id = '".$pasture_id."'
					AND l.move_out_date IS NULL
					AND (CURRENT_DATE - b.birth_date) > 365
				GROUP BY a.animal_registration, 
					a.animal_private_herd_id
				ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
	}
	$rs = $PDO->recordSetQuery($sql);
	if($rs) {
		// BMC 04.11.2016
		//	We're forcing this to pass by adding the '>=' so that we can show dead cows/calves.
		if($rs->Recordcount() >= 0) {	
			$i=0;
			// ********** CREATE THE PDF ********** //
			$pdf = new PDF(); 
			//$pdf->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
			
			// Build PDF
			$requested_font	= 'Times';
			$pdf->AliasNbPages();
			$pdf->SetTopMargin(10);
			$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
			$pdf->ZeroCount();
			$pdf->SetFont('Times','B', 10);
			
			$YearDate = $year;
			
			// Get the member name
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT member_name 
						FROM member 
						WHERE member_id = '".$member_id."'";
			} else if($PDO->DB_TYPE == "OLD") { 
				$sql = "SELECT member_name 
						FROM tbl_member 
						WHERE member_id='".$member_id."'";
			}
			
			$rsMember = $PDO->recordSetQuery($sql);
			if($rsMember) {
				if(!$rsMember->EOF) {
					$rsMember->MoveFirst();
					
					$MemberName = $rsMember->fields['member_name'];
				} else {
					$MemberName = $member_id;
				}
				
				$rsMember->Close();
			} else {
				$MemberName = $member_id;
			}
			
			// get the pasture name
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT pasture_name
						FROM pasture
						WHERE pasture_id = '".$pasture_id."'";
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT pasture_id AS pasture_name
						FROM tbl_pastures
						WHERE pk_id = '".$pasture_id."'";
			}
			$rsPastures = $PDO->recordSetQuery($sql);
			if($rsPastures) {
				if(!$rsPastures->EOF) {
					$rsPastures->MoveFirst();
					$pasture_name = $rsPastures->fields['pasture_name'];
				} else {
					$pasture_name = $pasture_id;
				}
				$rsPastures->Close();
			} else {
				$pasture_name = $pasture_id;
			}

            // BMC 11.07.2016
            //  -- get the member details
            $memberDetails = "";
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT c.address_line_1,
                            c.address_line_2,
                            c.city,
                            c.state,
                            c.zip_code,
                            c.phone_number
                        FROM member m
                            LEFT JOIN contact c ON m.member_id = c.member_id
                                AND c.is_primary
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line_1'];
                        $address_line_2 = $rsContact->fields['address_line_2'];
                        $city = $rsContact->fields['city'];
                        $state = $rsContact->fields['state'];
                        $zip_code = $rsContact->fields['zip_code'];
                        //$phone_number = $rsContact->fields['phone_number'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                        //$phone_number = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT a.address_line1,
                            a.address_line2,
                            a.address_city,
                            a.address_state,
                            a.address_postal_code
                        FROM tbl_member m
                            LEFT JOIN tbl_address a ON m.general_correspondance_address = a.address_id
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line1'];
                        $address_line_2 = $rsContact->fields['address_line2'];
                        $city = $rsContact->fields['address_city'];
                        $state = $rsContact->fields['address_state'];
                        $zip_code = $rsContact->fields['address_postal_code'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else {
                $memberDetails = "";
            }
			
			// build the title
			$pdf->Image(SITE_ROOT . '/images/digital_beef_logo_no_text_tm.png', 10, 10, 35);
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,5,"Digital Beef, LLC", '', 1, 'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(188,12,$memberDetails, '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,7.5,"Estimated Calving Report for ".$pasture_name, '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(188, 8, $MemberName."\n".$member_id, '', 1, 'C');
			$pdf->Cell(188, 6, "", '', 1, 'C');
			
			// Create an array of animals that have calved in the last 283 days and another array of those that are due to calf
			$DueToCalve = array();	// Cows that are due to calf
			$HaveACalf = array();	// Cows that have calved for this calving season
			
			// Find cows that are due to calve OR have already calved
			$rs->MoveFirst();
			while(!$rs->EOF) {
				// check if this animal is due to calve or already has a calf
				// BMC 03.28.2016
				//	Changed the date range from 365 days to 577 days to better pull the correct animals.
				//	That will add another 90 days to the equation
				if($PDO->DB_TYPE == "NEW") {
					$sql = "SELECT br.dam
							FROM animal_breeding br
								INNER JOIN animal a ON br.dam = a.dam
									AND br.sire = a.sire
								INNER JOIN animal_birth b ON a.registration = b.registration
							WHERE br.dam = '".$rs->fields['registration']."'
								AND (CURRENT_DATE - DATE(br.begin_date)) < 577
								AND (((DATE(b.birth_date) >= (((283-14) * interval '1 day') + DATE(br.begin_date)))
										AND (DATE(b.birth_date) <= (((283+14) * interval '1 day') + DATE(br.end_date))))
                                    OR ((DATE(b.birth_date) >= ((283-18-14) * interval '1 day') + DATE(br.ai_date)
										AND DATE(b.birth_date) <= ((283+18+14) * interval '1 day') + DATE(br.ai_date)))
                                    OR ((DATE(b.birth_date) >= ((283-18-14) * interval '1 day') + DATE(br.transfer_date)
										AND DATE(b.birth_date) <= ((283+18+14) * interval '1 day') + DATE(br.transfer_date))))";
				} else if($PDO->DB_TYPE == "OLD") {
					$sql = "SELECT agr.cow_registration AS dam
							FROM tbl_breeding_agreement agr
								INNER JOIN tbl_animal a ON agr.cow_registration = a.animal_dam
									AND agr.bull_registration = a.animal_sire
								INNER JOIN tbl_animal_data_birth b ON a.animal_registration = b.animal_registration
							WHERE agr.cow_registration = '".$rs->fields['registration']."'
								AND (CURRENT_DATE - DATE(agr.create_stamp)) < 577
								AND ((DATE(b.birth_date) >= (((283-14) * interval '1 day') + DATE(agr.begin_date)))
									AND (DATE(b.birth_date) <= (((283+14) * interval '1 day') + DATE(agr.end_date))))
								 
							UNION
							
							SELECT ai.cow_registration AS dam
							FROM tbl_breeding_ai ai
								INNER JOIN tbl_animal a ON ai.cow_registration = a.animal_dam
									AND ai.bull_registration = a.animal_sire
								INNER JOIN tbl_animal_data_birth b ON a.animal_registration = b.animal_registration
							WHERE ai.cow_registration = '".$rs->fields['registration']."'
								AND (CURRENT_DATE - DATE(ai.create_stamp)) < 577
								AND (DATE(b.birth_date) >= ((283-18-14) * interval '1 day') + DATE(ai.ai_date)
									AND DATE(b.birth_date) <= ((283+18+14) * interval '1 day') + DATE(ai.ai_date))
								
							UNION
		
							SELECT et.cow_registration AS dam
							FROM tbl_breeding_et et
								INNER JOIN tbl_animal a ON et.cow_registration = a.animal_dam
									AND et.bull_registration = a.animal_sire
								INNER JOIN tbl_animal_data_birth b ON a.animal_registration = b.animal_registration
							WHERE et.cow_registration = '".$rs->fields['registration']."'
								AND (CURRENT_DATE - DATE(et.create_stamp)) < 577
								AND (DATE(b.birth_date) >= ((283-18-14) * interval '1 day') + DATE(et.transfer_date)
									AND DATE(b.birth_date) <= ((283+18+14) * interval '1 day') + DATE(et.transfer_date))";
				}
				$rsDam = $PDO->recordSetQuery($sql);
				if($rsDam) {
					if(!$rsDam->EOF) {
						$rsDam->MoveFirst();
						// have already calved
						$HaveACalf[] = $rsDam->fields['dam'];
					} else {
						// Not yet calved
						$DueToCalve[] = $rs->fields['registration'];
					}
					$rsDam->Close();
				}
				
				$rs->MoveNext();
			}
	
			// Display Due to Calve
			if(count($DueToCalve) > 0) {
				$pdf->SetFont('Arial','B',7);
				$pdf->Cell(1,5,"",'R',0,'C');	
				$pdf->Cell(40,5,"Due to Calve: ".count($DueToCalve),'LRBT',1,'C');
				$pdf->Cell(1,5,"",'R',0,'C');	
				$pdf->Cell(20,5,"PHN",'LRBT',0,'C');
				$pdf->Cell(20,5,"Name",'LRBT',0,'C');	
				$pdf->Cell(20,5,"Pasture",'LRBT',0,'C');
				$pdf->Cell(60,5,"Estimated Calving Dates",'LRBT',1,'C');
			
				// Loop through each
				for($i = 0; $i < count($DueToCalve); $i++) {
					$animal_registration = $DueToCalve[$i];
					// Get estimated calving dates
					$CalvingRange = getEstimatedCalvingDates($animal_registration);
					
					// Get animal info
					if($PDO->DB_TYPE == "NEW") {
						$sql = "SELECT a.registration, 
									a.private_herd_number, 
									a.animal_name, 
									p.pasture_name
								FROM animal a
									LEFT JOIN pasture p ON p.pasture_id = '".$pasture_id."'
								WHERE a.registration = '".$animal_registration."'";
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "SELECT a.animal_registration AS registration, 
									a.animal_private_herd_id AS private_herd_number, 
									a.animal_name, 
									p.pasture_id AS pasture_name
								FROM tbl_animal a
									LEFT JOIN tbl_pastures p ON p.pk_id = '".$pasture_id."'
								WHERE a.animal_registration = '".$animal_registration."'";
					}
					$rsAnimalDetails = $PDO->recordSetQuery($sql);
					if($rsAnimalDetails) {
						if(!$rsAnimalDetails->EOF) {
							$rsAnimalDetails->MoveFirst();
							
							$pdf->SetFont('Arial','',7);
							$pdf->Cell(1,5,"",'R',0,'C');	
							$pdf->Cell(20,5,$rsAnimalDetails->fields["private_herd_number"],'LRBT',0,'C');
							$pdf->Cell(20,5,$rsAnimalDetails->fields["animal_name"],'LRBT',0,'C');	
							$pdf->Cell(20,5,$rsAnimalDetails->fields["pasture_name"],'LRBT',0,'C');
							
							if (strpos($CalvingRange, '*') !== FALSE) {
								$pdf->SetTextColor(150,50,50);
								$CalvingRange = str_replace("*", "", $CalvingRange);
								$pdf->Cell(60,5,$CalvingRange,'LRBT',1,'C');
								$pdf->SetTextColor(0,0,0);
							} else {
								$pdf->Cell(60,5,$CalvingRange,'LRBT',1,'C');
							}
						}
						$rsAnimalDetails->Close();
					}
				}
			}
			
			// Display Calved
			if(count($HaveACalf) > 0) {
				if(count($DueToCalve) > 0) {
					$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
				}
				
				$pdf->SetFont('Arial','B',7);
				$pdf->Cell(1,5,"",'R',0,'C');	
				$pdf->Cell(40,5,"With a Calf: ".count($HaveACalf),'LRBT',1,'C');
				$pdf->Cell(1,5,"",'R',0,'C');	
				$pdf->Cell(20,5,"PHN",'LRBT',0,'C');
				$pdf->Cell(20,5,"Name",'LRBT',0,'C');	
				$pdf->Cell(20,5,"Pasture",'LRBT',0,'C');
				$pdf->Cell(60,5,"Calf & Birth Date",'LRBT',1,'C');
			
				// Loop through each
				for($i = 0; $i < count($HaveACalf); $i++) {
					$animal_registration = $HaveACalf[$i];
					// Get animal info
					if($PDO->DB_TYPE == "NEW") {
						$sql = "SELECT a.registration, 
									a.private_herd_number, 
									a.animal_name, 
									p.pasture_name
								FROM animal a
									LEFT JOIN pasture p ON p.pasture_id = '".$pasture_id."'
								WHERE a.registration = '".$animal_registration."'";
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "SELECT a.animal_registration AS registration, 
									a.animal_private_herd_id AS private_herd_number, 
									a.animal_name, 
									p.pasture_id AS pasture_name
								FROM tbl_animal a
									LEFT JOIN tbl_pastures p ON p.pk_id = '".$pasture_id."'
								WHERE a.animal_registration = '".$animal_registration."'";
					}
					$rsAnim = $PDO->recordSetQuery($sql);
					if($rsAnim) {
						$rsAnim->MoveFirst();
						if(!$rsAnim->EOF) {
							// Get the calf
							$calf_phn 	= "";	// calf private herd id
							$calf_birth = "";	// calf birth date
							$calf_reg = getCalf($animal_registration);
							if($PDO->DB_TYPE == "NEW") {
								$sql = "SELECT a.registration, 
											a.private_herd_number,
											a.status,
											b.birth_date
										FROM animal a
											INNER JOIN animal_birth b ON a.registration = b.registration
										WHERE a.registration = '".$calf_reg."'";
							} else if($PDO->DB_TYPE == "OLD") {
								$sql = "SELECT a.animal_registration AS registration, 
											a.animal_private_herd_id AS private_herd_number,
											a.animal_record_status AS status,
											b.birth_date
										FROM tbl_animal a
											INNER JOIN tbl_animal_data_birth b ON a.animal_registration = b.animal_registration
										WHERE a.animal_registration = '".$calf_reg."'";
							}
							$rsCalf = $PDO->recordSetQuery($sql);
							if($rsCalf) {
								if(!$rsCalf->EOF) {
									$rsCalf->MoveFirst();
									$calf_phn 	= $rsCalf->fields['private_herd_number'];
									$calf_birth = $rsCalf->fields['birth_date'];
									
									// determine dead cell
									$showDead = "";
									$fillCell = "";
									$status = strtoupper(trim($rsCalf->fields['status']));
									if($status == "D"
										|| $status == 2
										|| $status == 4) {
										$showDead = " - DEAD";
										$pdf->SetFillColor(200,200,200);
										$fillCell = true;
									} else {
										$pdf->SetFillColor(255,255,255);
									}
								}
								$rsCalf->Close();
							}
							
							$pdf->SetFont('Arial','',7);
							$pdf->Cell(1,5,"",'R',0,'C');	
							$pdf->Cell(20,5,$rsAnim->fields["private_herd_number"],'LRBT',0,'C');
							$pdf->Cell(20,5,$rsAnim->fields["animal_name"],'LRBT',0,'C');	
							$pdf->Cell(20,5,$rsAnim->fields["pasture_name"],'LRBT',0,'C');
							$pdf->Cell(60,5, $calf_phn." - ".formatDate($calf_birth).$showDead, 'LRBT', 1, 'C', $fillCell);
						}
						$rsAnim->Close();
					}
				}
			}
			
			// Print the calving season on a new page.
			if(count($HaveACalf) > 0 || count($DueToCalve) > 0) {
				$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
			}
				
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(1,5,"",'R',0,'C');	
			$pdf->Cell(60,5,"Calving Season: ",'LBT',0,'R');
			$pdf->SetFont('Arial','',7);
			if(trim($BeginCalvingSeason) == "" && trim($EndCalvingSeason) == "") {
				$pdf->Cell(60, 5, "N/A",'RBT',1,'L');
			} else {
				if (isDateAfterCurrDate($BeginCalvingSeason) && !isDateAfterCurrDate($EndCalvingSeason)) {
					$pdf->SetTextColor(0,60,0);
					$pdf->Cell(60,5,date("M d, Y", $BeginCalvingSeason)." to ".date("M d, Y", $EndCalvingSeason),'RBT',1,'L');
					$pdf->SetTextColor(0,0,0);
				} else if(isDateAfterCurrDate($BeginCalvingSeason) && isDateAfterCurrDate($EndCalvingSeason)) {
					$pdf->SetTextColor(150,50,50);
					$pdf->Cell(60,5,date("M d, Y", $BeginCalvingSeason)." to ".date("M d, Y", $EndCalvingSeason),'RBT',1,'L');
					$pdf->SetTextColor(0,0,0);
				} else {
					$pdf->Cell(60, 5, date("M d, Y", $BeginCalvingSeason)." to ".date("M d, Y", $EndCalvingSeason),'RBT',1,'L');
				}
			}
		
			// create export directory, name, and url
			$exportDir = getExportDirectory($provider, $member_id);
			$timestamp = time();
			
			$exportName = "estimated_calving_report_app_".$member_id."_".$pasture_id.".pdf";
			$exportDir = $exportDir . "estimated_calving_report_app_".$member_id."_".$pasture_id.".pdf";
			
			if($PDO->DB_TYPE == "NEW") {
				// for the new databases we have to look in the members folder for the 
				// correct pdf directory
				$exportUrl 	= $PDO->FILE_DIR . $member_id . "/" . $exportName;
			} else if($PDO->DB_TYPE == "OLD") {
				$exportUrl 	= $PDO->FILE_DIR . $exportName;
			}
			
			// export the file
			$pdf->Output($exportDir);
			
			// If file name already exists, then update it!
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT * 
						FROM reports 
						WHERE report_title = '".$report_title."' 
							AND report_file_name ='".$exportName."'
							AND member_id = '".$member_id."'";
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT * 
						FROM tbl_member_reports_android 
						WHERE report_type = '".$report_title."' 
							AND report_file_name='".$exportName."'
							AND create_user = '".$member_id."'";
			}
			$rsCheckDuplicate = $PDO->recordSetQuery($sql);
			if($rsCheckDuplicate) {
				if(!$rsCheckDuplicate->EOF) {
					$rsCheckDuplicate->MoveFirst();
					// Update
					if($PDO->DB_TYPE == "NEW") {
						$sql = "UPDATE reports 
								SET report_date = '".date('Y-m-d h:i:s')."'
								WHERE member_id = '".$member_id."'
									AND report_title = '".$report_title."'
									AND report_file_name = '".$exportName."'
									AND report_format = 'P'";
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "UPDATE tbl_member_reports_android 
								SET create_stamp='".date('Y-m-d h:i:s')."'
								WHERE member_id='".$member_id."'
									AND report_type='".$report_title."'
									AND report_file_name='".$exportName."'
									AND report_format='P'
									AND create_user='".$member_id."'";
					}
					$PDO->executeQuery($sql);
				} else {
					// INSERT
					if($PDO->DB_TYPE == "NEW") {
						$sql = "INSERT INTO reports 
									(member_id, report_title, report_file_name, report_format, report_date) 
								VALUES 
									('".$member_id."','".$report_title."','".$exportName."', 'P', 
									'".date('Y-m-d h:i:s')."')";
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "INSERT INTO tbl_member_reports_android 
									(member_id, report_type, report_file_name, report_format, create_user, 
									create_stamp) 
								VALUES 
									('".$member_id."','".$report_title."','".$exportName."', 'P', '".$member_id."', 
									'".date('Y-m-d h:i:s')."')";
					}
					$PDO->executeQuery($sql);
				}
			}
		
			// send back the file name and the pdf url as the result
			$response["success"] = true;
			$response["pdf_url"] = $exportUrl;	// send back the url
			$response["message"] = "successfully generated pdf";
			echo json_encode($response);
		} else {
			$response["success"] = false;
			$response["message"] = "you have no calving data available";
			die(json_encode($response));
		}
	} else {
		$response["success"] = false;
		$response["message"] = "sql error";
		die(json_encode($response));
	}
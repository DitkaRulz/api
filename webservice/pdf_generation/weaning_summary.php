<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}

    header('Access-Control-Allow-Origin: *');
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	function formatSex($sex) {
		switch(strtoupper(trim($sex))) {
			case "C": return "Cow";
			case "B": return "Bull";
			case "S": return "Steer";
			default: return "N/A";
		}
	}
	
	function formatDate($date) {
		// BMC 02.01.2016
		//	This function will take in a date, most likely formatted 
		//	like YYYY-MM-DD and change the format to MM DD, YYYY
		//	as an example, Jan 01, 2016
		// BMC 02.03.2016
		//	If the date comes in as blank or null then this will display
		//	Dec 31, 1969!!! So we need to account for these input and
		//	display the proper output.  For now, we'll display nothing, just
		//	a blank string.
		
		if(trim($date) == "" 
			|| trim($date) == "null") {
			return "";
		} else {
			$date = strtotime($date);
			return date("M d, Y", $date);
		}
	}
	
	## -------------------- GENERATE PDF -------------------- ##
	$response["pdf_url"] = "";
	
	$MemberName = "";
	$YearDate	= "";
	
	$report_title = "Weaning Summary ".formatDate($start_date)." to ".formatDate($end_date);

	//** ------------------------------------ BUILD REPORT HERE ----------------------------------- **//
	// get the weaning data by this member on the particular date
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT a.registration, 
					a.private_herd_number, 
					a.sex, 
					a.sire, 
					w.wean_date, 
					w.weight AS wean_weight, 
					w.weight_adj AS wean_weight_adj,
					b.birth_date,
					b.weight AS birth_weight, 
					b.weight_adj AS birth_weight_adj,
					p.pasture_name,
					(w.wean_date - b.birth_date) AS animal_age
				FROM animal_wean w
					INNER JOIN animal a on w.registration = a.registration 
					INNER JOIN animal_birth b on b.registration = a.registration
					INNER JOIN ownership o ON w.registration = o.registration
					LEFT JOIN animal_location l ON w.registration = l.registration
						AND (l.move_in <= w.wean_date
							AND (l.move_out IS NULL
								OR l.move_out >= w.wean_date))
					LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
					                        AND p.member_id = o.member_id
				WHERE o.member_id = ?
					AND o.date_owned <= w.wean_date
					AND w.wean_date >= DATE(?)
					AND w.wean_date <= DATE(?)
				GROUP BY a.registration, 
					a.private_herd_number, 
					a.sex, 
					a.sire, 
					w.wean_date, 
					w.weight, 
					w.weight_adj,
					b.birth_date,
					b.weight, 
					b.weight_adj,
					p.pasture_name
				ORDER BY 
				    p.pasture_name ASC,
				    b.birth_date ASC";
		$params = array($member_id, $start_date, $end_date);
	} else if($PDO->DB_TYPE == "OLD") {
	    switch($provider) {
            case "ACRS_CATTLE":
            case "AMARS_CATTLE":
                $sql = "SELECT 
                          CASE 
                              WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le
                              WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re
                              ELSE ''
                          END AS private_herd_number,
                          a.animal_registration AS registration,
                          a.animal_sex AS sex, 
                          a.animal_sire AS sire, 
                          w.wean_date,
                          w.wean_weight, 
                          w.wean_weight_adj,
                          b.birth_date,
                          b.birth_weight, 
                          b.birth_weight_adj,
                          p.pasture_id AS pasture_name,
                          (w.wean_date - b.birth_date) AS animal_age
                        FROM tbl_animal_data_wean w
                            INNER JOIN tbl_animal a on w.animal_registration = a.animal_registration 
                            INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
                            LEFT JOIN tbl_animal_location l ON w.animal_registration = l.animal_registration
                                AND (l.move_in_date <= w.wean_date
                                    AND (l.move_out_date IS NULL
                                        OR l.move_out_date >= w.wean_date))
                            LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::VARCHAR
                            INNER JOIN tbl_ownership o ON w.animal_registration = o.animal_registration
                        WHERE o.owner_id = ?
                            AND o.ownership_date <= w.wean_date
                            AND w.wean_date >= DATE(?)
                            AND w.wean_date <= DATE(?)
                        GROUP BY a.animal_registration, 
                            a.tattoo_le,
                            a.tattoo_re, 
                            a.animal_sex, 
                            a.animal_sire, 
                            w.wean_date,
                            w.wean_weight, 
                            w.wean_weight_adj,
                            b.birth_date,
                            b.birth_weight, 
                            b.birth_weight_adj,
                            p.pasture_id
                        ORDER BY 
                            p.pasture_id ASC,
                            b.birth_date ASC";
                $params = array($member_id, $start_date, $end_date);
            break;
            default:
                $sql = "SELECT a.animal_registration AS registration, 
                          a.animal_private_herd_id AS private_herd_number, 
                          a.animal_sex AS sex, 
                          a.animal_sire AS sire, 
                          w.wean_date,
                          w.wean_weight, 
                          w.wean_weight_adj,
                          b.birth_date,
                          b.birth_weight, 
                          b.birth_weight_adj,
                          p.pasture_id AS pasture_name,
                          (w.wean_date - b.birth_date) AS animal_age
                        FROM tbl_animal_data_wean w
                            INNER JOIN tbl_animal a on w.animal_registration = a.animal_registration 
                            INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
                            LEFT JOIN tbl_animal_location l ON w.animal_registration = l.animal_registration
                                AND (l.move_in_date <= w.wean_date
                                    AND (l.move_out_date IS NULL
                                        OR l.move_out_date >= w.wean_date))
                            LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::VARCHAR
                            INNER JOIN tbl_ownership o ON w.animal_registration = o.animal_registration
                        WHERE o.owner_id = ?
                            AND o.ownership_date <= w.wean_date
                            AND w.wean_date >= DATE(?)
                            AND w.wean_date <= DATE(?)
                        GROUP BY a.animal_registration, 
                            a.animal_private_herd_id, 
                            a.animal_sex, 
                            a.animal_sire, 
                            w.wean_date,
                            w.wean_weight, 
                            w.wean_weight_adj,
                            b.birth_date,
                            b.birth_weight, 
                            b.birth_weight_adj,
                            p.pasture_id
                        ORDER BY 
                            p.pasture_id ASC,
                            b.birth_date ASC";
                $params = array($member_id, $start_date, $end_date);
            break;
        }
	}
	$rs = $PDO->recordSetQuery($sql, $params);
	if($rs) {
		// BUILD PDF
		if($rs->Recordcount() > 0) {
			// ********** CREATE THE PDF ********** //
			$pdf = new PDF(); 
			//$pdf->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
			
			// Build PDF
			$requested_font	= 'Times';
			$pdf->AliasNbPages();
			$pdf->SetTopMargin(10);
			$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
			$pdf->ZeroCount();
			$pdf->SetFont('Times','B', 10);
			
			$i=0;	// not sure about this
			
			// get the calf count
			$WeanCount = $rs->Recordcount();
			
			// Get the member name
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT member_name 
						FROM member 
						WHERE member_id = ?";
				$params = array($member_id);
			} else if($PDO->DB_TYPE == "OLD") { 
				$sql = "SELECT member_name 
						FROM tbl_member 
						WHERE member_id= ?";
				$params = array($member_id);
			}
			
			$rsMember = $PDO->recordSetQuery($sql, $params);
			if($rsMember) {
				if(!$rsMember->EOF) {
					$rsMember->MoveFirst();
					
					$MemberName = $rsMember->fields['member_name'];
				} else {
					$MemberName = $member_id;
				}
				
				$rsMember->Close();
			} else {
				$MemberName = $member_id;
			}

            // BMC 11.07.2016
            //  -- get the member details
            $memberDetails = "";
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT c.address_line_1,
                            c.address_line_2,
                            c.city,
                            c.state,
                            c.zip_code,
                            c.phone_number
                        FROM member m
                            LEFT JOIN contact c ON m.member_id = c.member_id
                                AND c.is_primary
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line_1'];
                        $address_line_2 = $rsContact->fields['address_line_2'];
                        $city = $rsContact->fields['city'];
                        $state = $rsContact->fields['state'];
                        $zip_code = $rsContact->fields['zip_code'];
                        //$phone_number = $rsContact->fields['phone_number'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                        //$phone_number = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT a.address_line1,
                            a.address_line2,
                            a.address_city,
                            a.address_state,
                            a.address_postal_code
                        FROM tbl_member m
                            LEFT JOIN tbl_address a ON m.general_correspondance_address = a.address_id
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line1'];
                        $address_line_2 = $rsContact->fields['address_line2'];
                        $city = $rsContact->fields['address_city'];
                        $state = $rsContact->fields['address_state'];
                        $zip_code = $rsContact->fields['address_postal_code'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else {
                $memberDetails = "";
            }
			
			// Get animal data
			if(!$rs->EOF) {	
				$animal_ADG = "";
				$animal_WDA = "";

                // get the pasture name
                $pasture_name = "";
                if ($PDO->DB_TYPE == "NEW") {
                    if (trim(strtoupper($rs->fields['pasture_name'])) == "") {
                        $pasture_name = "N/A";
                    } else {
                        $pasture_name = trim(strtoupper($rs->fields['pasture_name']));
                    }
                } else if ($PDO->DB_TYPE == "OLD") {
                    if (trim(strtoupper($rs->fields['pasture_name_1'])) == ""
                        || trim(strtoupper($rs->fields['pasture_name_1'])) == "NULL"
                        || trim(strtoupper($rs->fields['pasture_name_1'])) == NULL) {
                        if (trim(strtoupper($rs->fields['pasture_name_2'])) == ""
                            || trim(strtoupper($rs->fields['pasture_name_2'])) == "NULL"
                            || trim(strtoupper($rs->fields['pasture_name_2'])) == NULL) {
                            $pasture_name = "N/A";
                        } else {
                            $pasture_name = trim(strtoupper($rs->fields['pasture_name_2']));
                        }
                    } else {
                        $pasture_name = trim(strtoupper($rs->fields['pasture_name_1']));
                    }
                }

                // build the title
                $pdf->Image(SITE_ROOT . '/images/digital_beef_logo_no_text_tm.png', 10, 10, 35);
                $pdf->SetFont('Arial', 'B', 12);
                $pdf->Cell(188,5,"Digital Beef, LLC", '', 1, 'C');
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(188,12,$memberDetails, '', 1, 'C');
                $pdf->SetFont('Arial', 'B', 12);
                $pdf->Cell(188,7.5,"Weaning Summary ".formatDate($start_date)." to ".formatDate($end_date), '', 1, 'C');
                $pdf->SetFont('Arial', 'B', 9);
                $pdf->Cell(188,7.5, "Total Weaned: ".$WeanCount, '', 1, 'C');
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(188, 8, $MemberName."\n".$member_id, '', 1, 'C');
                $pdf->Cell(188, 6, "", '', 1, 'C');

                // Set the date printed
                $pdf->SetFont('Arial','',7);
                $pdf->Cell(3, 5, "", '', 0, 'C');
                $pdf->Cell(155 ,5, "", '', 0, 'C');
                $pdf->Cell(35, 5, "Date Printed: " . formatDate(date("m/d/Y")), '' , 0, 'R');
                $pdf->Cell(1,5,"",'',1,'C');

                // Set the headers
                $pdf->SetFont('Arial','B',7);
                $pdf->Cell(3, 5, "", '', 0, 'C');
                $pdf->Cell(5, 5, "", 'LRTB', 0, 'C');
                $pdf->Cell(30, 5, "Animal", 'LTRB', 0, 'C');
                $pdf->Cell(43, 5, "Statistics", 'LTRB', 0, 'C');
                $pdf->Cell(81, 5, "Wean", 'LTRB', 0, 'C');
                $pdf->Cell(35, 5, "Location", 'LTRB', 1, 'C');

                // set the header
                $pdf->SetFont('Arial','B',7);
                $pdf->Cell(3,5,"",'',0,'C');
                $pdf->Cell(5,5, "#", 'LTRB', 0, 'C');
                $pdf->Cell(30,5,"Tattoo",'LRBT',0,'C');
                $pdf->Cell(10,5,"Sex",'LRBT',0,'C');
                $pdf->Cell(18,5,"D.O.B",'LRBT',0,'C');
                $pdf->Cell(15,5,"Age (Days)",'LRBT',0,'C');
                $pdf->Cell(18,5,"Date",'LRBT', 0, 'C');
                $pdf->Cell(18,5,"Weight",'LRBT',0,'C');
                $pdf->Cell(15,5,"Adj. Weight",'LRBT',0,'C');
                $pdf->Cell(15,5,"ADG",'LRBT', 0, 'C');
                $pdf->Cell(15,5,"WDA",'LRBT',0,'C');
                $pdf->Cell(35,5,"Pasture",'LRBT',0,'C');
                $pdf->Cell(3,5,"",'L',1,'C');
				
				$rs->MoveFirst();
				$row = 0;
				while(!$rs->EOF) {
				    $row++;
				    $pasture_name_check = $pasture_name;
					// get the sire phn
                    $animal_sire = $rs->fields['sire'];
                    $sire_name = "";
                    $animal_sire_left_tattoo = "";
                    $animal_sire_right_tattoo = "";
                    if ($PDO->DB_TYPE == "NEW") {
                        $sql = "SELECT 
                                    animal_name,
                                    private_herd_number,
                                    left_tattoo,
                                    right_tattoo 
                                FROM 
                                    animal 
                                WHERE 
                                    registration = ?";
                        $params = array($animal_sire);
                    } else if ($PDO->DB_TYPE == "OLD") {
                        switch ($provider) {
                            case "ACRS_CATTLE":
                            case "AMARS_CATTLE":
                                $sql = "SELECT 
                                        animal_name,
                                        '' AS private_herd_number,
                                        tattoo_le AS left_tattoo,
                                        tattoo_re AS right_tattoo
                                    FROM 
                                        tbl_animal 
                                    WHERE 
                                        animal_registration = ?";
                                $params = array($animal_sire);
                                break;
                            default:
                                $sql = "SELECT 
                                            animal_name,
                                            animal_private_herd_id AS private_herd_number
                                        FROM 
                                          tbl_animal 
                                        WHERE 
                                          animal_registration = ?";
                                $params = array($animal_sire);
                                break;
                        }
                    }
					$rsSire = $PDO->recordSetQuery($sql, $params);
					if($rsSire) {
						if(!$rsSire->EOF) {
							$rsSire->MoveFirst();

                            $animal_sire_phn = trim($rsSire->fields['private_herd_number']);
                            $sire_name = $rsSire->fields['animal_name'];
                            $animal_sire_left_tattoo = trim($rsSire->fields['left_tattoo']);
                            $animal_sire_right_tattoo = trim($rsSire->fields['right_tattoo']);
						}
						$rsSire->Close();
					}
					
					// Calculate ADG and WDA
					$birth_date = strtotime($rs->fields['birth_date']);
					$wean_date = strtotime($rs->fields['wean_date']);
		
					// ADG = (Wean Wt. – Birth Wt.) ÷ Number of Days Between Weights
					$datediff = $wean_date - $birth_date;
					$days_between_weights = floor($datediff/(60*60*24));
					$animal_ADG = ($rs->fields['wean_weight'] - $rs->fields['birth_weight']) / $days_between_weights;
					
					// WDA = Final Weight ÷ Calf Age
					$datediff = $wean_date - $birth_date;
					$calf_age = floor($datediff/(60*60*24));
					$animal_WDA = $rs->fields['wean_weight'] / $calf_age;

                    // Get the tattoo and PHN
                    $phn = trim($rs->fields['private_herd_number']);
                    $rightTattoo = trim($rs->fields['right_tattoo']);
                    $leftTattoo = trim($rs->fields['left_tattoo']);

                    // Build the pdf
                    // LRM 07.14.2017: Start by adding the initial spacer and setting the height to 5
                    // Height is important for setting the headers for auto-creating a new page with data
                    $pdf->Cell(3,5,"",'',0,'C');

                    // LRM 07.14.2017:
                    // Get the Y location of the page and if it is in the first row after a new page
                    // Set these headers before moving on with the next row to add
                    // Needs to go after the first Cell
                    if ($pdf->GetY() == 10) {
                        // Set the date printed
                        $pdf->SetFont('Arial','',7);
                        $pdf->Cell(157 ,5, "", '', 0, 'C');
                        $pdf->Cell(35, 5, "Date Printed: " . formatDate(date("m/d/Y")), '' , 0, 'R');
                        $pdf->Cell(1,5,"",'',1,'C');

                        // Set the headers
                        $pdf->SetFont('Arial','B',7);
                        $pdf->Cell(3, 5, "", '', 0, 'C');
                        $pdf->Cell(5, 5, "", 'LRTB', 0, 'C');
                        $pdf->Cell(30, 5, "Animal", 'LTRB', 0, 'C');
                        $pdf->Cell(43, 5, "Statistics", 'LTRB', 0, 'C');
                        $pdf->Cell(81, 5, "Wean", 'LTRB', 0, 'C');
                        $pdf->Cell(35, 5, "Location", 'LTRB', 1, 'C');

                        // set the header
                        $pdf->SetFont('Arial','B',7);
                        $pdf->Cell(3,5,"",'',0,'C');
                        $pdf->Cell(5,5, "#", 'LTRB', 'C');
                        $pdf->Cell(30,5,"Tattoo",'LRBT',0,'C');
                        $pdf->Cell(10,5,"Sex",'LRBT',0,'C');
                        $pdf->Cell(18,5,"D.O.B",'LRBT',0,'C');
                        $pdf->Cell(15,5,"Age (Days)",'LRBT',0,'C');
                        $pdf->Cell(18,5,"Date",'LRBT', 0, 'C');
                        $pdf->Cell(18,5,"Weight",'LRBT',0,'C');
                        $pdf->Cell(15,5,"Adj. Weight",'LRBT',0,'C');
                        $pdf->Cell(15,5,"ADG",'LRBT', 0, 'C');
                        $pdf->Cell(15,5,"WDA",'LRBT',0,'C');
                        $pdf->Cell(35,5,"Pasture",'LRBT',0,'C');
                        $pdf->Cell(3,5,"",'L',1,'C');

                        $pdf->Cell(3,5,"",'R',0,'C');
                    }

                    $pdf->SetFont('Arial','',7);
                    $pdf->Cell(5,5,$row,'LRTB',0,'C');

                    // LRM 07/12/2017:
                    // Check if the LE or RE tattoo have a value.
                    // Calf Section
                    if ($rightTattoo !== "" || $leftTattoo !== "") {
                        if ($rightTattoo !== "" && $leftTattoo === "") {
                            $pdf->Cell(30, 5, $rightTattoo . "[RE]" . $showDead, 'LRBT', 0, 'L', $fillCell);
                        } elseif ($rightTattoo === "" && $leftTattoo !== "") {
                            $pdf->Cell(30, 5, $leftTattoo . "[LE]" . $showDead, 'LRBT', 0, 'L', $fillCell);
                        } elseif ($rightTattoo !== "" && $leftTattoo !== "") {
                            if ($rightTattoo !== $leftTattoo) {
                                $pdf->Cell(30, 5, $leftTattoo . "[LE] " . $rightTattoo . "[RE]" . $showDead, 'LRBT', 0, 'L', $fillCell);
                            } elseif ($rightTattoo === $leftTattoo) {
                                $pdf->Cell(30, 5, $rightTattoo . "[BE]", 'LTRB', 0, 'L');
                            }
                        }
                    } else {
                        $pdf->Cell(30, 5, $phn. "" . $showDead, 'LRBT', 0, 'L', $fillCell);
                    }

                    $pdf->Cell(10,5,$rs->fields['sex'],'LRBT',0,'C');
                    $pdf->Cell(18,5,formatDate($rs->fields['birth_date']),'LTRB',0,'C');
                    $pdf->Cell(15,5,$rs->fields['animal_age'],'LTRB',0,'C');
                    $pdf->Cell(18,5,formatDate($rs->fields['wean_date']),'LRBT', 0, 'C');
                    $pdf->Cell(18,5,$rs->fields['wean_weight'],'LTRB',0,'C');
                    $pdf->Cell(15,5,$rs->fields['wean_weight_adj'],'LTRB',0,'C');
                    $pdf->Cell(15,5,round($animal_ADG, 2),'LTRB',0,'C');
                    $pdf->Cell(15,5,round($animal_WDA, 2),'LTRB',0,'C');
                    $pdf->Cell(35,5,$rs->fields['pasture_name'],'LTRB',0,'C');
                    $pdf->Cell(3,5,"",'L',1,'C');

                    $rs->MoveNext();
				}
			} 
			
			$rs->Close();
		} else {
			$response["success"] = false;
			$response["message"] = "you have no weaning data available";
			die(json_encode($response));
		}
	} else {
		$response["success"] = false;
		$response["message"] = "sql error";
		die(json_encode($response));
	}
				
	// BMC 09.19.2016
	//	-- we want to know all the cattle that are less than 160 days old that
	//		were not weaned but were also weighed on this day.  that way we
	//		can make an informative decision on what animals were worked.
	//	-- this report will give us a more complete picture of those animals
	//		that were worked.
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT a.registration, 
					a.private_herd_number, 
					a.sex, 
					a.sire, 
					w.measurement_date, 
					w.weight AS other_weight,
					b.birth_date,
					b.weight AS birth_weight, 
					b.weight_adj AS birth_weight_adj,
					p.pasture_name,
					(w.measurement_date - b.birth_date) AS animal_age
				FROM animal_measurements w
					INNER JOIN animal a on w.registration = a.registration 
					INNER JOIN animal_birth b on b.registration = a.registration
					LEFT JOIN animal_location l ON w.registration = l.registration
						AND (l.move_in <= w.measurement_date
							AND (l.move_out IS NULL
								OR l.move_out >= w.measurement_date))
					LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
					INNER JOIN ownership o ON w.registration = o.registration
				WHERE o.member_id = ?
					AND o.date_owned <= w.measurement_date
					AND w.measurement_date >= DATE(?)
					AND w.measurement_date <= DATE(?)
					AND (w.measurement_date - b.birth_date) <= 160
				GROUP BY a.registration, 
					a.private_herd_number, 
					a.sex, 
					a.sire, 
					w.measurement_date, 
					w.weight,
					b.birth_date,
					b.weight, 
					b.weight_adj,
					p.pasture_name
				ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
		$params = array($member_id, $start_date, $end_date);
	} else if($PDO->DB_TYPE == "OLD") {
	    switch($provider) {
            case "ACRS_CATTLE":
            case "AMARS_CATTLE":
                $sql = "SELECT
                          CASE 
                              WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le
                              WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re
                              ELSE ''
                          END AS private_herd_number 
                          a.animal_registration AS registration,
                          a.animal_sex AS sex, 
                          a.animal_sire AS sire, 
                          w.measurement_date,
                          w.weight AS other_weight, 
                          b.birth_date,
                          b.birth_weight, 
                          b.birth_weight_adj,
                          p.pasture_id AS pasture_name,
                          (w.measurement_date - b.birth_date) AS animal_age
                        FROM tbl_animal_data_other_weight w
                            INNER JOIN tbl_animal a on w.animal_registration = a.animal_registration 
                            INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
                            LEFT JOIN tbl_animal_location l ON w.animal_registration = l.animal_registration
                                AND (l.move_in_date <= w.measurement_date
                                    AND (l.move_out_date IS NULL
                                        OR l.move_out_date >= w.measurement_date))
                            LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::VARCHAR
                            INNER JOIN tbl_ownership o ON w.animal_registration = o.animal_registration
                        WHERE o.owner_id = ?
                            AND o.ownership_date <= w.measurement_date
                            AND w.measurement_date >= DATE(?)
                            AND w.measurement_date <= DATE(?)
                            AND (w.measurement_date - b.birth_date) <= 160
                        GROUP BY a.animal_registration, 
                            a.animal_private_herd_id, 
                            a.animal_sex, 
                            a.animal_sire, 
                            w.measurement_date,
                            w.weight,
                            b.birth_date,
                            b.birth_weight, 
                            b.birth_weight_adj,
                            p.pasture_id
                        ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
                $params = array($member_id, $start_date, $end_date);
            break;
            default:
                $sql = "SELECT a.animal_registration AS registration, 
                          a.animal_private_herd_id AS private_herd_number, 
                          a.animal_sex AS sex, 
                          a.animal_sire AS sire, 
                          w.measurement_date,
                          w.weight AS other_weight, 
                          b.birth_date,
                          b.birth_weight, 
                          b.birth_weight_adj,
                          p.pasture_id AS pasture_name,
                          (w.measurement_date - b.birth_date) AS animal_age
                        FROM tbl_animal_data_other_weight w
                            INNER JOIN tbl_animal a on w.animal_registration = a.animal_registration 
                            INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
                            LEFT JOIN tbl_animal_location l ON w.animal_registration = l.animal_registration
                                AND (l.move_in_date <= w.measurement_date
                                    AND (l.move_out_date IS NULL
                                        OR l.move_out_date >= w.measurement_date))
                            LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::VARCHAR
                            INNER JOIN tbl_ownership o ON w.animal_registration = o.animal_registration
                        WHERE o.owner_id = ?
                            AND o.ownership_date <= w.measurement_date
                            AND w.measurement_date >= DATE(?)
                            AND w.measurement_date <= DATE(?)
                            AND (w.measurement_date - b.birth_date) <= 160
                        GROUP BY a.animal_registration, 
                            a.animal_private_herd_id, 
                            a.animal_sex, 
                            a.animal_sire, 
                            w.measurement_date,
                            w.weight,
                            b.birth_date,
                            b.birth_weight, 
                            b.birth_weight_adj,
                            p.pasture_id
                        ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
                $params = array($member_id, $start_date, $end_date);
            break;
        }
	}
	$rs = $PDO->recordSetQuery($sql, $params);
	if($rs) {
		// BUILD PDF
		if($rs->Recordcount() > 0) {
			// set the header
			$pdf->Cell(7,5,"",'',1,'C');
			
			$pdf->SetFont('Arial','B',7);				
			$pdf->Cell(3,5,"",'R',0,'C');
			$pdf->Cell(5, 5, "#", 'LTRB', 0, 'C');
			$pdf->Cell(18,5,"PHN",'LRBT',0,'C');			
			$pdf->Cell(18,5,"Age (Days)",'LRBT',0,'C');	
			$pdf->Cell(18,5,"D.O.B",'LRBT',0,'C');			
			$pdf->Cell(18,5,"Sex",'LRBT',0,'C');			
			$pdf->Cell(25,5,"Measurement Date",'LRBT',0,'C');
			$pdf->Cell(18,5,"Weight",'LRBT',0,'C');	
			$pdf->Cell(18,5,"ADG",'LRBT', 0, 'C');
			$pdf->Cell(18,5,"WDA",'LRBT',0,'C');		
			$pdf->Cell(20,5,"Pasture",'LRBT',0,'C');	
			$pdf->Cell(3,5,"",'L',1,'C');
			
			// Get animal data
			if(!$rs->EOF) {	
				$animal_ADG = "";
				$animal_WDA = "";
				
				$rs->MoveFirst();
				$row = 0;
				while(!$rs->EOF) {
				    $row++;
					// get the sire phn
					$animal_sire 	= $rs->fields['sire'];
					if($PDO->DB_TYPE == "NEW") {
						$sql = "SELECT private_herd_number 
								FROM animal 
								WHERE registration = ?";
						$params = array($animal_sire);
					} else if($PDO->DB_TYPE == "OLD") {
                        $sql = "SELECT animal_private_herd_id AS private_herd_number
								FROM tbl_animal 
								WHERE animal_registration = ?";
                        $params = array($animal_sire);
					}
					$rsSire = $PDO->recordSetQuery($sql, $params);
					if($rsSire) {
						if(!$rsSire->EOF) {
							$rsSire->MoveFirst();
							
							$animal_sire = $rsSire->fields['private_herd_number'];
						}
						$rsSire->Close();
					}
					
					// Calculate ADG and WDA
					$birth_date = strtotime($rs->fields['birth_date']);
					$measurement_date = strtotime($rs->fields['measurement_date']);
		
					// ADG = (Wean Wt. – Birth Wt.) ÷ Number of Days Between Weights
					$datediff = $measurement_date - $birth_date;
					$days_between_weights = floor($datediff/(60*60*24));
					$animal_ADG = ($rs->fields['other_weight'] - $rs->fields['birth_weight']) / $days_between_weights;
					
					// WDA = Final Weight ÷ Calf Age
					$datediff = $measurement_date - $birth_date;
					$calf_age = floor($datediff/(60*60*24));
					$animal_WDA = $rs->fields['other_weight'] / $calf_age;
					
					// Set Font
					$pdf->SetFont('Arial','',7);
					$pdf->Cell(3,5,"",'R',0,'C');
					$pdf->Cell(5, 5, $row, 'LTRB', 0, 'C');
					$pdf->Cell(18,5,$rs->fields['private_herd_number'],'LTRB',0,'C');
					$pdf->Cell(18,5,$rs->fields['animal_age'],'LTRB',0,'C');
					$pdf->Cell(18,5,formatDate($rs->fields['birth_date']),'LTRB',0,'C');
					$pdf->Cell(18,5,formatSex($rs->fields['sex']),'LRBT',0,'C');	
					$pdf->Cell(25,5,formatDate($rs->fields['measurement_date']),'LRBT',0,'C');
					$pdf->Cell(18,5,$rs->fields['other_weight'],'LTRB',0,'C');
					$pdf->Cell(18,5,round($animal_ADG, 2),'LTRB',0,'C');
					$pdf->Cell(18,5,round($animal_WDA, 2),'LTRB',0,'C');
					$pdf->Cell(20,5,$rs->fields['pasture_name'],'LTRB',0,'C');
					$pdf->Cell(3,5,"",'L',1,'C');
					
					$rs->MoveNext();
				}
			}
			$rs->Close();
		}
	} else {
		$response["success"] = false;
		$response["message"] = "sql error";
		die(json_encode($response));
	}
			
	// Now get totals and averages.
	// First set up header table
	// Page Break
	$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);

    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(6,5,"",'R',0,'C');
    $pdf->Cell(5, 5, "", 'LTRB', 0, 'C');
    $pdf->Cell(85,5,"Sire",'LRBT',0,'C');
    $pdf->Cell(45,5,"Daughters",'LRBT',0,'C');
    $pdf->Cell(45,5,"Sons",'LRBT',1,'C');

    $pdf->Cell(6,5,"",'R',0,'C');
    $pdf->Cell(5, 5, "#", 'LTRB', 0, 'C');
    $pdf->Cell(25,5,"PHN",'LRBT',0,'C');
    $pdf->Cell(60,5,"Name",'LRBT',0,'C');
    $pdf->Cell(15,5,"Count",'LRBT',0,'C');
    $pdf->Cell(15,5,"Avg Wt",'LRBT',0,'C');
    $pdf->Cell(15,5,"Avg Adj Wt",'LRBT',0,'C');
    $pdf->Cell(15,5,"Count",'LRBT',0,'C');
    $pdf->Cell(15,5,"Avg Wt",'LRBT',0,'C');
    $pdf->Cell(15,5,"Avg Adj Wt",'LRBT',0,'C');
    $pdf->Cell(6,5,"",'L',1,'C');
	
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT a.sire,
                    a.animal_name
				FROM animal_wean w
					INNER JOIN animal a on w.registration = a.registration 
					INNER JOIN ownership o ON w.registration = o.registration
				WHERE o.member_id = ? 
					AND o.date_owned <= w.wean_date
					AND w.wean_date >= DATE(?)
					AND w.wean_date <= DATE(?)
				GROUP BY a.sire,
				    a.animal_name";
		$params = array($member_id, $start_date, $end_date);
	} else if($PDO->DB_TYPE == "OLD") {
		$sql = "SELECT a.animal_sire AS sire,
                    a.animal_name
				FROM tbl_animal_data_wean w
					INNER JOIN tbl_animal a on w.animal_registration = a.animal_registration 
					INNER JOIN tbl_ownership o ON w.animal_registration = o.animal_registration
				WHERE o.owner_id = ?
					AND o.ownership_date <= w.wean_date  
					AND w.wean_date >= DATE(?)
					AND w.wean_date <= DATE(?)
				GROUP BY a.animal_sire
				    a.animal_name";
		$params = array($member_id, $start_date, $end_date);
	}
	
	$rsSireGroup = $PDO->recordSetQuery($sql, $params);
	if($rsSireGroup) {
		if(!$rsSireGroup->EOF) {
			$rsSireGroup->MoveFirst();
            $animal_sire = "";
            $total_cows = 0;
            $female_wean_weight = 0;
            $female_wean_weight_adj = 0;
            $total_males = 0;
            $male_wean_weight = 0;
            $male_wean_weight_adj = 0;
			$row = 0;
			while(!$rsSireGroup->EOF) {
			    $row++;
				// get the sire phn
				$animal_sire 	= $rsSireGroup->fields['sire'];
				$sire_name      = $rsSireGroup->fields['animal_name'];
				if($PDO->DB_TYPE == "NEW") {
					$sql = "SELECT private_herd_number 
							FROM animal 
							WHERE registration = ?";
					$params = array($animal_sire);
				} else if($PDO->DB_TYPE == "OLD") {
				    switch($provider) {
                        case "ACRS_CATTLE":
                        case "AMARS_CATTLE":
                            $sql = "SELECT
                                        CASE 
                                            WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le
                                            WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re
                                            ELSE ''
                                        END AS private_herd_number
                                    FROM tbl_animal 
                                    WHERE animal_registration = ?";
                            $params = array($animal_sire);
                        break;
                        default:
                            $sql = "SELECT animal_private_herd_id AS private_herd_number
                                    FROM tbl_animal 
                                    WHERE animal_registration = ?";
                            $params = array($animal_sire);
                        break;
                    }
				}
				$rsSire = $PDO->recordSetQuery($sql, $params);
				if($rsSire) {
					if(!$rsSire->EOF) {
						$rsSire->MoveFirst();
						
						$animal_sire = $rsSire->fields['private_herd_number'];
					}
					$rsSire->Close();
				}

                $animal_sire = $rsSireGroup->fields['sire'];
				// females: avg wean weight and adjusted wean weight
				if($PDO->DB_TYPE == "NEW") {
					$sql = "SELECT w.weight AS wean_weight, 
								w.weight_adj AS wean_weight_adj
							FROM animal_wean w
								INNER JOIN animal a on w.registration = a.registration
								INNER JOIN ownership o ON w.registration = o.registration 
							WHERE o.member_id = ? 
								AND o.date_owned <= w.wean_date
								AND w.wean_date >= DATE(?)
								AND w.wean_date <= DATE(?)
								AND a.sire = ?
								AND a.sex IN ('C')";
					$params = array($member_id, $start, $end_date, $animal_sire);
				} else if($PDO->DB_TYPE == "OLD") {
					$sql = "SELECT w.wean_weight, 
								w.wean_weight_adj
							FROM tbl_animal_data_wean w
								INNER JOIN tbl_animal a on w.animal_registration = a.animal_registration 
							INNER JOIN tbl_ownership o ON w.animal_registration = o.animal_registration
							WHERE o.owner_id = ?
								AND o.ownership_date <= w.wean_date 
								AND w.wean_date >= DATE(?)
								AND w.wean_date <= DATE(?)
								AND a.animal_sire = ?
								AND a.animal_sex IN ('C')";
					$params = array($member_id, $start_date, $end_date, $animal_sire);
				}
				$rsData = $PDO->recordSetQuery($sql, $params);
				if($rsData) {
					$female_wean_weight 	= 0;
					$female_wean_weight_adj = 0;
					
					if(!$rsData->EOF) {
						$total_cows = $rsData->Recordcount();

						$rsData->MoveFirst();
						while(!$rsData->EOF) {
							$female_wean_weight 		= $female_wean_weight + $rsData->fields['wean_weight'];
							$female_wean_weight_adj 	= $female_wean_weight_adj + $rsData->fields['wean_weight_adj'];
							
							$rsData->MoveNext();
						}
						
						$female_wean_weight 		= $female_wean_weight / $total_cows;
						$female_wean_weight_adj 	= $female_wean_weight_adj / $total_cows;
						
						$rsData->Close();
					} else {
						$total_cows				= 0;
						$female_wean_weight 	= 0;
						$female_wean_weight_adj = 0;
					}

				}

                $animal_sire = $rsSireGroup->fields['sire'];
				
				// males: avg wean weight and adjusted wean weight
				if($PDO->DB_TYPE == "NEW") {
					$sql = "SELECT w.weight AS wean_weight, 
								w.weight_adj AS wean_weight_adj
							FROM animal_wean w
								INNER JOIN animal a on w.registration = a.registration
								INNER JOIN ownership o ON w.registration = o.registration 
							WHERE o.member_id = ? 
								AND o.date_owned <= w.wean_date
								AND w.wean_date >= DATE(?)
								AND w.wean_date <= DATE(?)
								AND a.sire = ?
								AND a.sex IN ('B', 'S')";
					$params = array($member_id, $start_date, $end_date, $animal_sire);
				} else if($PDO->DB_TYPE == "OLD") {
					$sql = "SELECT w.wean_weight, 
								w.wean_weight_adj
							FROM tbl_animal_data_wean w
								INNER JOIN tbl_animal a on w.animal_registration = a.animal_registration 
								INNER JOIN tbl_ownership o ON w.animal_registration = o.animal_registration
							WHERE o.owner_id = ?
								AND o.ownership_date <= w.wean_date
								AND w.wean_date >= DATE(?)
								AND w.wean_date <= DATE(?)
								AND a.animal_sire = ?
								AND a.animal_sex IN ('B', 'S')";
					$params = array($member_id, $start_date, $end_date, $animal_sire);
				}
				$rsData = $PDO->recordSetQuery($sql, $params);
				if($rsData) {
					$male_wean_weight 	= 0;
					$male_wean_weight_adj = 0;
					
					if(!$rsData->EOF) {
						$total_males = $rsData->Recordcount();
						
						$rsData->MoveFirst();
						while(!$rsData->EOF) {
							$male_wean_weight 		= $male_wean_weight + $rsData->fields['wean_weight'];
							$male_wean_weight_adj 	= $male_wean_weight_adj + $rsData->fields['wean_weight_adj'];
							
							$rsData->MoveNext();
						}
						
						$male_wean_weight 		= $male_wean_weight / $total_males;
						$male_wean_weight_adj 	= $male_wean_weight_adj / $total_males;
						
						$rsData->Close();
					} else {
						$total_males 			= 0;
						$male_wean_weight 		= 0;
						$male_wean_weight_adj 	= 0;
					}
				}
			
				// If the sire PHN is not blank, then print it
				if($animal_sire != "") {
                    $pdf->SetFont('Arial','',7);
                    $pdf->Cell(6,5,"",'R',0,'L');
                    $pdf->Cell(5, 5, $row, 'LTRB', 0, 'C');
                    $pdf->Cell(25,5,$animal_sire,'LRBT',0,'L');
                    $pdf->Cell(60,5,$sire_name,'LRBT',0,'L');
                    $pdf->Cell(15,5,$total_cows,'LRBT',0,'C');
                    $pdf->Cell(15,5,($total_cows>0 ? round($female_wean_weight, 0) : ""),'LRBT',0,'C');
                    $pdf->Cell(15,5,($total_cows>0 && $female_wean_weight_adj>0 ? round($female_wean_weight_adj, 0) : ""),'LRBT',0,'C');
                    $pdf->Cell(15,5,$total_males,'LRBT',0,'C');
                    $pdf->Cell(15,5,($total_males>0 ? round($male_wean_weight, 0) : ""),'LRBT',0,'C');
                    $pdf->Cell(15,5,($total_males>0 && $male_wean_weight_adj>0 ? round($male_wean_weight_adj, 0) : ""),'LRBT',0,'C');
                    $pdf->Cell(6,5,"",'L',1,'C');
				}
				
				$rsSireGroup->MoveNext();
			}
		}
	}
				
	// create export directory, name, and url
	$exportDir = getExportDirectory($provider, $member_id);
	$timestamp = time();
	
	$exportName = "weaning_summary_app_".$member_id."_".$start_date."_to_".$end_date.".pdf";
	$exportDir = $exportDir . "weaning_summary_app_".$member_id."_".$start_date."_to_".$end_date.".pdf";
	
	if($PDO->DB_TYPE == "NEW") {
		// for the new databases we have to look in the members folder for the 
		// correct pdf directory
		$exportUrl 	= $PDO->FILE_DIR . $member_id . "/" . $exportName;
	} else if($PDO->DB_TYPE == "OLD") {
		$exportUrl 	= $PDO->FILE_DIR . $exportName;
	}
	
	// export the file
	$pdf->Output($exportDir);
	
	// If file name already exists, then update it!
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT * 
				FROM reports 
				WHERE report_title = ? 
					AND report_file_name = ?
					AND member_id = ?";
		$params = array($report_title, $exportName, $member_id);
	} else if($PDO->DB_TYPE == "OLD") {
		$sql = "SELECT * 
				FROM tbl_member_reports_android 
				WHERE report_type = ? 
					AND report_file_name= ?
					AND create_user = ?";
		$params = array($report_title, $exportName, $member_id);
	}
	$rsCheckDuplicate = $PDO->recordSetQuery($sql, $params);
	if($rsCheckDuplicate) {
		if(!$rsCheckDuplicate->EOF) {
			$rsCheckDuplicate->MoveFirst();
			// Update
			if($PDO->DB_TYPE == "NEW") {
				$sql = "UPDATE reports 
						SET report_date = ?
						WHERE member_id = ?
							AND report_title = ?
							AND report_file_name = ?
							AND report_format = 'P'";
				$params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName);
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "UPDATE tbl_member_reports_android 
						SET create_stamp= ?
						WHERE member_id= ?
							AND report_type= ?
							AND report_file_name= ?
							AND report_format='P'
							AND create_user= ?";
				$params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName, $member_id);
			}
			$PDO->executeQuery($sql, $params);
		} else {
			// INSERT
			if($PDO->DB_TYPE == "NEW") {
				$sql = "INSERT INTO reports 
							(member_id, report_title, report_file_name, report_format, report_date) 
						VALUES 
							(?, ?, ?, ?, ?)";
				$params = array($member_id, $report_title, $exportName, 'P', date('Y-m-d h:i:s'));
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "INSERT INTO tbl_member_reports_android 
							(member_id, report_type, report_file_name, report_format, create_user, 
							create_stamp) 
						VALUES 
							(?, ?, ?, ?, ?, ?)";
				$params = array($member_id, $report_title, $exportName, 'P', $member_id, date('Y-m-d h:i:s'));
			}
			$PDO->executeQuery($sql, $params);
		}
	}

	// send back the file name and the pdf url as the result
	$response["success"] = true;
	$response["pdf_url"] = $exportUrl;	// send back the url
	$response["message"] = "successfully generated pdf";
	echo json_encode($response);
    <?php
        # ------------------------------------------------------------------------------------------------------------ #
        header('Access-Control-Allow-Origin: *');
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        require_once(SITE_ROOT . "/includes/includes.php");
        # ------------------------------------------------------------------------------------------------------------ #

        ## -------------------- CONNECTION SETUP -------------------- ##
        $provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
        $PDO = new Connect($provider);

        ## -------------------- GLOBAL FUNCTIONS -------------------- ##
        function formatSex($sex) {
            switch(strtoupper(trim($sex))) {
                case "C": return "Cow";
                case "B": return "Bull";
                case "S": return "Steer";
                default: return "N/A";
            }
        }

        function formatDate($date) {
            // BMC 02.01.2016
            //	This function will take in a date, most likely formatted
            //	like YYYY-MM-DD and change the format to MM DD, YYYY
            //	as an example, Jan 01, 2016
            // BMC 02.03.2016
            //	If the date comes in as blank or null then this will display
            //	Dec 31, 1969!!! So we need to account for these input and
            //	display the proper output.  For now, we'll display nothing, just
            //	a blank string.

            if(trim($date) == ""
                || trim($date) == "null") {
                return "";
            } else {
                $date = strtotime($date);
                return date("M d, Y", $date);
            }
        }

        function getAdjustedBirthWeight($registration, $birth_date, $birth_weight, $sex) {
            global $PDO;

            // determine adjusted birth weight
            $birthWeightAdj = $birth_weight;	// default

            // get age of dam from birth date
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT (DATE(?) - b.birth_date) AS age_of_dam
                        FROM animal a 
                            INNER JOIN animal_birth b ON a.dam = b.registration
                        WHERE a.registration = ?";
                $params = array($birth_date, $registration);
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT (DATE(?) - b.birth_date) AS age_of_dam
                        FROM tbl_animal a 
                            INNER JOIN tbl_animal_data_birth b ON a.animal_dam = b.animal_registration
                        WHERE a.animal_registration = ?";
                $params = array($birth_date, $registration);
            }
            $rs = $PDO->recordSetQuery($sql, $params);
            if($rs) {
                if(!$rs->EOF) {
                    $rs->MoveFirst();
                    $ageOfDam = $rs->fields['age_of_dam'];
                } else {
                    $ageOfDam = 2900;
                }
                $rs->Close();
            } else {
                $ageOfDam = 2900;
            }

            // checks for potential null values
            if(!is_numeric($ageOfDam)) {
                $ageOfDam = 2900;
            }

            // ratios for bulls and cows
            $C1 = ($sex != "C" ? .01867209 	: .01392181);
            $C2 = ($sex != "C" ? .00000484 	: .00000328);
            $C3 = ($sex != "C" ? .00229253 	: .00177874);
            $A1 = ($sex != "C" ? 1929 		: 2122);
            $A2 = 2900;

            if($ageOfDam < $A1) {
                $birthWeightAdj = $birth_weight + (($C1 * ($A1 - $ageOfDam)) - ($C2 * (($A1 * $A1) - ($ageOfDam * $ageOfDam))));
            } elseif($ageOfDam > $A2) {
                $birthWeightAdj = $birth_weight + ($C3 * ($ageOfDam - $A2));
            } else {
                $birthWeightAdj = $birth_weight;
            }

            return round($birthWeightAdj, 0);
        }

        ## -------------------- GENERATE PDF -------------------- ##
        $response["pdf_url"] = "";

        $MemberName = "";
        $YearDate	= "";
        $CalfCount	= "";

        // specific request types
        // active_year = flag + current year, where "year" is the previous year
        $active_year = (isset($_REQUEST['active_year']) 	? 	$_REQUEST['active_year'] 	: date("Y"));

        // BMC 04.11.2016
        //	Add the animal record status to determine dead or alive calves.
        //	Split the sql up into two groups, where the animal record status is alive, A, or dead, D.
        // Current year, PLUS previous year, PLUS active animals, PLUS non-weaned animals
        if(trim($year) == trim($active_year)) {
            // BMC 01.12.2017
            //  -- the incoming request variables year and active_year are actually exactly the same
            //      so we can make year the previous year and leave active_year as is.
            //  -- we could make the change in the app but it's not necessary as it's a simple tweak right
            //      here.  now they will be different.
            $year -= 1;
            $report_title = "Active Calves " . $year . "-" . $active_year;
        } else {
            $report_title = "Active Calves " . $year . "-" . $active_year;
        }

        // This query gets the actual animal location (current)
        if($PDO->DB_TYPE == "NEW") {
            $sql = "SELECT
                      a.registration,
                      a.private_herd_number,
                      a.dam,
                      a.sire,
                      b.birth_date,
                      b.weight                      AS birth_weight,
                      b.weight_adj                  AS birth_weight_adj,
                      a.sex,
                      p.pasture_name,
                      a.status,
                      a.left_tattoo,
                      a.left_tattoo_location,
                      a.right_tattoo,
                      a.right_tattoo_location,
                      a.animal_name,
                      (CURRENT_DATE - b.birth_date) AS current_age
                    FROM
                      animal_birth b
                      INNER JOIN animal a ON b.registration = a.registration
                      INNER JOIN animal_location l ON b.registration = l.registration
                                                      AND l.move_out IS NULL
                      INNER JOIN ownership o ON b.registration = o.registration
                      INNER JOIN pasture p ON l.pasture_id = p.pasture_id
                                              AND p.member_id = o.member_id
                      LEFT JOIN animal_wean w ON b.registration = w.registration
                    WHERE
                      o.member_id = ?
                      AND b.birth_date >= DATE(?)
                      AND b.birth_date <= DATE(?)
                      AND a.status = ?
                      AND NOT o.superceded
                      AND NOT a.is_deleted
                      AND w.wean_date IS NULL
                    GROUP BY
                      b.birth_date,
                      a.registration,
                      a.private_herd_number,
                      a.dam,
                      a.sire,
                      b.weight,
                      b.weight_adj,
                      a.sex,
                      p.pasture_name,
                      a.status,
                      a.left_tattoo,
                      a.left_tattoo_location,
                      a.right_tattoo,
                      a.right_tattoo_location,
                      a.animal_name
                    ORDER BY
                      p.pasture_name ASC,
                      b.birth_date ASC";
            $params = array($member_id, $year."-01-01", $active_year."-12-31", "0");
        } else if($PDO->DB_TYPE == "OLD") {
            // note, pasture_name_1 and pasture_name_2 are necessary when the birth pasture
            // is stored as either the pk_id or the pasture_name, so we have to just check for both
            // situations with the old database.
            switch($provider) {
                case "ACRS_CATTLE":
                case "AMARS_CATTLE":
                $sql = "SELECT 
                            a.animal_registration         AS registration, 
                            ''                            AS private_herd_number, 
                            a.animal_dam                  AS dam, 
                            a.animal_sire                 AS sire,
                            b.birth_date, 
                            b.birth_weight, 
                            b.birth_weight_adj, 
                            b.animal_sex                  AS sex,
                            p.pasture_id                  AS pasture_name_1,
                            pp.pasture_id                 AS pasture_name_2,
                            a.animal_record_status        AS status,
                            a.tattoo_le                   AS left_tattoo,
                            a.tattoo_re                   AS right_tattoo,
                            a.animal_name,
                            (CURRENT_DATE - b.birth_date) AS current_age
                        FROM 
                            tbl_animal_data_birth b
                            INNER JOIN tbl_animal a ON a.animal_registration = b.animal_registration
                            INNER JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
                            INNER JOIN tbl_animal_location l ON b.animal_registration = l.animal_registration
                                                                AND l.move_out_date IS NULL
                            LEFT JOIN tbl_animal_data_wean w ON w.animal_registration = b.animal_registration
                            LEFT JOIN tbl_pastures p ON p.pk_id::VARCHAR = UPPER(l.pasture_id)
                                                        AND p.member_id = o.owner_id
                            LEFT JOIN tbl_pastures pp ON UPPER(pp.pasture_id) = UPPER(l.pasture_id)
                        WHERE 
                            o.owner_id = ? 
                            AND b.birth_date >= DATE(?) 
                            AND b.birth_date <= DATE(?)
                            AND a.animal_record_status = ?
                            AND NOT o.superceded_flag
                            AND w.wean_date IS NULL
                        GROUP BY 
                            a.animal_registration,
                            a.animal_dam, 
                            a.animal_sire,
                            b.birth_date, 
                            b.birth_weight, 
                            b.birth_weight_adj, 
                            b.animal_sex,
                            p.pasture_id, 
                            pp.pasture_id,
                            a.animal_record_status,
                            a.tattoo_le,
                            a.tattoo_re,
                            a.animal_name
                        ORDER BY
                            p.pasture_id ASC,
                            pp.pasture_id ASC, 
                            b.birth_date ASC";
                $params = array($member_id, $year . "-01-01", $active_year . "-12-31", "A");
                break;
                default:
                    $sql = "SELECT 
                                a.animal_registration         AS registration, 
                                a.animal_private_herd_id      AS private_herd_number, 
                                a.animal_dam                  AS dam, 
                                a.animal_sire                 AS sire,
                                b.birth_date, 
                                b.birth_weight, 
                                b.birth_weight_adj, 
                                b.animal_sex                  AS sex,
                                p.pasture_id                  AS pasture_name_1, 
                                pp.pasture_id                 AS pasture_name_2,
                                a.animal_record_status        AS status,
                                a.animal_name,
                                (CURRENT_DATE - b.birth_date) AS current_age
                            FROM 
                                tbl_animal_data_birth b
                                INNER JOIN tbl_animal a ON a.animal_registration = b.animal_registration
                                INNER JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
                                INNER JOIN tbl_animal_location l ON b.animal_registration = l.animal_registration
                                                                    AND l.move_out_date IS NULL
                                LEFT JOIN tbl_animal_data_wean w ON w.animal_registration = b.animal_registration
                                LEFT JOIN tbl_pastures p ON p.pk_id::VARCHAR = UPPER(l.pasture_id)
                                                            AND p.member_id = o.owner_id
                                LEFT JOIN tbl_pastures pp ON UPPER(pp.pasture_id) = UPPER(l.pasture_id)
                            WHERE 
                                o.owner_id = ? 
                                AND b.birth_date >= DATE(?) 
                                AND b.birth_date <= DATE(?)
                                AND a.animal_record_status = ?
                                AND NOT o.superceded_flag
                                AND w.wean_date IS NULL
                            GROUP BY 
                                a.animal_registration, 
                                a.animal_private_herd_id, 
                                a.animal_dam, 
                                a.animal_sire,
                                b.birth_date, 
                                b.birth_weight, 
                                b.birth_weight_adj, 
                                b.animal_sex,
                                p.pasture_id, 
                                pp.pasture_id,
                                a.animal_record_status,
                                a.animal_name
                            ORDER BY 
                                p.pasture_id ASC,
                                pp.pasture_id ASC,
                                b.birth_date ASC";
                    $params = array($member_id, $year . "-01-01", $active_year . "-12-31", "A");
                    break;
            }
        }

        $rs = $PDO->recordSetQuery($sql, $params);
        if($rs) {
            // BMC 04.11.2016
            //	We're forcing this to pass by adding the '>=' so that we can show dead cows/calves.
            $i=0;
            // ********** CREATE THE PDF ********** //
            $pdf = new PDF();
            //$pdf->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);

            // Build PDF
            $requested_font	= 'Times';
            $pdf->AliasNbPages();
            $pdf->SetTopMargin(10);
            $pdf->AddPage($orientation='L', $size='', $rotation=0, false, false);
            $pdf->ZeroCount();
            $pdf->SetFont('Times','B', 10);

            if(trim($year) == trim($active_year)) {
                $YearDate = " (active) " . $active_year;
            } else {
                $YearDate = " (active) " . $year . "-" . $active_year;
            }

            // Set calf count
            $CalfCount = $rs->Recordcount();


            // Get the member name
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT member_name 
                        FROM member 
                        WHERE member_id = ?";
                $params = array($member_id);
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT member_name 
                        FROM tbl_member 
                        WHERE member_id = ?";
                $params = array($member_id);
            }

            $rsMember = $PDO->recordSetQuery($sql, $params);
            if($rsMember) {
                if(!$rsMember->EOF) {
                    $rsMember->MoveFirst();

                    $MemberName = $rsMember->fields['member_name'];
                } else {
                    $MemberName = $member_id;
                }

                $rsMember->Close();
            } else {
                $MemberName = $member_id;
            }

            // BMC 11.07.2016
            //  -- get the member details
            $memberDetails = "";
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT c.address_line_1,
                            c.address_line_2,
                            c.city,
                            c.state,
                            c.zip_code,
                            c.phone_number
                        FROM member m
                            LEFT JOIN contact c ON m.member_id = c.member_id
                                AND c.is_primary
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line_1'];
                        $address_line_2 = $rsContact->fields['address_line_2'];
                        $city = $rsContact->fields['city'];
                        $state = $rsContact->fields['state'];
                        $zip_code = $rsContact->fields['zip_code'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT a.address_line1,
                            a.address_line2,
                            a.address_city,
                            a.address_state,
                            a.address_postal_code
                        FROM tbl_member m
                            LEFT JOIN tbl_address a ON m.general_correspondance_address = a.address_id
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line1'];
                        $address_line_2 = $rsContact->fields['address_line2'];
                        $city = $rsContact->fields['address_city'];
                        $state = $rsContact->fields['address_state'];
                        $zip_code = $rsContact->fields['address_postal_code'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else {
                $memberDetails = "";
            }

            // Get animal data
            if(!$rs->EOF) {
                // get the pasture name
                $pasture_name = "";
                if ($PDO->DB_TYPE == "NEW") {
                    if (trim(strtoupper($rs->fields['pasture_name'])) == "") {
                        $pasture_name = "N/A";
                    } else {
                        $pasture_name = trim(strtoupper($rs->fields['pasture_name']));
                    }
                } else if ($PDO->DB_TYPE == "OLD") {
                    if (trim(strtoupper($rs->fields['pasture_name_1'])) == ""
                        || trim(strtoupper($rs->fields['pasture_name_1'])) == "NULL"
                        || trim(strtoupper($rs->fields['pasture_name_1'])) == NULL) {
                        if (trim(strtoupper($rs->fields['pasture_name_2'])) == ""
                            || trim(strtoupper($rs->fields['pasture_name_2'])) == "NULL"
                            || trim(strtoupper($rs->fields['pasture_name_2'])) == NULL) {
                            $pasture_name = "N/A";
                        } else {
                            $pasture_name = trim(strtoupper($rs->fields['pasture_name_2']));
                        }
                    } else {
                        $pasture_name = trim(strtoupper($rs->fields['pasture_name_1']));
                    }
                }

                // build the title
                $pdf->Image($_SERVER['DOCUMENT_ROOT'] . '/images/digital_beef_logo_no_text_tm.png', 10, 10, 35);
                $pdf->SetFont('Arial', 'B', 12);
                $pdf->Cell(265, 5, "Digital Beef, LLC", '', 1, 'C');
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(265, 12, $memberDetails, '', 1, 'C');
                $pdf->SetFont('Arial', 'B', 12);
                $pdf->Cell(265, 7.5, $report_title . " by D.O.B", '', 1, 'C');
                $pdf->SetFont('Arial', 'B', 9);
                $pdf->Cell(265, 7.5, "Total Calves: " . $CalfCount, '', 1, 'C');
                $pdf->SetFont('Arial', 'B', 9);
                $pdf->Cell(265, 8, $MemberName . "\n" . $member_id, '', 1, 'C');
                $pdf->Cell(265, 6, "", '', 1, 'C');

                // set the pasture and the date printed
                $pdf->SetFont('Arial', 'B', 7);
                $pdf->Cell(1, 5, "", '', 0, 'C');
                $pdf->Cell(25, 5, "Pasture:", 'LBT', 0, 'C');
                $pdf->SetFont('Arial', '', 7);
                $pdf->Cell(30, 5, $pasture_name, 'RBT', 0, 'L');
                $pdf->Cell(180, 5, "", 'B', 0, 'C');
                $pdf->Cell(35, 5, "Date Printed: " . formatDate(date("m/d/Y")), '', 0, 'R');
                $pdf->Cell(1, 5, "", '', 1, 'C');

                // Set the headers
                $pdf->SetFont('Arial', 'B', 7);
                $pdf->Cell(1, 5, "", '', 0, 'C');
                $pdf->Cell(5, 5, "", 'LRTB', 0, 'C');
                $pdf->Cell(50, 5, "Dam", 'LTRB', 0, 'C');
                $pdf->Cell(50, 5, "Sire", 'LTRB', 0, 'C');
                $pdf->Cell(50, 5, "Calf", 'LTRB', 0, 'C');
                $pdf->Cell(115, 5, "Statistics", 'LTRB', 1, 'C');

                // Set the sub-headers
                $pdf->Cell(1, 5, "", 'R', 0, 'C');
                $pdf->Cell(5, 5, "#", 'LRTB', 0, 'C');
                $pdf->Cell(20, 5, "PHN / Tattoo", 'LRBT', 0, 'C');
                $pdf->Cell(30, 5, "Name", 'LRTB', 0, 'L');
                $pdf->Cell(20, 5, "PHN / Tattoo", 'LTRB', 0, 'C');
                $pdf->Cell(30, 5, "Name", 'LRTB', 0, 'C');
                $pdf->Cell(20, 5, "PHN / Tattoo", 'LRBT', 0, 'C');
                $pdf->Cell(30, 5, "Calf Name", 'LRTB', 0, 'L');
                $pdf->Cell(10, 5, "Sex", 'LRBT', 0, 'C');
                $pdf->Cell(20, 5, "D.O.B", 'LRBT', 0, 'C');
                $pdf->Cell(15, 5, "Age (Days)", 'LRBT', 0, 'C');
                $pdf->Cell(15, 5, "Weight", 'LRBT', 0, 'C');
                $pdf->Cell(15, 5, "Adj. Weight", 'LRBT', 0, 'C');
                $pdf->Cell(40, 5, "Weaning Window", 'LRTB', 0, 'C');
                $pdf->Cell(1, 5, "", 'L', 1, 'C');

                // loop through all the animal records and print out the information
                $rs->MoveFirst();
                $row = 0;
                while (!$rs->EOF) {
                    $row++;
                    $pasture_name_check = $pasture_name;

                    // get the dam phn
                    $animal_dam = $rs->fields['dam'];
                    $dam_name = "";
                    $animal_dam_phn = "";
                    $animal_dam_left_tattoo = "";
                    $animal_dam_right_tattoo = "";
                    if ($PDO->DB_TYPE == "NEW") {
                        $sql = "SELECT 
                                    private_herd_number, 
                                    animal_name,
                                    left_tattoo,
                                    right_tattoo
                                FROM animal 
                                WHERE registration = ?";
                        $params = array($animal_dam);
                    } else if ($PDO->DB_TYPE == "OLD") {
                        switch ($provider) {
                            case "ACRS_CATTLE":
                            case "AMARS_CATTLE":
                                $sql = "SELECT 
                                            '' AS private_herd_number, 
                                            animal_name,
                                            tattoo_le AS left_tattoo,
                                            tattoo_re AS right_tattoo
                                    FROM tbl_animal 
                                    WHERE animal_registration = ?";
                                $params = array($animal_dam);
                                break;
                            default:
                                $sql = "SELECT 
                                            animal_private_herd_id AS private_herd_number, 
                                            animal_name
                                        FROM tbl_animal 
                                        WHERE animal_registration = ?";
                                $params = array($animal_dam);
                                break;
                        }
                    }
                    $rsDam = $PDO->recordSetQuery($sql, $params);
                    if ($rsDam) {
                        if (!$rsDam->EOF) {
                            $rsDam->MoveFirst();

                            $animal_dam_phn = trim($rsDam->fields['private_herd_number']);
                            $dam_name = trim($rsDam->fields['animal_name']);
                            $animal_dam_left_tattoo = trim($rsDam->fields['left_tattoo']);
                            $animal_dam_right_tattoo = trim($rsDam->fields['right_tattoo']);
                        }
                        $rsDam->Close();
                    }

                    // get the sire phn
                    $animal_sire = $rs->fields['sire'];
                    $sire_name = "";
                    $animal_sire_left_tattoo = "";
                    $animal_sire_right_tattoo = "";
                    if ($PDO->DB_TYPE == "NEW") {
                        $sql = "SELECT 
                                    animal_name,
                                    private_herd_number,
                                    left_tattoo,
                                    right_tattoo 
                                FROM 
                                    animal 
                                WHERE 
                                    registration = ?";
                        $params = array($animal_sire);
                    } else if ($PDO->DB_TYPE == "OLD") {
                        switch ($provider) {
                            case "ACRS_CATTLE":
                            case "AMARS_CATTLE":
                                $sql = "SELECT 
                                        animal_name,
                                        '' AS private_herd_number,
                                        tattoo_le AS left_tattoo,
                                        tattoo_re AS right_tattoo
                                    FROM 
                                        tbl_animal 
                                    WHERE 
                                        animal_registration = ?";
                                $params = array($animal_sire);
                                break;
                            default:
                                $sql = "SELECT 
                                            animal_name,
                                            animal_private_herd_id AS private_herd_number
                                        FROM 
                                          tbl_animal 
                                        WHERE 
                                          animal_registration = ?";
                                $params = array($animal_sire);
                                break;
                        }
                    }
                    $rsSire = $PDO->recordSetQuery($sql, $params);
                    if ($rsSire) {
                        if (!$rsSire->EOF) {
                            $rsSire->MoveFirst();

                            $animal_sire_phn = trim($rsSire->fields['private_herd_number']);
                            $sire_name = $rsSire->fields['animal_name'];
                            $animal_sire_left_tattoo = trim($rsSire->fields['left_tattoo']);
                            $animal_sire_right_tattoo = trim($rsSire->fields['right_tattoo']);
                        }
                        $rsSire->Close();
                    }

                    // Re-get the pasture name
                    // We need to re-get the pasture name to properly check the current animal pasture
                    // and set it to the animal properly as well as to help when checking if we need to add a new page
                    $pasture_name = "";
                    if ($PDO->DB_TYPE == "NEW") {
                        if (trim(strtoupper($rs->fields['pasture_name'])) == "") {
                            $pasture_name = "N/A";
                        } else {
                            $pasture_name = trim(strtoupper($rs->fields['pasture_name']));
                        }
                    } else if ($PDO->DB_TYPE == "OLD") {
                        if (trim(strtoupper($rs->fields['pasture_name_1'])) == ""
                            || trim(strtoupper($rs->fields['pasture_name_1'])) == "NULL"
                            || trim(strtoupper($rs->fields['pasture_name_1'])) == NULL) {
                            if (trim(strtoupper($rs->fields['pasture_name_2'])) == ""
                                || trim(strtoupper($rs->fields['pasture_name_2'])) == "NULL"
                                || trim(strtoupper($rs->fields['pasture_name_2'])) == NULL) {
                                $pasture_name = "N/A";
                            } else {
                                $pasture_name = trim(strtoupper($rs->fields['pasture_name_2']));
                            }
                        } else {
                            $pasture_name = trim(strtoupper($rs->fields['pasture_name_1']));
                        }
                    }
                    // Add new page if we enter a new pasture
                    if ($pasture_name !== $pasture_name_check) {
                        $pdf->AddPage($orientation = 'L', $size = '', $rotation = 0, false, false);
                    }

                    // determine dead cells
                    $showDead = "";
                    $fillCell = "";
                    if (strtoupper(trim($rs->fields['status'])) == "D"
                        || strtoupper(trim($rs->fields['status'])) == "2"
                        || strtoupper(trim($rs->fields['status'])) == "4") {
                        $showDead = " - DEAD";
                        $pdf->SetFillColor(200, 200, 200);
                        $fillCell = true;
                    } else {
                        $pdf->SetFillColor(255, 255, 255);
                    }

                    // Get the tattoo and PHN
                    $phn = trim($rs->fields['private_herd_number']);
                    $rightTattoo = trim($rs->fields['right_tattoo']);
                    $leftTattoo = trim($rs->fields['left_tattoo']);

                    // Get the weaning date range
                    $earliest_wean = strtotime($rs->fields['birth_date']);
                    $wean_earliest = "";
                    $wean_latest = "";

                    $wean_earliest = date('Y-m-d', strtotime("+160 day", $earliest_wean));
                    $wean_latest = date('Y-m-d', strtotime("+205 day", $earliest_wean));

                    // Set Font
                    $pdf->Cell(1, 5, "", '', 0, 'C', '', '');

                    // LRM 07.14.2017:
                    // Get the Y location of the page and if it is in the first row after a new page
                    if ($pdf->GetY() == 10) {
                        // set pasture name and the date printed
                        $pdf->SetFont('Arial', 'B', 7);
                        $pdf->Cell(25, 5, "Pasture:", 'LBT', 0, 'C');
                        $pdf->SetFont('Arial', '', 7);
                        $pdf->Cell(30, 5, $pasture_name, 'RBT', 0, 'L');
                        $pdf->Cell(180, 5, "", 'B', 0, 'C');
                        $pdf->Cell(35, 5, "Date Printed: " . formatDate(date("m/d/Y")), '', 0, 'R');
                        $pdf->Cell(1, 5, "", '', 1, 'C');

                        // Set the headers
                        $pdf->SetFont('Arial', 'B', 7);
                        $pdf->Cell(1, 5, "", '', 0, 'C');
                        $pdf->Cell(5, 5, "", 'LRTB', 0, 'C');
                        $pdf->Cell(50, 5, "Dam", 'LTRB', 0, 'C');
                        $pdf->Cell(50, 5, "Sire", 'LTRB', 0, 'C');
                        $pdf->Cell(50, 5, "Calf", 'LTRB', 0, 'C');
                        $pdf->Cell(115, 5, "Statistics", 'LTRB', 1, 'C');
                        //$pdf->Cell(30, 5, "Location", 'LTRB', 1, 'C');

                        // Set the sub-headers
                        $pdf->Cell(1, 5, "", 'R', 0, 'C');
                        $pdf->Cell(5, 5, "#", 'LRTB', 0, 'C');
                        $pdf->Cell(20, 5, "PHN / Tattoo", 'LRBT', 0, 'C');
                        $pdf->Cell(30, 5, "Name", 'LRTB', 0, 'L');
                        $pdf->Cell(20, 5, "PHN / Tattoo", 'LTRB', 0, 'C');
                        $pdf->Cell(30, 5, "Name", 'LRTB', 0, 'C');
                        $pdf->Cell(20, 5, "PHN / Tattoo", 'LRBT', 0, 'C');
                        $pdf->Cell(30, 5, "Calf Name", 'LRTB', 0, 'L');
                        $pdf->Cell(10, 5, "Sex", 'LRBT', 0, 'C');
                        $pdf->Cell(20, 5, "D.O.B", 'LRBT', 0, 'C');
                        $pdf->Cell(15, 5, "Age (Days)", 'LRBT', 0, 'C');
                        $pdf->Cell(15, 5, "Weight", 'LRBT', 0, 'C');
                        $pdf->Cell(15, 5, "Adj. Weight", 'LRBT', 0, 'C');
                        $pdf->Cell(40, 5, "Weaning Window", 'LRTB', 0, 'C');
                        //$pdf->Cell(30, 5, "Current Pasture", 'LRBT', 0, 'C');
                        $pdf->Cell(1, 5, "", 'L', 1, 'C');

                        $pdf->Cell(1, 5, "", 'R', 0, 'C');
                    }

                    $pdf->SetFont('Arial', '', 7);
                    $pdf->Cell(5, 5, $row, 'LRTB', 0, 'C');

                    // LRM 07/12/2017:
                    // Check if the LE or RE tattoo have a value.

                    // Dam Section
                    if ($animal_dam_right_tattoo !== "" || $animal_dam_left_tattoo !== "") {
                        if ($animal_dam_right_tattoo !== "" && $animal_dam_left_tattoo === "") {
                            $pdf->Cell(20, 5, $animal_dam_right_tattoo . "[RE]" . $showDead, 'LRBT', 0, 'L', $fillCell);
                        } elseif ($animal_dam_right_tattoo === "" && $animal_dam_left_tattoo !== "") {
                            $pdf->Cell(20, 5, $animal_dam_left_tattoo . "[LE]" . $showDead, 'LRBT', 0, 'L', $fillCell);
                        } elseif ($animal_dam_right_tattoo !== "" && $animal_dam_left_tattoo !== "") {
                            if ($animal_dam_right_tattoo !== $animal_dam_left_tattoo) {
                                $pdf->Cell(20, 5, $animal_dam_left_tattoo . "[LE]" . $animal_dam_right_tattoo . "[RE]" . $showDead, "LTRB", 0, 'L');
                            } elseif ($animal_dam_right_tattoo === $animal_dam_left_tattoo) {
                                $pdf->Cell(20, 5, $animal_dam_right_tattoo . "[BE]", 'LTRB', 0, 'L');
                            }
                        }
                    } else {
                        $pdf->Cell(20, 5, $animal_dam_phn . "" . $showDead, 'LRBT', 0, 'L', $fillCell);
                    }
                    $pdf->Cell(30, 5, $dam_name, 'LRTB', 0, 'L');

                    // Sire Section
                    if ($animal_sire_right_tattoo !== "" || $animal_sire_left_tattoo !== "") {
                        if ($animal_sire_right_tattoo !== "" && $animal_sire_left_tattoo === "") {
                            $pdf->Cell(20, 5, $animal_sire_right_tattoo . "[RE]" . $showDead, 'LRBT', 0, 'L', $fillCell);
                        } elseif ($animal_sire_right_tattoo === "" && $animal_sire_left_tattoo !== "") {
                            $pdf->Cell(20, 5, $animal_sire_left_tattoo . "[LE]" . $showDead, 'LRBT', 0, 'L', $fillCell);
                        } elseif ($animal_sire_right_tattoo !== "" && $animal_sire_left_tattoo !== "") {
                            if ($animal_sire_right_tattoo !== $animal_sire_left_tattoo) {
                                $pdf->Cell(20, 5, $animal_sire_left_tattoo . "[LE]" . $animal_sire_right_tattoo . "[RE]" . $showDead, "LTRB", 0, 'L');
                            } elseif ($animal_sire_right_tattoo === $animal_sire_left_tattoo) {
                                $pdf->Cell(20, 5, $animal_sire_right_tattoo . "[BE]", 'LTRB', 0, 'L');
                            }
                        }
                    } else {
                        $pdf->Cell(20, 5, $animal_sire_phn . "" . $showDead, 'LRBT', 0, 'L', $fillCell);
                    }
                    $pdf->Cell(30, 5, $sire_name, 'LTRB', 0, 'L', $fillCell);

                    // Calf Section
                    if ($rightTattoo !== "" || $leftTattoo !== "") {
                        if ($rightTattoo !== "" && $leftTattoo === "") {
                            $pdf->Cell(20, 5, $rightTattoo . "[RE]" . $showDead, 'LRBT', 0, 'L', $fillCell);
                        } elseif ($rightTattoo === "" && $leftTattoo !== "") {
                            $pdf->Cell(20, 5, $leftTattoo . "[LE]" . $showDead, 'LRBT', 0, 'L', $fillCell);
                        } elseif ($rightTattoo !== "" && $leftTattoo !== "") {
                            if ($rightTattoo !== $leftTattoo) {
                                $pdf->Cell(20, 5, $leftTattoo . "[LE] " . $rightTattoo . "[RE]" . $showDead, 'LRBT', 0, 'L', $fillCell);
                            } elseif ($rightTattoo === $leftTattoo) {
                                $pdf->Cell(20, 5, $rightTattoo . "[BE]", 'LTRB', 0, 'L');
                            }
                        }
                    } else {
                        $pdf->Cell(20, 5, $phn. "" . $showDead, 'LRBT', 0, 'L', $fillCell);
                    }

                    $pdf->Cell(30, 5, $rs->fields['animal_name'], 'LTRB', 0, 'L');
                    $pdf->Cell(10, 5, $rs->fields['sex'], 'LTRB', 0, 'C', $fillCell);
                    $pdf->Cell(20, 5, formatDate($rs->fields['birth_date']), 'LTRB', 0, 'C', $fillCell);
                    $pdf->Cell(15, 5, $rs->fields['current_age'], 'LTRB', 0, 'C', $fillCell);
                    $pdf->Cell(15, 5, $rs->fields['birth_weight'], 'LTRB', 0, 'C', $fillCell);
                    $pdf->Cell(15, 5, getAdjustedBirthWeight($rs->fields['registration'],
                        $rs->fields['birth_date'],
                        $rs->fields['birth_weight'],
                        $rs->fields['sex']), 'LTRB', 0, 'C', $fillCell);
                    $pdf->Cell(40, 5, formatDate($wean_earliest) . " - " . formatDate($wean_latest), 'LTRB', 0, 'C');
                    $pdf->Cell(1, 5, "", 'L', 1, 'C');

                    $rs->MoveNext();
                }

                // ********** SHOW DEAD COWS ********** //
                // Current year, PLUS previous year, PLUS active animals, PLUS non-weaned animals
                $report_type = $report_type . " (active) " . $year . "-" . $active_year;

                // This query gets the actual animal location (current)
                if($PDO->DB_TYPE == "NEW") {
                    $sql = "SELECT a.registration, 
                                a.private_herd_number, 
                                a.dam, 
                                a.sire,
                                b.birth_date, 
                                b.weight AS birth_weight, 
                                b.weight_adj AS birth_weight_adj, 
                                a.sex,
                                p.pasture_name,
                                a.status
                            FROM animal_birth b
                                INNER JOIN animal a ON b.registration = a.registration
                                INNER JOIN animal_location l ON b.registration = l.registration
                                                                AND l.move_out IS NULL
                                INNER JOIN ownership o ON b.registration = o.registration
                                INNER JOIN pasture p ON l.pasture_id = p.pasture_id
                                                        AND p.member_id = o.member_id
                            WHERE o.member_id = ?
                                AND NOT o.superceded 
                                AND b.birth_date >= DATE(?) 
                                AND b.birth_date <= DATE(?)
                                AND a.status IN (?, ?)
                                AND NOT a.is_deleted
                                AND l.move_out IS NULL
                            GROUP BY a.registration, 
                                a.private_herd_number, 
                                a.dam, 
                                a.sire,
                                b.birth_date, 
                                b.weight, 
                                b.weight_adj, 
                                a.sex,
                                p.pasture_name,
                                a.status
                            ORDER BY b.birth_date ASC";
                    $params = array($member_id, $year."-01-01", $active_year."-12-31", "2", "4");
                } else if($PDO->DB_TYPE == "OLD") {
                    // note, pasture_name_1 and pasture_name_2 are necessary when the birth pasture
                    // is stored as either the pk_id or the pasture_name, so we have to just check for both
                    // situations with the old database.
                    switch($provider) {
                        case "ACRS_CATTLE":
                        case "AMARS_CATTLE":
                            $sql = "SELECT 
                                    CASE 
                                        WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le
                                        WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re
                                        ELSE ''
                                    END AS private_herd_number,
                                    a.animal_registration AS registration, 
                                    a.animal_dam AS dam, 
                                    a.animal_sire AS sire,
                                    b.birth_date, 
                                    b.birth_weight, 
                                    b.birth_weight_adj, 
                                    b.animal_sex AS sex,
                                    p.pasture_id AS pasture_name_1, 
                                    pp.pasture_id AS pasture_name_2,
                                    a.animal_record_status AS status
                                FROM tbl_animal_data_birth b
                                    INNER JOIN tbl_animal a ON a.animal_registration = b.animal_registration
                                    INNER JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
                                    INNER JOIN tbl_animal_location l ON b.animal_registration = l.animal_registration
                                    LEFT JOIN tbl_animal_data_wean w ON w.animal_registration = b.animal_registration
                                    LEFT JOIN tbl_pastures p ON p.pk_id::VARCHAR = UPPER(l.pasture_id)
                                    LEFT JOIN tbl_pastures pp ON UPPER(pp.pasture_id) = UPPER(l.pasture_id)
                                WHERE o.owner_id = ? 
                                    AND b.birth_date >= DATE(?) 
                                    AND b.birth_date <= DATE(?)
                                    AND a.animal_record_status = ?
                                    AND NOT o.superceded_flag
                                    AND w.wean_date IS NULL
                                    AND l.move_out_date IS NULL
                                GROUP BY a.animal_registration, 
                                    a.animal_private_herd_id, 
                                    a.animal_dam, 
                                    a.animal_sire,
                                    b.birth_date, 
                                    b.birth_weight, 
                                    b.birth_weight_adj, 
                                    b.animal_sex,
                                    p.pasture_id, 
                                    pp.pasture_id,
                                    a.animal_record_status,
                                    a.tattoo_le,
                                    a.tattoo_re
                                ORDER BY b.birth_date ASC";
                        break;
                        default:
                            $sql = "SELECT a.animal_registration AS registration, 
                                    a.animal_private_herd_id AS private_herd_number, 
                                    a.animal_dam AS dam, 
                                    a.animal_sire AS sire,
                                    b.birth_date, 
                                    b.birth_weight, 
                                    b.birth_weight_adj, 
                                    b.animal_sex AS sex,
                                    p.pasture_id AS pasture_name_1, 
                                    pp.pasture_id AS pasture_name_2,
                                    a.animal_record_status AS status
                                FROM tbl_animal_data_birth b
                                    INNER JOIN tbl_animal a ON a.animal_registration = b.animal_registration
                                    INNER JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
                                    INNER JOIN tbl_animal_location l ON b.animal_registration = l.animal_registration
                                    LEFT JOIN tbl_animal_data_wean w ON w.animal_registration = b.animal_registration
                                    LEFT JOIN tbl_pastures p ON p.pk_id::VARCHAR = UPPER(l.pasture_id)
                                    LEFT JOIN tbl_pastures pp ON UPPER(pp.pasture_id) = UPPER(l.pasture_id)
                                WHERE o.owner_id = ? 
                                    AND b.birth_date >= DATE(?) 
                                    AND b.birth_date <= DATE(?)
                                    AND a.animal_record_status = ?
                                    AND NOT o.superceded_flag
                                    AND w.wean_date IS NULL
                                    AND l.move_out_date IS NULL
                                GROUP BY a.animal_registration, 
                                    a.animal_private_herd_id, 
                                    a.animal_dam, 
                                    a.animal_sire,
                                    b.birth_date, 
                                    b.birth_weight, 
                                    b.birth_weight_adj, 
                                    b.animal_sex,
                                    p.pasture_id, 
                                    pp.pasture_id,
                                    a.animal_record_status
                                ORDER BY b.birth_date ASC";
                        break;
                    }
                    $params = array($member_id, $year."-01-01", $active_year."-12-31", "D");
                }

                $rsDeadAnimal = $PDO->recordSetQuery($sql, $params);
                if($rsDeadAnimal) {
                    if(!$rsDeadAnimal->EOF) {
                        // start on a new page
                        $pdf->AddPage($orientation='L', $size='', $rotation=0, false, false);

                        // build headers
                        $pdf->SetFont('Arial','B',7);
                        $pdf->Cell(1,5,"",'R',0,'C');
                        $pdf->Cell(30,5,"Total Dead Calves:",'LBT',0,'C');
                        $pdf->SetFont('Arial','',7);
                        $pdf->Cell(15,5,$rsDeadAnimal->RecordCount(),'RBT',0,'C');
                        $pdf->Cell(95 ,5, "", 'B', 0, 'C');
                        $pdf->Cell(35, 5, "Date Printed: " . formatDate(date("m/d/Y")), '' , 0, '');
                        $pdf->Cell(1,5,"",'',1,'C');

                        $pdf->SetFont('Arial','B',7);
                        $pdf->Cell(1,5,"",'R',0,'C');
                        $pdf->Cell(5, 5, "#", 'LRTB', 0, 'C');
                        $pdf->Cell(20,5,"PHN / Tattoo",'LRBT',0,'C');
                        $pdf->Cell(20,5,"Sex",'LRBT',0,'C');
                        $pdf->Cell(20,5,"Dam",'LRBT',0,'C');
                        $pdf->Cell(20,5,"Sire",'LRBT',0,'C');
                        $pdf->Cell(20,5,"D.O.B",'LRBT',0,'C');
                        $pdf->Cell(20,5,"Weight",'LRBT',0,'C');
                        $pdf->Cell(20,5,"Adj. Weight",'LRBT',0,'C');
                        $pdf->Cell(25,5,"Current Pasture",'LRBT',0,'C');
                        $pdf->Cell(1,5,"",'L',1,'C');

                        // loop through all the animal records
                        $rsDeadAnimal->MoveFirst();
                        $row = 0;
                        while(!$rsDeadAnimal->EOF) {
                            $row++;
                            // get the dam phn
                            $animal_dam 	= $rsDeadAnimal->fields['dam'];
                            $animal_dam_left_tattoo     = "";
                            $animal_dam_right_tattoo    = "";
                            if($PDO->DB_TYPE == "NEW") {
                                $sql = "SELECT 
                                            private_herd_number,
                                            left_tattoo,
                                            right_tattoo 
                                        FROM 
                                            animal 
                                        WHERE 
                                            registration = ?";
                                $params = array($animal_dam);
                            } else if($PDO->DB_TYPE == "OLD") {
                                switch($provider) {
                                    case "ACRS_CATTLE":
                                    case "AMARS_CATTLE":
                                        $sql = "SELECT 
                                                    animal_private_herd_id AS private_herd_number,
                                                    tattoo_le AS left_tattoo,
                                                    tattoo_re AS right_tattoo
                                                FROM 
                                                    tbl_animal 
                                                WHERE 
                                                    animal_registration = ?";
                                        $params = array($animal_dam);
                                        break;
                                    default:
                                        $sql = "SELECT 
                                                    animal_private_herd_id AS private_herd_number
                                                FROM 
                                                    tbl_animal 
                                                WHERE 
                                                    animal_registration = ?";
                                        $params = array($animal_dam);
                                        break;
                                }
                            }
                            /** @noinspection PhpUndefinedMethodInspection */
                            $rsDam = $PDO->recordSetQuery($sql, $params);
                            if($rsDam) {
                                if(!$rsDam->EOF) {
                                    $rsDam->MoveFirst();

                                    $animal_dam                 = $rsDam->fields['private_herd_number'];
                                    $animal_dam_left_tattoo     = $rsDam->fields['left_tattoo'];
                                    $animal_dam_right_tattoo    = $rsDam->fields['right_tattoo'];
                                }
                                $rsDam->Close();
                            }

                            // get the sire phn
                            $animal_sire                = $rs->fields['sire'];
                            $sire_name                  = "";
                            $animal_sire_left_tattoo    = "";
                            $animal_sire_right_tattoo   = "";
                            if($PDO->DB_TYPE == "NEW") {
                                $sql = "SELECT 
                                            private_herd_number,
                                            left_tattoo,
                                            right_tattoo 
                                        FROM 
                                            animal 
                                        WHERE 
                                            registration = ?";
                                $params = array($animal_sire);
                            } else if($PDO->DB_TYPE == "OLD") {
                                switch($provider) {
                                    case "ACRS_CATTLE":
                                    case "AMARS_CATTLE":
                                        $sql = "SELECT 
                                                    animal_private_herd_id AS private_herd_number,
                                                    tattoo_le AS left_tattoo,
                                                    tattoo_re AS right_tattoo
                                                FROM 
                                                    tbl_animal 
                                                WHERE 
                                                    animal_registration = ?";
                                            $params = array($animal_sire);
                                            break;
                                    default:
                                        $sql = "SELECT 
                                                    animal_private_herd_id AS private_herd_number
                                                FROM 
                                                    tbl_animal 
                                                WHERE 
                                                    animal_registration = ?";
                                    $params = array($animal_sire);
                                    break;
                                }
                            }
                            $rsSire = $PDO->recordSetQuery($sql, $params);
                            if($rsSire) {
                                if(!$rsSire->EOF) {
                                    $rsSire->MoveFirst();

                                    $animal_sire_phn            = $rsSire->fields['private_herd_number'];
                                    $animal_sire_left_tattoo    = $rsSire->fields['left_tattoo'];
                                    $animal_sire_right_tattoo   = $rsSire->fields['right_tattoo'];
                                }
                                $rsSire->Close();
                            }

                            // get the pasture name
                            $pasture_name = "";
                            if($PDO->DB_TYPE == "NEW") {
                                if(trim(strtoupper($rsDeadAnimal->fields['pasture_name'])) == "") {
                                    $pasture_name = "N/A";
                                } else {
                                    $pasture_name = trim(strtoupper($rsDeadAnimal->fields['pasture_name']));
                                }
                            } else if($PDO->DB_TYPE == "OLD") {
                                if(trim(strtoupper($rsDeadAnimal->fields['pasture_name_1'])) == ""
                                    || trim(strtoupper($rsDeadAnimal->fields['pasture_name_1'])) == "NULL"
                                    || trim(strtoupper($rsDeadAnimal->fields['pasture_name_1'])) == NULL) {
                                    if(trim(strtoupper($rsDeadAnimal->fields['pasture_name_2'])) == ""
                                        || trim(strtoupper($rsDeadAnimal->fields['pasture_name_2'])) == "NULL"
                                        || trim(strtoupper($rsDeadAnimal->fields['pasture_name_2'])) == NULL) {
                                        $pasture_name = "N/A";
                                    } else {
                                        $pasture_name = trim(strtoupper($rsDeadAnimal->fields['pasture_name_2']));
                                    }
                                } else {
                                    $pasture_name = trim(strtoupper($rsDeadAnimal->fields['pasture_name_1']));
                                }
                            }

                            // DEAD ANIMALS
                            // determine dead cells
                            $showDead = "";
                            $fillCell = "";
                            if(strtoupper(trim($rsDeadAnimal->fields['status'])) == "D"
                                || strtoupper(trim($rsDeadAnimal->fields['status'])) == 2
                                || strtoupper(trim($rsDeadAnimal->fields['status'])) == 4) {
                                $showDead = " - DEAD";
                                $pdf->SetFillColor(200,200,200);
                                $fillCell = true;
                            } else {
                                $pdf->SetFillColor(255,255,255);
                            }

                            // Set Font
                            $pdf->SetFont('Arial','',7);
                            $pdf->Cell(1,5,"",'R',0,'C');
                            $pdf->Cell(5, 5, $row, 'LTRB', 0, 'C');
                            $pdf->Cell(20,5,$rsDeadAnimal->fields['private_herd_number'].$showDead,'LTRB',0,'C', $fillCell);
                            $pdf->Cell(20,5,formatSex($rsDeadAnimal->fields['sex']), 'LTRB', 0,'C', $fillCell);
                            $pdf->Cell(20,5,$animal_dam,'LTRB',0,'C', $fillCell);
                            $pdf->Cell(20,5,$animal_sire,'LTRB',0,'C', $fillCell);
                            $pdf->Cell(20,5,formatDate($rsDeadAnimal->fields['birth_date']),'LTRB',0,'C', $fillCell);
                            $pdf->Cell(20,5,$rsDeadAnimal->fields['birth_weight'],'LTRB',0,'C', $fillCell);
                            $pdf->Cell(20,5,getAdjustedBirthWeight($rsDeadAnimal->fields['registration'],
                                $rsDeadAnimal->fields['birth_date'],
                                $rsDeadAnimal->fields['birth_weight'],
                                $rsDeadAnimal->fields['sex']),'LTRB',0,'C', $fillCell);
                            $pdf->Cell(25,5,$pasture_name,'LTRB',0,'C', $fillCell);
                            $pdf->Cell(1,5,"",'L',1,'C');

                            $rsDeadAnimal->MoveNext();
                        }
                    }
                }

                // ********** GET SIRE STATS ********** //
                if($active_year == "") {
                    if($PDO->DB_TYPE == "NEW") {
                        $sql = "SELECT a.sire
                                FROM animal_birth b
                                    INNER JOIN animal a on b.registration = a.registration
                                    INNER JOIN ownership o ON a.registration = o.registration
                                WHERE o.member_id = ?
                                    AND NOT o.superceded 
                                    AND b.birth_date >= DATE(?) 
                                    AND b.birth_date <= DATE(?)
                                GROUP BY a.sire";
                        $params = array($member_id, date($year."-01-01"), date($year."-12-31"));
                    } else if($PDO->DB_TYPE == "OLD") {
                        $sql = "SELECT a.animal_sire AS sire
                                FROM tbl_animal_data_birth b
                                    INNER JOIN tbl_animal a on b.animal_registration = a.animal_registration 
                                WHERE b.herd_id = ? 
                                    AND b.birth_date >= DATE(?) 
                                    AND b.birth_date <= DATE(?)
                                GROUP BY a.animal_sire";
                        $params = array($member_id, date($year."-01-01"), date($year."-12-31"));
                    }
                } else {
                    if($PDO->DB_TYPE == "NEW") {
                        $sql = "SELECT a.sire
                                FROM animal a
                                    INNER JOIN animal_birth b ON a.registration = b.registration 
                                    INNER JOIN ownership o ON a.registration = o.registration
                                    LEFT JOIN animal_wean w ON a.registration = w.registration
                                WHERE o.member_id = ?
                                    AND NOT o.superceded
                                    AND b.birth_date >= DATE(?) 
                                    AND b.birth_date <= DATE(?)
                                    AND a.status = ?
                                    AND NOT a.is_deleted
                                    AND w.wean_date IS NULL
                                GROUP BY a.sire";
                        $params = array($member_id, date($year."-01-01"), date($active_year."-12-31"), "0");
                    } else if($PDO->DB_TYPE == "OLD") {
                        $sql = "SELECT a.animal_sire AS sire
                                FROM tbl_animal a
                                    INNER JOIN tbl_animal_data_birth b ON a.animal_registration = b.animal_registration 
                                    INNER JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
                                    LEFT JOIN tbl_animal_data_wean w ON a.animal_registration = w.animal_registration
                                WHERE o.owner_id = ?
                                    AND b.birth_date >= DATE(?) 
                                    AND b.birth_date <= DATE(?)
                                    AND a.animal_record_status = ?
                                    AND NOT o.superceded_flag
                                    AND w.wean_date IS NULL
                                GROUP BY a.animal_sire";
                        $params = array($member_id, date($year."-01-01"), date($active_year."-12-31"), "A");
                    }
                }

                $rsSireGroup = $PDO->recordSetQuery($sql, $params);
                if($rsSireGroup) {
                    if(!$rsSireGroup->EOF) {
                        // Now get totals and averages.
                        // start on a new page
                        $pdf->AddPage($orientation='L', $size='', $rotation=0, false, false);

                        // Set Font
                        $pdf->Cell(1,5,"",'',0,'C', '', '');
                        $pdf->SetFont('Arial', '', 7);
                        $pdf->Cell(145, 5, "", '', 0, 'C');
                        $pdf->Cell(35, 5, "Date Printed: " . formatDate(date("m/d/Y")), '', 1, 'R');

                        // Set the header
                        $pdf->SetFont('Arial','B',7);
                        $pdf->Cell(2, 5, "", '', 0 , 'C');
                        $pdf->Cell(5,5,"",'LTRB',0,'C');
                        $pdf->Cell(25,5,"Sire",'LRBT',0,'C');
                        $pdf->Cell(75,5,"Daughter",'LRBT',0,'C');
                        $pdf->Cell(75,5,"Son",'LRBT',0,'C');
                        $pdf->Cell(1,5,"",'L',1,'C');

                        // Set the sub-headers
                        $pdf->Cell(2,5,"",'R',0,'C');
                        $pdf->Cell(5,5,"#",'LTRB',0,'C');
                        $pdf->Cell(25,5,"PHN / Tattoo",'LRBT',0,'C');
                        $pdf->Cell(25,5,"Female Count",'LRBT',0,'C');
                        $pdf->Cell(25,5,"Wgt Avg (Female)",'LRBT',0,'C');
                        $pdf->Cell(25,5,"Wgt Adj Avg (Female)",'LRBT',0,'C');
                        $pdf->Cell(25,5,"Male Count",'LRBT',0,'C');
                        $pdf->Cell(25,5,"Wgt Avg (Male)",'LRBT',0,'C');
                        $pdf->Cell(25,5,"Wgt Adj Avg (Male)",'LRBT',0,'C');
                        $pdf->Cell(1,5,"",'L',1,'C');

                        $rsSireGroup->MoveFirst();
                        $animal_sire = "";
                        $row = 0;
                        while(!$rsSireGroup->EOF) {
                            // get the sire phn
                            $animal_sire 	= $rsSireGroup->fields['sire'];
                            if($PDO->DB_TYPE == "NEW") {
                                $sql = "SELECT private_herd_number 
                                        FROM animal 
                                        WHERE registration = ?";
                                $params = array($animal_sire);
                            } else if($PDO->DB_TYPE == "OLD") {
                                $sql = "SELECT animal_private_herd_id AS private_herd_number
                                        FROM tbl_animal 
                                        WHERE animal_registration = ?";
                                $params = array($animal_sire);
                            }
                            $rsSire = $PDO->recordSetQuery($sql, $params);
                            if($rsSire) {
                                if(!$rsSire->EOF) {
                                    $rsSire->MoveFirst();

                                    $animal_sire = $rsSire->fields['private_herd_number'];
                                }
                                $rsSire->Close();
                            }

                            // GET AVG BirthWeight and BirthWeightAdj
                            // BMC 04.11.2016
                            //	Only get stats on active animals, exclude dead
                            // BMC 04.13.2016
                            //	Get's the averages split by sex

                            // get female
                            if($active_year == "") {
                                if($PDO->DB_TYPE == "NEW") {
                                    $sql = "SELECT b.weight AS birth_weight, 
                                                b.weight_adj AS birth_weight_adj
                                            FROM animal_birth b
                                                INNER JOIN animal a on b.registration = a.registration 
                                                INNER JOIN ownership o ON a.registration = o.registration
                                            WHERE o.member_id = ?
                                                AND NOT o.superceded 
                                                AND b.birth_date >= DATE(?) 
                                                AND b.birth_date <= DATE(?)
                                                AND a.sire = ?
                                                AND a.status = ?
                                                AND a.sex = ?";
                                    $params = array($member_id, date($year."-01-01"), date($year."-12-31"), $rsSireGroup->fields['sire'], "0", "C");
                                } else if($PDO->DB_TYPE == "OLD") {
                                    $sql = "SELECT b.birth_weight, 
                                                b.birth_weight_adj
                                            FROM tbl_animal_data_birth b
                                                INNER JOIN tbl_animal a on b.animal_registration = a.animal_registration 
                                            WHERE b.herd_id = ?
                                                AND b.birth_date >= DATE(?) 
                                                AND b.birth_date <= DATE(?)
                                                AND a.animal_sire = ?
                                                AND a.animal_record_status = ?
                                                AND a.animal_sex = ?";
                                    $params = array($member_id, date($year."-01-01"), date($year."-12-31"), $rsSireGroup->fields['sire'], "A", "C");
                                }
                            } else {
                                if($PDO->DB_TYPE == "NEW") {
                                    $sql = "SELECT b.weight AS birth_weight, 
                                                b.weight_adj AS birth_weight_adj
                                            FROM animal_birth b
                                                INNER JOIN animal a on b.registration = a.registration 
                                                INNER JOIN ownership o ON a.registration = o.registration
                                                LEFT JOIN animal_wean w ON a.registration = w.registration
                                            WHERE o.member_id = ?
                                                AND NOT o.superceded
                                                AND b.birth_date >= DATE(?) 
                                                AND b.birth_date <= DATE(?)
                                                AND w.wean_date IS NULL
                                                AND a.sire = ?
                                                AND a.status = ?
                                                AND a.sex = ?";
                                    $params = array($member_id, date($year."-01-01"), date($active_year."-12-31"), $rsSireGroup->fields['sire'], "0", "C");
                                } else if($PDO->DB_TYPE == "OLD") {
                                    $sql = "SELECT b.birth_weight, 
                                                b.birth_weight_adj
                                            FROM tbl_animal_data_birth b
                                                INNER JOIN tbl_animal a on b.animal_registration = a.animal_registration 
                                                INNER JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
                                                LEFT JOIN tbl_animal_data_wean w ON a.animal_registration = w.animal_registration
                                            WHERE o.owner_id = ? 
                                                AND b.birth_date >= DATE(?) 
                                                AND b.birth_date <= DATE(?)
                                                AND NOT o.superceded_flag
                                                AND w.wean_date IS NULL
                                                AND a.animal_sire = ?
                                                AND a.animal_record_status = ?
                                                AND a.animal_sex = ?";
                                    $params = array($member_id, date($year."-01-01"), date($active_year."-12-31"), $rsSireGroup->fields['sire'], "A", "C");
                                }
                            }
                            $rsData = $PDO->recordSetQuery($sql, $params);
                            if($rsData) {
                                $total_animals		= 0;
                                $birth_weight 		= 0;
                                $birth_weight_adj 	= 0;

                                if(!$rsData->EOF) {
                                    $total_animals = $rsData->Recordcount();

                                    $rsData->MoveFirst();
                                    while(!$rsData->EOF) {
                                        $birth_weight 		= $birth_weight + $rsData->fields['birth_weight'];
                                        $birth_weight_adj 	= $birth_weight_adj + $rsData->fields['birth_weight_adj'];

                                        $rsData->MoveNext();
                                    }

                                    $birth_weight 		= $birth_weight / $total_animals;
                                    $birth_weight_adj 	= $birth_weight_adj / $total_animals;

                                    $rsData->Close();
                                } else {
                                    $total_animals		= 0;
                                    $birth_weight 		= 0;
                                    $birth_weight_adj 	= 0;
                                }
                            }
                            $total_cows 		= $total_animals;
                            $fem_bweight 		= $birth_weight;
                            $fem_bweight_adj 	= $birth_weight_adj;

                            // get male
                            if($active_year == "") {
                                if($PDO->DB_TYPE == "NEW") {
                                    $sql = "SELECT b.weight AS birth_weight, 
                                                b.weight_adj AS birth_weight_adj
                                            FROM animal_birth b
                                                INNER JOIN animal a on b.registration = a.registration 
                                                INNER JOIN ownership o ON a.registration = o.registration
                                            WHERE o.member_id = ?
                                                AND NOT o.superceded 
                                                AND b.birth_date >= DATE(?) 
                                                AND b.birth_date <= DATE(?)
                                                AND a.sire = ?
                                                AND a.status = ?
                                                AND a.sex IN (?, ?)
                                                AND b.weight > ?";
                                    $params = array($member_id, date($year."-01-01"), date($year."-12-31"), $rsSireGroup->fields['sire'], "0", "B", "S", "0");
                                } else if($PDO->DB_TYPE == "OLD") {
                                    $sql = "SELECT b.birth_weight, 
                                                b.birth_weight_adj
                                            FROM tbl_animal_data_birth b
                                                INNER JOIN tbl_animal a on b.animal_registration = a.animal_registration 
                                            WHERE o.owner_id = ?
                                                AND NOT o.superceded_flag
                                                AND b.birth_date >= DATE(?) 
                                                AND b.birth_date <= DATE(?)
                                                AND w.wean_date IS NULL
                                                AND a.animal_sire = ?
                                                AND a.animal_record_status = ?
                                                AND a.animal_sex IN (?, ?)
                                                AND b.birth_weight > ?";
                                    $params = array($member_id, date($year."-01-01"), date($year."-12-31"), $rsSireGroup->fields['sire'], "A", "B", "S", "0");
                                }
                            } else {
                                if($PDO->DB_TYPE == "NEW") {
                                    $sql = "SELECT b.weight AS birth_weight, 
                                                b.weight_adj AS birth_weight_adj
                                            FROM animal_birth b
                                                INNER JOIN animal a on b.registration = a.registration 
                                                INNER JOIN ownership o ON a.registration = o.registration
                                                LEFT JOIN animal_wean w ON a.registration = w.registration
                                            WHERE o.member_id = ? 
                                                AND NOT o.superceded
                                                AND b.birth_date >= DATE(?) 
                                                AND b.birth_date <= DATE(?)
                                                AND w.wean_date IS NULL
                                                AND a.sire = ?
                                                AND a.status = ?
                                                AND a.sex IN (?, ?)
                                                AND b.weight > ?";
                                    $params = array($member_id, date($year."-01-01"), date($active_year."-12-31"), $rsSireGroup->fields['sire'], "0", "B", "S", "0");
                                } else if($PDO->DB_TYPE == "OLD") {
                                    $sql = "SELECT b.birth_weight, 
                                                b.birth_weight_adj
                                            FROM tbl_animal_data_birth b
                                                INNER JOIN tbl_animal a on b.animal_registration = a.animal_registration 
                                                INNER JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
                                                LEFT JOIN tbl_animal_data_wean w ON a.animal_registration = w.animal_registration
                                            WHERE o.owner_id = ?
                                                AND NOT o.superceded_flag
                                                AND b.birth_date >= DATE(?) 
                                                AND b.birth_date <= DATE(?)
                                                AND w.wean_date IS NULL
                                                AND a.animal_sire = ?
                                                AND a.animal_record_status = ?
                                                AND a.animal_sex IN (?, ?)
                                                AND b.birth_weight > ?";
                                    $params = array($member_id, date($year."-01-01"), date($active_year."-12-31"), $rsSireGroup->fields['sire'], "A", "B", "S", "0");
                                }
                            }
                            $rsData = $PDO->recordSetQuery($sql, $params);
                            if($rsData) {
                                $total_animals		= 0;
                                $birth_weight 		= 0;
                                $birth_weight_adj 	= 0;

                                if(!$rsData->EOF) {
                                    $total_animals = $rsData->Recordcount();

                                    $rsData->MoveFirst();
                                    while(!$rsData->EOF) {
                                        $birth_weight 		= $birth_weight + $rsData->fields['birth_weight'];
                                        $birth_weight_adj 	= $birth_weight_adj + $rsData->fields['birth_weight_adj'];

                                        $rsData->MoveNext();
                                    }

                                    $birth_weight 		= $birth_weight / $total_animals;
                                    $birth_weight_adj 	= $birth_weight_adj / $total_animals;

                                    $rsData->Close();
                                } else {
                                    $total_animals		= 0;
                                    $birth_weight 		= 0;
                                    $birth_weight_adj 	= 0;
                                }
                            }

                            $total_bulls 		= $total_animals;
                            $male_bweight 		= $birth_weight;
                            $male_bweight_adj 	= $birth_weight_adj;

                            if($animal_sire != "") {
                            // Set Font
                            $pdf->Cell(1,5,"",'',0,'C', '', '');

                            // LRM 07.14.2017:
                            // Get the Y location of the page and if it is in the first row after a new page
                            // Set these headers before moving on with the next row to add
                            // Needs to go after the first Cell
                            if ($pdf->GetY() == 10) {
                                $pdf->SetFont('Arial', '', 7);
                                $pdf->Cell(1,5,"",'',0,'C', '', '');
                                $pdf->Cell(140, 5, "", '', 0, 'C');
                                $pdf->Cell(35, 5, "Date Printed: " . formatDate(date("m/d/Y")), '', 1, 'R');

                                $pdf->SetFont('Arial','B',7);
                                $pdf->Cell(1,5,"",'',0,'C', '', '');
                                $pdf->Cell(5,5,"#",'LTRB',0,'C');
                                $pdf->Cell(25,5,"Sire",'LRBT',0,'C');
                                $pdf->Cell(25,5,"Female Count",'LRBT',0,'C');
                                $pdf->Cell(25,5,"Wgt Avg (Female)",'LRBT',0,'C');
                                $pdf->Cell(25,5,"Wgt Adj Avg (Female)",'LRBT',0,'C');
                                $pdf->Cell(25,5,"Male Count",'LRBT',0,'C');
                                $pdf->Cell(25,5,"Wgt Avg (Male)",'LRBT',0,'C');
                                $pdf->Cell(25,5,"Wgt Adj Avg (Male)",'LRBT',1,'C');

                                $pdf->Cell(5, 5, "", '', 0, 'C');
                            }

                            // If the sire PHN is not blank, then print it

                                $row++;
                                $pdf->SetFont('Arial','',7);
                                $pdf->Cell(1,5,"",'R',0,'C');
                                $pdf->Cell(5,5,$row,'LRTB',0,'C');
                                $pdf->Cell(25,5,$animal_sire,'LRBT',0,'C');
                                $pdf->Cell(25,5,$total_cows,'LRBT',0,'C');
                                $pdf->Cell(25,5,round($fem_bweight, 0, PHP_ROUND_HALF_UP),'LRBT',0,'C');
                                $pdf->Cell(25,5,round($fem_bweight_adj, 0, PHP_ROUND_HALF_UP),'LRBT',0,'C');
                                $pdf->Cell(25,5,$total_bulls,'LRBT',0,'C');
                                $pdf->Cell(25,5,round($male_bweight, 0, PHP_ROUND_HALF_UP),'LRBT',0,'C');
                                $pdf->Cell(25,5,round($male_bweight_adj, 0, PHP_ROUND_HALF_UP),'LRBT',0,'C');
                                $pdf->Cell(5,5,"",'L',1,'C');
                            }

                            $rsSireGroup->MoveNext();
                        }
                    }
                }

                // create export directory, name, and url
                $exportDir = getExportDirectory($provider, $member_id);
                $timestamp = time();

                if(trim($year) == trim($active_year)) {
                    $exportName = "active_calves_app_" . $member_id . "_" . $active_year . ".pdf";
                    $exportDir = $exportDir . "active_calves_app_" . $member_id . "_" . $active_year . ".pdf";
                } else {
                    $exportName = "active_calves_app_" . $member_id . "_" . $year . "_" . $active_year . ".pdf";
                    $exportDir = $exportDir . "active_calves_app_" . $member_id . "_" . $year . "_" . $active_year . ".pdf";
                }

                if($PDO->DB_TYPE == "NEW") {
                    // for the new databases we have to look in the members folder for the
                    // correct pdf directory
                    $exportUrl 	= $PDO->FILE_DIR . $member_id . "/" . $exportName;
                } else if($PDO->DB_TYPE == "OLD") {
                    $exportUrl 	= $PDO->FILE_DIR . $exportName;
                }

                // export the file
                //ob_get_clean();
                $pdf->Output($exportDir);

                // If file name already exists, then update it!
                if($PDO->DB_TYPE == "NEW") {
                    $sql = "SELECT * 
                            FROM reports 
                            WHERE report_title = ?
                                AND report_file_name = ?
                                AND member_id = ?";
                    $params = array($report_title, $exportName, $member_id);
                } else if($PDO->DB_TYPE == "OLD") {
                    $sql = "SELECT * 
                            FROM tbl_member_reports_android 
                            WHERE report_type = ? 
                                AND report_file_name = ?
                                AND create_user = ?";
                    $params = array($report_title, $exportName, $member_id);
                }
                $rsCheckDuplicate = $PDO->recordSetQuery($sql, $params);
                if($rsCheckDuplicate) {
                    if(!$rsCheckDuplicate->EOF) {
                        $rsCheckDuplicate->MoveFirst();
                        // Update
                        if($PDO->DB_TYPE == "NEW") {
                            $sql = "UPDATE reports 
                                    SET report_date = ?
                                    WHERE member_id = ?
                                        AND report_title = ?
                                        AND report_file_name = ?
                                        AND report_format = ?";
                            $params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName, "P");
                        } else if($PDO->DB_TYPE == "OLD") {
                            $sql = "UPDATE tbl_member_reports_android 
                                    SET create_stamp = ?
                                    WHERE member_id = ?
                                        AND report_type = ?
                                        AND report_file_name = ?
                                        AND report_format = ?
                                        AND create_user = ?";
                            $params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName, "P", $member_id);
                        }
                        $PDO->executeQuery($sql, $params);
                    } else {
                        if($PDO->DB_TYPE == "NEW") {
                            $sql = "INSERT INTO reports 
                                    (member_id, report_title, report_file_name, report_format, report_date) 
                                    VALUES 
                                    (?, ?, ?, ?, ?)";
                            $params = array($member_id, $report_title, $exportName, "P", date('Y-m-d h:i:s'));
                        } else if($PDO->DB_TYPE == "OLD") {
                            $sql = "INSERT INTO tbl_member_reports_android 
                                    (member_id, report_type, report_file_name, report_format, create_user, 
                                    create_stamp) 
                                    VALUES 
                                    (?, ?, ?, ?, ?, ?)";
                            $params = array($member_id, $report_title, $exportName, "P", $member_id, date('Y-m-d h:i:s'));
                        }
                        $PDO->executeQuery($sql, $params);
                    }
                }

                // send back the file name and the pdf url as the result
                $response["success"] = true;
                $response["pdf_url"] = $exportUrl;	// send back the url
                $response["message"] = "successfully generated pdf";
                echo json_encode($response);
            } else {
                $response["success"] = false;
                $response["message"] = "error building pdf report";
                die(json_encode($response));
            }
        } else {
            $response["success"] = false;
            $response["message"] = "sql error";
            die(json_encode($response));
        }
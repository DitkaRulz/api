<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}

    header('Access-Control-Allow-Origin: *');
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	function formatSex($sex) {
		switch(strtoupper(trim($sex))) {
			case "C": return "Cow";
			case "B": return "Bull";
			case "S": return "Steer";
			default: return "N/A";
		}
	}
	
	function formatDate($date) {
		// BMC 02.01.2016
		//	This function will take in a date, most likely formatted 
		//	like YYYY-MM-DD and change the format to MM DD, YYYY
		//	as an example, Jan 01, 2016
		// BMC 02.03.2016
		//	If the date comes in as blank or null then this will display
		//	Dec 31, 1969!!! So we need to account for these input and
		//	display the proper output.  For now, we'll display nothing, just
		//	a blank string.
		
		if(trim($date) == "" 
			|| trim($date) == "null") {
			return "";
		} else {
			$date = strtotime($date);
			return date("M d, Y", $date);
		}
	}
	
	## -------------------- GENERATE PDF -------------------- ##
	$response["pdf_url"] = "";
	
	$MemberName = "";
	$YearDate	= "";
	$TotalCount	= "";
	
	//** ------------------------------------ BUILD REPORT HERE ----------------------------------- **//
	$report_title = "Notes / Comments Report ".$year;
		
	// This query will get the BIRTH PASTURE
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT a.private_herd_number, 
					p.pasture_name, 
					n.comment_date,
					n.comment AS comment_note
				FROM animal_comments n
					INNER JOIN animal a ON n.registration = a.registration
					INNER JOIN ownership o ON n.registration = o.registration
					LEFT JOIN animal_location l ON n.registration = l.registration
                        AND (l.move_in <= n.comment_date 
                            AND (l.move_out >= n.comment_date 
                                OR l.move_out IS NULL))
					LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
				WHERE n.comment_date = DATE('".$year."')
					AND o.member_id = '".$member_id."'
					AND o.date_owned <= n.comment_date 
				ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
	} else if($PDO->DB_TYPE == "OLD") {
		// This will get all the notes data
		$sql = "SELECT a.animal_private_herd_id AS private_herd_number, 
					p.pasture_id AS pasture_name, 
					n.create_stamp AS comment_date, 
					n.comment AS comment_note
				FROM tbl_animal_notes n
					INNER JOIN tbl_animal a ON n.animal_registration = a.animal_registration
					INNER JOIN tbl_ownership o ON n.animal_registration = o.animal_registration
					LEFT JOIN tbl_animal_location l ON n.animal_registration = l.animal_registration
                        AND (l.move_in_date <= n.create_stamp
                            AND (l.move_out_date >= n.create_stamp
                                OR l.move_out_date IS NULL))
					LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
				WHERE n.create_stamp = DATE('".$year."')
					AND o.owner_id = '".$member_id."'
					AND o.ownership_date <= n.create_stamp
				ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
	}
	$rs = $PDO->recordSetQuery($sql);
	if($rs) {
		// BUILD PDF
		if($rs->Recordcount() > 0) {
			// ********** CREATE THE PDF ********** //
			$pdf = new PDF(); 
			//$pdf->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
			
			// Build PDF
			$requested_font	= 'Times';
			$pdf->AliasNbPages();
			$pdf->SetTopMargin(10);
			$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
			$pdf->ZeroCount();
			$pdf->SetFont('Times','B', 10);
			
			$i=0;	// not sure about this
			
			// set the year
			$YearDate = $year;
			
			// get the cow count
			$TotalCount = $rs->Recordcount();
			
			// Get the member name
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT member_name 
						FROM member 
						WHERE member_id = '".$member_id."'";
			} else if($PDO->DB_TYPE == "OLD") { 
				$sql = "SELECT member_name 
						FROM tbl_member 
						WHERE member_id='".$member_id."'";
			}
			
			$rsMember = $PDO->recordSetQuery($sql);
			if($rsMember) {
				if(!$rsMember->EOF) {
					$rsMember->MoveFirst();
					
					$MemberName = $rsMember->fields['member_name'];
				} else {
					$MemberName = $member_id;
				}
				
				$rsMember->Close();
			} else {
				$MemberName = $member_id;
			}

            // BMC 11.07.2016
            //  -- get the member details
            $memberDetails = "";
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT c.address_line_1,
                            c.address_line_2,
                            c.city,
                            c.state,
                            c.zip_code,
                            c.phone_number
                        FROM member m
                            LEFT JOIN contact c ON m.member_id = c.member_id
                                AND c.is_primary
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line_1'];
                        $address_line_2 = $rsContact->fields['address_line_2'];
                        $city = $rsContact->fields['city'];
                        $state = $rsContact->fields['state'];
                        $zip_code = $rsContact->fields['zip_code'];
                        //$phone_number = $rsContact->fields['phone_number'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                        //$phone_number = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT a.address_line1,
                            a.address_line2,
                            a.address_city,
                            a.address_state,
                            a.address_postal_code
                        FROM tbl_member m
                            LEFT JOIN tbl_address a ON m.general_correspondance_address = a.address_id
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line1'];
                        $address_line_2 = $rsContact->fields['address_line2'];
                        $city = $rsContact->fields['address_city'];
                        $state = $rsContact->fields['address_state'];
                        $zip_code = $rsContact->fields['address_postal_code'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else {
                $memberDetails = "";
            }
			
			// build the title
			$pdf->Image(SITE_ROOT . '/images/digital_beef_logo_no_text_tm.png', 10, 10, 35);
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,5,"Digital Beef, LLC", '', 1, 'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(188,12,$memberDetails, '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,7.5,"Notes / Comments Reports for ".formatDate($YearDate), '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(188,7.5, "Total Comments: ".$TotalCount, '', 1, 'C');
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(188, 8, $MemberName."\n".$member_id, '', 1, 'C');
			$pdf->Cell(188, 6, "", '', 1, 'C');
		
			// Display the header
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(1,5,"",'R',0,'C');	
			$pdf->Cell(20,5,"PHN",'LRBT',0,'C');	
			$pdf->Cell(20,5,"Pasture",'LRBT',0,'C');
			$pdf->Cell(20,5,"Date",'LRBT',0,'C');
			$pdf->Cell(125,5,"Comment",'LRBT',0,'C');
			$pdf->Cell(1,5,"",'',1,'C');
				
			$rs->MoveFirst();	
			while(!$rs->EOF) {
				// Display the notes
				$pdf->SetFont('Arial','',7);
				$pdf->Cell(1,5,"",'R',0,'C');	
				$pdf->Cell(20,5,$rs->fields['private_herd_number'],'LRBT',0,'C');	
				$pdf->Cell(20,5,$rs->fields['pasture_name'],'LRBT',0,'C');	
				$pdf->Cell(20,5,$rs->fields['comment_date'],'LRBT',0,'C');	
				$pdf->Cell(125,5,$rs->fields['comment_note'],'LRBT',0,'L');	
				$pdf->Cell(1,5,"",'',1,'C');	
				$rs->MoveNext();
			}		
			
			$rs->Close();
			
			// create export directory, name, and url
			$exportDir = getExportDirectory($provider, $member_id);
			$timestamp = time();
			
			$exportName = "comments_report_app_".$member_id."_".$year.".pdf";
			$exportDir = $exportDir . "comments_report_app_".$member_id."_".$year.".pdf";
			
			if($PDO->DB_TYPE == "NEW") {
				// for the new databases we have to look in the members folder for the 
				// correct pdf directory
				$exportUrl 	= $PDO->FILE_DIR . $member_id . "/" . $exportName;
			} else if($PDO->DB_TYPE == "OLD") {
				$exportUrl 	= $PDO->FILE_DIR . $exportName;
			}
			
			// export the file
			$pdf->Output($exportDir);
			
			// If file name already exists, then update it!
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT * 
						FROM reports 
						WHERE report_title = '".$report_title."' 
							AND report_file_name ='".$exportName."'
							AND member_id = '".$member_id."'";
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT * 
						FROM tbl_member_reports_android 
						WHERE report_type = '".$report_title."' 
							AND report_file_name='".$exportName."'
							AND create_user = '".$member_id."'";
			}
			$rsCheckDuplicate = $PDO->recordSetQuery($sql);
			if($rsCheckDuplicate) {
				if(!$rsCheckDuplicate->EOF) {
					$rsCheckDuplicate->MoveFirst();
					// Update
					if($PDO->DB_TYPE == "NEW") {
						$sql = "UPDATE reports 
								SET report_date = '".date('Y-m-d h:i:s')."'
								WHERE member_id = '".$member_id."'
									AND report_title = '".$report_title."'
									AND report_file_name = '".$exportName."'
									AND report_format = 'P'";
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "UPDATE tbl_member_reports_android 
								SET create_stamp='".date('Y-m-d h:i:s')."'
								WHERE member_id='".$member_id."'
									AND report_type='".$report_title."'
									AND report_file_name='".$exportName."'
									AND report_format='P'
									AND create_user='".$member_id."'";
					}
					$PDO->executeQuery($sql);
				} else {
					// INSERT
					if($PDO->DB_TYPE == "NEW") {
						$sql = "INSERT INTO reports 
									(member_id, report_title, report_file_name, report_format, report_date) 
								VALUES 
									('".$member_id."','".$report_title."','".$exportName."', 'P', 
									'".date('Y-m-d h:i:s')."')";
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "INSERT INTO tbl_member_reports_android 
									(member_id, report_type, report_file_name, report_format, create_user, 
									create_stamp) 
								VALUES 
									('".$member_id."','".$report_title."','".$exportName."', 'P', '".$member_id."', 
									'".date('Y-m-d h:i:s')."')";
					}
					$PDO->executeQuery($sql);
				}
			}
		
			// send back the file name and the pdf url as the result
			$response["success"] = true;
			$response["pdf_url"] = $exportUrl;	// send back the url
			$response["message"] = "successfully generated pdf";
			echo json_encode($response);
		} else {
			$response["success"] = false;
			$response["message"] = "you have no pregnancy data available";
			die(json_encode($response));
		}
	} else {
		$response["success"] = false;
		$response["message"] = "sql error";
		die(json_encode($response));
	}
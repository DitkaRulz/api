<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}

    header('Access-Control-Allow-Origin: *');
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	function formatSex($sex) {
		switch(strtoupper(trim($sex))) {
			case "C": return "Cow";
			case "B": return "Bull";
			case "S": return "Steer";
			default: return "N/A";
		}
	}
	
	function formatDate($date) {
		// BMC 02.01.2016
		//	This function will take in a date, most likely formatted 
		//	like YYYY-MM-DD and change the format to MM DD, YYYY
		//	as an example, Jan 01, 2016
		// BMC 02.03.2016
		//	If the date comes in as blank or null then this will display
		//	Dec 31, 1969!!! So we need to account for these input and
		//	display the proper output.  For now, we'll display nothing, just
		//	a blank string.
		
		if(trim($date) == "" 
			|| trim($date) == "null") {
			return "";
		} else {
			$date = strtotime($date);
			return date("M d, Y", $date);
		}
	}
	
	## -------------------- GENERATE PDF -------------------- ##
	$response["pdf_url"] = "";
	
	$MemberName = "";
	
	$report_title = "Heat Summary ".$start_date." to ".$end_date;

	//** ------------------------------------ BUILD REPORT HERE ----------------------------------- **//
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT a.private_herd_number,
					e.estrous_date,
					e.time_period,
					p.pasture_name, 
					a.eid
				FROM animal_estrous e
					INNER JOIN animal a ON e.registration = a.registration
					INNER JOIN ownership o ON e.registration = o.registration
					LEFT JOIN animal_location l ON e.registration = l.registration
					AND (l.move_in <= e.estrous_date
					  AND (l.move_out IS NULL
						  OR l.move_out >= e.estrous_date))
					LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
				WHERE o.member_id = ?
					AND o.date_owned <= e.estrous_date
					AND e.estrous_date >= DATE(?)
					AND e.estrous_date <= DATE(?)
				ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
		$params = array($member_id, $start_date, $end_date);
	} else if($PDO->DB_TYPE == "OLD") {
		$sql = "SELECT h.animal_phn AS private_herd_number,
					h.heat_date AS estrous_date,
					h.am_pm AS time_period,
					p.pasture_id AS pasture_name, 
					a.eid
				FROM tbl_animal_heat h
					INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
					LEFT JOIN tbl_animal_location l ON h.animal_registration = l.animal_registration
						AND (l.move_in_date <= h.heat_date
						  AND (l.move_out_date IS NULL
							  OR l.move_out_date >= h.heat_date))
					LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
				WHERE h.owner_id = ?
					AND h.heat_date >= DATE(?)
					AND h.heat_date <= DATE(?)
				ORDER BY (substring(h.animal_phn, '^[0-9]+'))::int,substring(h.animal_phn, '[^0-9_].*$')";
		$params = array($member_id, $start_date, $end_date);
	}
	
	$rs = $PDO->recordSetQuery($sql, $params);
	if($rs) {
		if(!$rs->EOF) {
			// ********** CREATE THE PDF ********** //
			$pdf = new PDF(); 
			//$pdf->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
			
			// Build PDF
			$requested_font	= 'Times';
			$pdf->AliasNbPages();
			$pdf->SetTopMargin(10);
			$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
			$pdf->ZeroCount();
			$pdf->SetFont('Times','B', 10);
			
			$i=0;	// not sure about this
			
			// get the cow count
			$TotalCount = $rs->Recordcount();
			
			// Get the member name
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT member_name 
						FROM member 
						WHERE member_id = ?";
				$params = array($member_id);
			} else if($PDO->DB_TYPE == "OLD") { 
				$sql = "SELECT member_name 
						FROM tbl_member 
						WHERE member_id = ?";
				$params = array($member_id);
			}
			
			$rsMember = $PDO->recordSetQuery($sql, $params);
			if($rsMember) {
				if(!$rsMember->EOF) {
					$rsMember->MoveFirst();
					
					$MemberName = $rsMember->fields['member_name'];
				} else {
					$MemberName = $member_id;
				}
				
				$rsMember->Close();
			} else {
				$MemberName = $member_id;
			}

            // BMC 11.07.2016
            //  -- get the member details
            $memberDetails = "";
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT c.address_line_1,
                            c.address_line_2,
                            c.city,
                            c.state,
                            c.zip_code,
                            c.phone_number
                        FROM member m
                            LEFT JOIN contact c ON m.member_id = c.member_id
                                AND c.is_primary
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line_1'];
                        $address_line_2 = $rsContact->fields['address_line_2'];
                        $city = $rsContact->fields['city'];
                        $state = $rsContact->fields['state'];
                        $zip_code = $rsContact->fields['zip_code'];
                        //$phone_number = $rsContact->fields['phone_number'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                        //$phone_number = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT a.address_line1,
                            a.address_line2,
                            a.address_city,
                            a.address_state,
                            a.address_postal_code
                        FROM tbl_member m
                            LEFT JOIN tbl_address a ON m.general_correspondance_address = a.address_id
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line1'];
                        $address_line_2 = $rsContact->fields['address_line2'];
                        $city = $rsContact->fields['address_city'];
                        $state = $rsContact->fields['address_state'];
                        $zip_code = $rsContact->fields['address_postal_code'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else {
                $memberDetails = "";
            }
			
			// build the title
			$pdf->Image(SITE_ROOT . '/images/digital_beef_logo_no_text_tm.png', 10, 10, 35);
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,5,"Digital Beef, LLC", '', 1, 'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(188,12,$memberDetails, '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,7.5,"Estrous Summary for ".formatDate($start_date)." to ".formatDate($end_date), '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(188,7.5, "Total Observed: ".$TotalCount, '', 1, 'C');
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(188, 8, $MemberName."\n".$member_id, '', 1, 'C');
			$pdf->Cell(188, 6, "", '', 1, 'C');
	
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(1,5,"",'R',0,'C');
			$pdf->Cell(5, 5, "#", 'LTRB', 0, 'C');
			$pdf->Cell(25,5,"PHN",'LRBT',0,'C');			
			$pdf->Cell(25,5,"EID",'LRBT',0,'C');		
			$pdf->Cell(25,5,"Estrous Date",'LRBT',0,'C');		
			$pdf->Cell(25,5,"Time Period",'LRBT',0,'C');	
			$pdf->Cell(25,5,"Pasture",'LRBT',0,'C');
			$pdf->Cell(1,5,"",'L',1,'C');
			
			$rs->MoveFirst();
			$row = 0;
			while(!$rs->EOF) {
			    $row++;
				$pdf->SetFont('Arial','',7);
				$pdf->Cell(1,5,"",'R',0,'C');
				$pdf->Cell(5, 5, $row, 'LTRB', 0, 'C');
				$pdf->Cell(25,5,$rs->fields['private_herd_number'],'LTRB',0,'C');
				$pdf->Cell(25,5,$rs->fields['eid'],'LTRB',0,'C');
				$pdf->Cell(25,5,formatDate($rs->fields['estrous_date']),'LTRB',0,'C');
				$pdf->Cell(25,5,$rs->fields['time_period'],'LTRB',0,'C');
				$pdf->Cell(25,5,$rs->fields['pasture_name'],'LTRB',0,'C');
				$pdf->Cell(1,5,"",'L',1,'C');
				
				$rs->MoveNext();
			}
			
			$rs->Close();
				
			// create export directory, name, and url
			$exportDir = getExportDirectory($provider, $member_id);
			$timestamp = time();
			
			$exportName = "estrous_summary_app_".$member_id."_".$start_date."_to_".$end_date.".pdf";
			$exportDir = $exportDir . "estrous_summary_app_".$member_id."_".$start_date."_to_".$end_date.".pdf";
			
			if($PDO->DB_TYPE == "NEW") {
				// for the new databases we have to look in the members folder for the 
				// correct pdf directory
				$exportUrl 	= $PDO->FILE_DIR . $member_id . "/" . $exportName;
			} else if($PDO->DB_TYPE == "OLD") {
				$exportUrl 	= $PDO->FILE_DIR . $exportName;
			}
			
			// export the file
			$pdf->Output($exportDir);
			
			// If file name already exists, then update it!
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT * 
						FROM reports 
						WHERE report_title = ? 
							AND report_file_name = ?
							AND member_id = ?";
				$params = array($report_title, $exportName, $member_id);
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT * 
						FROM tbl_member_reports_android 
						WHERE report_type = ? 
							AND report_file_name = ?
							AND create_user = ?";
				$params = array($report_title, $exportName, $member_id);
			}
			$rsCheckDuplicate = $PDO->recordSetQuery($sql, $params);
			if($rsCheckDuplicate) {
				if(!$rsCheckDuplicate->EOF) {
					$rsCheckDuplicate->MoveFirst();
					// Update
					if($PDO->DB_TYPE == "NEW") {
						$sql = "UPDATE reports 
								SET report_date = ?
								WHERE member_id = ?
									AND report_title = ?
									AND report_file_name = ?
									AND report_format = 'P'";
						$params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName);
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "UPDATE tbl_member_reports_android 
								SET create_stamp = ?
								WHERE member_id = ?
									AND report_type = ?
									AND report_file_name = ?
									AND report_format = 'P'
									AND create_user = ?";
						$params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName, $member_id);
					}
					$PDO->executeQuery($sql, $params);
				} else {
					// INSERT
					if($PDO->DB_TYPE == "NEW") {
						$sql = "INSERT INTO reports 
									(member_id, report_title, report_file_name, report_format, report_date) 
								VALUES 
									(?, ?, ?, ?, ?)";
						$params = array($member_id, $report_title, $exportName, 'P', date('Y-m-d h:i:s'));
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "INSERT INTO tbl_member_reports_android 
									(member_id, report_type, report_file_name, report_format, create_user, 
									create_stamp) 
								VALUES 
									(?, ?, ?, ?, ?, ?)";
						$params = array($member_id, $report_title, $exportName, 'P', $member_id, date('Y-m-d h:i:s'));
					}
					$PDO->executeQuery($sql, $params);
				}
			}
		
			// send back the file name and the pdf url as the result
			$response["success"] = true;
			$response["pdf_url"] = $exportUrl;	// send back the url
			$response["message"] = "successfully generated pdf";
			echo json_encode($response);
		} else {
			$response["success"] = false;
			$response["message"] = "you have no estrous data available";
			die(json_encode($response));
		}
	} else {
		$response["success"] = false;
		$response["message"] = "sql error";
		die(json_encode($response));
	}
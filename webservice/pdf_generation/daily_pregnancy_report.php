<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}

    header('Access-Control-Allow-Origin: *');
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	function formatSex($sex) {
		switch(strtoupper(trim($sex))) {
			case "C": return "Cow";
			case "B": return "Bull";
			case "S": return "Steer";
			default: return "N/A";
		}
	}
	
	function formatDate($date) {
		// BMC 02.01.2016
		//	This function will take in a date, most likely formatted 
		//	like YYYY-MM-DD and change the format to MM DD, YYYY
		//	as an example, Jan 01, 2016
		// BMC 02.03.2016
		//	If the date comes in as blank or null then this will display
		//	Dec 31, 1969!!! So we need to account for these input and
		//	display the proper output.  For now, we'll display nothing, just
		//	a blank string.
		
		if(trim($date) == "" 
			|| trim($date) == "null") {
			return "";
		} else {
			$date = strtotime($date);
			return date("M d, Y", $date);
		}
	}

    function formatPalpationResult($result) {
        switch($result) {
            case "OPEN":
            case "0":
                return "Open";
            case "1":
                return "0-30 Days";
            case "2":
                return "31-60 Days";
            case "3":
                return "61-90 Days";
            case "4":
                return "91-120 Days";
            case "5":
                return "121-150 Days";
            case "6":
                return "151-180 Days";
            case "7":
                return "181-210 Days";
            case "8":
                return "211-240 Days";
            case "9":
                return "241-270 Days";
            default:
                return "N/A";
        }
    }
	
	## -------------------- GENERATE PDF -------------------- ##
	$response["pdf_url"] = "";
	
	$MemberName = "";
	$YearDate	= "";
	
	$report_title = "Pregnancy Report ".$year;

	//** ------------------------------------ BUILD REPORT HERE ----------------------------------- **//
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT a.private_herd_number,
					a.eid,
					br.type,
					br.sire,
					br.palpation_date,
					br.palpation_result,
					p.pasture_name,
					a.left_tattoo,
					a.right_tattoo,
					br.begin_date,
					br.end_date,
					br.ai_date
				FROM animal a
					INNER JOIN animal_birth b ON a.registration = b.registration
					INNER JOIN animal_breeding br ON a.registration = br.dam
					INNER JOIN ownership o ON br.dam = o.registration
					LEFT JOIN animal_location l ON a.registration = l.registration
					AND (l.move_in <= br.palpation_date
					  AND (l.move_out IS NULL
						  OR l.move_out >= br.palpation_date))
					LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
				WHERE o.member_id = ?
					AND br.palpation_date = DATE(?)
				GROUP BY a.private_herd_number,
					a.eid,
					br.type,
					br.sire,
					br.palpation_date,
					br.palpation_result,
					p.pasture_name,
					a.left_tattoo,
					a.right_tattoo,
					br.begin_date,
					br.end_date,
					br.ai_date
				ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$'), type DESC";
		$params = array($member_id, $year);
	} else if($PDO->DB_TYPE == "OLD") {
	    switch($provider) {
            case "ACRS_CATTLE":
            case "AMARS_CATTLE":
                $sql = "SELECT z.*
                        FROM ((SELECT 
                                  CASE 
                                    WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le
                                    WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re
                                    ELSE ''
                                  END AS private_herd_number,
                                  a.eid,
                                    'NS' AS type,
                                    bAgr.bull_registration as sire,
                                  bAgr.palpation_date,
                                  bAgr.palpation_result,
                                  pAgr.pasture_id AS pasture_name
                               FROM tbl_animal a
                                 INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
                                 INNER JOIN tbl_breeding_agreement bAgr ON bAgr.cow_registration = a.animal_registration
                                 LEFT JOIN tbl_animal_location l on l.animal_registration = a.animal_registration
                                                                    AND (l.move_in_date <= bAgr.palpation_date
                                                                         AND (l.move_out_date IS NULL
                                                                              OR l.move_out_date >= bAgr.palpation_date))
                                 LEFT JOIN tbl_pastures pAgr on l.pasture_id = pAgr.pk_id::VARCHAR
                               WHERE bAgr.cow_owner_id = ?
                                     AND bAgr.palpation_date = DATE(?)
                               GROUP BY a.tattoo_le,
                                 a.tattoo_re,
                                 a.eid,
                                 bAgr.bull_registration,
                                 bAgr.palpation_date,
                                 bAgr.palpation_result,
                                 pAgr.pasture_id
                               ORDER BY (substring(a.tattoo_le, '^[0-9]+'))::int,substring(a.tattoo_le, '[^0-9_].*$'), type DESC)
                        
                              UNION
                        
                              (SELECT 
                                 CASE 
                                    WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le
                                    WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re
                                    ELSE ''
                                 END AS private_herd_number,
                                 a.eid,
                                      'AI' AS type,
                                      bAi.bull_registration as sire,
                                 bAi.palpation_date,
                                 bAi.palpation_result,
                                      pAi.pasture_id AS pasture_name
                               FROM tbl_animal a
                                 INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
                                 INNER JOIN tbl_breeding_ai bAi ON bAi.cow_registration = a.animal_registration
                                 LEFT JOIN tbl_animal_location l on l.animal_registration = a.animal_registration
                                                                    AND (l.move_in_date <= bAi.palpation_date
                                                                         AND (l.move_out_date IS NULL
                                                                              OR l.move_out_date >= bAi.palpation_date))
                                 LEFT JOIN tbl_pastures pAi on l.pasture_id = pAi.pk_id::VARCHAR
                               WHERE bAi.cow_owner_id = ?
                                     AND bAi.palpation_date = DATE(?)
                               GROUP BY a.tattoo_le,
                                 a.tattoo_re,
                                 a.eid,
                                 bAi.bull_registration,
                                 bAi.palpation_date,
                                 bAi.palpation_result,
                                 pAi.pasture_id
                               ORDER BY (substring(a.tattoo_le, '^[0-9]+'))::int,substring(a.tattoo_le, '[^0-9_].*$'), type DESC)
                        
                              UNION
                        
                              (SELECT 
                                 CASE 
                                    WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le
                                    WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re
                                    ELSE ''
                                 END AS private_herd_number,
                                 a.eid,
                                      'ET' AS type,
                                      bEt.bull_registration as sire,
                                 bEt.palpation_date,
                                 bEt.palpation_result,
                                      pEt.pasture_id AS pasture_name
                               FROM tbl_animal a
                                 INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
                                 INNER JOIN tbl_breeding_et bEt ON bEt.cow_registration = a.animal_registration
                                 LEFT JOIN tbl_animal_location l on l.animal_registration = a.animal_registration
                                                                    AND (l.move_in_date <= bEt.palpation_date
                                                                         AND (l.move_out_date IS NULL
                                                                              OR l.move_out_date >= bEt.palpation_date))
                                 LEFT JOIN tbl_pastures pEt on l.pasture_id = pEt.pk_id::VARCHAR
                               WHERE bEt.cow_owner_id = ?
                                     AND bEt.palpation_date = DATE(?)
                               GROUP BY a.tattoo_le,
                                 a.tattoo_re,
                                 a.eid,
                                 bEt.bull_registration,
                                 bEt.palpation_date,
                                 bEt.palpation_result,
                                 pEt.pasture_id
                               ORDER BY (substring(a.tattoo_le, '^[0-9]+'))::int,substring(a.tattoo_le, '[^0-9_].*$'), type DESC)) z
                        GROUP BY z.private_herd_number,
                          z.eid,
                          z.type,
                          z.sire,
                          z.palpation_date,
                          z.palpation_result,
                          z.pasture_name
                        ORDER BY (substring(z.private_herd_number, '^[0-9]+'))::int,substring(z.private_herd_number, '[^0-9_].*$'), type DESC";
            $params = array($member_id, $year,
                $member_id, $year,
                $member_id ,$year);
            break;
            default:
                $sql = "SELECT z.*
				FROM ((SELECT a.animal_private_herd_id AS private_herd_number, 
						a.eid,
						'NS' AS type,
						bAgr.bull_registration as sire,
						bAgr.palpation_date, 
						bAgr.palpation_result,
						pAgr.pasture_id AS pasture_name
					FROM tbl_animal a 
						INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
						INNER JOIN tbl_breeding_agreement bAgr ON bAgr.cow_registration = a.animal_registration
						LEFT JOIN tbl_animal_location l on l.animal_registration = a.animal_registration
							AND (l.move_in_date <= bAgr.palpation_date
							  AND (l.move_out_date IS NULL
								  OR l.move_out_date >= bAgr.palpation_date))
						LEFT JOIN tbl_pastures pAgr on l.pasture_id = pAgr.pk_id::VARCHAR
					WHERE bAgr.cow_owner_id = ?
						AND bAgr.palpation_date = DATE(?)
					GROUP BY a.animal_private_herd_id, 
						a.eid,
						bAgr.bull_registration, 
						bAgr.palpation_date, 
						bAgr.palpation_result,
						pAgr.pasture_id
					ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$'), type DESC)
							   
					UNION
					 
					(SELECT a.animal_private_herd_id AS private_herd_number, 
						a.eid,
						'AI' AS type,
						bAi.bull_registration as sire, 
						bAi.palpation_date, 
						bAi.palpation_result,
						pAi.pasture_id AS pasture_name
					FROM tbl_animal a 
						INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
						INNER JOIN tbl_breeding_ai bAi ON bAi.cow_registration = a.animal_registration
						LEFT JOIN tbl_animal_location l on l.animal_registration = a.animal_registration
							AND (l.move_in_date <= bAi.palpation_date
							  AND (l.move_out_date IS NULL
								  OR l.move_out_date >= bAi.palpation_date))
						LEFT JOIN tbl_pastures pAi on l.pasture_id = pAi.pk_id::VARCHAR
					WHERE bAi.cow_owner_id = ?
						AND bAi.palpation_date = DATE(?)
					GROUP BY a.animal_private_herd_id, 
						a.eid,
						bAi.bull_registration, 
						bAi.palpation_date, 
						bAi.palpation_result,
						pAi.pasture_id
					ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$'), type DESC)
						 
					UNION
						   
					(SELECT a.animal_private_herd_id AS private_herd_number, 
						a.eid,
						'ET' AS type,
						bEt.bull_registration as sire, 
						bEt.palpation_date,
						bEt.palpation_result,
						pEt.pasture_id AS pasture_name
					FROM tbl_animal a 
						INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
						INNER JOIN tbl_breeding_et bEt ON bEt.cow_registration = a.animal_registration
						LEFT JOIN tbl_animal_location l on l.animal_registration = a.animal_registration
							AND (l.move_in_date <= bEt.palpation_date
							  AND (l.move_out_date IS NULL
								  OR l.move_out_date >= bEt.palpation_date))
						LEFT JOIN tbl_pastures pEt on l.pasture_id = pEt.pk_id::VARCHAR
					WHERE bEt.cow_owner_id = ?
						AND bEt.palpation_date = DATE(?)
					GROUP BY a.animal_private_herd_id, 
						a.eid, 
						bEt.bull_registration,
						bEt.palpation_date,
						bEt.palpation_result,
						pEt.pasture_id
					ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$'), type DESC)) z
				GROUP BY z.private_herd_number,
					z.eid,
					z.type,
					z.sire,
					z.palpation_date,
					z.palpation_result,
					z.pasture_name
				ORDER BY (substring(z.private_herd_number, '^[0-9]+'))::int,substring(z.private_herd_number, '[^0-9_].*$'), type DESC";
            $params = array($member_id, $year,
                $member_id, $year,
                $member_id ,$year);
            break;
        }
	}
	$rs = $PDO->recordSetQuery($sql, $params);
	if($rs) {
		// BUILD PDF
		if($rs->Recordcount() > 0) {
			// ********** CREATE THE PDF ********** //
			$pdf = new PDF(); 
			//$pdf->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
			
			// Build PDF
			$requested_font	= 'Times';
			$pdf->AliasNbPages();
			$pdf->SetTopMargin(10);
			$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
			$pdf->ZeroCount();
			$pdf->SetFont('Times','B', 10);
			
			$i=0;	// not sure about this
			
			// set the year
			$YearDate = $year;
			
			// get the cow count
			$CowCount = $rs->Recordcount();
			
			// Get the member name
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT member_name 
						FROM member 
						WHERE member_id = ?";
				$params = array($member_id);
			} else if($PDO->DB_TYPE == "OLD") { 
				$sql = "SELECT member_name 
						FROM tbl_member 
						WHERE member_id= ?";
				$params = array($member_id);
			}
			
			$rsMember = $PDO->recordSetQuery($sql, $params);
			if($rsMember) {
				if(!$rsMember->EOF) {
					$rsMember->MoveFirst();
					
					$MemberName = $rsMember->fields['member_name'];
				} else {
					$MemberName = $member_id;
				}
				
				$rsMember->Close();
			} else {
				$MemberName = $member_id;
			}

            // BMC 11.07.2016
            //  -- get the member details
            $memberDetails = "";
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT c.address_line_1,
                            c.address_line_2,
                            c.city,
                            c.state,
                            c.zip_code,
                            c.phone_number
                        FROM member m
                            LEFT JOIN contact c ON m.member_id = c.member_id
                                AND c.is_primary
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line_1'];
                        $address_line_2 = $rsContact->fields['address_line_2'];
                        $city = $rsContact->fields['city'];
                        $state = $rsContact->fields['state'];
                        $zip_code = $rsContact->fields['zip_code'];
                        //$phone_number = $rsContact->fields['phone_number'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                        //$phone_number = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT a.address_line1,
                            a.address_line2,
                            a.address_city,
                            a.address_state,
                            a.address_postal_code
                        FROM tbl_member m
                            LEFT JOIN tbl_address a ON m.general_correspondance_address = a.address_id
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line1'];
                        $address_line_2 = $rsContact->fields['address_line2'];
                        $city = $rsContact->fields['address_city'];
                        $state = $rsContact->fields['address_state'];
                        $zip_code = $rsContact->fields['address_postal_code'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else {
                $memberDetails = "";
            }
			
			// build the title
			$pdf->Image(SITE_ROOT . '/images/digital_beef_logo_no_text_tm.png', 10, 10, 35);
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,5,"Digital Beef, LLC", '', 1, 'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(188,12,$memberDetails, '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,7.5,"Pregnancy Report for ".formatDate($YearDate), '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(188,7.5, "Total Palpated: ".$CowCount, '', 1, 'C');
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(188, 8, $MemberName."\n".$member_id, '', 1, 'C');
			$pdf->Cell(188, 6, "", '', 1, 'C');

            // LRM 07.14.2017:
            // set the first page headers of the table
            $pdf->SetFont('Arial', '', 7);
            $pdf->Cell(161, 5, "", '', 0, 'C');
            $pdf->Cell(35, 5, "Date Printed: " . formatDate(date("m/d/Y")), '', 1, 'R');

            // Set the headers
            $pdf->SetFont('Arial', 'B', 7);
            $pdf->Cell(1, 5, "", 'R', 0, 'C');
            $pdf->Cell(5, 5, "", 'LTRB', 0, 'C');
            $pdf->Cell(40, 5, "Dam", 'LTRB', 0, 'C');
            $pdf->Cell(75, 5, "Location Details", 'LTRB', 0, 'C');
            $pdf->Cell(25, 5, "Sir", 'LTRB', 0, 'C');
            $pdf->Cell(50, 5, "Palpation", 'LTRB', 1, 'C');

            // Set the sub-headers
            $pdf->Cell(1, 5, "", 'R', 0, 'C');
            $pdf->Cell(5, 5, "#", 'LRTB', 0, 'C');
            $pdf->Cell(25, 5, "PHN / Tattoo", 'LRBT', 0, 'C');
            $pdf->Cell(15, 5, "EID", 'LRBT', 0, 'C');
            $pdf->Cell(30, 5, "Pasture", 'LRBT', 0, 'C');
            $pdf->Cell(35, 5, "Date Of Exposure Or Service", 'LRBT', 0, 'C');
            $pdf->Cell(10, 5, "Type", 'LRBT', 0, 'C');
            $pdf->Cell(25, 5, "PHN / Tattoo", 'LRBT', 0, 'C');
            $pdf->Cell(25, 5, "Date", 'LRBT', 0, 'C');
            $pdf->Cell(25, 5, "Result", 'LRBT', 0, 'C');
            $pdf->Cell(1, 5, "", 'L', 1, 'C');
			
			//*** Now return to the beginning !!! IMPORTANT***
			$rs->MoveFirst();
					
			// now get the animal data
			if(!$rs->EOF) {	
				$rs->MoveFirst();
				$row = 0;
				while(!$rs->EOF) {
				    $row++;
					// get the sire phn
					$animal_sire = $rs->fields['sire'];
                    $animal_sire_phn            = "";
                    $animal_sire_left_tattoo    = "";
                    $animal_sire_right_tattoo   = "";
					if($PDO->DB_TYPE == "NEW") {
						$sql = "SELECT private_herd_number,
                                    left_tattoo,
                                    right_tattoo
								FROM animal 
								WHERE registration = ?";
						$params = array($animal_sire);
					} else if($PDO->DB_TYPE == "OLD") {
					    switch($provider) {
                            case "ACRS_CATTLE":
                            case "AMARS_CATTLE":
                                $sql = "SELECT 
                                            CASE 
                                                WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le,
                                                WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re,
                                                ELSE ''
                                            END AS private_herd_number
                                        FROM 
                                            tbl_animal 
                                        WHERE 
                                            animal_registration = ?";
                                $params = array($animal_sire);
                            break;
                            default:
                                $sql = "SELECT animal_private_herd_id AS private_herd_number
								        FROM tbl_animal 
								        WHERE animal_registration = ?";
                                $params = array($animal_sire);
                            break;
                        }
					}
					$rsSire = $PDO->recordSetQuery($sql, $params);
					if($rsSire) {
						if(!$rsSire->EOF) {
							$rsSire->MoveFirst();

                            $animal_sire_phn            = trim($rsSire->fields['private_herd_number']);
                            $animal_sire_left_tattoo    = trim($rsSire->fields['left_tattoo']);
                            $animal_sire_right_tattoo   = trim($rsSire->fields['right_tattoo']);
						}
						$rsSire->Close();
					}

                    // BMC 05.26.2017
                    //  -- we want to show the actual result, rather than
                    //      just open or pregnant, this is specific to the pregnancy report only
                    $palpation_result = formatPalpationResult($rs->fields['palpation_result']);
					/*
					$palpation_result = "";
					if($rs->fields['palpation_result'] == 0 
					|| $rs->fields['palpation_result'] == '') {
						$palpation_result = "Open";
					} else {
					    // format the result
						$palpation_result = "Pregnant";
					}
					//*/

                    // LRM 7/13/2017: added to get the pasture name
                    $pasture_name = "";
                    if(trim(strtoupper($rs->fields['pasture_name'])) == "") {
                        $pasture_name = "N/A";
                    } else {
                        $pasture_name = trim(strtoupper($rs->fields['pasture_name']));
                    }

                    $phn = trim($rs->fields['private_herd_number']);
                    $rightTattoo = trim($rs->fields['right_tattoo']);
                    $leftTattoo = trim($rs->fields['left_tattoo']);

                    // LRM 07.13.2017: checking what the type of pregnancy was to get a date or date range
                    $serviceDateAI  = "";
                    $exposureDate   = "";
                    $serviceDateET  = "";
                    $exposureBegin  = $rs->fields['begin_date'];
                    $exposureEnd    = $rs->fields['end_date'];
                    $type           = $rs->fields['type'];
                    if (trim($exposureEnd) !== "") {
                        $exposureDate = formatDate($exposureBegin) . " - " . formatDate($exposureEnd);
                    } else {
                        $exposureDate = formatDate($exposureBegin) . " - Present";
                    }
                    if ($rs->fields['ai_date'] !== "") {
                        $serviceDateAI = formatDate($rs->fields['ai_date']);
                    }
                    if ($rs->fields['transfer_date'] !== "") {
                        $serviceDateET = formatDate($rs->fields['transfer_date']);
                    }

                    // Build the pdf
                    // LRM 07.14.2017: Start by adding the initial spacer and setting the height to 5
                    // Height is important for setting the headers for auto-creating a new page with data
                    $pdf->Cell(1,5,"",'',0,'C');

                    // LRM 07.14.2017:
                    // Get the Y location of the page and if it is in the first row after a new page
                    // Set these headers before moving on with the next row to add
                    // Needs to go after the first Cell
                    if ($pdf->GetY() == 10) {
                        $pdf->SetFont('Arial', '', 7);
                        $pdf->Cell(161, 5, "", '', 0, 'C');
                        $pdf->Cell(35, 5, "Date Printed: " . formatDate(date("m/d/Y")), '', 1, 'R');

                        // Set the headers
                        $pdf->SetFont('Arial', 'B', 7);
                        $pdf->Cell(1, 5, "", 'R', 0, 'C');
                        $pdf->Cell(5, 5, "", 'LTRB', 0, 'C');
                        $pdf->Cell(40, 5, "Dam", 'LTRB', 0, 'C');
                        $pdf->Cell(75, 5, "Location Details", 'LTRB', 0, 'C');
                        $pdf->Cell(25, 5, "Sir", 'LTRB', 0, 'C');
                        $pdf->Cell(50, 5, "Palpation", 'LTRB', 1, 'C');

                        // Set the sub-headers
                        $pdf->Cell(1, 5, "", 'R', 0, 'C');
                        $pdf->Cell(5, 5, "#", 'LRTB', 0, 'C');
                        $pdf->Cell(25, 5, "Tattoo", 'LRBT', 0, 'C');
                        $pdf->Cell(15, 5, "EID", 'LRBT', 0, 'C');
                        $pdf->Cell(30, 5, "Pasture", 'LRBT', 0, 'C');
                        $pdf->Cell(35, 5, "Date Of Exposure Or Service", 'LRBT', 0, 'C');
                        $pdf->Cell(10, 5, "Type", 'LRBT', 0, 'C');
                        $pdf->Cell(25, 5, "Tattoo", 'LRBT', 0, 'C');
                        $pdf->Cell(25, 5, "Date", 'LRBT', 0, 'C');
                        $pdf->Cell(25, 5, "Result", 'LRBT', 0, 'C');
                        $pdf->Cell(1, 5, "", 'L', 1, 'C');

                        $pdf->Cell(1, 5, "", 'R', 0, 'C');
                    }

                    $pdf->SetFont('Arial','',7);
                    $pdf->Cell(5,5,$row,'LRTB',0,'C');

                    // LRM 07/12/2017:
                    // Check if the LE or RE tattoo have a value for the DAM.
                    if ($rightTattoo !== "" || $leftTattoo !== "") {
                        if ($rightTattoo !== "" && $leftTattoo === "") {
                            $pdf->Cell(25, 5, $rightTattoo . "[RE]", 'LRBT', 0, 'L', $fillCell);
                        } elseif ($rightTattoo === "" && $leftTattoo !== "") {
                            $pdf->Cell(25, 5, $leftTattoo . "[LE]", 'LRBT', 0, 'L', $fillCell);
                        } elseif ($rightTattoo !== "" && $leftTattoo !== "") {
                            if ($rightTattoo !== $leftTattoo) {
                                $pdf->Cell(25, 5, $leftTattoo . "[LE] " . $rightTattoo . "[RE]", 'LRBT', 0, 'L', $fillCell);
                            } elseif ($rightTattoo === $leftTattoo) {
                                $pdf->Cell(25, 5, $rightTattoo . "[BE]", 'LTRB', 0, 'L');
                            }
                        }
                    } else {
                        $pdf->Cell(25, 5, $phn, 'LRBT', 0, 'L', $fillCell);
                    }

                    $pdf->Cell(15,5, $rs->fields['eid'],'LTRB',0,'C');
                    $pdf->Cell(30,5, $pasture_name,'LTRB',0,'C');

                    // LRM 07.13,2017: checking what we will print for the dates / service dates
                    if ($type == "NS") {
                        $pdf->Cell(35, 5, $exposureDate, 'LRTB', 0, 'C');
                    } elseif ($type == "AI") {
                        $pdf->Cell(35, 5, $serviceDateAI, 'LRTB', 0, 'C');
                    } elseif ($type == "ET") {
                        $pdf->Cell(35, 5, $serviceDateET, 'LTRB', 0, 'C');
                    }

                    $pdf->Cell(10, 5, $type, 'LRBT', 0, 'C');

                    // LRM 07/12/2017:
                    // Check if the LE or RE tattoo have a value for the SIRE.
                    if ($animal_sire_right_tattoo !== "" || $animal_sire_left_tattoo !== "") {
                        if ($animal_sire_right_tattoo !== "" && $animal_sire_left_tattoo === "") {
                            $pdf->Cell(25, 5, $animal_sire_right_tattoo . "[RE]", 'LRBT', 0, 'L', $fillCell);
                        } elseif ($animal_sire_right_tattoo === "" && $animal_sire_left_tattoo !== "") {
                            $pdf->Cell(25, 5, $animal_sire_left_tattoo . "[LE]", 'LRBT', 0, 'L', $fillCell);
                        } elseif ($animal_sire_right_tattoo !== "" && $animal_sire_left_tattoo !== "") {
                            if ($animal_sire_right_tattoo !== $animal_sire_left_tattoo) {
                                $pdf->Cell(25, 5, $animal_sire_left_tattoo . "[LE]" . $animal_sire_right_tattoo . "[RE]", "LTRB", 0, 'L');
                            } elseif ($animal_sire_right_tattoo === $animal_sire_left_tattoo) {
                                $pdf->Cell(25, 5, $animal_sire_right_tattoo . "[BE]", 'LTRB', 0, 'L');
                            }
                        }
                    } else {
                        $pdf->Cell(25, 5, $animal_sire_phn, 'LRBT', 0, 'L', $fillCell);
                    }

                    $pdf->Cell(25,5,formatDate($rs->fields['palpation_date']),'LTRB',0,'C');
                    $pdf->Cell(25,5,$palpation_result,'LTRB',1,'C');

                    if($rs->fields['palpation_result'] == 0
                        || $rs->fields['palpation_result'] == '') {
                        $total_open++;
                    } else {
                        $total_pregnant++;
                    }

                    $rs->MoveNext();
                }
            }
            $rs->Close();
			
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(1,5,"",'',1,'C');
			$pdf->Cell(1,5,"",'R',0,'C');	
			$pdf->Cell(40,5,"Total Open:",'LBT',0,'C');
			$pdf->SetFont('Arial','',7);	
			$pdf->Cell(10,5,$total_open,'RBT',0,'C');
			
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(40,5,"Total Pregnant:",'LBT',0,'C');	
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(10,5,$total_pregnant,'RBT',0,'C');
				
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(40,5,"Total Palpated:",'LBT',0,'C');	
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(10,5,($total_open+$total_pregnant),'RBT',1,'C');	
			$pdf->Cell(1,5,"",'',1,'C');
			
			// BMC 10.21.2016
			//	-- need to add a key
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(1,5,"",'',0,'C');	
			$pdf->Cell(40,5,"Key [Type]:",'LRBT',1,'L');
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(1,5,"",'',0,'C');
			$pdf->Cell(40,5,"NS = Natural Service",'LR',1,'L');
			$pdf->Cell(1,5,"",'',0,'C');
			$pdf->Cell(40,5,"AI = Artificial Insemination",'LR',1,'L');
			$pdf->Cell(1,5,"",'',0,'C');
			$pdf->Cell(40,5,"ET = Embryo Transfer",'LRB',1,'L');
			
			// create export directory, name, and url
			$exportDir = getExportDirectory($provider, $member_id);
			$timestamp = time();
			
			$exportName = "pregnancy_summary_app_".$member_id."_".$year.".pdf";
			$exportDir = $exportDir . "pregnancy_summary_app_".$member_id."_".$year.".pdf";
			
			if($PDO->DB_TYPE == "NEW") {
				// for the new databases we have to look in the members folder for the 
				// correct pdf directory
				$exportUrl 	= $PDO->FILE_DIR . $member_id . "/" . $exportName;
			} else if($PDO->DB_TYPE == "OLD") {
				$exportUrl 	= $PDO->FILE_DIR . $exportName;
			}
			
			// export the file
			$pdf->Output($exportDir);
			
			// If file name already exists, then update it!
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT * 
						FROM reports 
						WHERE report_title = ? 
							AND report_file_name = ?
							AND member_id = ?";
				$params = array($report_title, $exportName, $member_id);
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT * 
						FROM tbl_member_reports_android 
						WHERE report_type = ? 
							AND report_file_name= ?
							AND create_user = ?";
				$params = array($report_title, $exportName, $member_id);
			}
			$rsCheckDuplicate = $PDO->recordSetQuery($sql, $params);
			if($rsCheckDuplicate) {
				if(!$rsCheckDuplicate->EOF) {
					$rsCheckDuplicate->MoveFirst();
					// Update
					if($PDO->DB_TYPE == "NEW") {
						$sql = "UPDATE reports 
								SET report_date = ?
								WHERE member_id = ?
									AND report_title = ?
									AND report_file_name = ?
									AND report_format = 'P'";
						$params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName);
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "UPDATE tbl_member_reports_android 
								SET create_stamp= ?
								WHERE member_id= ?
									AND report_type= ?
									AND report_file_name= ?
									AND report_format='P'
									AND create_user= ?";
						$params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName, $member_id);
					}
					$PDO->executeQuery($sql, $params);
				} else {
					// INSERT
					if($PDO->DB_TYPE == "NEW") {
						$sql = "INSERT INTO reports 
									(member_id, report_title, report_file_name, report_format, report_date) 
								VALUES 
									(?, ?, ?, ?, ?)";
						$params = array($member_id, $report_title, $exportName, 'P', date('Y-m-d h:i:s'));
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "INSERT INTO tbl_member_reports_android 
									(member_id, report_type, report_file_name, report_format, create_user, 
									create_stamp) 
								VALUES 
									(?, ?, ?, ?, ?, ?)";
						$params = array($member_id, $report_title, $exportName, 'P', $member_id, date('Y-m-d h:i:s'));
					}
					$PDO->executeQuery($sql, $params);
				}
			}
		
			// send back the file name and the pdf url as the result
			$response["success"] = true;
			$response["pdf_url"] = $exportUrl;	// send back the url
			$response["message"] = "successfully generated pdf";
			echo json_encode($response);
		} else {
			$response["success"] = false;
			$response["message"] = "you have no pregnancy data available";
			die(json_encode($response));
		}
	} else {
		$response["success"] = false;
		$response["message"] = "sql error";
		die(json_encode($response));
	}
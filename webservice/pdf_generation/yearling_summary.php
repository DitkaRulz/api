<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}

    header('Access-Control-Allow-Origin: *');
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	function formatSex($sex) {
		switch(strtoupper(trim($sex))) {
			case "C": return "Cow";
			case "B": return "Bull";
			case "S": return "Steer";
			default: return "N/A";
		}
	}
	
	function formatDate($date) {
		// BMC 02.01.2016
		//	This function will take in a date, most likely formatted 
		//	like YYYY-MM-DD and change the format to MM DD, YYYY
		//	as an example, Jan 01, 2016
		// BMC 02.03.2016
		//	If the date comes in as blank or null then this will display
		//	Dec 31, 1969!!! So we need to account for these input and
		//	display the proper output.  For now, we'll display nothing, just
		//	a blank string.
		
		if(trim($date) == "" 
			|| trim($date) == "null") {
			return "";
		} else {
			$date = strtotime($date);
			return date("M d, Y", $date);
		}
	}
	
	## -------------------- GENERATE PDF -------------------- ##
	$response["pdf_url"] = "";
	
	$MemberName = "";
	$YearDate	= "";
	$YearCount  = 0;
	
	$report_title = "Yearling Summary ".formatDate($start_date)." to ".formatDate($end_date);

	//** ------------------------------------ BUILD REPORT HERE ----------------------------------- **//
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT a.registration, 
					a.private_herd_number, 
					a.sex, 
					a.sire, 
					y.year_date, 
					y.weight AS year_weight, 
					y.weight_adj AS year_weight_adj, 
					p.pasture_name, 
					b.birth_date,
					w.wean_date, 
					w.weight AS wean_weight, 
					w.weight_adj AS wean_weight_adj,
					(y.year_date - b.birth_date) AS animal_age
			FROM animal_year y
				INNER JOIN animal a on y.registration = a.registration 
				INNER JOIN animal_birth b on b.registration = a.registration
                INNER JOIN ownership o ON y.registration = o.registration
				LEFT JOIN animal_wean w on w.registration = a.registration
                LEFT JOIN animal_location l on y.registration = l.registration
                	AND (l.move_in <= y.year_date
                    	AND (l.move_out IS NULL
                        	OR l.move_out >= y.year_date))
				LEFT JOIN pasture p on l.pasture_id = p.pasture_id
			WHERE o.member_id = ?
				AND y.year_date >= DATE(?)
				AND y.year_date <= DATE(?)
            	AND o.date_owned <= y.year_date 
			ORDER BY 
			    p.pasture_name ASC,
			    b.birth_date ASC";
		$params = array($member_id, $start_date, $end_date);
	} else if($PDO->DB_TYPE == "OLD") {
	    switch($provider) {
            case "ACRS_CATTLE":
            case "AMARS_CATTLE":
                $sql = "SELECT a.animal_registration AS registration, 
                            CASE 
                                WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le
                                WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re
                                ELSE ''
                            END AS private_herd_number, 
                            a.animal_sex AS sex, 
                            a.animal_sire AS sire, 
                            y.year_date, 
                            y.year_weight, 
                            y.year_weight_adj, 
                            p.pasture_id AS pasture_name, 
                            b.birth_date,
                            w.wean_date, 
                            w.wean_weight, 
                            w.wean_weight_adj,
                            (y.year_date - b.birth_date) AS animal_age
                        FROM tbl_animal_data_year y
                            INNER JOIN tbl_animal a on y.animal_registration = a.animal_registration 
                            INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
                            LEFT JOIN tbl_animal_data_wean w on w.animal_registration = a.animal_registration
                            LEFT JOIN tbl_pastures p on p.pk_id::varchar = y.pasture_id
                            INNER JOIN tbl_ownership o ON y.animal_registration = o.animal_registration
                        WHERE o.owner_id = ?
                            AND y.year_date >= DATE(?)
                            AND y.year_date <= DATE(?)
                            AND o.ownership_date <= y.year_date
                        GROUP BY a.animal_registration, 
                            a.tattoo_le,
                            a.tattoo_re, 
                            a.animal_sex, 
                            a.animal_sire, 
                            y.year_date, 
                            y.year_weight, 
                            y.year_weight_adj, 
                            p.pasture_id, 
                            b.birth_date,
                            w.wean_date, 
                            w.wean_weight, 
                            w.wean_weight_adj
                        ORDER BY 
                            p.pasture_id ASC,
                            b.birth_date ASC";
                $params = array($member_id, $start_date, $end_date);
            break;
            default:
                $sql = "SELECT a.animal_registration AS registration, 
                            a.animal_private_herd_id AS private_herd_number, 
                            a.animal_sex AS sex, 
                            a.animal_sire AS sire, 
                            y.year_date, 
                            y.year_weight, 
                            y.year_weight_adj, 
                            p.pasture_id AS pasture_name, 
                            b.birth_date,
                            w.wean_date, 
                            w.wean_weight, 
                            w.wean_weight_adj,
                            (y.year_date - b.birth_date) AS animal_age
                        FROM tbl_animal_data_year y
                            INNER JOIN tbl_animal a on y.animal_registration = a.animal_registration 
                            INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
                            LEFT JOIN tbl_animal_data_wean w on w.animal_registration = a.animal_registration
                            LEFT JOIN tbl_pastures p on p.pk_id::varchar = y.pasture_id
                            INNER JOIN tbl_ownership o ON y.animal_registration = o.animal_registration
                        WHERE o.owner_id = ?
                            AND y.year_date >= DATE(?)
                            AND y.year_date <= DATE(?)
                            AND o.ownership_date <= y.year_date
                        GROUP BY a.animal_registration, 
                            a.animal_private_herd_id, 
                            a.animal_sex, 
                            a.animal_sire, 
                            y.year_date, 
                            y.year_weight, 
                            y.year_weight_adj, 
                            p.pasture_id, 
                            b.birth_date,
                            w.wean_date, 
                            w.wean_weight, 
                            w.wean_weight_adj
                        ORDER BY 
                            p.pasture_id ASC,
                            b.birth_date ASC";
                $params = array($member_id, $start_date, $end_date);
            break;
        }
	}
	$rs = $PDO->recordSetQuery($sql, $params);
	if($rs) {
		// BUILD PDF
		if($rs->Recordcount() > 0) {
			// ********** CREATE THE PDF ********** //
			$pdf = new PDF(); 
			//$pdf->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
			
			// Build PDF
			$requested_font	= 'Times';
			$pdf->AliasNbPages();
			$pdf->SetTopMargin(10);
			$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
			$pdf->ZeroCount();
			$pdf->SetFont('Times','B', 10);
			
			$i=0;	// not sure about this
			
			// get the calf count
			$YearCount = $rs->Recordcount();
			
			// Get the member name
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT member_name 
						FROM member 
						WHERE member_id = ?";
				$params = array($member_id);
			} else if($PDO->DB_TYPE == "OLD") { 
				$sql = "SELECT member_name 
						FROM tbl_member 
						WHERE member_id= ?";
				$params = array($member_id);
			}
			
			$rsMember = $PDO->recordSetQuery($sql, $params);
			if($rsMember) {
				if(!$rsMember->EOF) {
					$rsMember->MoveFirst();
					
					$MemberName = $rsMember->fields['member_name'];
				} else {
					$MemberName = $member_id;
				}
				
				$rsMember->Close();
			} else {
				$MemberName = $member_id;
			}

            // BMC 11.07.2016
            //  -- get the member details
            $memberDetails = "";
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT c.address_line_1,
                            c.address_line_2,
                            c.city,
                            c.state,
                            c.zip_code,
                            c.phone_number
                        FROM member m
                            LEFT JOIN contact c ON m.member_id = c.member_id
                                AND c.is_primary
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line_1'];
                        $address_line_2 = $rsContact->fields['address_line_2'];
                        $city = $rsContact->fields['city'];
                        $state = $rsContact->fields['state'];
                        $zip_code = $rsContact->fields['zip_code'];
                        //$phone_number = $rsContact->fields['phone_number'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                        //$phone_number = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT a.address_line1,
                            a.address_line2,
                            a.address_city,
                            a.address_state,
                            a.address_postal_code
                        FROM tbl_member m
                            LEFT JOIN tbl_address a ON m.general_correspondance_address = a.address_id
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line1'];
                        $address_line_2 = $rsContact->fields['address_line2'];
                        $city = $rsContact->fields['address_city'];
                        $state = $rsContact->fields['address_state'];
                        $zip_code = $rsContact->fields['address_postal_code'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else {
                $memberDetails = "";
            }
			
			// build the title
			$pdf->Image(SITE_ROOT . '/images/digital_beef_logo_no_text_tm.png', 10, 10, 35);
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,5,"Digital Beef, LLC", '', 1, 'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(188,12,$memberDetails, '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,7.5,"Yearling Summary ".formatDate($start_date)." to ".formatDate($end_date), '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(188,7.5, "Total Worked: ".$YearCount, '', 1, 'C');
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(188, 8, $MemberName."\n".$member_id, '', 1, 'C');
			$pdf->Cell(188, 6, "", '', 1, 'C');
			
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(4,5,"",'R',0,'C');
			$pdf->Cell(5, 5, "#", 'LTRB', 0, 'C');
			$pdf->Cell(20,5,"PHN",'LRBT',0,'C');				
			$pdf->Cell(20,5,"Age (Days)",'LRBT',0,'C');		
			$pdf->Cell(20,5,"D.O.B",'LRBT',0,'C');	
			$pdf->Cell(20,5,"Year Date",'LRBT', 0, 'C');				
			$pdf->Cell(20,5,"Yearling Weight",'LRBT',0,'C');	
			$pdf->Cell(20,5,"ADG",'LRBT',0,'C');		
			$pdf->Cell(20,5,"Adj. Weight",'LRBT',0,'C');		
			$pdf->Cell(20,5,"WDA",'LRBT',0,'C');			
			$pdf->Cell(20,5,"Pasture",'LRBT',0,'C');	
			$pdf->Cell(4,5,"",'L',1,'C');
			
			// Get animal data
			if(!$rs->EOF) {	
				$animal_ADG = "";
				$animal_WDA = "";
				$rs->MoveFirst();
				$row = 0;
				while(!$rs->EOF) {
				    $row++;
					// get the sire phn
					$animal_sire 	= $rs->fields['sire'];
					if($PDO->DB_TYPE == "NEW") {
						$sql = "SELECT private_herd_number 
								FROM animal 
								WHERE registration = ?";
						$params = array($animal_sire);
					} else if($PDO->DB_TYPE == "OLD") {
                        switch($provider) {
                            case "ACRS_CATTLE":
                            case "AMARS_CATTLE":
                                $sql = "SELECT 
                                        CASE 
                                            WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le
                                            WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re
                                            ELSE ''
                                        END AS private_herd_number
                                    FROM 
                                        tbl_animal 
                                    WHERE 
                                        animal_registration = ?";
                                $params = array($animal_sire);
                                break;
                            default;
                                $sql = "SELECT 
                                        animal_private_herd_id AS private_herd_number
                                    FROM 
                                        tbl_animal 
                                    WHERE 
                                        animal_registration = ?";
                                $params = array($animal_sire);
                                break;
                        }
					}
					$rsSire = $PDO->recordSetQuery($sql, $params);
					if($rsSire) {
						if(!$rsSire->EOF) {
							$rsSire->MoveFirst();
							
							$animal_sire = $rsSire->fields['private_herd_number'];
						}
						$rsSire->Close();
					}
					
					// Calculate ADG and WDA
					$birth_date = strtotime($rs->fields['birth_date']);
					$wean_date = strtotime($rs->fields['wean_date']);
					$year_date = strtotime($rs->fields['year_date']);
		
					// ADG = (Year Wt. – Weaning Wt.) ÷ Number of Days Between Weights
					$datediff = $year_date - $wean_date;
					$days_between_weights = floor($datediff/(60*60*24));
					$animal_ADG = ($rs->fields['year_weight'] - $rs->fields['wean_weight']) / $days_between_weights;
					
					// WDA = Year Weight ÷ Calf Age
					$datediff = $year_date - $birth_date;
					$calf_age = floor($datediff/(60*60*24));
					$animal_WDA = $rs->fields['year_weight'] / $calf_age;
					
					// Set Font
					$pdf->SetFont('Arial','',7);
					$pdf->Cell(4,5,"",'R',0,'C');
					$pdf->Cell(5, 5, $row, 'LTRB', 0, 'C');
					$pdf->Cell(20,5,$rs->fields['private_herd_number'],'LTRB',0,'C');
					$pdf->Cell(20,5,$rs->fields['animal_age'],'LTRB',0,'C');
					$pdf->Cell(20,5,formatDate($rs->fields['birth_date']),'LTRB',0,'C');
					$pdf->Cell(20,5,formatDate($rs->fields['year_date']),'LRBT', 0, 'C');
					$pdf->Cell(20,5,$rs->fields['year_weight'],'LTRB',0,'C');
					$pdf->Cell(20,5,round($animal_ADG, 2),'LTRB',0,'C');
					$pdf->Cell(20,5,$rs->fields['year_weight_adj'],'LTRB',0,'C');
					$pdf->Cell(20,5,round($animal_WDA, 2),'LTRB',0,'C');
					$pdf->Cell(20,5,$rs->fields['pasture_name'],'LTRB',0,'C');
					$pdf->Cell(4,5,"",'L',1,'C');
					
					$rs->MoveNext();
				}
				
				// Now get totals and averages.
				// First set up header table
				// Page Break
				
				$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
				
				$pdf->SetFont('Arial','B',7);
				$pdf->Cell(1,5,"",'R',0,'C');
				$pdf->Cell(5, 5, "#", 'LTRB', 0, 'C');
				$pdf->Cell(30,5,"Sire",'LRBT',0,'C');		
				$pdf->Cell(30,5,"Animal Count",'LRBT',0,'C');			
				$pdf->Cell(30,5,"Year Weight Average",'LRBT',0,'C');		
				$pdf->Cell(30,5,"Year Weight Adj. Average",'LRBT',0,'C');
				$pdf->Cell(1,5,"",'L',1,'C');
				
				if($PDO->DB_TYPE == "NEW") {
					$sql = "SELECT a.sire
							FROM animal_year y
								INNER JOIN animal a on y.registration = a.registration 
								INNER JOIN ownership o ON y.registration = o.registration
							WHERE o.member_id = ? 
								AND o.date_owned <= y.year_date
								AND y.year_date >= DATE(?)
								AND y.year_date <= DATE(?)
							GROUP BY a.sire";
					$params = array($member_id, $start_date, $end_date);
				} else if ($PDO->DB_TYPE == "OLD") {
					$sql = "SELECT a.animal_sire AS sire
							FROM tbl_animal_data_year y
								INNER JOIN tbl_animal a on y.animal_registration = a.animal_registration 
								INNER JOIN tbl_ownership o ON y.animal_registration = o.animal_registration
							WHERE o.owner_id = ?
								AND o.ownership_date <= y.year_date 
								AND y.year_date >= DATE(?)
								AND y.year_date <= DATE(?)
							GROUP BY a.animal_sire";
					$params = array($member_id, $start_date, $end_date);
				}
				$rsSireGroup = $PDO->recordSetQuery($sql, $params);
				if($rsSireGroup) {
					if(!$rsSireGroup->EOF) {
						$rsSireGroup->MoveFirst();
						$animal_sire = "";
						$row = 0;
						while(!$rsSireGroup->EOF) {
						    $row++;
							// get the sire phn
							$animal_sire 	= $rsSireGroup->fields['sire'];
							if($PDO->DB_TYPE == "NEW") {
								$sql = "SELECT private_herd_number 
										FROM animal 
										WHERE registration = ?";
								$params = array($animal_sire);
							} else if($PDO->DB_TYPE == "OLD") {
                                switch($provider) {
                                    case "ACRS_CATTLE":
                                    case "AMARS_CATTLE":
                                        $sql = "SELECT 
                                        CASE 
                                            WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le
                                            WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re
                                            ELSE ''
                                        END AS private_herd_number
                                    FROM 
                                        tbl_animal 
                                    WHERE 
                                        animal_registration = ?";
                                        $params = array($animal_sire);
                                        break;
                                    default;
                                        $sql = "SELECT 
                                        animal_private_herd_id AS private_herd_number
                                    FROM 
                                        tbl_animal 
                                    WHERE 
                                        animal_registration = ?";
                                        $params = array($animal_sire);
                                        break;
                                }
							}
							$rsSire = $PDO->recordSetQuery($sql, $params);
							if($rsSire) {
								if(!$rsSire->EOF) {
									$rsSire->MoveFirst();
									
									$animal_sire = $rsSire->fields['private_herd_number'];
								}
								$rsSire->Close();
							}
							
							// GET AVG YearWeight and YearWeightAdj
							if($PDO->DB_TYPE == "NEW") {
								$sql = "SELECT y.weight AS year_weight, 
										y.weight_adj AS year_weight_adj
									FROM animal_year y
										INNER JOIN animal a on y.registration = a.registration 
										INNER JOIN ownership o ON y.registration = o.registration
									WHERE o.member_id = ? 
										AND o.date_owned <= y.year_date
										AND y.year_date >= DATE(?)
										AND y.year_date <= DATE(?)
										AND a.sire = ?";
								$params = array($member_id, $start_date, $end_date, $rsSireGroup->fields['sire']);
							} else if($PDO->DB_TYPE == "OLD") {
								$sql = "SELECT y.year_weight, 
											y.year_weight_adj
										FROM tbl_animal_data_year y
											INNER JOIN tbl_animal a on y.animal_registration = a.animal_registration 
											INNER JOIN tbl_ownership o ON y.animal_registration = o.animal_registration
										WHERE o.owner_id = ?
											AND o.ownership_date <= y.year_date 
											AND y.year_date >= DATE(?)
											AND y.year_date <= DATE(?)
											AND a.animal_sire= ?";
								$params = array($member_id, $start_date, $end_datem, $rsSireGroup->fields['sire']);
							}
							$rsData = $PDO->recordSetQuery($sql, $params);
							if($rsData) {
								$year_weight = 0;
								$year_weight_adj = 0;
								
								if(!$rsData->EOF) {
									$total_animals = $rsData->Recordcount();
									
									$rsData->MoveFirst();
									while(!$rsData->EOF) {
										$year_weight 		= $year_weight + $rsData->fields['year_weight'];
										$year_weight_adj 	= $year_weight_adj + $rsData->fields['year_weight_adj'];
										
										$rsData->MoveNext();
									}
									
									$year_weight 		= $year_weight / $total_animals;
									$year_weight_adj 	= $year_weight_adj / $total_animals;
									
									$rsData->Close();
								} else {
									$year_weight = "0";
									$year_weight = "0";
								}
							}
						
							// If the sire PHN is not blank, then print it
							if($animal_sire != "") {
								$pdf->SetFont('Arial','',7);
								$pdf->Cell(1,5,"",'R',0,'C');
                                $pdf->Cell(5, 5, $row, 'LTRB', 0, 'C');
								$pdf->Cell(30,5,$animal_sire,'LRBT',0,'C');		
								$pdf->Cell(30,5,$total_animals,'LRBT',0,'C');			
								$pdf->Cell(30,5,round($year_weight, 2),'LRBT',0,'C');		
								$pdf->Cell(30,5,round($year_weight_adj, 2),'LRBT',0,'C');
								$pdf->Cell(1,5,"",'L',1,'C');
							}
							
							$rsSireGroup->MoveNext();
						}
					}
				}
				
				// create export directory, name, and url
				$exportDir = getExportDirectory($provider, $member_id);
				$timestamp = time();
				
				$exportName = "yearling_summary_app_".$member_id."_".$start_date."_to_".$end_date.".pdf";
				$exportDir = $exportDir . "yearling_summary_app_".$member_id."_".$start_date."_to_".$end_date.".pdf";
				
				if($PDO->DB_TYPE == "NEW") {
					// for the new databases we have to look in the members folder for the 
					// correct pdf directory
					$exportUrl 	= $PDO->FILE_DIR . $member_id . "/" . $exportName;
				} else if($PDO->DB_TYPE == "OLD") {
					$exportUrl 	= $PDO->FILE_DIR . $exportName;
				}
				
				// export the file
				$pdf->Output($exportDir);
				
				// If file name already exists, then update it!
				if($PDO->DB_TYPE == "NEW") {
					$sql = "SELECT * 
							FROM reports 
							WHERE report_title = ? 
								AND report_file_name = ?
								AND member_id = ?";
					$params = array($report_title, $exportName, $member_id);
				} else if($PDO->DB_TYPE == "OLD") {
					$sql = "SELECT * 
							FROM tbl_member_reports_android 
							WHERE report_type = ? 
								AND report_file_name= ?
								AND create_user = ?";
					$params = array($report_title, $exportName, $member_id);
				}
				$rsCheckDuplicate = $PDO->recordSetQuery($sql, $params);
				if($rsCheckDuplicate) {
					if(!$rsCheckDuplicate->EOF) {
						$rsCheckDuplicate->MoveFirst();
						// Update
						if($PDO->DB_TYPE == "NEW") {
							$sql = "UPDATE reports 
									SET report_date = ?
									WHERE member_id = ?
										AND report_title = ?
										AND report_file_name = ?
										AND report_format = 'P'";
							$params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName);
						} else if($PDO->DB_TYPE == "OLD") {
							$sql = "UPDATE tbl_member_reports_android 
									SET create_stamp= ?
									WHERE member_id= ?
										AND report_type= ?
										AND report_file_name= ?
										AND report_format='P'
										AND create_user= ?";
							$params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName, $member_id);
						}
						$PDO->executeQuery($sql, $params);
					} else {
						// INSERT
						if($PDO->DB_TYPE == "NEW") {
							$sql = "INSERT INTO reports 
										(member_id, report_title, report_file_name, report_format, report_date) 
									VALUES 
										(?, ?, ?, ?, ?)";
							$params = array($member_id, $report_title, $exportName, 'P', date('Y-m-d h:i:s'));
						} else if($PDO->DB_TYPE == "OLD") {
							$sql = "INSERT INTO tbl_member_reports_android 
										(member_id, report_type, report_file_name, report_format, create_user, 
										create_stamp) 
									VALUES 
										(?, ?, ?, ?, ?, ?)";
							$params = array($member_id, $report_title, $exportName, 'P', $member_id, date('Y-m-d h:i:s'));
						}
						$PDO->executeQuery($sql, $params);
					}
				}
			
				// send back the file name and the pdf url as the result
				$response["success"] = true;
				$response["pdf_url"] = $exportUrl;	// send back the url
				$response["message"] = "successfully generated pdf";
				echo json_encode($response);
			} else {
				$response["success"] = false;
				$response["message"] = "you have no yearling data available";
				die(json_encode($response));
			}
		} else {
			$response["success"] = false;
			$response["message"] = "you have no yearling data available sql error";
			die(json_encode($response));
		}
	} else {
		$response["success"] = false;
		$response["message"] = "sql error";
		die(json_encode($response));
	}
<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}

    header('Access-Control-Allow-Origin: *');
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	function formatSex($sex) {
		switch(strtoupper(trim($sex))) {
			case "C": return "Cow";
			case "B": return "Bull";
			case "S": return "Steer";
			default: return "N/A";
		}
	}
	
	function formatDate($date) {
		// BMC 02.01.2016
		//	This function will take in a date, most likely formatted 
		//	like YYYY-MM-DD and change the format to MM DD, YYYY
		//	as an example, Jan 01, 2016
		// BMC 02.03.2016
		//	If the date comes in as blank or null then this will display
		//	Dec 31, 1969!!! So we need to account for these input and
		//	display the proper output.  For now, we'll display nothing, just
		//	a blank string.
		
		if(trim($date) == "" 
			|| trim($date) == "null") {
			return "";
		} else {
			$date = strtotime($date);
			return date("M d, Y", $date);
		}
	}
	
	## -------------------- GENERATE PDF -------------------- ##
	$response["pdf_url"] = "";
	
	$MemberName = "";
	$YearDate	= "";
	
	$report_title = "ET Breeding Summary ".$year;

	//** ------------------------------------ BUILD REPORT HERE ----------------------------------- **//
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT 
                    a.private_herd_number,
					a.eid, 
					br.sire,
					br.transfer_date,
					p.pasture_name,
					a.left_tattoo,
					a.right_tattoo
				FROM 
				    animal a 
					INNER JOIN animal_birth b on b.registration = a.registration
					INNER JOIN animal_breeding br ON br.dam = a.registration
						AND type = 'ET'
					INNER JOIN ownership o ON a.registration = o.registration
					LEFT JOIN animal_location l on l.registration = a.registration
						AND (l.move_in <= br.transfer_date
						  AND (l.move_out IS NULL
							  OR l.move_out >= br.transfer_date))
					LEFT JOIN pasture p on l.pasture_id = p.pasture_id
				WHERE 
				    (br.transfer_date - b.birth_date) > 720
					AND o.member_id = ?
					AND o.date_owned <= br.transfer_date
					AND(br.transfer_date >= DATE(?) 
						AND br.transfer_date <= DATE(?))
				GROUP BY 
				    a.private_herd_number,
					a.eid, 
					br.sire,
					br.transfer_date,
					p.pasture_name,
					a.left_tattoo,
					a.right_tattoo
				ORDER BY 
				    br.transfer_date ASC";
		$params = array($member_id, $year.'-01-01', $year.'-12-31');
	} else if($PDO->DB_TYPE == "OLD") {
	    switch($provider) {
            case "ACRS_CATTLE":
            case "AMARS_CATTLE":
                $sql = "SELECT 
                            CASE 
                                WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le
                                WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re
                                ELSE ''
                            END AS private_herd_number,
                            a.eid, 
                            br.bull_registration AS sire,
                            br.transfer_date,
                            pAi.pasture_id AS pasture_name
                        FROM 
                            tbl_animal a 
                            INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
                            INNER JOIN tbl_breeding_et br ON br.cow_registration = a.animal_registration
                            LEFT JOIN tbl_animal_location l on l.animal_registration = a.animal_registration
                                AND (l.move_in_date <= br.transfer_date
                                  AND (l.move_out_date IS NULL
                                      OR l.move_out_date >= br.transfer_date))
                            LEFT JOIN tbl_pastures pAi on l.pasture_id = pAi.pk_id::varchar
                        WHERE 
                            (br.transfer_date - b.birth_date) >  720
                            AND br.cow_owner_id = ?
                            AND(br.transfer_date >= DATE(?) 
                                AND br.transfer_date <= DATE(?))
                        GROUP BY 
                            a.tattoo_le,
                            a.tattoo_re,
                            a.eid, 
                            br.bull_registration,
                            br.transfer_date,
                            pAi.pasture_id
                        ORDER BY 
                            br.transfer_date ASC";
                $params = array($member_id, $year.'-01-01', $year.'-12-31');
            break;
            default:
                $sql = "SELECT 
                            a.animal_private_herd_id AS private_herd_number,
                            a.eid, 
                            br.bull_registration AS sire,
                            br.transfer_date,
                            pAi.pasture_id AS pasture_name
                        FROM 
                            tbl_animal a 
                            INNER JOIN tbl_animal_data_birth b on b.animal_registration = a.animal_registration
                            INNER JOIN tbl_breeding_et br ON br.cow_registration = a.animal_registration
                            LEFT JOIN tbl_animal_location l on l.animal_registration = a.animal_registration
                                AND (l.move_in_date <= br.transfer_date
                                  AND (l.move_out_date IS NULL
                                      OR l.move_out_date >= br.transfer_date))
                            LEFT JOIN tbl_pastures pAi on l.pasture_id = pAi.pk_id::varchar
                        WHERE 
                            (br.transfer_date - b.birth_date) >  720
                            AND br.cow_owner_id = ?
                            AND(br.transfer_date >= DATE(?) 
                                AND br.transfer_date <= DATE(?))
                        GROUP BY 
                            a.animal_private_herd_id, a.eid, 
                            br.bull_registration,
                            br.transfer_date,
                            pAi.pasture_id
                        ORDER BY 
                            br.transfer_date ASC";
                $params = array($member_id, $year.'-01-01', $year.'-12-31');
            break;
        }
	}
	$rs = $PDO->recordSetQuery($sql, $params);
	if($rs) {
		if(!$rs->EOF) {
			// ********** CREATE THE PDF ********** //
			$pdf = new PDF(); 
			//$pdf->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
			
			// Build PDF
			$requested_font	= 'Times';
			$pdf->AliasNbPages();
			$pdf->SetTopMargin(10);
			$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
			$pdf->ZeroCount();
			$pdf->SetFont('Times','B', 10);
			
			$i=0;	// not sure about this
			
			// set the year
			$YearDate = $year;
			
			// get the cow count
			$TotalCount = $rs->Recordcount();
			
			// Get the member name
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT member_name 
						FROM member 
						WHERE member_id = ?";
				$params = array($member_id);
			} else if($PDO->DB_TYPE == "OLD") { 
				$sql = "SELECT member_name 
						FROM tbl_member 
						WHERE member_id= ?";
				$params = array($member_id);
			}
			
			$rsMember = $PDO->recordSetQuery($sql, $params);
			if($rsMember) {
				if(!$rsMember->EOF) {
					$rsMember->MoveFirst();
					
					$MemberName = $rsMember->fields['member_name'];
				} else {
					$MemberName = $member_id;
				}
				
				$rsMember->Close();
			} else {
				$MemberName = $member_id;
			}

            // BMC 11.07.2016
            //  -- get the member details
            $memberDetails = "";
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT c.address_line_1,
                            c.address_line_2,
                            c.city,
                            c.state,
                            c.zip_code,
                            c.phone_number
                        FROM member m
                            LEFT JOIN contact c ON m.member_id = c.member_id
                                AND c.is_primary
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line_1'];
                        $address_line_2 = $rsContact->fields['address_line_2'];
                        $city = $rsContact->fields['city'];
                        $state = $rsContact->fields['state'];
                        $zip_code = $rsContact->fields['zip_code'];
                        //$phone_number = $rsContact->fields['phone_number'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                        //$phone_number = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT a.address_line1,
                            a.address_line2,
                            a.address_city,
                            a.address_state,
                            a.address_postal_code
                        FROM tbl_member m
                            LEFT JOIN tbl_address a ON m.general_correspondance_address = a.address_id
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line1'];
                        $address_line_2 = $rsContact->fields['address_line2'];
                        $city = $rsContact->fields['address_city'];
                        $state = $rsContact->fields['address_state'];
                        $zip_code = $rsContact->fields['address_postal_code'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else {
                $memberDetails = "";
            }
			
			// build the title
			$pdf->Image(SITE_ROOT . '/images/digital_beef_logo_no_text_tm.png', 10, 10, 35);
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,5,"Digital Beef, LLC", '', 1, 'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(188,12,$memberDetails, '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,7.5,"ET Breeding Summary for ".formatDate($YearDate), '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(188,7.5, "Total Worked: ".$TotalCount, '', 1, 'C');
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(188, 8, $MemberName."\n".$member_id, '', 1, 'C');
			$pdf->Cell(188, 6, "", '', 1, 'C');

            // LRM 07.14.2017:
            // set the first page headers of the table
            $pdf->SetFont('Arial', '', 7);
            $pdf->Cell(145, 5, "", '', 0, 'C');
            $pdf->Cell(35, 5, "Date Printed: " . formatDate(date("m/d/Y")), '', 1, 'R');

            // Set the headers
            $pdf->SetFont('Arial', 'B', 7);
            $pdf->Cell(1,5,"",'R',0,'C');
            $pdf->Cell(5, 5, "#", 'LRTB', 0, 'C');
            $pdf->Cell(35,5,"Dam PHN",'LRBT',0,'C');
            $pdf->Cell(35,5,"EID",'LRBT',0,'C');
            $pdf->Cell(35,5,"Pasture",'LRBT',0,'C');
            $pdf->Cell(35,5,"Sire PHN",'LRBT',0,'C');
            $pdf->Cell(35,5,"AI Date",'LRBT',0,'C');
            $pdf->Cell(1,5,"",'L',1,'C');
			
			$rs->MoveFirst();
			$row = 0;
			while(!$rs->EOF) {
			    $row++;
				// get the sire phn
                $animal_sire                = $rs->fields['sire'];
                $animal_sire_phn            = "";
                $animal_sire_left_tattoo    = "";
                $animal_sire_right_tattoo   = "";
                if($PDO->DB_TYPE == "NEW") {
                    $sql = "SELECT 
                                private_herd_number,
                                left_tattoo,
                                right_tattoo
							FROM 
                                animal 
							WHERE 
							    registration = ?";
                    $params = array($animal_sire);
                } else if($PDO->DB_TYPE == "OLD") {
                    switch($provider) {
                        case "ACRS_CATTLE":
                        case "AMARS_CATTLE":
                            $sql = "SELECT 
                                        CASE 
                                            WHEN a.tattoo_le IS NOT NULL THEN a.tattoo_le
                                            WHEN a.tattoo_re IS NOT NULL THEN a.tattoo_re
                                            ELSE ''
                                        END AS private_herd_number
                                    FROM 
                                        tbl_animal 
                                    WHERE 
                                        animal_registration = ?";
                            $params = array($animal_sire);
                            break;
                        default;
                            $sql = "SELECT 
                                        animal_private_herd_id AS private_herd_number
                                    FROM 
                                        tbl_animal 
                                    WHERE 
                                        animal_registration = ?";
                            $params = array($animal_sire);
                            break;
                    }
                }
                $rsSire = $PDO->recordSetQuery($sql, $params);
                if($rsSire) {
                    if(!$rsSire->EOF) {
                        $rsSire->MoveFirst();

                        $animal_sire_phn            = trim($rsSire->fields['private_herd_number']);
                        $animal_sire_left_tattoo    = trim($rsSire->fields['left_tattoo']);
                        $animal_sire_right_tattoo   = trim($rsSire->fields['right_tattoo']);
                    }
                    $rsSire->Close();
                }

                $phn            = trim($rs->fields['private_herd_number']);
                $rightTattoo    = trim($rs->fields['right_tattoo']);
                $leftTattoo     = trim($rs->fields['left_tattoo']);

                // Build the pdf
                // LRM 07.14.2017: Start by adding the initial spacer and setting the height to 5
                $pdf->Cell(1,5,"",'',0,'C');

                // LRM 07.14.2017:
                // Get the Y location of the page and if it is in the first row after a new page
                if ($pdf->GetY() == 10) {
                    $pdf->SetFont('Arial', '', 7);
                    $pdf->Cell(145, 5, "", '', 0, 'C');
                    $pdf->Cell(35, 5, "Date Printed: " . formatDate(date("m/d/Y")), '', 1, 'R');

                    // Set the headers
                    $pdf->SetFont('Arial', 'B', 7);
                    $pdf->Cell(1,5,"",'R',0,'C');
                    $pdf->Cell(5, 5, "#", 'LRTB', 0, 'C');
                    $pdf->Cell(35,5,"Dam Tattoo",'LRBT',0,'C');
                    $pdf->Cell(35,5,"EID",'LRBT',0,'C');
                    $pdf->Cell(35,5,"Pasture",'LRBT',0,'C');
                    $pdf->Cell(35,5,"Sire Tattoo",'LRBT',0,'C');
                    $pdf->Cell(35,5,"AI Date",'LRBT',0,'C');
                    $pdf->Cell(1,5,"",'L',1,'C');

                    $pdf->Cell(1, 5, "", 'R', 0, 'C');
                }

                $pdf->SetFont('Arial','',7);
                $pdf->Cell(5,5,$row,'LRTB',0,'C');

                // LRM 07/12/2017:
                // Check if the LE or RE tattoo have a value for the DAM.
                if ($rightTattoo !== "" || $leftTattoo !== "") {
                    if ($rightTattoo !== "" && $leftTattoo === "") {
                        $pdf->Cell(35, 5, $rightTattoo . "[RE]", 'LRBT', 0, 'L', $fillCell);
                    } elseif ($rightTattoo === "" && $leftTattoo !== "") {
                        $pdf->Cell(35, 5, $leftTattoo . "[LE]", 'LRBT', 0, 'L', $fillCell);
                    } elseif ($rightTattoo !== "" && $leftTattoo !== "") {
                        if ($rightTattoo !== $leftTattoo) {
                            $pdf->Cell(35, 5, $leftTattoo . "[LE] " . $rightTattoo . "[RE]", 'LRBT', 0, 'L', $fillCell);
                        } elseif ($rightTattoo === $leftTattoo) {
                            $pdf->Cell(35, 5, $rightTattoo . "[BE]", 'LTRB', 0, 'L');
                        }
                    }
                } else {
                    $pdf->Cell(35, 5, $phn, 'LRBT', 0, 'L', $fillCell);
                }

                $pdf->Cell(35,5,$rs->fields['eid'],'LTRB',0,'C');
                $pdf->Cell(35,5,$rs->fields['pasture_name'],'LTRB',0,'C');

                // LRM 07/12/2017:
                // Check if the LE or RE tattoo have a value for the SIRE.
                if ($animal_sire_right_tattoo !== "" || $animal_sire_left_tattoo !== "") {
                    if ($animal_sire_right_tattoo !== "" && $animal_sire_left_tattoo === "") {
                        $pdf->Cell(35, 5, $animal_sire_right_tattoo . "[RE]", 'LRBT', 0, 'L', $fillCell);
                    } elseif ($animal_sire_right_tattoo === "" && $animal_sire_left_tattoo !== "") {
                        $pdf->Cell(35, 5, $animal_sire_left_tattoo . "[LE]", 'LRBT', 0, 'L', $fillCell);
                    } elseif ($animal_sire_right_tattoo !== "" && $animal_sire_left_tattoo !== "") {
                        if ($animal_sire_right_tattoo !== $animal_sire_left_tattoo) {
                            $pdf->Cell(35, 5, $animal_sire_left_tattoo . "[LE]" . $animal_sire_right_tattoo . "[RE]", "LTRB", 0, 'L');
                        } elseif ($animal_sire_right_tattoo === $animal_sire_left_tattoo) {
                            $pdf->Cell(35, 5, $animal_sire_right_tattoo . "[BE]", 'LTRB', 0, 'L');
                        }
                    }
                } else {
                    $pdf->Cell(35, 5, $animal_sire_phn, 'LRBT', 0, 'L', $fillCell);
                }

                $pdf->Cell(35,5,formatDate($rs->fields['transfer_date']),'LTRB',0,'C');
                $pdf->Cell(1,5,"",'L',1,'C');

                $rs->MoveNext();
			}
			$rs->Close();
	
			// create export directory, name, and url
			$exportDir = getExportDirectory($provider, $member_id);
			$timestamp = time();
			
			$exportName = "et_breeding_summary_app_".$member_id."_".$year.".pdf";
			$exportDir = $exportDir . "et_breeding_summary_app_".$member_id."_".$year.".pdf";
			
			if($PDO->DB_TYPE == "NEW") {
				// for the new databases we have to look in the members folder for the 
				// correct pdf directory
				$exportUrl 	= $PDO->FILE_DIR . $member_id . "/" . $exportName;
			} else if($PDO->DB_TYPE == "OLD") {
				$exportUrl 	= $PDO->FILE_DIR . $exportName;
			}
			
			// export the file
			$pdf->Output($exportDir);
			
			// If file name already exists, then update it!
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT * 
						FROM reports 
						WHERE report_title = ? 
							AND report_file_name = ?
							AND member_id = ?";
				$params = array($report_title, $exportName, $member_id);
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT * 
						FROM tbl_member_reports_android 
						WHERE report_type = ? 
							AND report_file_name= ?
							AND create_user = ?";
				$params = array($report_title, $exportName, $member_id);
			}
			$rsCheckDuplicate = $PDO->recordSetQuery($sql, $params);
			if($rsCheckDuplicate) {
				if(!$rsCheckDuplicate->EOF) {
					$rsCheckDuplicate->MoveFirst();
					// Update
					if($PDO->DB_TYPE == "NEW") {
						$sql = "UPDATE reports 
								SET report_date = ?
								WHERE member_id = ?
									AND report_title = ?
									AND report_file_name = ?
									AND report_format = 'P'";
						$params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName);
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "UPDATE tbl_member_reports_android 
								SET create_stamp= ?
								WHERE member_id= ?
									AND report_type= ?
									AND report_file_name= ?
									AND report_format='P'
									AND create_user= ?";
						$params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName, $member_id);
					}
					$PDO->executeQuery($sql, $params);
				} else {
					// INSERT
					if($PDO->DB_TYPE == "NEW") {
						$sql = "INSERT INTO reports 
									(member_id, report_title, report_file_name, report_format, report_date) 
								VALUES 
									(?, ?, ?, ?, ?)";
						$params = array($member_id, $report_title, $exportName, 'P', date('Y-m-d h:i:s'));
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "INSERT INTO tbl_member_reports_android 
									(member_id, report_type, report_file_name, report_format, create_user, 
									create_stamp) 
								VALUES 
									(?, ?, ?, ?, ?, ?)";
						$params = array($member_id, $report_title, $exportName, 'P', $member_id, date('Y-m-d h:i:s'));
					}
					$PDO->executeQuery($sql);
				}
			}
		
			// send back the file name and the pdf url as the result
			$response["success"] = true;
			$response["pdf_url"] = $exportUrl;	// send back the url
			$response["message"] = "successfully generated pdf";
			echo json_encode($response);
		} else {
			$response["success"] = false;
			$response["message"] = "you have no pregnancy data available";
			die(json_encode($response));
		}
	} else {
		$response["success"] = false;
		$response["message"] = "sql error";
		die(json_encode($response));
	}
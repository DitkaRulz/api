<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}

    header('Access-Control-Allow-Origin: *');
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	function formatSex($sex) {
		switch(strtoupper(trim($sex))) {
			case "C": return "Cow";
			case "B": return "Bull";
			case "S": return "Steer";
			default: return "N/A";
		}
	}
	
	function formatDate($date) {
		// BMC 02.01.2016
		//	This function will take in a date, most likely formatted 
		//	like YYYY-MM-DD and change the format to MM DD, YYYY
		//	as an example, Jan 01, 2016
		// BMC 02.03.2016
		//	If the date comes in as blank or null then this will display
		//	Dec 31, 1969!!! So we need to account for these input and
		//	display the proper output.  For now, we'll display nothing, just
		//	a blank string.
		
		if(trim($date) == "" 
			|| trim($date) == "null") {
			return "";
		} else {
			$date = strtotime($date);
			return date("M d, Y", $date);
		}
	}
	
	## -------------------- GENERATE PDF -------------------- ##
	$response["pdf_url"] = "";
	
	$MemberName = "";
	$YearDate	= "";
	
	$report_title = "Health Report ".$year;

	//** ------------------------------------ BUILD REPORT HERE ----------------------------------- **//
	
	// This first sql gives the total number of animals worked
	// using the record count.  the actual data sql's are below
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT a.registration
				FROM animal_health h
					INNER JOIN animal a ON h.registration = a.registration
					INNER JOIN ownership o ON h.registration = o.registration
					LEFT JOIN animal_location l ON h.registration = l.registration
					  AND (l.move_in <= DATE(?) 
						  AND (l.move_out >= DATE(?) 
							  OR l.move_out IS NULL))
					LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
				WHERE h.date_administered = DATE(?)
					AND o.member_id = ?
					AND o.date_owned <= DATE(?)
				GROUP BY a.registration";
		$params = array($year, $year, $year, $member_id, $year);
	} else if($PDO->DB_TYPE == "OLD") {
		$sql = "SELECT a.animal_registration
				FROM tbl_animal_health h
					INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
					INNER JOIN tbl_ownership o ON h.animal_registration = o.animal_registration
					LEFT JOIN tbl_animal_location l ON h.animal_registration = l.animal_registration
					  AND (l.move_in_date <= DATE(?) 
						  AND (l.move_out_date >= DATE(?) 
							  OR l.move_out_date IS NULL))
					LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
				WHERE h.date_administered = DATE(?)
					AND o.owner_id = ?
					AND o.ownership_date <= DATE(?)
				GROUP BY a.animal_registration";
		$params = array($year, $year, $year, $member_id, $year);
	}
	
	$rs = $PDO->recordSetQuery($sql, $params);
	if($rs) {
		if(!$rs->EOF) {
			// ********** CREATE THE PDF ********** //
			$pdf = new PDF(); 
			//$pdf->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
			
			// Build PDF
			$requested_font	= 'Times';
			$pdf->AliasNbPages();
			$pdf->SetTopMargin(10);
			$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
			$pdf->ZeroCount();
			$pdf->SetFont('Times','B', 10);
			
			$i=0;	// not sure about this
			
			// set the year
			$YearDate = $year;
			
			// get the cow count
			$TotalCount = $rs->Recordcount();
			
			// Get the member name
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT member_name 
						FROM member 
						WHERE member_id = ?";
				$params = array($member_id);
			} else if($PDO->DB_TYPE == "OLD") { 
				$sql = "SELECT member_name 
						FROM tbl_member 
						WHERE member_id = ?";
				$params = array($member_id);
			}
			
			$rsMember = $PDO->recordSetQuery($sql, $params);
			if($rsMember) {
				if(!$rsMember->EOF) {
					$rsMember->MoveFirst();
					
					$MemberName = $rsMember->fields['member_name'];
				} else {
					$MemberName = $member_id;
				}
				
				$rsMember->Close();
			} else {
				$MemberName = $member_id;
			}

            // BMC 11.07.2016
            //  -- get the member details
            $memberDetails = "";
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT c.address_line_1,
                            c.address_line_2,
                            c.city,
                            c.state,
                            c.zip_code,
                            c.phone_number
                        FROM member m
                            LEFT JOIN contact c ON m.member_id = c.member_id
                                AND c.is_primary
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line_1'];
                        $address_line_2 = $rsContact->fields['address_line_2'];
                        $city = $rsContact->fields['city'];
                        $state = $rsContact->fields['state'];
                        $zip_code = $rsContact->fields['zip_code'];
                        //$phone_number = $rsContact->fields['phone_number'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                        //$phone_number = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT a.address_line1,
                            a.address_line2,
                            a.address_city,
                            a.address_state,
                            a.address_postal_code
                        FROM tbl_member m
                            LEFT JOIN tbl_address a ON m.general_correspondance_address = a.address_id
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line1'];
                        $address_line_2 = $rsContact->fields['address_line2'];
                        $city = $rsContact->fields['address_city'];
                        $state = $rsContact->fields['address_state'];
                        $zip_code = $rsContact->fields['address_postal_code'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else {
                $memberDetails = "";
            }
			
			// build the title
			$pdf->Image(SITE_ROOT . '/images/digital_beef_logo_no_text_tm.png', 10, 10, 35);
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,5,"Digital Beef, LLC", '', 1, 'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(188,12,$memberDetails, '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,7.5,"Health Report for ".formatDate($YearDate), '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(188,7.5, "Total Worked: ".$TotalCount, '', 1, 'C');
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(188, 8, $MemberName."\n".$member_id, '', 1, 'C');
			$pdf->Cell(188, 6, "", '', 1, 'C');
	
			// *****This will give adult cows > 1 year (365 days) old*****
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT a.registration,
							a.private_herd_number, 
							h.date_administered, 
							h.product_name, 
							p.pasture_name
						FROM animal_health h
							INNER JOIN animal a ON h.registration = a.registration
                            INNER JOIN animal_birth b ON h.registration = b.registration
							INNER JOIN ownership o ON h.registration = o.registration
							LEFT JOIN animal_location l ON h.registration = l.registration
                            	AND (l.move_in <= DATE(?) 
                                	AND (l.move_out >= DATE(?) 
                                    	OR l.move_out IS NULL))
							LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
						WHERE h.date_administered = DATE(?)
							AND o.member_id = ?
                            AND o.date_owned <= DATE(?)
							AND (DATE(?) - b.birth_date) > 365
						ORDER BY 
						    p.pasture_name ASC,
						    h.date_administered ASC";
				$params = array($year, $year, $year, $member_id, $year, $year);
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT a.animal_registration AS registration,
							a.animal_private_herd_id AS private_herd_number, 
							h.date_administered, 
							h.product_name, 
							p.pasture_id AS pasture_name
						FROM tbl_animal_health h
							INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
							INNER JOIN tbl_animal_data_birth b ON h.animal_registration = b.animal_registration
							INNER JOIN tbl_ownership o ON h.animal_registration = o.animal_registration
							LEFT JOIN tbl_animal_location l ON h.animal_registration = l.animal_registration
                            	AND (l.move_in_date <= DATE(?) 
                                	AND (l.move_out_date >= DATE(?) 
                                    	OR l.move_out_date IS NULL))
							LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
						WHERE h.date_administered = DATE(?)
							AND o.owner_id = ?
                            AND o.ownership_date <= DATE(?)
							AND (DATE(?) - b.birth_date) > 365
						ORDER BY 
						    p.pasture_id ASC,
						    h.date_administered ASC";
                $params = array($year, $year, $year, $member_id, $year, $year);
			}
			$rsAnimals = $PDO->recordSetQuery($sql, $params);
			
			// Split the health into groups by product name.
			// so find out how many groups there are
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT h.product_name
						FROM animal_health h
						WHERE h.date_administered = DATE(?) 
						GROUP BY h.product_name
						ORDER BY h.product_name";
				$params = array($year);
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT h.product_name
						FROM tbl_animal_health h
						WHERE h.date_administered = DATE(?) 
						GROUP BY h.product_name
						ORDER BY h.product_name";
				$params = array($year);
			}
			$rsProducts = $PDO->recordSetQuery($sql, $params);
			if($rsProducts) {
				$rsProducts->MoveFirst();
				if(!$rsProducts->EOF) {
					// Get total adult animals
					if($PDO->DB_TYPE == "NEW") {
						$sql = "SELECT a.registration,
									a.private_herd_number
								FROM animal_health h
									INNER JOIN animal a ON h.registration = a.registration
									INNER JOIN ownership o ON h.registration = o.registration
									LEFT JOIN animal_location l ON h.registration = l.registration
                                      	AND (l.move_in <= DATE(?) 
                                        	AND (l.move_out >= DATE(?) 
                                            	OR l.move_out IS NULL))
									LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
									INNER JOIN animal_birth b ON h.registration = b.registration
								WHERE h.date_administered = DATE(?)
									AND o.member_id = ?
                                    AND o.date_owned <= DATE(?)
									AND (DATE(?) - b.birth_date) > 365
								GROUP BY a.registration,
									a.private_herd_number
								ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
						$params = array($year, $year, $year, $member_id, $year, $year);
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "SELECT a.animal_registration AS registration,
									a.animal_private_herd_id AS private_herd_number
								FROM tbl_animal_health h
									INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
									INNER JOIN tbl_ownership o ON h.animal_registration = o.animal_registration
									LEFT JOIN tbl_animal_location l ON h.animal_registration = l.animal_registration
                                      	AND (l.move_in_date <= DATE(?)
                                        	AND (l.move_out_date >= DATE(?)
                                            	OR l.move_out_date IS NULL))
									LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
									INNER JOIN tbl_animal_data_birth b ON h.animal_registration = b.animal_registration
								WHERE h.date_administered = DATE(?)
									AND o.owner_id = ?
                                    AND o.ownership_date <= DATE(?)
									AND (DATE(?) - b.birth_date) > 365
								GROUP BY a.animal_registration,
									a.animal_private_herd_id
								ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
                        $params = array($year, $year, $yearm, $member_id, $year, $year);
					}
					$rsTotalAdults = $PDO->recordSetQuery($sql, $params);
					if($rsTotalAdults) {
						if($rsTotalAdults->RecordCount() > 0) {
							// Create the headers
							// Print the group/product name header
							$pdf->SetFont('Arial','B',7);
							$pdf->Cell(1,5,"",'R',0,'C');	
							$pdf->Cell(30,5,"Total Adult:",'LBT',0,'C');
							$pdf->SetFont('Arial','',7);
							$pdf->Cell(10,5,$rsTotalAdults->RecordCount(),'RBT',0,'C');
							$pdf->Cell(1,5,"",'',1,'C');
							$rsTotalAdults->Close();
							
							$pdf->SetFont('Arial','B',7);
							$pdf->Cell(1,5,"",'R',0,'C');	
							$pdf->Cell(20,5,"Product Name:",'LRBT',0,'C');			
							
							while(!$rsProducts->EOF) {
								$pdf->Cell(20,5,$rsProducts->fields['product_name'],'LRBT',0,'C');	
								$rsProducts->MoveNext();
							}
							
							$pdf->Cell(1,5,"",'L',1,'C');
							$pdf->Cell(1,5,"",'R',0,'C');	
							$pdf->Cell(20,5,"PHN",'LRBT',0,'C');
					
							$rsProducts->MoveFirst();	
							while(!$rsProducts->EOF) {
								$pdf->Cell(20,5,"Received?",'LRBT',0,'C');	
								$rsProducts->MoveNext();
							}	
							
							$pdf->Cell(20,5,"Pasture",'LRBT',0,'C');	
							$pdf->Cell(1,5,"",'',1,'C');
							
							// Loop through every animal record, check if the vaccination exists for that record
							// and then display the date for it!
							$rsProducts->MoveFirst();
							$rsAnimals->MoveFirst();
							
							// since it's ordered by PHN i get the first PHN and see if it matches the next one.  If it doesn't match I start a new row.
							while(!$rsAnimals->EOF) {
								$row_phn = $rsAnimals->fields['private_herd_number'];
								
								$pdf->SetFont('Arial','',7);
								$pdf->Cell(1,5,"",'R',0,'C');	
								$pdf->Cell(20,5,$row_phn,'LRBT',0,'C');
				
								$rsProducts->MoveFirst();
								while(!$rsProducts->EOF ) {
									if($PDO->DB_TYPE == "NEW") {
										$sql = "SELECT h.date_administered
												FROM animal_health h
													INNER JOIN animal a ON h.registration = a.registration
												WHERE h.product_name = ?
													AND a.registration = ?
													AND h.date_administered = DATE(?)";
										$params = array($rsProducts->fields['product_name'], $rsAnimals->fields['registration'], $year);
									} else if($PDO->DB_TYPE == "OLD") {
										$sql = "SELECT h.date_administered
												FROM tbl_animal_health h
													INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
												WHERE h.product_name = ?
													AND a.animal_registration = ?
													AND h.date_administered = DATE(?)";
										$params = array($rsProducts->fields['product_name'], $rsAnimals->fields['registration'], $year);
									}
									$rsDate = $PDO->recordSetQuery($sql, $params);
									if($rsDate) {
										if(!$rsDate->EOF) {
											// Date shows that it was given the vaccination
											$pdf->Cell(20,5,(trim($rsDate->fields['date_administered']) == "" ? "No" : "Yes"),'LRBT',0,'C');	
										} else {
											// no date means no vacc was found so it didn't get it!
											$pdf->Cell(20,5,"",'LRBT',0,'C');	
										}
									} else {
										$pdf->Cell(20,5,"",'LRBT',0,'C');	
									}
									
									$rsProducts->MoveNext();
								}
								
								$pdf->Cell(20,5,$rsAnimals->fields['pasture_name'],'LRBT',0,'C');	
								$pdf->Cell(1,5,"",'L',1,'C');
								
								// Now move to the next animal in the list until the PHN changes
								while($row_phn == $rsAnimals->fields['private_herd_number']
								&& !$rsAnimals->EOF) {
									$rsAnimals->MoveNext();
								}
							}
						}
					}
				}
				$rsProducts->Close();
			}
			$rsAnimals->Close();
		
			// *****This will give calves < 1 year (365 days) old*****
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT a.registration,
							a.private_herd_number, 
							h.date_administered, 
							h.product_name, 
							p.pasture_name
						FROM animal_health h
							INNER JOIN animal a ON h.registration = a.registration
                            INNER JOIN animal_birth b ON h.registration = b.registration
							INNER JOIN ownership o ON h.registration = o.registration
							LEFT JOIN animal_location l ON h.registration = l.registration
                            	AND (l.move_in <= DATE(?) 
                                	AND (l.move_out >= DATE(?) 
                                    	OR l.move_out IS NULL))
							LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
						WHERE h.date_administered = DATE(?)
							AND o.member_id = ?
                            AND o.date_owned <= DATE(?)
							AND (DATE(?) - b.birth_date) < 365
						ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
				$params = array($year, $year, $year, $member_id, $year, $year);
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT a.animal_registration AS registration,
							a.animal_private_herd_id AS private_herd_number, 
							h.date_administered, 
							h.product_name, 
							p.pasture_id AS pasture_name
						FROM tbl_animal_health h
							INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
							INNER JOIN tbl_animal_data_birth b ON h.animal_registration = b.animal_registration
							INNER JOIN tbl_ownership o ON h.animal_registration = o.animal_registration
							LEFT JOIN tbl_animal_location l ON h.animal_registration = l.animal_registration
                            	AND (l.move_in_date <= DATE(?) 
                                	AND (l.move_out_date >= DATE(?) 
                                    	OR l.move_out_date IS NULL))
							LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
						WHERE h.date_administered = DATE(?)
							AND o.owner_id = ?
                            AND o.ownership_date <= DATE(?)
							AND (DATE(?) - b.birth_date) < 365
						ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
				$params = array($year, $year, $year, $member_id, $year, $year);
			}
			$rsAnimals = $PDO->recordSetQuery($sql, $params);
			
			// Split the health into groups by product name.
			// so find out how many groups there are
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT h.product_name
						FROM animal_health h
						WHERE h.date_administered = DATE(?) 
						GROUP BY h.product_name
						ORDER BY h.product_name";
				$params = array($year);
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT h.product_name
						FROM tbl_animal_health h
						WHERE h.date_administered = DATE(?) 
						GROUP BY h.product_name
						ORDER BY h.product_name";
				$params = array($year);
			}
			$rsProducts = $PDO->recordSetQuery($sql, $params);
			if($rsProducts) {
				$rsProducts->MoveFirst();
				if(!$rsProducts->EOF) {
					// Get total calves
					if($PDO->DB_TYPE == "NEW") {
						$sql = "SELECT a.registration,
									a.private_herd_number
								FROM animal_health h
									INNER JOIN animal a ON h.registration = a.registration
									INNER JOIN ownership o ON h.registration = o.registration
									LEFT JOIN animal_location l ON h.registration = l.registration
                                      	AND (l.move_in <= DATE(?) 
                                        	AND (l.move_out >= DATE(?) 
                                            	OR l.move_out IS NULL))
									LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
									INNER JOIN animal_birth b ON h.registration = b.registration
								WHERE h.date_administered = DATE(?)
									AND o.member_id = ?
                                    AND o.date_owned <= DATE(?)
									AND (DATE(?) - b.birth_date) < 365
								GROUP BY a.registration,
									a.private_herd_number
								ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
                        $params = array($year, $year, $year, $member_id, $year, $year);
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "SELECT a.animal_registration AS registration,
									a.animal_private_herd_id AS private_herd_number
								FROM tbl_animal_health h
									INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
									INNER JOIN tbl_ownership o ON h.animal_registration = o.animal_registration
									LEFT JOIN tbl_animal_location l ON h.animal_registration = l.animal_registration
                                      	AND (l.move_in_date <= DATE(?) 
                                        	AND (l.move_out_date >= DATE(?)
                                            	OR l.move_out_date IS NULL))
									LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
									INNER JOIN tbl_animal_data_birth b ON h.animal_registration = b.animal_registration
								WHERE h.date_administered = DATE(?)
									AND o.owner_id = ?
                                    AND o.ownership_date <= DATE(?)
									AND (DATE(?) - b.birth_date) < 365
								GROUP BY a.animal_registration,
									a.animal_private_herd_id
								ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
                        $params = array($year, $year, $year, $member_id, $year, $year);
					}
					$rsTotalCalves = $PDO->recordSetQuery($sql, $params);
					
					if($rsTotalCalves->RecordCount() > 0) {
						// Create the headers
						// Print the group/product name header
						$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
						
						$pdf->SetFont('Arial','B',7);
						$pdf->Cell(1,5,"",'R',0,'C');	
						$pdf->Cell(30,5,"Total Calves:",'LBT',0,'C');
						$pdf->SetFont('Arial','',7);
						$pdf->Cell(10,5,$rsTotalCalves->RecordCount(),'RBT',1,'C');
						$rsTotalCalves->Close();
						
						// Display the normal header
						$pdf->SetFont('Arial','B',7);
						$pdf->Cell(1,5,"",'R',0,'C');	
						$pdf->Cell(20,5,"Product Name:",'LRBT',0,'C');			
						
						while(!$rsProducts->EOF) {
							$pdf->Cell(20,5,$rsProducts->fields['product_name'],'LRBT',0,'C');	
							$rsProducts->MoveNext();
						}
						
						$pdf->Cell(1,5,"",'L',1,'C');
						$pdf->Cell(1,5,"",'R',0,'C');
						$pdf->Cell(20,5,"PHN",'LRBT',0,'C');
				
						$rsProducts->MoveFirst();	
						while(!$rsProducts->EOF) {
							$pdf->Cell(20,5,"Received?",'LRBT',0,'C');	
							$rsProducts->MoveNext();
						}	
						
						$pdf->Cell(20,5,"Pasture",'LRBT',0,'C');	
						$pdf->Cell(1,5,"",'',1,'C');
						
						// Loop through every animal record, check if the vaccination exists for that record
						// and then display the date for it!
						$rsProducts->MoveFirst();
						$rsAnimals->MoveFirst();
						
						// since it's ordered by PHN i get the first PHN and see if it matches the next one.  
						// If it doesn't match I start a new row.
						while(!$rsAnimals->EOF) {
							$row_phn = $rsAnimals->fields['private_herd_number'];
							
							$pdf->SetFont('Arial','',7);
							$pdf->Cell(1,5,"",'R',0,'C');
							$pdf->Cell(20,5,$row_phn,'LRBT',0,'C');
			
							$rsProducts->MoveFirst();
							while(!$rsProducts->EOF ) {
								if($PDO->DB_TYPE == "NEW") {
									$sql = "SELECT h.date_administered
											FROM animal_health h
												INNER JOIN animal a ON h.registration = a.registration
											WHERE h.product_name = ?
												AND a.registration = ?
												AND h.date_administered = DATE(?)";
									$params = array($rsProducts->fields['product_name'], $rsAnimals->fields['registration'], $year);
								} else if($PDO->DB_TYPE == "OLD") {
									$sql = "SELECT h.date_administered
											FROM tbl_animal_health h
												INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
											WHERE h.product_name = ?
												AND a.animal_registration = ?
												AND h.date_administered = DATE(?)";
									$params = array($rsProducts->fields['product_name'], $rsAnimals->fields['registration'], $year);
								}
								$rsDate = $PDO->recordSetQuery($sql, $params);
								if($rsDate) {
									if(!$rsDate->EOF) {
										// Date shows that it was given the vaccination
										$pdf->Cell(20,5,(trim($rsDate->fields['date_administered']) == "" ? "No" : "Yes"),'LRBT',0,'C');	
									} else {
										// no date means no vacc was found so it didn't get it!
										$pdf->Cell(20,5,"",'LRBT',0,'C');	
									}
								} else {
									$pdf->Cell(20,5,"",'LRBT',0,'C');	
								}
								
								$rsProducts->MoveNext();
							}
							
							$pdf->Cell(20,5,$rsAnimals->fields['pasture_name'],'LRBT',0,'C');	
							$pdf->Cell(1,5,"",'L',1,'C');
							
							// Now move to the next animal in the list until the PHN changes
							while($row_phn == $rsAnimals->fields['private_herd_number']
							&& !$rsAnimals->EOF) {
								$rsAnimals->MoveNext();
							}
						}
					}
				}
				$rsProducts->Close();
			}
			$rsAnimals->Close();
			
			// *** UNIQUE PASTURES WORKED *** //
			$pastures = array();
			
			// Get worked pastures, grouped
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT p.pasture_name
						FROM animal_health h
							INNER JOIN animal a ON h.registration = a.registration
							INNER JOIN ownership o ON h.registration = o.registration
							LEFT JOIN animal_location l ON h.registration = l.registration
							  AND (l.move_in <= DATE(?) 
								  AND (l.move_out >= DATE(?) 
									  OR l.move_out IS NULL))
							LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
						WHERE h.date_administered = DATE(?)
							AND o.member_id = ?
							AND o.date_owned <= DATE(?)
						GROUP BY p.pasture_name
						ORDER BY p.pasture_name";
				$params = array($year, $year, $year, $member_id, $year);
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT p.pasture_id AS pasture_name
						FROM tbl_animal_health h
							INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
							INNER JOIN tbl_ownership o ON h.animal_registration = o.animal_registration
							LEFT JOIN tbl_animal_location l ON h.animal_registration = l.animal_registration
							  AND (l.move_in_date <= DATE(?) 
								  AND (l.move_out_date >= DATE(?) 
									  OR l.move_out_date IS NULL))
							LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
						WHERE h.date_administered = DATE(?)
							AND o.owner_id = ?
							AND o.ownership_date <= DATE(?)
						GROUP BY p.pasture_id
						ORDER BY p.pasture_id";
                $params = array($year, $year, $year, $member_id, $year);
			}
			$rsPastures = $PDO->recordSetQuery($sql, $params);
			while(!$rsPastures->EOF) {
				$unique_pasture = true;
				for($i = 0; $i < count($pastures); $i++) {
					if($pastures[$i] == $rsPastures->fields['pasture_name']) {
						$unique_pasture = false;
					}
				}
				
				if($unique_pasture == true) {
					$pastures[] = $rsPastures->fields['pasture_name'];
				}
				
				$rsPastures->MoveNext();
			}
		
			// *** MISSING ANIMALS *** //
			// if the number of animals worked in each pasture is greater than 50% then we
			// will look in that pasture for the animals missed.
			for($i = 0; $i < count($pastures); $i++) {
				// Get the PK_ID of each pasture we're working with
				$pasture_id = "";
				if($PDO->DB_TYPE == "NEW") {
					$sql = "SELECT pasture_id
							FROM pasture
							WHERE pasture_name = ?[?]
								AND member_id = ?";
					$params = array($pastures, $i, $member_id);
				} else if($PDO->DB_TYPE == "OLD") {
					$sql = "SELECT pk_id AS pasture_id
							FROM tbl_pastures 
							WHERE pasture_id = ?[?]
								AND member_id = ?";
					$params = array($pastures, $i, $member_id);
				}
				
				$rsPkId	= $PDO->recordSetQuery($sql, $params);
				if($rsPkId) {
					if(!$rsPkId->EOF) {
						$rsPkId->MoveFirst();
						
						$pasture_id = $rsPkId->fields['pasture_id'];
						
						// Determine if this is a worked pasture
						if($PDO->DB_TYPE == "NEW") {
							$sql = "SELECT a.private_herd_number
									FROM animal_health h
										INNER JOIN animal a ON h.registration = a.registration
										INNER JOIN ownership o ON h.registration = o.registration
										LEFT JOIN animal_location l ON h.registration = l.registration
                                            AND (l.move_in <= DATE(?) 
                                                AND (l.move_out >= DATE(?) 
                                                    OR l.move_out IS NULL))
										LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
									WHERE h.date_administered = DATE(?)
										AND o.member_id = ?
                                        AND o.date_owned <= DATE(?)
										AND p.pasture_id = ?
										AND a.status = '0'
										AND NOT a.is_deleted
									GROUP BY a.private_herd_number
									ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
							$params = array($year, $year, $year, $member_id, $year, $pasture_id);
						} else if($PDO->DB_TYPE == "OLD") {
							$sql = "SELECT a.animal_private_herd_id AS private_herd_number
									FROM tbl_animal_health h
										INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
										INNER JOIN tbl_ownership o ON h.animal_registration = o.animal_registration
										LEFT JOIN tbl_animal_location l ON h.animal_registration = l.animal_registration
                                            AND (l.move_in_date <= DATE(?) 
                                                AND (l.move_out_date >= DATE(?) 
                                                    OR l.move_out_date IS NULL))
										LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
									WHERE h.date_administered = DATE(?)
										AND o.owner_id = ?
                                        AND o.ownership_date <= DATE(?)
										AND p.pk_id = ?
										AND a.animal_record_status = 'A'
									GROUP BY a.animal_private_herd_id
									ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
                            $params = array($year, $year, $year, $member_id, $year, $pasture_id);
						}
						$rsAnimalsInPasture = $PDO->recordSetQuery($sql, $params);
						$count_worked = $rsAnimalsInPasture->RecordCount();
						if($PDO->DB_TYPE == "NEW") {
							$sql = "SELECT a.private_herd_number 
									FROM animal a
										INNER JOIN ownership o ON a.registration = o.registration
										LEFT JOIN animal_location l ON a.registration = l.registration
                                            AND (l.move_in <= DATE(?) 
                                                AND (l.move_out >= DATE(?) 
                                                    OR l.move_out IS NULL))
										LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
									WHERE o.member_id = ?
                                        AND o.date_owned <= DATE(?)
										AND p.pasture_id = ?
										AND a.status = '0'
										AND NOT a.is_deleted
									GROUP BY a.private_herd_number
									ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
							$params = array($year, $year, $member_id, $year, $pasture_id);
						} else if($PDO->DB_TYPE == "OLD") {
							$sql = "SELECT a.animal_private_herd_id AS private_herd_number
									FROM tbl_animal a
										INNER JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
										LEFT JOIN tbl_animal_location l ON a.animal_registration = l.animal_registration
                                            AND (l.move_in_date <= DATE(?) 
                                                AND (l.move_out_date >= DATE(?) 
                                                    OR l.move_out_date IS NULL))
										LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
									WHERE o.owner_id = ?
                                        AND o.ownership_date <= DATE(?)
										AND p.pk_id = ?
										AND a.animal_record_status = 'A'
									GROUP BY a.animal_private_herd_id
									ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
                            $params = array($year, $year, $member_id, $year, $pasture_id);
						}
						
						$rsTotalAnimalsInPasture = $PDO->recordSetQuery($sql, $params);
						$count_total = $rsTotalAnimalsInPasture->RecordCount();
						
						// if this is a worked pasture (greater than 50% worked) then we will include missed animals.
						if($count_total == 0) {
							$result = 0;
						} else {
							$result = floatval($count_worked / $count_total);
						}
						
						if($result >= .5) {
							//21/38 == .55
							// Get all the missing animals from this pasture
							if($PDO->DB_TYPE == "NEW") {
								$sql = "SELECT a.private_herd_number, 
											p.pasture_name
										FROM animal a
											LEFT JOIN animal_location l ON a.registration = l.registration
                                                AND (l.move_in <= DATE(?) 
                                                    AND (l.move_out >= DATE(?) 
                                                        OR l.move_out IS NULL))
											LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
										WHERE l.pasture_id = ?
											AND a.status = '0'
											AND NOT a.is_deleted
										GROUP BY a.private_herd_number, 
                                        	p.pasture_name
										ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
								$params = array($year, $year, $pasture_id);
								$rsMissingAnimals = $PDO->recordSetQuery($sql, $params);
								
								$sql = "SELECT a.private_herd_number
										FROM animal_health h
											INNER JOIN animal a ON h.registration = a.registration
											INNER JOIN ownership o ON h.registration = o.registration
											LEFT JOIN animal_location l ON a.registration = l.registration
                                                AND (l.move_in <= DATE(?) 
                                                    AND (l.move_out >= DATE(?) 
                                                        OR l.move_out IS NULL))
											LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
										WHERE h.date_administered = DATE(?)
											AND o.member_id = ?
											AND o.date_owned <= DATE(?)
											AND p.pasture_id = ?
											AND a.status = '0'
											AND NOT a.is_deleted
										GROUP BY a.private_herd_number
										ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
								$params = array($year, $year. $year, $member_id, $year, $pasture_id);
								$rsActualWorked = $PDO->recordSetQuery($sql, $params);
							} else if($PDO->DB_TYPE == "OLD") {
								$sql = "SELECT a.animal_private_herd_id AS private_herd_number, 
											p.pasture_id AS pasture_name
										FROM tbl_animal a
											LEFT JOIN tbl_animal_location l ON a.animal_registration = l.animal_registration
                                                AND (l.move_in_date <= DATE(?) 
                                                    AND (l.move_out_date >= DATE(?) 
                                                        OR l.move_out_date IS NULL))
											LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
										WHERE l.pasture_id = ?
											AND (CASE 
												WHEN a.status_code_date <= DATE(?) THEN a.animal_record_status 
												ELSE 'A' 
											END) = 'A'												
											AND a.animal_record_status = 'A'
										GROUP BY a.animal_private_herd_id, 
                                        	p.pasture_id
										ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
								$params = array($year, $year, $pasture_id, $year);
								$rsMissingAnimals = $PDO->recordSetQuery($sql, $params);
								
								$sql = "SELECT a.animal_private_herd_id AS private_herd_number
										FROM tbl_animal_health h
											INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
											INNER JOIN tbl_ownership o ON h.animal_registration = o.animal_registration
											LEFT JOIN tbl_animal_location l ON h.animal_registration = l.animal_registration
                                            	AND (l.move_in_date <= DATE(?) 
                                                    AND (l.move_out_date >= DATE(?) 
                                                        OR l.move_out_date IS NULL))
											LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
										WHERE h.date_administered = DATE(?)
											AND o.owner_id = ?
											AND o.ownership_date <= DATE(?)
											AND p.pk_id = ?
											AND a.animal_record_status = 'A'
										GROUP BY a.animal_private_herd_id
										ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
								$params = array($year, $year, $year, $member_id, $year, $pasture_id);
								$rsActualWorked = $PDO->recordSetQuery($sql, $params);
							}
							
							// This will give all the animals in the pasture, but I still need to subtract out the
							// animals that have already been included above.
							$MissingAnimals = array();
							$MissingPastures = array();
							if($rsMissingAnimals) {
								if(!$rsMissingAnimals->EOF) {
									$rsMissingAnimals->MoveFirst();
									while(!$rsMissingAnimals->EOF) {
										$rsActualWorked->MoveFirst();
										$animal_missed = true;
										while(!$rsActualWorked->EOF) {
											if($rsActualWorked->fields['private_herd_number'] == $rsMissingAnimals->fields['private_herd_number']) {
												$animal_missed = false;
											}
											$rsActualWorked->MoveNext();
										}
										
										if($animal_missed == true) {
											$MissingAnimals[] = $rsMissingAnimals->fields['private_herd_number'];
											$MissingPastures[] = $rsMissingAnimals->fields['pasture_name'];
										}
										
										$rsMissingAnimals->MoveNext();
									}
								}
							}
							
							// PRINT the total missed and the animals missed
							if(count($MissingAnimals) > 0) {
								// Page Break
								$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
								
								$pdf->SetFont('Arial','B',7);
								$pdf->Cell(1,5,"",'R',0,'C');	
								$pdf->Cell(40,5,"Total 'Did Not Work' from ".$pastures[$i].": ",'LBT',0,'C');
								$pdf->SetFont('Arial','',7);			
								$pdf->Cell(10,5,count($MissingAnimals),'RBT',0,'C');	
								$pdf->SetFont('Arial','B',7);	
								$pdf->Cell(1,5,"",'L',1,'C');
								$pdf->Cell(1,5,"",'R',0,'C');	
								$pdf->Cell(25,5,"PHN",'LRBT',0,'C');
								$pdf->Cell(25,5,"Pasture",'LRBT',0,'C');
								$pdf->Cell(5,5,"",'',1,'C');
								
								for($x = 0; $x < count($MissingAnimals); $x++) {
									$pdf->SetFont('Arial','',7);
									$pdf->Cell(1,5,"",'R',0,'C');
									$pdf->Cell(25,5,$MissingAnimals[$x],'LTRB',0,'C');
									$pdf->Cell(25,5,$MissingPastures[$x],'LTRB',0,'C');
									$pdf->Cell(1,5,"",'',1,'C');
								}
							}
							$pdf->Cell(1,5,"",'',1,'C');
						}
						
						$rsPkId->Close();
					}
				}
			}
		
			// *** EXTRA ANIMALS *** //
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT a.private_herd_number, 
							p.pasture_name
						FROM animal_health h
							INNER JOIN animal a ON h.registration = a.registration
							INNER JOIN ownership o ON h.registration = o.registration
                            LEFT JOIN animal_location l ON a.registration = l.registration
								AND (l.move_in <= DATE(?) 
                                    AND (l.move_out >= DATE(?) 
                                        OR l.move_out IS NULL))
                            LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
						WHERE h.date_administered = DATE(?)
							AND o.member_id = ?
							AND o.date_owned <= DATE(?)
						GROUP BY a.private_herd_number, 
							p.pasture_name
						ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
				$params = array($year, $year, $year, $member_id, $year);
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT a.animal_private_herd_id AS private_herd_number, 
							p.pasture_id AS pasture_name
						FROM tbl_animal_health h
							INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
							INNER JOIN tbl_ownership o ON h.animal_registration = o.animal_registration
							LEFT JOIN tbl_animal_location l ON h.animal_registration = l.animal_registration
                            	AND (l.move_in_date <= DATE(?) 
                                    AND (l.move_out_date >= DATE(?) 
                                        OR l.move_out_date IS NULL))
							LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
						WHERE h.date_administered = DATE(?)
							AND o.owner_id = ?
							AND o.ownership_date <= DATE(?)
						GROUP BY a.animal_private_herd_id, 
							p.pasture_id
						ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
				$params = array($year, $year, $year, $member_id, $year);
			}
			$rsAnimals = $PDO->recordSetQuery($sql, $params);
			if($rsAnimals) {
				$ExtraAnimal = array();
				$AnimalPasture = array();
				$extraPastures = array();
				
				for($i = 0; $i < count($pastures); $i++) {
					// Only save the animals if the pasture is not a main pasture, meaning
					// if the pasture was not fully worked.
					
					// BMC 03.09.2016
					//	Escape single quotes for the query
					$pastures[$i] = str_replace("'", "''", $pastures[$i]);
					
					if($PDO->DB_TYPE == "NEW") {
						$sql = "SELECT a.private_herd_number
								FROM animal_health h
									INNER JOIN animal a ON h.registration = a.registration
									INNER JOIN ownership o ON h.registration = o.registration
									LEFT JOIN animal_location l ON h.registration = l.registration
                                    	AND (l.move_in <= DATE(?) 
                                            AND (l.move_out >= DATE(?) 
                                                OR l.move_out IS NULL))
									LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
								WHERE h.date_administered = DATE(?)
									AND o.member_id = ?
									AND o.date_owned <= DATE(?)
									AND p.pasture_name = ?[?]
								GROUP BY a.private_herd_number
								ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
						$params = array($year, $year, $year, $member_id, $year, $pasture_id, $i);
						$rsAnimalsInPasture = $PDO->recordSetQuery($sq, $params);
						$count_worked = $rsAnimalsInPasture->RecordCount();
						
						$sql = "SELECT a.private_herd_number 
								FROM animal a
									INNER JOIN ownership o ON a.registration = o.registration
									LEFT JOIN animal_location l ON a.registration = l.registration
                                          AND (l.move_in <= DATE(?) 
                                              AND (l.move_out >= DATE(?) 
                                                  OR l.move_out IS NULL))
                                    LEFT JOIN pasture p ON l.pasture_id = p.pasture_id
								WHERE o.member_id = ?
									AND o.date_owned <= DATE(?)
									AND p.pasture_name = ?[?]
								GROUP BY a.private_herd_number
								ORDER BY (substring(a.private_herd_number, '^[0-9]+'))::int,substring(a.private_herd_number, '[^0-9_].*$')";
                        $params = array($year, $year, $member_id, $year, $pasture_id, $i);
						$rsTotalAnimalsInPasture = $PDO->recordSetQuery($sql, $params);
						$count_total = $rsTotalAnimalsInPasture->RecordCount();
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "SELECT a.animal_private_herd_id AS private_herd_number
								FROM tbl_animal_health h
									INNER JOIN tbl_animal a ON h.animal_registration = a.animal_registration
									INNER JOIN tbl_ownership o ON h.animal_registration = o.animal_registration
									LEFT JOIN tbl_animal_location l ON h.animal_registration = l.animal_registration
                                    	AND (l.move_in_date <= DATE(?) 
                                            AND (l.move_out_date >= DATE(?) 
                                                OR l.move_out_date IS NULL))
									LEFT JOIN tbl_pastures p ON l.pasture_id = p.pk_id::varchar
								WHERE h.date_administered = DATE(?)
									AND o.owner_id = ?
									AND o.ownership_date <= DATE(?)
									AND p.pasture_id = ?[?]
								GROUP BY a.animal_private_herd_id
								ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
                        $params = array($year, $year, $year, $member_id, $year, $pasture_id, $i);
						$rsAnimalsInPasture = $PDO->recordSetQuery($sql, $params);
						$count_worked = $rsAnimalsInPasture->RecordCount();
						
						$sql = "SELECT a.animal_private_herd_id AS private_herd_number 
								FROM tbl_animal a
									INNER JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
									LEFT JOIN tbl_animal_location l ON a.animal_registration = l.animal_registration
                                    	AND (l.move_in_date <= DATE(?) 
                                            AND (l.move_out_date >= DATE(?) 
                                                OR l.move_out_date IS NULL))
									LEFT JOIN tbl_pastures p ON p.pk_id::varchar = l.pasture_id
								WHERE o.owner_id = ?
									AND o.ownership_date <= DATE(?)
									AND p.pasture_id = ?[?]
								GROUP BY a.animal_private_herd_id
								ORDER BY (substring(a.animal_private_herd_id, '^[0-9]+'))::int,substring(a.animal_private_herd_id, '[^0-9_].*$')";
                        $params = array($year, $year, $member_id, $year, $pasture_id, $i);
						$rsTotalAnimalsInPasture = $PDO->recordSetQuery($sql, $params);
						$count_total = $rsTotalAnimalsInPasture->RecordCount();
					}
					
					// If less than 25% of the animals from the given pasture have been worked, then we will add them to the extras!
					// Otherwise we'll say the pasture was worked!
					if(floatval($count_worked / $count_total) < .25) $extraPastures[] = $pastures[$i];
				}
		
				for($i = 0; $i < count($extraPastures); $i++) {
					$rsAnimals->MoveFirst();
					while(!$rsAnimals->EOF) {
						if($rsAnimals->fields['pasture_name'] == $extraPastures[$i]) {
							$ExtraAnimal[] 	= $rsAnimals->fields['private_herd_number'];
							$AnimalPasture[] = $rsAnimals->fields['pasture_name'];
						}
						$rsAnimals->MoveNext();
					}
				}
				
				// PRINT the total extra animals
				if(count($ExtraAnimal) > 0) {
					// Page Break
					$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
					
					$pdf->SetFont('Arial','B',7);
					$pdf->Cell(1,5,"",'R',0,'C');	
					$pdf->Cell(40,5,"Total Extra: ",'LBT',0,'C');	
					$pdf->SetFont('Arial','',7);		
					$pdf->Cell(10,5,count($ExtraAnimal),'RBT',0,'C');	
					$pdf->SetFont('Arial','B',7);	
					$pdf->Cell(1,5,"",'L',1,'C');
					$pdf->Cell(1,5,"",'R',0,'C');	
					$pdf->Cell(25,5,"PHN",'LRBT',0,'C');
					$pdf->Cell(25,5,"Pasture",'LRBT',0,'C');
					$pdf->Cell(1,5,"",'',1,'C');
					
					for($i = 0; $i < count($ExtraAnimal); $i++) {
						$pdf->SetFont('Arial','',7);
						$pdf->Cell(1,5,"",'R',0,'C');
						$pdf->Cell(25,5,$ExtraAnimal[$i],'LTRB',0,'C');
						$pdf->Cell(25,5,$AnimalPasture[$i],'LTRB',0,'C');
						$pdf->Cell(1,5,"",'',1,'C');
					}
				}
			}
			
			// create export directory, name, and url
			$exportDir = getExportDirectory($provider, $member_id);
			$timestamp = time();
			
			$exportName = "synchronization_report_app_".$member_id."_".$year.".pdf";
			$exportDir = $exportDir . "synchronization_report_app_".$member_id."_".$year.".pdf";
			
			if($PDO->DB_TYPE == "NEW") {
				// for the new databases we have to look in the members folder for the 
				// correct pdf directory
				$exportUrl 	= $PDO->FILE_DIR . $member_id . "/" . $exportName;
			} else if($PDO->DB_TYPE == "OLD") {
				$exportUrl 	= $PDO->FILE_DIR . $exportName;
			}
			
			// export the file
			$pdf->Output($exportDir);
			
			// If file name already exists, then update it!
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT * 
						FROM reports 
						WHERE report_title = ? 
							AND report_file_name = ?
							AND member_id = ?";
				$params = array($report_title, $exportName, $member_id);
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT * 
						FROM tbl_member_reports_android 
						WHERE report_type = ? 
							AND report_file_name = ?
							AND create_user = ?";
				$params = array($report_title, $exportName, $member_id);
			}
			$rsCheckDuplicate = $PDO->recordSetQuery($sql, $params);
			if($rsCheckDuplicate) {
				if(!$rsCheckDuplicate->EOF) {
					$rsCheckDuplicate->MoveFirst();
					// Update
					if($PDO->DB_TYPE == "NEW") {
						$sql = "UPDATE reports 
								SET report_date = ?
								WHERE member_id = ?
									AND report_title = ?
									AND report_file_name = ?
									AND report_format = 'P'";
						$params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName);
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "UPDATE tbl_member_reports_android 
								SET create_stamp = ?
								WHERE member_id = ?
									AND report_type = ?
									AND report_file_name = ?
									AND report_format = 'P'
									AND create_user = ?";
						$params = array(date('Y-m-d h:i:s'), $member_id, $report_title, $exportName, $member_id);
					}
					$PDO->executeQuery($sql, $params);
				} else {
					// INSERT
					if($PDO->DB_TYPE == "NEW") {
						$sql = "INSERT INTO reports 
									(member_id, report_title, report_file_name, report_format, report_date) 
								VALUES 
									(?, ?, ?, ?, ?)";
						$params = array($member_id, $report_title, $exportName, 'P', date('Y-m-d h:i:s'));
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "INSERT INTO tbl_member_reports_android 
									(member_id, report_type, report_file_name, report_format, create_user, 
									create_stamp) 
								VALUES 
									(?, ?, ?, ?, ?, ?)";
                        $params = array($member_id, $report_title, $exportName, 'P', $member_id, date('Y-m-d h:i:s'));
					}
					$PDO->executeQuery($sql, $params);
				}
			}
		
			// send back the file name and the pdf url as the result
			$response["success"] = true;
			$response["pdf_url"] = $exportUrl;	// send back the url
			$response["message"] = "successfully generated pdf";
			echo json_encode($response);
		} else {
			$response["success"] = false;
			$response["message"] = "you have no health reports available";
			die(json_encode($response));
		}
	} else {
		$response["success"] = false;
		$response["message"] = "sql error";
		die(json_encode($response));
	}
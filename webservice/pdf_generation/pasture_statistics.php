<?php
	# ------------------------------------------------------------------------------------------------------------ #
	if (!defined('SITE_ROOT')) { 
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}

    header('Access-Control-Allow-Origin: *');
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
	
	require_once(SITE_ROOT . "/includes/includes.php");
	# ------------------------------------------------------------------------------------------------------------ #

	## -------------------- CONNECTION SETUP -------------------- ##
	$provider = (isset($_REQUEST['provider']) ? strtoupper(trim($_REQUEST['provider'])) : "");
	$PDO = new Connect($provider);
	
	## -------------------- GLOBAL FUNCTIONS -------------------- ##
	function formatSex($sex) {
		switch(strtoupper(trim($sex))) {
			case "C": return "Cow";
			case "B": return "Bull";
			case "S": return "Steer";
			default: return "N/A";
		}
	}
	
	function formatDate($date) {
		// BMC 02.01.2016
		//	This function will take in a date, most likely formatted 
		//	like YYYY-MM-DD and change the format to MM DD, YYYY
		//	as an example, Jan 01, 2016
		// BMC 02.03.2016
		//	If the date comes in as blank or null then this will display
		//	Dec 31, 1969!!! So we need to account for these input and
		//	display the proper output.  For now, we'll display nothing, just
		//	a blank string.
		
		if(trim($date) == "" 
			|| trim($date) == "null") {
			return "";
		} else {
			$date = strtotime($date);
			return date("M d, Y", $date);
		}
	}
	
	## -------------------- GENERATE PDF -------------------- ##
	$response["pdf_url"] = "";
	
	$MemberName = "";
	$YearDate	= "";
	$TotalCount	= "";
	
	//** ------------------------------------ BUILD REPORT HERE ----------------------------------- **//
	$report_title = "Pasture Statistics ".$year;
	
	if($PDO->DB_TYPE == "NEW") {
		$sql = "SELECT p.pasture_id, 
					p.pasture_name
				FROM pasture p 
				WHERE p.member_id = '".$member_id."'
					AND p.pasture_id = '".$pasture_id."'
				ORDER BY p.pasture_name, p.pasture_id";
	} else if($PDO->DB_TYPE == "OLD") {
		$sql = "SELECT p.pk_id AS pasture_id, 
					p.pasture_id AS pasture_name
				FROM tbl_pastures p 
				WHERE p.member_id = '".$member_id."'
					AND p.pk_id = '".$pasture_id."'
				ORDER BY p.pasture_id";
	}
	
	$rs = $PDO->recordSetQuery($sql);
	if($rs) {
		// BUILD PDF
		if($rs->Recordcount() > 0) {
			// ********** CREATE THE PDF ********** //
			$pdf = new PDF(); 
			//$pdf->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
			
			// Build PDF
			$requested_font	= 'Times';
			$pdf->AliasNbPages();
			$pdf->SetTopMargin(10);
			$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
			$pdf->ZeroCount();
			$pdf->SetFont('Times','B', 10);
			
			$i=0;	// not sure about this
			
			// get the cow count
			$TotalCount = $rs->Recordcount();
			
			// Get the member name
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT member_name 
						FROM member 
						WHERE member_id = '".$member_id."'";
			} else if($PDO->DB_TYPE == "OLD") { 
				$sql = "SELECT member_name 
						FROM tbl_member 
						WHERE member_id='".$member_id."'";
			}
			
			$rsMember = $PDO->recordSetQuery($sql);
			if($rsMember) {
				if(!$rsMember->EOF) {
					$rsMember->MoveFirst();
					
					$MemberName = $rsMember->fields['member_name'];
				} else {
					$MemberName = $member_id;
				}
				
				$rsMember->Close();
			} else {
				$MemberName = $member_id;
			}
			
			$pasture_name = $rs->fields['pasture_name'];

            // BMC 11.07.2016
            //  -- get the member details
            $memberDetails = "";
            if($PDO->DB_TYPE == "NEW") {
                $sql = "SELECT c.address_line_1,
                            c.address_line_2,
                            c.city,
                            c.state,
                            c.zip_code,
                            c.phone_number
                        FROM member m
                            LEFT JOIN contact c ON m.member_id = c.member_id
                                AND c.is_primary
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line_1'];
                        $address_line_2 = $rsContact->fields['address_line_2'];
                        $city = $rsContact->fields['city'];
                        $state = $rsContact->fields['state'];
                        $zip_code = $rsContact->fields['zip_code'];
                        //$phone_number = $rsContact->fields['phone_number'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                        //$phone_number = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else if($PDO->DB_TYPE == "OLD") {
                $sql = "SELECT a.address_line1,
                            a.address_line2,
                            a.address_city,
                            a.address_state,
                            a.address_postal_code
                        FROM tbl_member m
                            LEFT JOIN tbl_address a ON m.general_correspondance_address = a.address_id
                        WHERE m.member_id = ?";
                $params = array($member_id);
                $rsContact = $PDO->recordSetQuery($sql, $params);
                if ($rsContact) {
                    if (!$rsContact->EOF) {
                        $rsContact->MoveFirst();
                        $address_line_1 = $rsContact->fields['address_line1'];
                        $address_line_2 = $rsContact->fields['address_line2'];
                        $city = $rsContact->fields['address_city'];
                        $state = $rsContact->fields['address_state'];
                        $zip_code = $rsContact->fields['address_postal_code'];
                    } else {
                        $address_line_1 = "";
                        $address_line_2 = "";
                        $city = "";
                        $state = "";
                        $zip_code = "";
                    }
                    $rsContact->Close();

                    $memberDetails = $address_line_1 . "\n" . $address_line_2 . "\n" . $city . ", " . $state . " " . $zip_code;
                }
            } else {
                $memberDetails = "";
            }
			
			// build the title
			$pdf->Image(SITE_ROOT . '/images/digital_beef_logo_no_text_tm.png', 10, 10, 35);
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,5,"Digital Beef, LLC", '', 1, 'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(188,12,$memberDetails, '', 1, 'C');
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->Cell(188,7.5,"Pasture Statistics for ".$pasture_name, '', 1, 'C');
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(188, 8, $MemberName."\n".$member_id, '', 1, 'C');
			$pdf->Cell(188, 6, "", '', 1, 'C');
			
			// Loop through each pasture and get the animals details.  Print out the headers
			$pageCount = 0;
			while(!$rs->EOF) {
				// Increment page so only 2 pastures per page
				if(($pageCount % 2 == 0) && ($pageCount != 0)) {
					$pdf->AddPage($orientation='P', $size='', $rotation=0, false, false);
				}
				
				// now get the pasture statistics
				if($PDO->DB_TYPE == "NEW") {
					$sql = "SELECT a.sex,
								COUNT(b.registration) AS birth_count,
								COUNT(w.registration) AS wean_count,
								COUNT(y.registration) AS year_count,
								SUM(b.weight) AS total_birth_weight,
								SUM(w.weight) AS total_wean_weight,
								SUM(y.weight) AS total_year_weight,
								ROUND(AVG(b.weight), 0) AS average_birth_weight,
								ROUND(AVG(w.weight), 0) AS average_wean_weight,
								ROUND(AVG(y.weight), 0) AS average_year_weight
							FROM animal a
								INNER JOIN animal_location l ON a.registration = l.registration
								INNER JOIN ownership o ON a.registration = o.registration
								INNER JOIN pasture p ON l.pasture_id = p.pasture_id
								INNER JOIN animal_birth b ON a.registration = b.registration
								LEFT JOIN animal_wean w ON a.registration = w.registration
								LEFT JOIN animal_year y ON a.registration = y.registration
							WHERE p.pasture_id = '".$rs->fields['pasture_id']."'
								AND o.member_id = '".$member_id."'
								AND NOT o.superceded
								AND l.move_out IS NULL
								AND a.status = '0'
								AND NOT a.is_deleted
							GROUP BY a.sex";
				} else if($PDO->DB_TYPE == "OLD") {
					$sql = "SELECT a.animal_sex AS sex,
								COUNT(b.animal_registration) AS birth_count,
								COUNT(w.animal_registration) AS wean_count,
								COUNT(y.animal_registration) AS year_count,
								SUM(b.birth_weight) AS total_birth_weight,
								SUM(w.wean_weight) AS total_wean_weight,
								SUM(y.year_weight) AS total_year_weight,
								ROUND(AVG(b.birth_weight), 0) AS average_birth_weight,
								ROUND(AVG(w.wean_weight), 0) AS average_wean_weight,
								ROUND(AVG(y.year_weight), 0) AS average_year_weight
							FROM tbl_animal a
								INNER JOIN tbl_animal_location l ON a.animal_registration = l.animal_registration
								INNER JOIN tbl_ownership o ON a.animal_registration = o.animal_registration
								INNER JOIN tbl_pastures p ON l.pasture_id = p.pk_id::VARCHAR
								INNER JOIN tbl_animal_data_birth b ON a.animal_registration = b.animal_registration
								LEFT JOIN tbl_animal_data_wean w ON a.animal_registration = w.animal_registration
								LEFT JOIN tbl_animal_data_year y ON a.animal_registration = y.animal_registration
							WHERE p.pk_id = '".$rs->fields['pasture_id']."'
								AND o.owner_id = '".$member_id."'
								AND NOT o.superceded_flag
								AND l.move_out_date IS NULL
								AND a.animal_record_status = 'A'
							GROUP BY a.animal_sex";				}
				$rsStats = $PDO->recordSetQuery($sql);
				if($rsStats) {
					if(!$rsStats->EOF) {
						
						$total_head = 0;
						$rsStats->MoveFirst();
						while(!$rsStats->EOF) {
							// Every animal will have a birth record, so count this field to get the total head.
							$total_head += $rsStats->fields['birth_count'];
							$rsStats->MoveNext();
						}
						
						// Create Headers
						$pdf->SetFont('Arial','B',7);
						$pdf->Cell(1,5,"",'R',0,'C');	
						$pdf->Cell(25,5,$rs->fields['pasture_name'],'LBTR',0,'C');
						$pdf->Cell(50,5,"Total Head: ".$total_head,'LBTR',1,'C');
						
						// Create sub headers
						$pdf->Cell(1,5,"",'R',0,'C');
						$pdf->Cell(25,5,"Sex",'LBT',0,'C');
						$pdf->Cell(25,5,"",'LBT',0,'C');
						$pdf->Cell(25,5,"Birth",'BT',0,'C');
						$pdf->Cell(25,5,"Wean",'LBT',0,'C');
						$pdf->Cell(25,5,"Year",'LBTR',1,'C');
						
						$pdf->SetFont('Arial','',7);
						
						$rsStats->MoveFirst();
						while(!$rsStats->EOF) {
							// Will create 3 boxes for each sex group, C=Cows, B=Bulls, S=Steers
							$pdf->Cell(1,5,"",'R',0,'C');
						
							// Create the category dependent on sex
							if($rsStats->fields['sex'] == "C") {
								$pdf->Cell(25,5,"Cows",'LT',0,'C');
							} else if($rsStats->fields['sex'] == "B") {
								$pdf->Cell(25,5,"Bulls",'LT',0,'C');
							} else if($rsStats->fields['sex'] == "S") {
								$pdf->Cell(25,5,"Steers",'LT',0,'C');
							}
							
							// Counts
							$pdf->Cell(25,6.5,"Animal Count:",'LT',0,'R');
								$pdf->Cell(25,5,$rsStats->fields['birth_count'],'T',0,'C');
								$pdf->Cell(25,5,$rsStats->fields['wean_count'],'T',0,'C');
								$pdf->Cell(25,5,$rsStats->fields['year_count'],'TR',1,'C');
							// Total Weight
							$pdf->Cell(1,5,"",'R',0,'C');
							$pdf->Cell(25,5,"",'LR',0,'C');	// blank
							$pdf->Cell(25,5,"Total Weight (lbs):",'',0,'R');
								$pdf->Cell(25,5,$rsStats->fields['total_birth_weight'],'',0,'C');
								$pdf->Cell(25,5,$rsStats->fields['total_wean_weight'],'',0,'C');
								$pdf->Cell(25,5,$rsStats->fields['total_year_weight'],'R',1,'C');
							// Average Weight
							$pdf->Cell(1,5,"",'R',0,'C');
							$pdf->Cell(25,5,"",'LBR',0,'C');	// blank
							$pdf->Cell(25,5,"Average Weight (lbs):",'B',0,'R');
								$pdf->Cell(25,5,$rsStats->fields['average_birth_weight'],'B',0,'C');
								$pdf->Cell(25,5,$rsStats->fields['average_wean_weight'],'B',0,'C');
								$pdf->Cell(25,5,$rsStats->fields['average_year_weight'],'BR',1,'C');		
							
							$rsStats->MoveNext();
						}
						$rsStats->Close();
					}
				}	
				
				// Skip an extra line
				$pageCount++;
				$pdf->Cell(1,5,"",'',1,'C');
				$rs->MoveNext();
			}
			$rs->Close();
				
			// create export directory, name, and url
			$exportDir = getExportDirectory($provider, $member_id);
			$timestamp = time();
			
			$exportName = "pasture_statistics_app_".$member_id."_".$pasture_id.".pdf";
			$exportDir = $exportDir . "pasture_statistics_app_".$member_id."_".$pasture_id.".pdf";
			
			if($PDO->DB_TYPE == "NEW") {
				// for the new databases we have to look in the members folder for the 
				// correct pdf directory
				$exportUrl 	= $PDO->FILE_DIR . $member_id . "/" . $exportName;
			} else if($PDO->DB_TYPE == "OLD") {
				$exportUrl 	= $PDO->FILE_DIR . $exportName;
			}
			
			// export the file
			$pdf->Output($exportDir);
			
			// If file name already exists, then update it!
			if($PDO->DB_TYPE == "NEW") {
				$sql = "SELECT * 
						FROM reports 
						WHERE report_title = '".$report_title."' 
							AND report_file_name ='".$exportName."'
							AND member_id = '".$member_id."'";
			} else if($PDO->DB_TYPE == "OLD") {
				$sql = "SELECT * 
						FROM tbl_member_reports_android 
						WHERE report_type = '".$report_title."' 
							AND report_file_name='".$exportName."'
							AND create_user = '".$member_id."'";
			}
			$rsCheckDuplicate = $PDO->recordSetQuery($sql);
			if($rsCheckDuplicate) {
				if(!$rsCheckDuplicate->EOF) {
					$rsCheckDuplicate->MoveFirst();
					// Update
					if($PDO->DB_TYPE == "NEW") {
						$sql = "UPDATE reports 
								SET report_date = '".date('Y-m-d h:i:s')."'
								WHERE member_id = '".$member_id."'
									AND report_title = '".$report_title."'
									AND report_file_name = '".$exportName."'
									AND report_format = 'P'";
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "UPDATE tbl_member_reports_android 
								SET create_stamp='".date('Y-m-d h:i:s')."'
								WHERE member_id='".$member_id."'
									AND report_type='".$report_title."'
									AND report_file_name='".$exportName."'
									AND report_format='P'
									AND create_user='".$member_id."'";
					}
					$PDO->executeQuery($sql);
				} else {
					// INSERT
					if($PDO->DB_TYPE == "NEW") {
						$sql = "INSERT INTO reports 
									(member_id, report_title, report_file_name, report_format, report_date) 
								VALUES 
									('".$member_id."','".$report_title."','".$exportName."', 'P', 
									'".date('Y-m-d h:i:s')."')";
					} else if($PDO->DB_TYPE == "OLD") {
						$sql = "INSERT INTO tbl_member_reports_android 
									(member_id, report_type, report_file_name, report_format, create_user, 
									create_stamp) 
								VALUES 
									('".$member_id."','".$report_title."','".$exportName."', 'P', '".$member_id."', 
									'".date('Y-m-d h:i:s')."')";
					}
					$PDO->executeQuery($sql);
				}
			}
		
			// send back the file name and the pdf url as the result
			$response["success"] = true;
			$response["pdf_url"] = $exportUrl;	// send back the url
			$response["message"] = "successfully generated pdf";
			echo json_encode($response);
		} else {
			$response["success"] = false;
			$response["message"] = "you have no pregnancy data available";
			die(json_encode($response));
		}
	} else {
		$response["success"] = false;
		$response["message"] = "sql error";
		die(json_encode($response));
	}
<?php
class PDF extends FPDF {
	var $memberId;
	var $headerCount = 0;
	
	function ZeroCount() {
		// overriding the original function
		$this->page_count = '1';
	}
	
	function setMemberId($memberId) {
		$this->memberId = $memberId;
	}
	
	function getMemberId() {
		return $this->memberId;
	}
	
	function Header($showHeader=true) {
		if($showHeader && $this->headerCount == 0) {
			/*
			// BMC 09.11.2016
			//	-- do not create any header for the API
			$this->headerCount++;
			// print logo of association
			$this->Image(getAssociationURL().getAssociationLogoUrl(), 10, 20, 35);
	
			// set header font
			$this->SetFont('Arial', 'B', 12);
			
			// header
			$this->Cell(50);
			$this->Cell(95, 5, $this->AssociationName, 0, 1, 'C');
			$this->SetFont('Arial', 'B', 10);
			$this->Cell(50);
			$this->Cell(95, 20, 
				$this->AssociationAddress
				."\n".$this->AssociationCityStateZip
				."\n".$this->AssociationPhone
				."\n".$this->AssociationFax,0,0,'C');
			*/
		}
	}
	
	function Footer($showFooter=true) {	
		if($showFooter) {	
			$this->SetY(-15);
			$this->SetFont('Arial','B',6);
			$this->Cell(0,5,"Page ".$this->page_count."",0,0,'C');
			$this->page_count++;
		}
	}
	
	function LoadData($file) {
		$lines 	= file($file);
		$data 	= array();
		foreach($lines as $line) {
			$data[] = explode(';', chop($line));
		}
		
		return $data;
	}

	function BasicTable($header, $data) {
		//Header
		foreach($header as $col) {
			$this->Cell(40,7,$col,1);
		}
		
		$this->Ln();
		
		//Data
		foreach($data as $row) {
			foreach($row as $col) {
				$this->Cell(40,6,$col,1);
			}
			
			$this->Ln();
		}
	}
	
	function ImprovedTable($header,$data) {
		$w = array(40,35,40,45);	//Column widths
		
		//Header
		for($i = 0; $i < count($header); $i++) {
			$this->Cell($w[$i],7,$header[$i],1,0,'C');
		}
		
		$this->Ln();
		
		//Data
		foreach($data as $row) {
			$this->Cell($w[0],6,$row[0],'LR');
			$this->Cell($w[1],6,$row[1],'LR');
			$this->Cell($w[2],6,number_format($row[2]),'LR',0,'R');
			$this->Cell($w[3],6,number_format($row[3]),'LR',0,'R');
			
			$this->Ln();
		}
		
		$this->Cell(array_sum($w),0,'','T');	//Closure line
	}
	
	function FancyTable($header,$data, $widths, $align, $cols) {
		//Colors, line width and bold font
		$this->SetFillColor(255,0,0);
		$this->SetTextColor(255);
		$this->SetDrawColor(128,0,0);
		$this->SetLineWidth(.3);
		$this->SetFont('Arial','B',8);
		
		//Header
		for($i = 0; $i < $cols; $i++) {
			$this->Cell($widths[$i],7,$header[$i],1,0,'C',1);
		}
		
		$this->Ln();
		
		//Color and font restoration
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->SetFont('Arial','',7);
		
		//Data
		$fill = 0;
		foreach($data as $row) {
			$this->Cell($widths[0],6,$row[0],'LR',0,$align[0],$fill);
			
			if($cols>1) {
				$this->Cell($widths[1],6,$row[1],'LR',0,$align[1],$fill);
			}
			
			if($cols>2) {
				$this->Cell($widths[2],6,$row[2],'LR',0,$align[2],$fill);
			}

			if($cols>3) {
				$this->Cell($widths[3],6,$row[3],'LR',0,$align[3],$fill);
			}
			if($cols>4) {
				$this->Cell($widths[4],6,$row[4],'LR',0,$align[4],$fill);
			}
			
			if($cols>5) {
				$this->Cell($widths[5],6,$row[5],'LR',0,$align[5],$fill);
			}
			
			if($cols>6) {
				$this->Cell($widths[6],6,$row[6],'LR',0,$align[6],$fill);
			}
			
			$this->Ln();
			
			$fill = !$fill;
		}
	
		$this->Cell(195,0,'','T');
	
	}
	
	var $B;
	var $I;
	var $U;
	var $HREF;
	// ********** CONSTRUCTOR ********** //
	function PDF($orientation = 'P', $unit = 'mm', $format = 'A4') {
		// defaults to portrait orientation
		// defaults to milimeter (mm) unit spacing
		// defaults to the A4 paper format
		
		//Call parent constructor
		$this->__construct($orientation, $unit, $format);
		
		//Initialization
		$this->B=0;
		$this->I=0;
		$this->U=0;
		$this->HREF='';
	}
	
	function WriteHTML($html) {
		//HTML parser
		$html = str_replace("\n",' ',$html);
		$a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
		
		foreach($a as $i => $e) {
			if($i%2==0) {
				//Text
				if($this->HREF) {
					$this->PutLink($this->HREF,$e);
				} else {
					$this->Write(5,$e);
				}
			} else {
				//Tag
				if($e{0} == '/') {
					$this->CloseTag(strtoupper(substr($e,1)));
				} else {
					//Extract attributes
					$a2 = explode(' ',$e);
					$tag = strtoupper(array_shift($a2));
					$attr = array();
					foreach($a2 as $v) {
						if(preg_match('/^([^=]*)=["\']?([^"\']*)["\']?$/',$v,$a3)) {
							$attr[strtoupper($a3[1])]=$a3[2];
						}
					}
					
					$this->OpenTag($tag,$attr);
				}
			}
		}
	}
	
	function OpenTag($tag, $attr) {
		//Opening tag
		if($tag == 'B' or $tag == 'I' or $tag == 'U') {
			$this->SetStyle($tag,true);
		}
		
		if($tag == 'A') {
			$this->HREF = $attr['HREF'];
		}
		
		if($tag == 'BR') {
			$this->Ln(5);
		}
	}
	
	function CloseTag($tag) {
		//Closing tag
		if($tag == 'B' or $tag == 'I' or $tag == 'U') {
			$this->SetStyle($tag, false);
		}
		
		if($tag == 'A') {
			$this->HREF = '';
		}
	}
	
	function SetStyle($tag,$enable) {
		//Modify style and select corresponding font
		$this->$tag += ($enable ? 1 : -1);
		$style = '';
		foreach(array('B','I','U') as $s) {
			if($this->$s > 0) {
				$style .= $s;
			}
		}
		
		$this->SetFont('', $style);
	}
	
	function PutLink($URL, $txt) {
		//Put a hyperlink
		$this->SetTextColor(0,0,255);
		$this->SetStyle('U',true);
		$this->Write(5,$txt, $URL);
		$this->SetStyle('U', false);
		$this->SetTextColor(0);
	}
	
	var $outlines = array();
	var $OutlineRoot;	
	function Bookmark($txt, $level = 0, $y = 0) {
		if($y==-1) {
			$y=$this->GetY();
		}
		
		$this->outlines[] = array('t'=>$txt,'l'=>$level,'y'=>$y,'p'=>$this->PageNo());
	}
		
	function _putbookmarks() {
		$nb = count($this->outlines);
		if($nb == 0) {
			return;
		}
		
		$lru = array();
		$level = 0;
		
		foreach($this->outlines as $i=>$o) {
			if($o['l'] > 0) {
				$parent = $lru[$o['l']-1];
				
				//Set parent and last pointers
				$this->outlines[$i]['parent'] = $parent;
				$this->outlines[$parent]['last'] = $i;
				
				if($o['l'] > $level) {
					//Level increasing: set first pointer
					$this->outlines[$parent]['first'] = $i;
				}
			} else {
				$this->outlines[$i]['parent'] = $nb;
			}
			
			if($o['l']<=$level and $i>0) {
				//Set prev and next pointers
				$prev=$lru[$o['l']];
				$this->outlines[$prev]['next']=$i;
				$this->outlines[$i]['prev']=$prev;
			}
			
			$lru[$o['l']]=$i;
			$level=$o['l'];
		}
		
		//Outline items
		$n = $this->n+1;
		
		foreach($this->outlines as $i=>$o) {
			$this->_newobj();
			$this->_out('<</Title '.$this->_textstring($o['t']));
			$this->_out('/Parent '.($n+$o['parent']).' 0 R');
			
			if(isset($o['prev'])) {
				$this->_out('/Prev '.($n+$o['prev']).' 0 R');
			}
			
			if(isset($o['next'])) {
				$this->_out('/Next '.($n+$o['next']).' 0 R');
			}
			
			if(isset($o['first'])) {
				$this->_out('/First '.($n+$o['first']).' 0 R');
			}
			
			if(isset($o['last'])) {
				$this->_out('/Last '.($n+$o['last']).' 0 R');
			}
			
			$this->_out(sprintf('/Dest [%d 0 R /XYZ 0 %.2f null]',1+2*$o['p'],($this->h-$o['y'])*$this->k));
			$this->_out('/Count 0>>');
			$this->_out('endobj');
		}
		
		//Outline root
		$this->_newobj();
		$this->OutlineRoot=$this->n;
		$this->_out('<</Type /Outlines /First '.$n.' 0 R');
		$this->_out('/Last '.($n+$lru[0]).' 0 R>>');
		$this->_out('endobj');
	}	
	
	function _putresources() {
		parent::_putresources();
		$this->_putbookmarks();
	}	
	
	function _putcatalog() {
		parent::_putcatalog();
		if(count($this->outlines)>0) {
			$this->_out('/Outlines '.$this->OutlineRoot.' 0 R');
			$this->_out('/PageMode /UseOutlines');
		}
	}
	
	var $widths;
	var $aligns;	
	function SetWidths($w) {
		//Set the array of column widths
		$this->widths = $w;
	}	
	
	function SetAligns($a) {
		//Set the array of column alignments
		$this->aligns = $a;
	}	
	
	function Row($data) {
		//Calculate the height of the row
		$nb = 0;
		for($i = 0; $i < count($data); $i++) {
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		}
		
		$h = 5 * $nb;
		
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		
		//Draw the cells of the row
		for($i = 0; $i < count($data); $i++) {
			$w = $this->widths[$i];
			$a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			
			//Save the current position
			$x = $this->GetX();
			$y = $this->GetY();
			
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			
			//Print the text
			$this->MultiCell($w,5,$data[$i],1,$a);
			
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		
		//Go to the next line
		$this->Ln($h);
	}	
	
	function CheckPageBreak($h) {
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger) {
			$this->AddPage($this->CurOrientation, '', 0, false, false);
		}
	}	
	
	function NbLines($w, $txt) {
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		
		if($w==0) {
			$w=$this->w-$this->rMargin-$this->x;
		}
		
		$wmax = ($w-2*$this->cMargin)*1000/$this->FontSize;
		$s = str_replace("\r",'',$txt);
		$nb = strlen($s);
		
		if($nb > 0 and $s[$nb-1] == "\n") {
			$nb--;
		}
		
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb) {
			$c = $s[$i];
			if($c=="\n") {
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			
			if($c==' ') {
				$sep=$i;
			}
			
			$l+=$cw[$c];
			if($l>$wmax) {
				if($sep==-1) {
					if($i==$j) {
						$i++;
					}
				} else {
					$i=$sep+1;
				}
				
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			} else {
				$i++;
			}
		}
		
		return $nl;
	}
	
	function RoundedRect($x, $y, $w, $h,$r, $style = '') {
        $k = $this->k;
        $hp = $this->h;
        if($style == 'F') {
            $op = 'f';
		} else if($style == 'FD' or $style == 'DF') {
            $op = 'B';
		} else {
            $op = 'S';
		}
		
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2f %.2f m',($x+$r)*$k,($hp-$y)*$k ));
        $xc = $x+$w-$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2f %.2f l', $xc*$k,($hp-$y)*$k ));
        $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
        $xc = $x+$w-$r ;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2f %.2f l',($x+$w)*$k,($hp-$yc)*$k));
        $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
        $xc = $x+$r ;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2f %.2f l',$xc*$k,($hp-($y+$h))*$k));
        $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2f %.2f l',($x)*$k,($hp-$yc)*$k ));
        $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
	}
	
    function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
		$h = $this->h;
		$this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c ', $x1*$this->k, ($h-$y1)*$this->k, $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}
	
	function TextWithDirection($x,$y,$txt,$direction = 'R') {
		$txt=str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$txt)));
		
		if ($direction == 'R') {
			$s=sprintf('BT %.2f %.2f %.2f %.2f %.2f %.2f Tm (%s) Tj ET',1,0,0,1,$x*$this->k,($this->h-$y)*$this->k,$txt);
		} else if ($direction == 'L') {
			$s=sprintf('BT %.2f %.2f %.2f %.2f %.2f %.2f Tm (%s) Tj ET',-1,0,0,-1,$x*$this->k,($this->h-$y)*$this->k,$txt);
		} else if ($direction == 'U') {
			$s=sprintf('BT %.2f %.2f %.2f %.2f %.2f %.2f Tm (%s) Tj ET',0,1,-1,0,$x*$this->k,($this->h-$y)*$this->k,$txt);
		} else if ($direction == 'D') {
			$s=sprintf('BT %.2f %.2f %.2f %.2f %.2f %.2f Tm (%s) Tj ET',0,-1,1,0,$x*$this->k,($this->h-$y)*$this->k,$txt);
		} else {
			$s=sprintf('BT %.2f %.2f Td (%s) Tj ET',$x*$this->k,($this->h-$y)*$this->k,$txt);
		}
		
		if ($this->ColorFlag) {
			$s='q '.$this->TextColor.' '.$s.' Q';
		}
		
		$this->_out($s);
	}
	
	function VCell($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0) {
		//Output a cell
		$k=$this->k;
		if($this->y+$h>$this->PageBreakTrigger and !$this->InFooter and $this->AcceptPageBreak()) {
			$x=$this->x;
			$ws=$this->ws;
			if($ws>0) {
				$this->ws=0;
				$this->_out('0 Tw');
			}
			
			$this->AddPage($this->CurOrientation);
			$this->x=$x;
			if($ws>0) {
				$this->ws=$ws;
				$this->_out(sprintf('%.3f Tw',$ws*$k));
			}
		}
		
		if($w==0) {
			$w=$this->w-$this->rMargin-$this->x;
		}
		
		$s='';
		// begin change Cell function 
		if($fill==1 or $border>0) {
			if($fill==1) {
				$op=($border>0) ? 'B' : 'f';
			} else {
				$op='S';
			}
			
			if ($border>1) {
				$s=sprintf(' q %.2f w %.2f %.2f %.2f %.2f re %s Q ',$border, $this->x*$k,($this->h-$this->y)*$k,$w*$k,-$h*$k,$op);
			} else {
				$s=sprintf('%.2f %.2f %.2f %.2f re %s ',$this->x*$k,($this->h-$this->y)*$k,$w*$k,-$h*$k,$op);
			}
		}
		
		if(is_string($border)) {
			$x=$this->x;
			$y=$this->y;
			if(is_int(strpos($border,'L'))) {
				$s.=sprintf('%.2f %.2f m %.2f %.2f l S ',$x*$k,($this->h-$y)*$k,$x*$k,($this->h-($y+$h))*$k);
			} else if(is_int(strpos($border,'l'))) {
				$s.=sprintf('q 2 w %.2f %.2f m %.2f %.2f l S Q ',$x*$k,($this->h-$y)*$k,$x*$k,($this->h-($y+$h))*$k);
			}
			
			if(is_int(strpos($border,'T'))) {
				$s.=sprintf('%.2f %.2f m %.2f %.2f l S ',$x*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-$y)*$k);
			} else if(is_int(strpos($border,'t'))) {
				$s.=sprintf('q 2 w %.2f %.2f m %.2f %.2f l S Q ',$x*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-$y)*$k);
			}
			
			if(is_int(strpos($border,'R'))) {
				$s.=sprintf('%.2f %.2f m %.2f %.2f l S ',($x+$w)*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
			} else if(is_int(strpos($border,'r'))) {
				$s.=sprintf('q 2 w %.2f %.2f m %.2f %.2f l S Q ',($x+$w)*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
			}
			
			if(is_int(strpos($border,'B'))) {
				$s.=sprintf('%.2f %.2f m %.2f %.2f l S ',$x*$k,($this->h-($y+$h))*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
			} else if(is_int(strpos($border,'b'))) {
				$s.=sprintf('q 2 w %.2f %.2f m %.2f %.2f l S Q ',$x*$k,($this->h-($y+$h))*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
			}
		}
		
		if(trim($txt)!='') {
			$cr=substr_count($txt,"\n");
			
			if ($cr>0) { // Multi line
				$txts = explode("\n", $txt);
				$lines = count($txts);
				
				for($l=0;$l<$lines;$l++) {
					$txt=$txts[$l];
					$w_txt=$this->GetStringWidth($txt);
					
					if ($align=='U') {
						$dy=$this->cMargin+$w_txt;
					} else if($align=='D') {
						$dy=$h-$this->cMargin;
					} else {
						$dy=($h+$w_txt)/2;
					}
					
					$txt=str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$txt)));
					if($this->ColorFlag) {
						$s.='q '.$this->TextColor.' ';
					}
					
					$s.=sprintf('BT 0 1 -1 0 %.2f %.2f Tm (%s) Tj ET ',
						($this->x+.5*$w+(.7+$l-$lines/2)*$this->FontSize)*$k,
						($this->h-($this->y+$dy))*$k,$txt);
					if($this->ColorFlag) {
						$s.='Q ';
					}
				}
			} else { // Single line
				$w_txt=$this->GetStringWidth($txt);
				$Tz=100;
				if ($w_txt>$h-2*$this->cMargin) {
					$Tz=($h-2*$this->cMargin)/$w_txt*100;
					$w_txt=$h-2*$this->cMargin;
				}
				
				if ($align=='U') {
					$dy=$this->cMargin+$w_txt;
				} else if($align=='D') {
					$dy=$h-$this->cMargin;
				} else {
					$dy=($h+$w_txt)/2;
				}
				
				$txt=str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$txt)));
				
				if($this->ColorFlag) {
					$s.='q '.$this->TextColor.' ';
				}
				
				$s.=sprintf('q BT 0 1 -1 0 %.2f %.2f Tm %.2f Tz (%s) Tj ET Q ',
							($this->x+.5*$w+.3*$this->FontSize)*$k,
							($this->h-($this->y+$dy))*$k,$Tz,$txt);
				
				if($this->ColorFlag) {
					$s.='Q ';
				}
			}
		}
		
		// end change Cell function 
		if($s) {
			$this->_out($s);
		}
		
		$this->lasth=$h;
		if($ln>0) {
			//Go to next line
			$this->y+=$h;
			if($ln==1) {
				$this->x=$this->lMargin;
			}
		} else {
			$this->x+=$w;
		}
	}
	
	function Cell($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='') {
		//Output a cell
		$k=$this->k;
		if($this->y+$h>$this->PageBreakTrigger and !$this->InFooter and $this->AcceptPageBreak()) {
			$x=$this->x;
			$ws=$this->ws;
			
			if($ws>0) {
				$this->ws=0;
				$this->_out('0 Tw');
			}
			
			$this->AddPage($this->CurOrientation);
			$this->x=$x;
			
			if($ws>0) {
				$this->ws=$ws;
				$this->_out(sprintf('%.3f Tw',$ws*$k));
			}
		}
		
		if($w==0) {
			$w=$this->w-$this->rMargin-$this->x;
		}
		
		$s='';
		
		// begin change Cell function 12.08.2003 
		if($fill==1 or $border>0) {
			if($fill==1) {
				$op=($border>0) ? 'B' : 'f';
			} else {
				$op='S';
			}
			
			if ($border>1) {
				$s=sprintf(' q %.2f w %.2f %.2f %.2f %.2f re %s Q ',$border,
					$this->x*$k,($this->h-$this->y)*$k,$w*$k,-$h*$k,$op);
			} else {
				$s=sprintf('%.2f %.2f %.2f %.2f re %s ',$this->x*$k,($this->h-$this->y)*$k,$w*$k,-$h*$k,$op);
			}
		}
		
		if(is_string($border)) {
			$x=$this->x;
			$y=$this->y;
			
			if(is_int(strpos($border,'L'))) {
				$s.=sprintf('%.2f %.2f m %.2f %.2f l S ',$x*$k,($this->h-$y)*$k,$x*$k,($this->h-($y+$h))*$k);
			} else if(is_int(strpos($border,'l'))) {
				$s.=sprintf('q 2 w %.2f %.2f m %.2f %.2f l S Q ',$x*$k,($this->h-$y)*$k,$x*$k,($this->h-($y+$h))*$k);
			}
			
			if(is_int(strpos($border,'T'))) {
				$s.=sprintf('%.2f %.2f m %.2f %.2f l S ',$x*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-$y)*$k);
			} else if(is_int(strpos($border,'t'))) {
				$s.=sprintf('q 2 w %.2f %.2f m %.2f %.2f l S Q ',$x*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-$y)*$k);
			}
			
			if(is_int(strpos($border,'R'))) {
				$s.=sprintf('%.2f %.2f m %.2f %.2f l S ',($x+$w)*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
			} else if(is_int(strpos($border,'r'))) {
				$s.=sprintf('q 2 w %.2f %.2f m %.2f %.2f l S Q ',($x+$w)*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
			}
			
			if(is_int(strpos($border,'B'))) {
				$s.=sprintf('%.2f %.2f m %.2f %.2f l S ',$x*$k,($this->h-($y+$h))*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
			} else if(is_int(strpos($border,'b'))) {
				$s.=sprintf('q 2 w %.2f %.2f m %.2f %.2f l S Q ',$x*$k,($this->h-($y+$h))*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
			}
		}
		
		if (trim($txt)!='') {
			$cr=substr_count($txt,"\n");
			
			if ($cr>0) { // Multi line
				$txts = explode("\n", $txt);
				$lines = count($txts);
				
				for($l = 0; $l < $lines; $l++) {
					$txt=$txts[$l];
					$w_txt=$this->GetStringWidth($txt);
					
					if($align=='R') {
						$dx=$w-$w_txt-$this->cMargin;
					} elseif($align=='C') {
						$dx=($w-$w_txt)/2;
					} else {
						$dx=$this->cMargin;
					}
	
					$txt=str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$txt)));
					
					if($this->ColorFlag) {
						$s.='q '.$this->TextColor.' ';
					}
					
					$s.=sprintf('BT %.2f %.2f Td (%s) Tj ET ',
						($this->x+$dx)*$k,
						($this->h-($this->y+.5*$h+(.7+$l-$lines/2)*$this->FontSize))*$k,
						$txt);
						
					if($this->underline) {
						$s.=' '.$this->_dounderline($this->x+$dx,$this->y+.5*$h+.3*$this->FontSize,$txt);
					}
					
					if($this->ColorFlag) {
						$s.='Q ';
					}
					
					if($link) {
						$this->Link($this->x+$dx,$this->y+.5*$h-.5*$this->FontSize,$w_txt,$this->FontSize,$link);
					}
				}
			} else { // Single line
				$w_txt=$this->GetStringWidth($txt);
				$Tz=100;
				
				if ($w_txt>$w-2*$this->cMargin) { // Need compression
					$Tz=($w-2*$this->cMargin)/$w_txt*100;
					$w_txt=$w-2*$this->cMargin;
				}
				
				if($align=='R') {
					$dx=$w-$w_txt-$this->cMargin;
				} else if($align=='C') {
					$dx=($w-$w_txt)/2;
				} else {
					$dx=$this->cMargin;
				}
				
				$txt=str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$txt)));
				if($this->ColorFlag) {
					$s.='q '.$this->TextColor.' ';
				}
				
				$s.=sprintf('q BT %.2f %.2f Td %.2f Tz (%s) Tj ET Q ',
							($this->x+$dx)*$k,
							($this->h-($this->y+.5*$h+.3*$this->FontSize))*$k,
							$Tz,$txt);
							
				if($this->underline) {
					$s.=' '.$this->_dounderline($this->x+$dx,$this->y+.5*$h+.3*$this->FontSize,$txt);
				}
				
				if($this->ColorFlag) {
					$s.='Q ';
				}
				
				if($link) {
					$this->Link($this->x+$dx,$this->y+.5*$h-.5*$this->FontSize,$w_txt,$this->FontSize,$link);
				}
			}
		}
		
		// end change Cell function 12.08.2003
		if($s) {
			$this->_out($s);
		}
		
		$this->lasth=$h;
		if($ln > 0) {
			//Go to next line
			$this->y+=$h;
			if($ln==1) {
				$this->x=$this->lMargin;
			}
		} else {
			$this->x+=$w;
		}
	}
	
	var $RefActive=0;        //Flag indicating that the index is being processed
	var $ChangePage=0;       //Flag indicating that a page break has occurred
	var $Reference=array();  //Array containing the references
	var $col=0;              //Current column number
	var $NbCol;              //Total number of columns
	var $y0;                 //Top ordinate of columns
	function Reference($txt,$lnk) {
		$Present=0;
		$size=sizeof($this->Reference);	
		//Search the reference in the array
		for ($i=0;$i<$size;$i++) {
			if ($this->Reference[$i]['t']==$txt) {
				$Present=1;
				$this->Reference[$i]['p'].=','.$this->PageNo();
			}
		}	
		
		//If not found, add it
		if ($Present==0) {
			$this->Reference[]=array('t'=>$txt,'p'=>$this->PageNo(),'l'=>$lnk);
		}
	}	
	
	function CreateReference($NbCol) {
		//Initialization
		$this->RefActive=1;
		$this->SetFontSize(8);	
		
		//New page
		$this->AddPage();
		$this->Bookmark('Index');	
		
		//Save the ordinate
		$this->y0=$this->GetY();
		$this->NbCol=$NbCol;
		$size=sizeof($this->Reference);
		$PageWidth=$this->w-$this->lMargin-$this->rMargin;	
		
		for ($i=0;$i<$size;$i++) {	
			//Handles page break and new position
			if ($this->ChangePage==1) {
				$this->ChangePage=0;
				$this->y0=$this->GetY()-$this->FontSize-1;
			}	
			
			//LibellLabel
			$str=$this->Reference[$i]['t'];
			$lnk=$this->Reference[$i]['l'];
			$strsize=$this->GetStringWidth($str);
			$this->Cell($strsize+2,$this->FontSize+2,$str,0,0,'R',0,$lnk);	
			
			//Dots
			//Computes the widths
			$ColWidth = ($PageWidth/$NbCol)-2;
			$w=$ColWidth-$this->GetStringWidth($this->Reference[$i]['p'])-($strsize+4);
			
			if ($w<10) {
				$w=10;
			}
			$nb=$w/$this->GetStringWidth('.');
			$dots=str_repeat('.',$nb-2);
			$this->Cell($w,$this->FontSize+2,$dots,0,0,'L');	
			//Page number
			$Largeur=$ColWidth-$strsize-$w;
			$this->MultiCell($Largeur,$this->FontSize+1,$this->Reference[$i]['p'],0,1,'R');
		}
		
		$this->RefActive==0;
	}	
}

// BMC 02.26.2016
// 	no idea what this does...
function cmp ($a, $b) {
    return strnatcmp(strtolower($a['t']), strtolower($b['t']));
}
?>
<?php
// BMC 02.21.2017
//  -- this class will help me write to log files so I don't have to deal with the minor details every time
class Logs {
    var $errorLogFile;

    function __construct() {
        // BMC 02.21.2017
        //  -- the constructor should initialize the log file that we want to write to
        if (!defined('SITE_ROOT')) {
            define('SITE_ROOT', dirname(dirname(__FILE__)));
        }

        $this->errorLogFile = SITE_ROOT . '/logs/error_logs.txt';
    }

    function getLogDate() {
        // BMC 02.21.2017
        //  -- get the current time this log was written and return it
        return date("Y-m-d H:i:s");
    }

    function writeToErrorLog($errorMessage) {
        // BMC 02.21.2017
        //  -- prepend the date and separate with a pipe, then put the error message and append a new line character
        $errorMessage = $this->getLogDate(). " | " . $errorMessage . "\n";

        // Write the contents to the file,
        // using the FILE_APPEND flag to append the content to the end of the file
        // and the LOCK_EX flag to prevent anyone else writing to the file at the same time
        file_put_contents($this->errorLogFile, $errorMessage, FILE_APPEND | LOCK_EX);
    }
}
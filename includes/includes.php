<?php
    header('Access-Control-Allow-Origin: *');
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
	// ********** DATE TIME ZONE ********** //
	date_default_timezone_set('America/Chicago');
	
	if (!defined('SITE_ROOT')) { // this is needed in case a one-time script has already defined it.
		define('SITE_ROOT', dirname(dirname(__FILE__)));
	}
	
	// ********** PDO (DATABASE) INCLUDES ********** //
	include_once(SITE_ROOT . "/includes/adodb_to_pdo.php");			// pdo functions
	include_once(SITE_ROOT . "/includes/Connect.php");				// connection functions
	
	// ********** PDF INCLUDES ********** //
	if (PHP_SAPI != 'cli') {
		require_once(SITE_ROOT . "/fpdf/fpdf.php");					// 	Contains the PDF class given by http://www.fpdf.org/
		define("FPDF_FONTPATH", SITE_ROOT . "/fpdf/font/");			// 	Defines the font path used in the FPDF class
		require_once(SITE_ROOT . "/fpdi/fpdi.php");					// 	Contains the PDF class given by http://www.fpdf.org/
		
		// ********** PDF HELPER CLASS ********** //
		include_once(SITE_ROOT . "/includes/Pdf.php");				// 	Custom PDF class which extends the main FPDF class
	}
	
	// ********** PHP MAILER ********** //
	include_once(SITE_ROOT . "/PHPMailer/PHPMailerAutoload.php");	// 	PHP Mailer to send proper mails from the server
	
	// ********** GLOBAL FUNCTIONS ********** //
	include_once(SITE_ROOT . "/includes/functions.php");			// global functions, must go before any other class
	
	// ********** API CLASS INCLUDES ********** //
	include_once(SITE_ROOT . "/includes/SyncAdapter.php");			// 	API SyncAdapter webservice class

    // ********** LOGS CLASS ********** //
    include_once(SITE_ROOT . "/includes/Logs.php");                  // LOGS class to write log files
	
	// ********** GLOBAL FUNCTIONS ********** //
	function getExportDirectory($provider, $member_id) {
		// BMC 09.11.2016
		//	-- this function will make the export directory if it does not exist
		// 		and then it will assign the file name and return it back;
		//	-- the member_id part is only useful for the new databases
		$exportFile = "";
		$exportDir = "";
		
		switch($provider) {
				case "TEST_DB":	# LITE-REGISTRY
					$exportDir = "/home/digitalbeef/domains/test.digitalbeef.com/public_html/exports/".$member_id;
					break;
                case "CWCF_CATTLE":	# COWCALF
                    $exportDir = "/home/digitalbeef/domains/cowcalf.digitalbeef.com/public_html/exports/".$member_id;
                    break;
				case "CPS_SWINE":	# CERTIFIED PEDIGREE SWINE
					$exportDir = "/home/digitalbeef/domains/cps.digitalswine.com/public_html/exports/".$member_id;
					break;
				case "NSR_SWINE":	# NATIONAL SWINE REGISTRY
					$exportDir = "/home/digitalbeef/domains/nsr.digitalswine.com/public_html/exports/".$member_id;
					break;
				case "BKS_SWINE":	# BERKSHIRE (NATIONAL SWINE REGISTRY)
					$exportDir = "/home/digitalbeef/domains/berkshire.digitalswine.com/public_html/exports/".$member_id;
					break;
				case "ABHA_CATTLE":	# AMERICAN BLACK HEREFORD ASSOCIATION
					$exportDir = "/home/digitalbeef/domains/abha.digitalbeef.com/public_html/exports/".$member_id;
					break;
				case "AAKA_CATTLE":	# AKAUSHI
					$exportDir = "/home/digitalbeef/domains/akaushi.digitalbeef.com/public_html/exports/herd_reports";
					break;
				case "ACRS_CATTLE":	# CHIANINA
					$exportDir = "/home/digitalbeef/domains/chianina.digitalbeef.com/public_html/exports/herd_reports";
					break;
				case "AGVA_CATTLE":	# USA GELBVIEH
					$exportDir = "/home/digitalbeef/domains/gelbvieh.digitalbeef.com/public_html/exports/herd_reports";
					break;
				case "CDGV_CATTLE":	# CAN GELBVIEH
					$exportDir = "/home/digitalbeef/domains/cdgv.digitalbeef.com/public_html/exports/herd_reports";
					break;
				case "NALRS_CATTLE": # USA LIMOUSIN
					$exportDir = "/home/digitalbeef/domains/limousin.digitalbeef.com/public_html/exports/herd_reports";
					break;
				case "CLRS_CATTLE":	# CAN LIMOUSIN
					$exportDir = "/home/digitalbeef/domains/cla.digitalbeef.com/public_html/exports/herd_reports";
					break;
				case "AMARS_CATTLE": # MAINE-ANJOU
					$exportDir = "/home/digitalbeef/domains/maine-anjou.digitalbeef.com/public_html/exports/herd_reports";
					break;
				case "ASA_CATTLE": # USA SHORTHORN
					$exportDir = "/home/digitalbeef/domains/shorthorn.digitalbeef.com/public_html/exports/herd_reports";
					break;
				default:
					die("cannot create export directory");
					break;	
		}
		
		// attempt to create directory if it does not already exist
		if (!file_exists($exportDir)) {
			mkdir($exportDir, 0755, true);
		}
		
		// now return the directory
		return $exportDir."/";	// we append the forward slash here as it'll make things easier later on
	}
?>
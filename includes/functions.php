    <?php
    function getColumnIndex($cols, $search_value) {
        // this will take in the comma delimited string of columns and explode
        // it to an actual array.  then it will try to find the search value
        // and return the index
        $columns = explode(",", $cols);
        for($i = 0; $i < count($columns); $i++) {
            if(trim($columns[$i]) == $search_value) {
                return $i;
            }
        }

        return -1;
    }

    function getValueFromIndex($vals, $index) {
        // this will get the associated value from the comma delimited string of values
        // and return the value.
        $values = explode(",", $vals);

        // we have to take into account that each value will be surrounded with
        // single quotes.  So we need to remove these.  then we need to trim that
        // value so we're only getting the actual value with no spaces.
        if($index == -1) {
            return "";	// return a blank value
        } else {
            return trim(str_replace("'", "", $values[$index]));
        }
    }

    function getValuePlaceHolders($vals) {
        // this function will simply do a count on the number of values that
        // were passed and create a ? for each value so we can use the params
        // later on
        $values = explode(",", $vals);

        $placeHolders = "";
        for($i = 0; $i < count($values); $i++) {
            if($i == 0) {
                $placeHolders .= "?";
            } else {
                $placeHolders .= ",?";
            }
        }

        return $placeHolders;
    }

    function clearValuesOfQuotes($values) {
        // this function will get the values and clear them of quotes
        // so we can pass them into the params.  it will take in the
        // string array and return the cleaned string array back
        $clean_values = array();
        for($i = 0; $i < count($values); $i++) {
            // trim and remove the single quotes
            $clean_values[] = trim(str_replace("'", "", $values[$i]));
        }

        return $clean_values;
    }

    function getColumnToValueString($columns, $values) {
        // this will take in two arrays, the columns and values.
        // it will check to make sure they are the same size and
        // then it will set the column = ? form in a comma delimited
        // string.
        $columns = explode(",", $columns);
        $values = explode(",", $values);

        if(count($columns) == count($values)) {
            // this should always be the case
            $column_to_value = "";
            for($i = 0; $i < count($columns); $i++) {
                if($i == 0) {
                    $column_to_value .= $columns[$i]." = ?";
                } else {
                    $column_to_value .= ", ".$columns[$i]." = ?";
                }
            }

            return $column_to_value;
        } else {
            // if this is not the case then we need to fail properly
            return NULL;
        }
    }

    function setValueAtIndex($index, $value, $values_array) {
        // we are given the values array which we will need to put into
        // a string array using the comma delimiter.  then we will look for the
        // particular index within this string array and replace the value of that
        // index with the new value.  Then we will put this all back into a string
        // that is comma delimited!

        if($index == -1) {
            return $values_array;
        } else {
            $values = explode(",", $values_array);	// put into string array
            $values[$index] = "'".$value."'";		// change the value at the index
            $values_array = implode(",", $values);	// put back into comma delimited string
        }

        return $values_array;
    }

    function modifyValuesWithActualReg($columns_array, $values_array, $PDO) {
        if($PDO->DB_TYPE === "NEW") {
            $tempReg = getValueFromIndex($values_array, getColumnIndex($columns_array, "registration"));
        } else if($PDO->DB_TYPE === "OLD") {
            $tempReg = getValueFromIndex($values_array, getColumnIndex($columns_array, "animal_registration"));
        } else {
            $tempReg = "";
        }

        // get the actual registration from the temporary registration
        // and then change the value at that index with the new registration
        $sql = "SELECT actual_registration
                    FROM temp_registration_check
                    WHERE temp_registration = ?";
        $params = array($tempReg);
        $rs = $PDO->recordSetQuery($sql, $params);
        if($rs) {
            if(!$rs->EOF) {
                // actual registration exists
                $rs->MoveFirst();
                $actualReg = strtoupper(trim($rs->fields['actual_registration']));
            } else {
                $actualReg = $tempReg;
            }
            $rs->Close();
        } else {
            $actualReg = $tempReg;
        }

        // return the values array back with the updated registration
        if($PDO->DB_TYPE === "NEW") {
            return setValueAtIndex(getColumnIndex($columns_array, "registration"), $actualReg, $values_array);
        } else if($PDO->DB_TYPE === "OLD") {
            return setValueAtIndex(getColumnIndex($columns_array, "animal_registration"), $actualReg, $values_array);
        } else {
            return $values_array;
        }

    }

    function modifySqlQueryWithActualReg($columns_array, $values_array, $sql_query, $PDO) {
        if($PDO->DB_TYPE === "NEW") {
            $tempReg = getValueFromIndex($values_array, getColumnIndex($columns_array, "registration"));
        } else if($PDO->DB_TYPE === "OLD") {
            $tempReg = getValueFromIndex($values_array, getColumnIndex($columns_array, "animal_registration"));
        } else {
            $tempReg = "";
        }

        // get the actual registration from the temporary registration
        // and then change the value at that index with the new registration
        $sql = "SELECT actual_registration
                FROM temp_registration_check
                WHERE temp_registration = ?";
        $params = array($tempReg);
        $rs = $PDO->recordSetQuery($sql, $params);
        if($rs) {
            if(!$rs->EOF) {
                // actual registration exists
                $rs->MoveFirst();
                $actualReg = strtoupper(trim($rs->fields['actual_registration']));
            } else {
                $actualReg = $tempReg;
            }
            $rs->Close();
        } else {
            $actualReg = $tempReg;
        }

        // return the actual updated sql query
        return str_replace($tempReg, $actualReg, $sql_query);
    }

    function modifyWhereClauseWithActualReg($where_clause, $PDO) {
        // BMC 06.01.2017
        //    -- because the where clause is a single string, we don't need to provide all the other
        //      stuff, we just do a simple function to find the reg
        $string_parts = explode("'", $where_clause);
        $tempReg = "";
        foreach($string_parts as $part) {
            if (strpos($part, 'TEMP_REG_DB') !== false) {
                $tempReg = $part;
                break;
            }
        }

        // get the actual registration from the temporary registration
        // and then change the value at that index with the new registration
        $sql = "SELECT actual_registration
                FROM temp_registration_check
                WHERE temp_registration = ?";
        $params = array($tempReg);
        $rs = $PDO->recordSetQuery($sql, $params);
        if($rs) {
            if(!$rs->EOF) {
                // actual registration exists
                $rs->MoveFirst();
                $actualReg = strtoupper(trim($rs->fields['actual_registration']));
            } else {
                $actualReg = $tempReg;
            }
            $rs->Close();
        } else {
            $actualReg = $tempReg;
        }

        // return the actual updated where clause
        return str_replace($tempReg, $actualReg, $where_clause);
    }

    function getCurlImage($url) {
        // BMC 11.29.2016
        // will get the image data from the cgi stream
        $headers[] = 'Accept: image/gif, image/x-bitmap, image/jpeg, image/png';
        $headers[] = 'Connection: Keep-Alive';
        $headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
        $user_agent = 'php';
        $process = curl_init($url);
        curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($process, CURLOPT_TIMEOUT, 5);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
        $return = curl_exec($process);
        curl_close($process);
        return $return;
    }
    ?>
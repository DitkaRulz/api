<?php
class SyncAdapter {
	var $PDO;		// connection class given by the constructor
	var $MAIL_USER;	// php mailer username
	var $MAIL_PASS;	// php mailer password

	function __construct($PDO=NULL) {
		// we'll set the connection class
		$this->PDO = $PDO;
		$this->MAIL_USER = "support@digitalbeef.com";
		$this->MAIL_PASS = "d1g1t@lbeef";
	}

	function sendMail($message, $subject=NULL, $toEmail=NULL) {
		// BMC 10.06.2016
		//	-- this function will send a mail using the support credentials to whoever
		//		the toEmail address says.  It will default to the support email

		if(is_null($subject)) {
			$subject = "Feedback Submission - App Sync Webservice";
		}

		if(is_null($toEmail)) {
			$toEmail = "support@digitalbeef.com";
			$toEmailName = "Feedback Submission";
		} else {
			$toEmailName = $toEmail;
		}

		// SMTP Authentication Credentials
		$mailUser = $this->MAIL_USER;
		$mailPass = $this->MAIL_PASS;

		// Mailer for Client
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = 'smtp.gmail.com';
		$mail->SMTPAuth = true;
		$mail->Username = $mailUser;
		$mail->Password = $mailPass;
		$mail->SMTPSecure = 'tls';
		$mail->Port = 587;

		// build the message now
		$mail_message = "<html><body>";
		$mail_message .= $message;
		$mail_message .= "</body></html>";

		$mail->setFrom($mailUser, "DigitalBeef, LLC"); 			// From
		$mail->addAddress(strtolower($toEmail), $toEmailName); 	// To
		$mail->isHTML(true);
		//$mail->SMTPDebug = 2;

		$mail->Subject 	= $subject;
		$mail->Body 	= $mail_message;

		// attempt to send the email
		// the return will be true if it sent okay and false if it didn't
		// so we can handle it accordingly
		return $mail->send();
	}

	function getMemberIdFromMembershipId($membership_id) {
		// BMC 09.12.2016
		//	-- this function is useful for several associations that use
		//		a non-numeric membership id
		if(is_numeric($membership_id)) {
			return $membership_id;
		} else {
			$membership_id = trim(strtoupper($membership_id));
			$sql = "SELECT member_id
					FROM tbl_member
					WHERE UPPER(membership_id) = UPPER(?)";
			$params = array($membership_id);
			$rs = $this->PDO->recordSetQuery($sql, $params);
			if($rs) {
				if(!$rs->EOF) {
					$rs->MoveFirst();
					return $rs->fields['member_id'];
				} else {
					return $membership_id;
				}
			} else {
				return $membership_id;
			}
		}
	}

	function GetCustomerData($REQUEST, $provider) {
		// BMC 10.17.2016
		//	-- we're getting in a customer ID.  If this is akaushi or shorthorn
		//		then we'll need to convert it to the actual id.
		//	-- get the customer information and send it back in the customer array
		$response["success"] = false;
		$response["message"] = "processing digitalbeef api webservice...";
		$response["array"]	= array();

		$customer_id = (isset($REQUEST['customer_id']) ? $REQUEST['customer_id'] : NULL);

		if(is_null($customer_id)) {
			$response["success"] = false;
			$response["message"] = "ERROR GCD001: failed to get customer data";
			$response["array"]	= array();
			return json_encode($response);
		} else {
			if($this->PDO->DB_TYPE === "NEW") {
				$sql = "SELECT m.member_id,
							m.member_name,
							c.address_line_1,
							c.address_line_2,
							c.city,
							c.state,
							c.zip_code,
							c.country,
							c.email,
							c.phone_number
						FROM member m
							LEFT JOIN contact c ON m.member_id = c.member_id
								AND c.is_primary
						WHERE m.member_id = ?";
				$params = array($customer_id);
				$rs = $this->PDO->recordSetQuery($sql, $params);
				if($rs) {
					if(!$rs->EOF) {
						// send all the data back
						$response["success"] = true;
						$response["message"] = "customer data received successfully";
						array_push($response["array"], $rs->fields);
						return json_encode($response);
					} else {
						$response["success"] = false;
						$response["message"] = "ERROR GCD004: failed to get customer data";
						$response["array"]	= array();
						return json_encode($response);
					}
				} else {
					$response["success"] = false;
					$response["message"] = "ERROR GCD003: failed to get customer data";
					$response["array"]	= array();
					return json_encode($response);
				}
			} else if($this->PDO->DB_TYPE === "OLD") {
				// first convert the alpha-numeric id to the numeric id if applicable
				switch($provider) {
					case "AAKA_CATTLE":
					case "ASA_CATTLE":
                    case "NALRS_CATTLE":
						$customer_id = $this->getMemberIdFromMembershipId($customer_id);
						break;
					default:
						break;
				}

				$sql = "SELECT m.member_id,
							m.member_name,
							a.address_line1 AS address_line_1,
							a.address_line2 AS address_line_2,
							a.address_city AS city,
							a.address_state AS state,
							a.address_postal_code AS zip_code,
							a.address_country AS country,
							a.address_email AS email,
							'' AS phone_number
						FROM tbl_member m
                        	LEFT JOIN tbl_address a ON m.general_correspondance_address = a.address_id
						WHERE m.member_id = ?";
						//(p.phone_area_code::VARCHAR || p.phone_prefix::VARCHAR || p.phone_suffix::VARCHAR) AS phone_number
						//LEFT JOIN tbl_phone_number p ON m.general_correspondance_address = p.phone_number_id
				$params = array($customer_id);
				$rs = $this->PDO->recordSetQuery($sql, $params);
				if($rs) {
					if(!$rs->EOF) {
						// send all the data back
						$response["success"] = true;
						$response["message"] = "customer data received successfully";
						array_push($response["array"], $rs->fields);
						return json_encode($response);
					} else {
						$response["success"] = false;
						$response["message"] = "ERROR GCD005: failed to get customer data";
						$response["array"]	= array();
						return json_encode($response);
					}
				} else {
					$response["success"] = false;
					$response["message"] = "ERROR GCD006: failed to get customer data";
					$response["array"]	= array();
					return json_encode($response);
				}
			} else {
				$response["success"] = false;
				$response["message"] = "ERROR GCD002: failed to get customer data";
				$response["array"]	= array();
				return json_encode($response);
			}
		}
	}

	function WooCommerceDeleteMember($REQUEST) {
		// BMC 10.24.2016
		//	-- we will receive a token and a member id from the woo commerce people
		//		given these we will delete the record by the member_id
		$response["success"] = false;
		$response["message"] = "processing digitalbeef api webservice...";

		// here are the input request variables we receive
		$member_id 	= (isset($REQUEST['member_id']) 	? $REQUEST['member_id'] 	: NULL);
		$token 		= (isset($REQUEST['token']) 		? $REQUEST['token'] 		: NULL);

		if(is_null($token)) {
			$response["success"] = false;
			$response["message"] = "ERROR WCDM001: failed to delete member from database";
			return json_encode($response);
		} else if(is_null($member_id)) {
			$response["success"] = false;
			$response["message"] = "ERROR WCDM002: failed to delete member from database";
			return json_encode($response);
		} else {
			// check the token
			if($token !== "1fbc29612193318a3686ee8969f48acf") {
				$response["success"] = false;
				$response["message"] = "ERROR WCDM003: failed to delete member from database";
				return json_encode($response);
			} else {
				// we have the correct token so we can continue on
				if($member_id !== "1" && $member_id !== 1 && $member_id !== true) {
					// account for removing the entire database
					// don't let these options run
					$sql = "DELETE FROM digitalbeef_subscription 
							WHERE member_id = ?";
					$params = array($member_id);
					if($this->PDO->executeQuery($sql, $params, false)) {
						$response["success"] = true;
						$response["message"] = "member has been successfully deleted from the database";
						return json_encode($response);
					} else {
						$response["success"] = false;
						$response["message"] = "ERROR WCDM005: failed to delete member from database";
						return json_encode($response);
					}
				} else {
					$response["success"] = false;
					$response["message"] = "ERROR WCDM004: failed to delete member from database";
					return json_encode($response);
				}
			}
		}
	}

	function WooCommerceUpdate($REQUEST) {
		// BMC 10.13.2016
		//	-- we need to be able to update the subscription table
		//		here we can send in a member_id of what to update then we
		//		can send in the column as well as a value that should be updated
		$response["success"] = false;
		$response["message"] = "processing digitalbeef api webservice...";

		// here are the input request variables we receive
		$member_id 	= (isset($REQUEST['member_id']) 	? $REQUEST['member_id'] 	: "");
		$column 	= (isset($REQUEST['column']) 		? $REQUEST['column'] 		: "");
		$value 		= (isset($REQUEST['value']) 		? $REQUEST['value'] 		: "");

		if(is_null($member_id)) {
			$response["success"] = false;
			$response["message"] = "ERROR WCU001: failed to update column";
			return json_encode($response);
		} else {
			// prep data here
			switch($column) {
				case "password":
					$value = md5($value);
					break;
				default:
					break;
			}

			$sql = "UPDATE digitalbeef_subscription
					SET ".$column." = ?
					WHERE member_id = ?";
			$params = array($value, $member_id);
			if($this->PDO->executeQuery($sql, $params, false)) {
				$response["success"] = true;
				$response["message"] = "update success";
				return json_encode($response);
			} else {
				$response["success"] = true;
				$response["message"] = "ERROR WCU002: failed to update column";
				return json_encode($response);
			}
		}
	}

	function WooCommerceFreeTrialPush($REQUEST, $provider) {
		// BMC 10.11.2016
		//	-- woo commerce wants to push data from the free trial form to the database
		//	-- we'll receive the data through request variables and send it to the
		//		database and return a success/fail with a message if all goes well.
		$response["success"] = false;
		$response["message"] = "processing digitalbeef api webservice...";

		// here are the input request variables we receive
		$member_id 				= (isset($REQUEST['member_id']) 				? $REQUEST['member_id'] 				: "");				// varchar - unique - not null
		$password 				= (isset($REQUEST['password']) 					? $REQUEST['password'] 					: "");				// varchar - md5 - not null
		$subscription_plan 		= (isset($REQUEST['subscription_plan']) 		? $REQUEST['subscription_plan'] 		: "");				// varchar - not null
		$subscription_start_date= (isset($REQUEST['subscription_start_date']) 	? $REQUEST['subscription_start_date'] 	: date("Y-m-d"));	// timestamp - not null
		$subscription_begin_date= (isset($REQUEST['subscription_begin_date']) 	? $REQUEST['subscription_begin_date'] 	: date("Y-m-d"));	// timestamp - not null
		$subscription_end_date	= (isset($REQUEST['subscription_end_date']) 	? $REQUEST['subscription_end_date'] 	: date("Y-m-d"));	// timestamp - not null
		$first_name				= (isset($REQUEST['first_name']) 				? $REQUEST['first_name'] 				: "");				// varchar - not null
		$last_name 				= (isset($REQUEST['last_name']) 				? $REQUEST['last_name'] 				: "");				// varchar - not null
		$ranch_name 			= (isset($REQUEST['ranch_name']) 				? $REQUEST['ranch_name'] 				: "");				// varchar
		$email_address 			= (isset($REQUEST['email_address']) 			? $REQUEST['email_address'] 			: "");				// varchar - not null
		$phone_number 			= (isset($REQUEST['phone_number']) 				? $REQUEST['phone_number'] 				: "");				// varchar
		$address_line_1			= (isset($REQUEST['address_line_1']) 			? $REQUEST['address_line_1'] 			: "");				// varchar
		$address_line_2			= (isset($REQUEST['address_line_2']) 			? $REQUEST['address_line_2'] 			: "");				// varchar
		$city 					= (isset($REQUEST['city']) 						? $REQUEST['city'] 						: "");				// varchar
		$state 					= (isset($REQUEST['state']) 					? $REQUEST['state'] 					: "");				// varchar
		$zip_code 				= (isset($REQUEST['zip_code']) 					? $REQUEST['zip_code'] 					: "");				// varchar

		// everything is stored in the table "digitalbeef_subscription"
		// first check to see if this member_id already exists
		$sql = "SELECT _id
				FROM digitalbeef_subscription
				WHERE member_id = ?";
		$params = array($member_id);
		$rs = $this->PDO->recordSetQuery($sql, $params);
		if($rs) {
			if($rs->EOF) {
				// no member exists so we're okay
				// now insert the new member into the database
				$sql = "INSERT INTO digitalbeef_subscription
							(member_id, password, subscription_plan, subscription_start_date, subscription_begin_date,
							subscription_end_date, first_name, last_name, ranch_name, email_address, phone_number,
							address_line_1, address_line_2, city, state, zip_code)
						VALUES
							(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				$params 	= array();
				$params[] 	= $member_id;
				$params[] 	= md5($password); 	// md5 the password
				$params[] 	= $subscription_plan;
				$params[] 	= $subscription_start_date;
				$params[] 	= $subscription_begin_date;
				$params[] 	= $subscription_end_date;
				$params[] 	= $first_name;
				$params[] 	= $last_name;
				$params[] 	= $ranch_name;
				$params[] 	= $email_address;
				$params[] 	= $phone_number;
				$params[] 	= $address_line_1;
				$params[] 	= $address_line_2;
				$params[] 	= $city;
				$params[] 	= $state;
				$params[]	= $zip_code;
				if($this->PDO->executeQuery($sql, $params, false)) {
					// BMC 10.27.2016
					//	-- we need to add the member information on the new database

					if($this->PDO->DB_TYPE === "NEW") {
					    // BMC 12.01.2016
                        //  -- we are ONLY inserting new member data for TEST and for CowCalf
                        //  -- the others shouldn't need this, for example, abha, greenacres
                        //      they will already have their membership information set
                        switch($provider) {
                            case "TEST_DB":
                            case "CWCF_CATTLE":
                                $sql = "INSERT INTO member
                                        (member_id, member_password, member_name, membership_type, ranch_name, member_since,
                                        membership_begin, membership_end, active_flag, mobile_app_active_flag,
                                        is_staff, is_admin, legacy_password, is_registered)
                                        VALUES
                                        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                                $params 	= array();
                                $params[]	= $member_id;
                                $params[]	= md5($password);
                                $params[]	= $first_name." ".$last_name;
                                $params[]	= "M";
                                $params[]	= $ranch_name;
                                $params[]	= $subscription_start_date;
                                $params[]	= $subscription_begin_date;
                                $params[]	= $subscription_end_date;
                                $params[]	= 't';
                                $params[]	= 't';
                                $params[]	= 'f';
                                $params[]	= 'f';
                                $params[]	= $password;
                                $params[]	= 't';
                                if($this->PDO->executeQuery($sql, $params, false)) {
                                    // now add the primary contact information
                                    $sql = "INSERT INTO contact
                                            (member_id, nickname, address_line_1, address_line_2, city,
                                            state, zip_code, email, phone_number, is_primary)
                                        VALUES
                                            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                                    $params 	= array();
                                    $params[]	= $member_id;
                                    $params[]	= "PRIMARY";
                                    $params[]	= $address_line_1;
                                    $params[]	= $address_line_2;
                                    $params[]	= $city;
                                    $params[]	= $state;
                                    $params[]	= $zip_code;
                                    $params[]	= $email_address;
                                    $params[]	= $phone_number;
                                    $params[]	= true;
                                    if($this->PDO->executeQuery($sql, $params, false)) {
                                        // inserted new member into the system
                                        $response["success"] = true;
                                        $response["message"] = "free trial data entered into the database successfully";
                                        return json_encode($response);
                                    } else {
                                        $response["success"] = false;
                                        $response["message"] = "ERROR WCFTP005: failed query";
                                        return json_encode($response);
                                    }
                                } else {
                                    $response["success"] = false;
                                    $response["message"] = "ERROR WCFTP004: failed query".$sql;
                                    $response["message"] .= "<br><pre>".print_r($params)."</pre>";
                                    return json_encode($response);
                                }
                                break;
                            default:
                                // the data was entered successfully
                                $response["success"] = true;
                                $response["message"] = "free trial data entered into the database successfully";
                                return json_encode($response);
                                break;
                        }
					} else if($this->PDO->DB_TYPE === "OLD") {
						// the data was entered successfully
						$response["success"] = true;
						$response["message"] = "free trial data entered into the database successfully";
						return json_encode($response);
					} else {
						$response["success"] = false;
						$response["message"] = "ERROR WCFTP006: failed query";
						return json_encode($response);
					}
				} else {
					// the query failed to enter
					$response["success"] = false;
					$response["message"] = "ERROR WCFTP003: failed query";
					return json_encode($response);
				}
			} else {
				// this member id already exists so we can't insert it or update it
				$response["success"] = false;
				$response["message"] = "ERROR WCFTP002: ".$member_id." already exists in the database";
				return json_encode($response);
			}
		} else {
			// something went wrong with the sql query
			$response["success"] = false;
			$response["message"] = "ERROR WCFTP001: failed query";
			return json_encode($response);
		}
	}

	function LoginWooCommerce($REQUEST, $provider) {
		// BMC 10.04.2016
		//	-- we receive in the member_id and the password from the webservice request variable
		//		then we validate the login credentials.
		//	-- once validated we return the encoded json as the result, which will be echoed out
		//		in the sync_webservice
		$response["success"] = false;
		$response["message"] = "processing digitalbeef api webservice...";
		$response["array"] 	 = array();

		$member_id	= (isset($REQUEST['username']) ? $REQUEST['username'] : "");
		$password 	= (isset($REQUEST['password']) ? $REQUEST['password'] : "");

		if($this->PDO->DB_TYPE === "NEW") {
			// BMC 10.11.2016
			//	-- updated with the new digitalbeef_subscription table
			$sql = "SELECT d.member_id,
                        d.subscription_plan,
                        d.subscription_start_date,
                        d.subscription_begin_date,
                        d.subscription_end_date,
                        d.first_name,
                        d.last_name,
                        d.ranch_name,
                        d.email_address,
                        d.phone_number,
                        d.address_line_1,
                        d.address_line_2,
                        d.city,
                        d.state,
                        d.zip_code,
                        COUNT (a.registration) AS num_head
					FROM digitalbeef_subscription d
                        LEFT JOIN ownership o ON d.member_id = o.member_id
                        	AND NOT o.superceded
                        LEFT JOIN animal a ON o.registration = a.registration
                        	AND a.status = '0'
                            AND NOT a.is_deleted
					WHERE d.member_id = ?
						AND d.password = ?
                    GROUP BY d.member_id,
                        d.subscription_plan,
                        d.subscription_start_date,
                        d.subscription_begin_date,
                        d.subscription_end_date,
                        d.first_name,
                        d.last_name,
                        d.ranch_name,
                        d.email_address,
                        d.phone_number,
                        d.address_line_1,
                        d.address_line_2,
                        d.city,
                        d.state,
                        d.zip_code
					LIMIT 1";
			$params = array($member_id, md5($password));
			$rs = $this->PDO->recordSetQuery($sql, $params);
			if($rs) {
				if(!$rs->EOF) {
					$rs->MoveFirst();

					array_push($response["array"], $rs->fields);

					$response["success"] = true;
					$response["message"] = "login successful";
					return json_encode($response);
				} else {
					$response["success"] = false;
					$response["message"] = "invalid login credentials";
					return json_encode($response);
				}
			} else {
				$response["success"] = false;
				$response["message"] = "invalid sql: ". $sql;
				return json_encode($response);
			}
		} else if($this->PDO->DB_TYPE === "OLD") {
			// BMC 08.16.2016
			//	-- add the login webservice for the old databases
			//		if i recall correctly, this requires mysql access so i'll have to modify the connect.php
			//	-- keep in mind the password coming from the mysql is not encrypted!!!  so we just
			//		compare direct strings
			// BMC 09.12.2016
			//	-- handle the membership_id to member_id conversion for groups like akaushi and others
			switch($provider) {
				case "AAKA_CATTLE":
				case "ASA_CATTLE":
                case "NALRS_CATTLE":
					$member_id = $this->getMemberIdFromMembershipId($member_id);
					break;
				default:
					break;
			}

			$sql = "SELECT * FROM _users WHERE pn_uname = '".$member_id."' AND pn_pass = '".md5($password)."'";
    		$rs = $this->PDO->recordSetQueryMySql($sql);
			if($rs) {
				if(!$rs->EOF) {
					// BMC 10.11.2016
					//	-- updated with the new digitalbeef_subscription table
					$sql = "SELECT d.member_id,
								d.subscription_plan,
								d.subscription_start_date,
								d.subscription_begin_date,
								d.subscription_end_date,
								d.first_name,
								d.last_name,
								d.ranch_name,
								d.email_address,
								d.phone_number,
								d.address_line_1,
								d.address_line_2,
								d.city,
								d.state,
								d.zip_code,
								COUNT (a.animal_registration) AS num_head
							FROM digitalbeef_subscription d
								LEFT JOIN tbl_ownership o ON d.member_id = o.owner_id::VARCHAR
									AND NOT o.superceded_flag
								LEFT JOIN tbl_animal a ON o.animal_registration = a.animal_registration
									AND a.animal_record_status = 'A'
							WHERE d.member_id = ?
								AND d.password = ?
							GROUP BY d.member_id,
								d.subscription_plan,
								d.subscription_start_date,
								d.subscription_begin_date,
								d.subscription_end_date,
								d.first_name,
								d.last_name,
								d.ranch_name,
								d.email_address,
								d.phone_number,
								d.address_line_1,
								d.address_line_2,
								d.city,
								d.state,
								d.zip_code
							LIMIT 1";
					$params = array($member_id, md5($password));
					$rs = $this->PDO->recordSetQuery($sql, $params);
					if($rs) {
						if(!$rs->EOF) {
							$rs->MoveFirst();
							array_push($response["array"], $rs->fields);

							$response["success"] = true;
							$response["message"] = "login successful";
							return json_encode($response);
						} else {
							$response["success"] = false;
							$response["message"] = "failed to get member data";
							return json_encode($response);
						}
					} else {
						$response["success"] = false;
						$response["message"] = "invalid sql: ". $sql;
						return json_encode($response);
					}
				} else {
					$response["success"] = false;
					$response["message"] = "invalid login credentials: ".$sql;
					return json_encode($response);
				}
			} else {
				$response["success"] = false;
				$response["message"] = "invalid sql: ". $sql;
				return json_encode($response);
			}
		} else {
            $response["success"] = false;
            $response["message"] = "invalid login credentials";
            return json_encode($response);
        }
	}

    function CheckLoginExists($REQUEST, $provider) {
        // BMC 12.06.2016
        //  -- check login exists for the woo commerce free trial for associations with pre-existing login's
        $response["success"] = false;
        $response["message"] = "processing digitalbeef api webservice...";
        $response["array"] 	 = array();

        $member_id	= (isset($REQUEST['username']) ? $REQUEST['username'] : "");
        $password 	= (isset($REQUEST['password']) ? $REQUEST['password'] : "");

        if($this->PDO->DB_TYPE === "NEW") {
            switch ($provider) {
                case "ABHA_CATTLE":
                case "CPS_SWINE":
                case "NSR_SWINE":
                case "BKS_SWINE":
                    $sql = "SELECT member_id
                            FROM member
                            WHERE member_id = ?
                            AND member_password = ?";
                    $params = array($member_id, md5($password));
                    $rs = $this->PDO->recordSetQuery($sql, $params);
                    if($rs) {
                        if (!$rs->EOF) {
                            $response["success"] = true;
                            $response["message"] = "login successful, user exists";
                            return json_encode($response);
                        } else {
                            $response["success"] = false;
                            $response["message"] = "login failed, user does not exist";
                            return json_encode($response);
                        }
                    } else {
                        $response["success"] = false;
                        $response["message"] = "ERROR CLE0002: login failed, sql error";
                        return json_encode($response);
                    }
                    break;
                default:
                    $response["success"] = true;
                    $response["message"] = "login successful, user exists";
                    return json_encode($response);
                    break;
            }

        } else if($this->PDO->DB_TYPE === "OLD") {
            // BMC 12.27.2016
            //  -- sql doesn't seem to need to use the "member_id"
            //      the mysql database looks up the membership id instead
            /*
            switch ($provider) {
                case "AAKA_CATTLE":
                case "ASA_CATTLE":
                case "NALRS_CATTLE":
                    $member_id = $this->getMemberIdFromMembershipId($member_id);
                    break;
                default:
                    break;
            }
            */

            $sql = "SELECT * FROM _users WHERE UPPER(pn_uname) = UPPER('" . $member_id . "') AND pn_pass = '" . md5($password) . "'";
            $rs = $this->PDO->recordSetQueryMySql($sql);
            if ($rs) {
                if (!$rs->EOF) {
                    $response["success"] = true;
                    $response["message"] = "login successful, user exists";
                    return json_encode($response);
                } else {
                    $response["success"] = false;
                    $response["message"] = "login failed, user does not exist";
                    return json_encode($response);
                }
            } else {
                $response["success"] = false;
                $response["message"] = "ERROR CLE0001: login failed, sql error";
                return json_encode($response);
            }
        } else {
            $response["success"] = false;
            $response["message"] = "ERROR CLE0003: unable to determine provider";
            return json_encode($response);
        }
    }

	function Login($REQUEST, $provider) {
		// BMC 10.04.2016
		//	-- we receive in the member_id and the password from the webservice request variable
		//		then we validate the login credentials.
		//	-- once validated we return the encoded json as the result, which will be echoed out
		//		in the sync_webservice
		// BMC 10.17.2016
		//	-- this needs to be updated when the subscription service is live, using the
		//		table: digitalbeef_subscription to get the subscription details
        // TODO BMC 07.21.2017
        //  -- we will have to update the login process to use the secure method of connection
        //      for all new databases
		$response["success"] = false;
		$response["message"] = "processing digitalbeef api webservice...";
		$response["array"] 	 = array();

		$member_id	= (isset($REQUEST['username']) ? $REQUEST['username'] : "");
		$password 	= (isset($REQUEST['password']) ? $REQUEST['password'] : "");

		if($this->PDO->DB_TYPE === "NEW") {
			$sql = "SELECT *
					FROM member
					WHERE UPPER(member_id) = UPPER(?)
						AND member_password = ?";
			$params = array($member_id, md5($password));
			$rs = $this->PDO->recordSetQuery($sql, $params);
			if($rs) {
				if(!$rs->EOF) {
					$rs->MoveFirst();

					array_push($response["array"], $rs->fields);

					$response["success"] = true;
					$response["message"] = "login successful";
					return json_encode($response);
				} else {
					$response["success"] = false;
					$response["message"] = "invalid login credentials";
					return json_encode($response);
				}
			} else {
				$response["success"] = false;
				$response["message"] = "ERROR L004: failed to log in";
				return json_encode($response);
			}
		} else if($this->PDO->DB_TYPE === "OLD") {
			// BMC 08.16.2016
			//	-- add the login webservice for the old databases
			//		iirc, this requires mysql access so i'll have to modify the connect.php
			//	-- keep in mind the password coming from the mysql is not encrypted!!!  so we just
			//		compare direct strings
			// BMC 09.12.2016
			//	-- handle the membership_id to member_id conversion for groups like akaushi and others
			switch($provider) {
				case "AAKA_CATTLE":
				case "ASA_CATTLE":
                case "NALRS_CATTLE":
                    $membership_id = $member_id;
					$member_id = $this->getMemberIdFromMembershipId($member_id);
					$response['member_id'] = $member_id;
					break;
				default:
					$response['member_id'] = $member_id;
					break;
			}

			// BMC 10.20.2016
			//	-- use parameters rather than setting the params inline
			//		applied changes to the Connect.php class.
			$sql = "SELECT * FROM _users WHERE UPPER(pn_uname) = UPPER(?) AND LTRIM(RTRIM(pn_pass)) = LTRIM(RTRIM(?))";
            switch ($provider) {
                case "NALRS_CATTLE":
                case "ASA_CATTLE":
                case "AAKA_CATTLE":
                    $params = array($membership_id, md5($password));
                    break;
                default:
                    $params = array($member_id, md5($password));
                    break;
            }

    		$rs = $this->PDO->recordSetQueryMySql($sql, $params);
			if($rs) {
				if(!$rs->EOF) {
					// the difference between the new database and the old database is that
					// we have to do this extra sql query to the postgresql database and
					// then get the member data.
					// BMC 10.14.2016
					//	-- this will ensure that any member that wants to log into the app
					// 		that is part of an association will have to have at least have
					//		an active subscription.
                    /*
					$sql = "SELECT m.*
							FROM tbl_member m
                            INNER JOIN tbl_android_members a ON m.member_id = a.member_id
                            	AND a.active_flag
							WHERE UPPER(m.member_id::VARCHAR) = UPPER(?::VARCHAR)";
                    */
					// BMC 06.15.2017
                    //  -- no longer require the tbl_android members to be active when logging in
                    //  -- cast the integer member_id's to varchars to allow for UPPER's
                    $sql = "SELECT m.*
							FROM tbl_member m
							WHERE UPPER(m.member_id::VARCHAR) = UPPER(?::VARCHAR)";
                    $params = array($member_id);
					$rs = $this->PDO->recordSetQuery($sql, $params);
					if($rs) {
						if(!$rs->EOF) {
							$rs->MoveFirst();
							array_push($response["array"], $rs->fields);

							$response["success"] = true;
							$response["message"] = "login successful";
							return json_encode($response);
						} else {
							$response["success"] = false;
							$response["message"] = "invalid login credentials";
							return json_encode($response);
						}
					} else {
						$response["success"] = false;
						$response["message"] = "ERROR L001: failed to log in";
						return json_encode($response);
					}
				} else {
					$response["success"] = false;
					$response["message"] = "ERROR L002: failed to log in";
					return json_encode($response);
				}
			} else {
				$response["success"] = false;
				$response["message"] = "ERROR L003: failed to log in";
				return json_encode($response);
			}
		} else {
            $response["success"] = false;
            $response["message"] = "invalid login credentials";
            return json_encode($response);
        }
	}

	function SyncPull($REQUEST) {
		// BMC 10.04.2016
		// 	-- the web service will sync the data from the server to the tablet
		$response["success"] = false;
		$response["message"] = "processing digitalbeef api webservice...";
		$response["array"] 	 = array();

		$member_id = (isset($REQUEST['member_id']) ? $REQUEST['member_id'] : "");

		if($member_id != "") {
			// TODO BMC 10.04.2016
			//	-- this needs to be inline with the tablet method
			//	-- we need to build the queries with command, table, columns, values etc
			$sql = "SELECT _id,
						query_timestamp,
						sql_query
					FROM syncable_queries
					WHERE member_id = ?
					ORDER BY query_timestamp ASC";
			$params = array($member_id);
			$rs = $this->PDO->recordSetQuery($sql, $params);
			if($rs) {
				if(!$rs->EOF) {
					$rs->MoveFirst();
					while(!$rs->EOF) {
						// BMC 07.07.2016
						//	-- PostGreSQL to SQLite conversion
						//		replace all instances of [IS NULL] with [= 'null']
						$rs->fields['sql_query'] = str_replace('IS NULL', "= 'null'", $rs->fields['sql_query']);

						array_push($response["array"], $rs->fields);

						$rs->MoveNext();
					}

					$response["success"] = true;
					$response["message"] = "syncable queries pulled successfully";
					return json_encode($response);
				} else {
					$response["success"] = true;
					$response["message"] = "no syncable queries available";
					return json_encode($response);
				}
			} else {
				$response["success"] = false;
				$response["message"] = "failed to run query: ".$sql;
				return json_encode($response);
			}
		} else {
			$response["success"] = false;
			$response["message"] = "failed to specify a member id";
			return json_encode($response);
		}
	}

	function SyncCompleted($REQUEST) {
		// BMC 10.04.2016
		// 	-- the web service will remove the data from the server as it has successfully been synced with the tablet
		$response["success"] = false;
		$response["message"] = "processing digitalbeef api webservice...";
		$response["array"] 	 = array();

		$member_id = (isset($REQUEST['member_id']) ? $REQUEST['member_id'] : "");

		if($member_id != "") {
			$sql = "DELETE FROM syncable_queries
					WHERE member_id = ?";
			$params = array($member_id);
			if($this->PDO->executeQuery($sql, $params, false)) {
				$response["success"] = true;
				$response["message"] = "syncables removed successfully";
				return json_encode($response);
			} else {
				$response["success"] = false;
				$response["message"] = "failed to run query: ".$sql;
				return json_encode($response);
			}
		} else {
			$response["success"] = false;
			$response["message"] = "failed to specify a member id";
			return json_encode($response);
		}
	}

	function SyncPush($REQUEST, $provider) {
		$beta_sync 	= (isset($REQUEST['beta_sync']) ? $REQUEST['beta_sync'] : "false");
		if($beta_sync === "true") {
			// BMC 10.05.2016
			// 	-- this is the new method we will use when pushing data to the server
			//		it's currently in testing so we don't have to worry about breaking anything
			$response["success"] = false;
			$response["message"] = "processing digitalbeef api webservice...";
			$response["array"] 	 = array();

			$query_timestamp 	= (isset($REQUEST['query_timestamp']) 	? $REQUEST['query_timestamp'] 	: "");
			$sql_query 			= (isset($REQUEST['sql_query']) 		? $REQUEST['sql_query'] 		: "");
			$command 			= (isset($REQUEST['command']) 			? $REQUEST['command'] 			: NULL);
			$table_string 		= (isset($REQUEST['table_string']) 		? $REQUEST['table_string'] 		: NULL);
			$columns_array 		= (isset($REQUEST['columns_array']) 	? $REQUEST['columns_array'] 	: NULL);
			$values_array 		= (isset($REQUEST['values_array']) 		? $REQUEST['values_array'] 		: NULL);
			$where_clause 		= (isset($REQUEST['where_clause']) 		? $REQUEST['where_clause'] 		: NULL);

			// BMC 10.04.2016
			//	-- if the command variable is null then we're using the old method
			//		so we can continue to sync the queries as we used to do.  But if the
			//		command variable is non-null then we'll use the new sync methods to
			//		sync the data to the server
            // BMC 11.04.2016
            //  -- initialized the JSON string to null this will be pulled from each
            //      included php file below
            $json = NULL;
			switch($command) {
				case "INSERT":
					include_once(SITE_ROOT . "/webservice/syncables/insert.php");
					break;
				case "UPDATE":
					include_once(SITE_ROOT . "/webservice/syncables/update.php");
					break;
				case "DELETE":
					include_once(SITE_ROOT . "/webservice/syncables/delete.php");
					break;
				default:
					// BMC 10.05.2016
					//	-- if the command is not set then we'll send the user to the "query" method
					//		where they can perform the default actions from the old sync method which
					//		will sync the sql query rather than breaking it apart, which is the new method.
					include_once(SITE_ROOT . "/webservice/syncables/query.php");
					break;
			}

			// finally return the json encoded response
			return $json;
		} else {
			// BMC 10.05.2016
			//	-- this is the original method that will run if test_mode is not running
			//		we have to manually set test_mode to "true" if we want it to run otherwise
			//		this will go because it will be set to "false"
			$response["success"] = false;
			$response["message"] = "processing digitalbeef api webservice...";
			$response["array"] 	 = array();

			$timestamp 	= (isset($REQUEST['query_timestamp']) 	? $REQUEST['query_timestamp'] 	: "");
			$sql 		= (isset($REQUEST['sql_query']) 		? $REQUEST['sql_query'] 		: "");

			// use the older syncing method which will sync by the query
			if($timestamp != "") {
				// BMC 07.07.2016
				//	-- SQLite to PostGreSQL conversion
				//		replace all instances of [= 'null'] with [IS NULL]
				$sql = str_replace("= 'null'", "IS NULL", $sql);

				// BMC 09.01.2016
				//	-- we will deal with adding a new animal to the database.
				//		coming from the app we will be given some temporary registration id
				//		and we need to change this temporary registration id so that we get an
				//		actual registration to be entered into the system.
				//	-- to do this i will search for the keyword of the temporary registration
				//		then i will replace it with the new registration.
				//	-- i'll store the temporary id to make sure i don't generate a new registration
				//		for each query that comes in, but only use the new registration where ever this
				//		temporary registration is found.
				//	-- $sql = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS registration";

				// check if there is a temporary registration
				$response["has_temp_registration"] = false;
				$start = strpos($sql, 'TEMP_REG_DB');
				if ($start !== false) {
					$updateRegFirstTime = true;	// we only want to tell the app to update the registrations on the first time
												// otherwise leave it alone, the work is already done.
					$tempReg 			= substr($sql, $start, 20);	// substr(string, start, length);
					$actualReg 			= "";
					// check if we've seen this temporary registration before
					$sql_chk = "SELECT actual_registration
								FROM temp_registration_check
								WHERE temp_registration = ?";
					$params = array($tempReg);
					$rs = $this->PDO->recordSetQuery($sql_chk, $params);
					if($rs) {
						if(!$rs->EOF) {
							// we have seen this temporary registration before
							// so we just need to get the actual registration and
							// we can continue on
							$rs->MoveFirst();
							$actualReg = strtoupper(trim($rs->fields['actual_registration']));
							$updateRegFirstTime = false;
						} else {
							// we have not seen this temporary registration before
							// so we must first get the actual registration and
							// insert the record into the table
							if($this->PDO->DB_TYPE === "NEW") {
								// we grab the registration like this from the new databases
								// any changes with a new database should be done here
								$sql_chk = "SELECT ('DB'::character varying::text || nextval('registration_sequence'::regclass)::character varying::text) AS actual_registration";
							} else if($this->PDO->DB_TYPE === "OLD") {
								// depending on the provider we need to generate the registration
								// with a specific sequence.  we can do that by following the rules
								// to get the proper registration below
								switch($provider) {
									case "CWCF_CATTLE":	# COWCALF
										$sql_chk = "SELECT ('DB'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
										break;
									case "AAKA_CATTLE":	# AKAUSHI
										// TODO BMC 09.01.2016
										//	-- determine the possible prefix for the new animals
										// 	--	Possible Prefix: 	AF: full blood
										//							AP: pure blood
										//							X: 	cross
										//							1T: terminal
										$sql_chk = "SELECT ('AF'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
										break;
									case "ACRS_CATTLE":	# CHIANINA
										// TODO BMC 09.01.2016
										//	-- possible prefixs:	RG:	registered
										//							PR:	performance
										$sql_chk = "SELECT ('RG'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
										break;
									case "AGVA_CATTLE":	# USA GELBVIEH
										// TODO BMC 09.01.2016
										//	-- possible prefix:	AMCX:	smart-select commercial
										//							AMGV:	registered gelbvieh
										//							AMAN:	american angus based
										$sql_chk = "SELECT ('AMGV'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
										break;
									case "CDGV_CATTLE":	# CAN GELBVIEH
										// BMC 09.01.2016
										//	-- possible prefix:	CDGV:	canadian gelbvieh
										$sql_chk = "SELECT ('CDGV'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
										break;
									case "NALRS_CATTLE": # USA LIMOUSIN
										// TODO BMC 09.01.2016
										//	-- possible prefix:	UR:	performance	(classification)
										//							NF: full blood	(classification)
										//							NP:	pure blood	(classification)
										//							NX:	commercial	(classification)
										//							registered: (classification).(sex =='C' ? 'F' : 'M')
										$sql_chk = "SELECT ('UR'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
										break;
									case "CLRS_CATTLE":	# CAN LIMOUSIN
										// TODO BMC 09.01.2016
										//	-- possible prefix:	UR:	performance	(classification)
										//							NF: full blood	(classification)
										//							NP:	pure blood	(classification)
										//							NX:	commercial	(classification)
										//							registered: (classification).(sex =='C' ? 'F' : 'M')
										$sql_chk = "SELECT ('UR'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
										break;
									case "AMARS_CATTLE": # MAINE-ANJOU
										// BMC 09.01.2016
										//	-- possible prefix:	P:	performance
										//							<blank>: registered
										$sql_chk = "SELECT (''::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
										break;
									case "ASA_CATTLE": # USA SHORTHORN
										// TODO BMC 09.01.2016
										//	-- possible prefixs:	AR:	shorthorn plus (1/4, 5/16, 3/8, 7/16, 1/2, 9/16, 5/8, 11/16, 3/4, 13/16, 7/8)
										//							DR: durham red (50%, 7/16th Dam or 15/16 Sire)
										//							U:	unregistered/performance
										$sql_chk = "SELECT ('AR'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";;
										break;
									default:
										$sql_chk = "SELECT ('DB'::character varying::text || nextval('seq_animal_registration'::regclass)::character varying::text) AS actual_registration";
									break;
								}
							}

							$rs = $this->PDO->recordSetQuery($sql_chk);
							if($rs) {
								if(!$rs->EOF) {
									$rs->MoveFirst();
									$actualReg = strtoupper(trim($rs->fields['actual_registration']));

									// now insert the temp and actual registrations into the table
									$sql_chk = "INSERT INTO temp_registration_check
													(temp_registration, actual_registration)
												VALUES
													(?, ?)";
									$params = array($tempReg, $actualReg);
									$this->PDO->executeQuery($sql_chk, $params);
								} else {
									$response["success"] = false;
									$response["message"] = "failed to handle temp registration: ERROR 3";
									return json_encode($response);
								}
							} else {
								$response["success"] = false;
								$response["message"] = "failed to handle temp registration: ERROR 2";
								return json_encode($response);
							}
						}
					} else {
						$response["success"] = false;
						$response["message"] = "failed to handle temp registration: ERROR 1";
						return json_encode($response);
					}

					// update the sql statement with the new registration
					if($actualReg != "") {
						// this will run normally below
						$sql = str_replace($tempReg, $actualReg, $sql);
					}

					// now tell the app that we have an actual registration to replace
					// for the temp registrations.  send back the actual registration
					// to update all instances of this within the app
					// we only want to update the registrations on the app the first time
					// that this comes through, every other query we run server side with
					// the changes.  the app should already have been updated
					if($updateRegFirstTime) {
						$response["has_temp_registration"] 	= true;
						$response["temp_registration"] 		= $tempReg;
						$response["actual_registration"] 	= $actualReg;
					}
				}

				// BMC 09.12.2016
				//	-- handle feedback submission
				//	-- if we see that the query is a specific insert query for feedback
				//		then we must handle it so that it doesn't get put into the database
				// check if there is a temporary registration
				if($this->PDO->DB_TYPE === "NEW") {
					$start = strpos($sql, 'INSERT INTO feedback');
				} else if($this->PDO->DB_TYPE === "OLD") {
					$start = strpos($sql, 'INSERT INTO tbl_feedback');
				}

				// use php mailer to send an email to the support account
				if ($start !== false) {
					## ------------------ PREPARE MAILERS ------------------ ##
					$message  = "<p>".$sql."</p>";

					// attempt to send the email
					if ($this->sendMail($message)) {
						$response["success"] = true;
						$response["message"] = "feedback submitted successfully";
						return json_encode($response);
					} else {
						$response["success"] = false;
						$response["message"] = "failed to send feedback";
						return json_encode($response);
					}
				} else {
					// if this is not feedback then we submit the query
					if($this->PDO->executeQuery($sql)) {
						$response["success"] = true;
						$response["message"] = "syncable query pushed successfully";
						return json_encode($response);
					} else {
						$response["success"] = false;
						$response["message"] = "failed to run query: ".$sql;
						return json_encode($response);
					}
				}
			} else {
                $response["success"] = false;
                $response["message"] = "failed to run query";
                return json_encode($response);
            }
		}
	}

	function SyncFull($REQUEST, $provider) {
		// BMC 10.04.2016
		// 	-- the web service will pull all the data related to the given member_id that has been passed
		$response["success"] = false;
		$response["message"] = "processing digitalbeef api webservice...";
		$response["array"] 	 = array();

		$member_id = (isset($REQUEST['member_id']) ? $REQUEST['member_id'] : "");

		if($this->PDO->DB_TYPE === "NEW") {
			// download the new database
			include_once(SITE_ROOT . "/webservice/syncables/download_new_db.php");

			// delete all the syncable queries when we are done
            $sql = "DELETE FROM syncable_queries
                    WHERE member_id = ?";
            $params = array($member_id);
            if(!$this->PDO->executeQuery($sql, $params)) {
                $response["success"] = false;
                $response["message"] = "failed to run query: " . $sql;
                return json_encode($response);
            }
		} else if($this->PDO->DB_TYPE === "OLD") {
			// download the old database
			include_once(SITE_ROOT . "/webservice/syncables/download_old_db.php");

            // delete all the syncable queries when we are done
            $sql = "DELETE FROM syncable_queries
                    WHERE member_id = ?";
            $params = array($member_id);
            if(!$this->PDO->executeQuery($sql, $params)) {
                $response["success"] = false;
                $response["message"] = "failed to run query: " . $sql;
                return json_encode($response);
            }
		} else {
			$response["success"] = false;
			$response["message"] = "failed to sync database";
			return json_encode($response);
		}

		// finally send all the data back to the user
		$response["success"] = true;
		$response["message"] = "full sync complete";
		return json_encode($response);
	}
}
?>
<?php
    // BMC 11.09.2016
    //  -- these functions are used to validate data.
    //  -- they are not being used anywhere just yet, but they originally came from Jim.
	function fnVerifyVariableIsDate($d) {
		$formats = array("d.m.Y", "d/m/Y", "m/d/Y", "Ymd", "Y-m-d"); 
		$formats = array("m/d/Y", "mdY", "m.d.Y", "m-d-Y", "Ymd", "Y-m-d", "d.m.Y", "d/m/Y"); 
		foreach ($formats as $format) {
			$date = DateTime::createFromFormat($format, $d);
			if ($date !== false)  return date('n/j/Y',strtotime($d));
		}
		return "";
	}
	
	function bigintval($value) {
		$value = trim($value);
		if (ctype_digit($value)) {
			return $value;
		}
		
		$value = preg_replace("/[^0-9](.*)$/", '', $value);
		
		if (ctype_digit($value)) {
			return $value;
		}
		return 0;
	}
	
	function fnGetFieldInformation($table_name,
								  $column_name) {
		$retVal = array('','','','');
		$sql = "select data_type,
					   character_maximum_length,
					   numeric_precision,
					   numeric_scale
				  from information_schema.columns 
				 where table_name ='".$table_name."' 
				   and column_name='".$column_name."'";
		$rs = rsHerdbook($sql);
		
		if($rs) {
			if(!$rs->EOF) {
				$rs->MoveFirst();
				$retVal[0] = $rs->fields['data_type'];
				$retVal[1] = $rs->fields['character_maximum_length'];
				$retVal[2] = $rs->fields['numeric_precision'];
				$retVal[3] = $rs->fields['numeric_scale'];
			}
		}
		return $retVal;
	}
	
	function fnValidateFieldInput($table_name,
								  $column_name,
								  $val, 
								  $data_type, 
								  $character_maximum_length, 
								  $numeric_precision, 
								  $numeric_scale) {
		if($table_name!='') {
			$arr = fnGetFieldInformation($table_name,$column_name);
			$data_type 					= $arr[0];
			$character_maximum_length 	= $arr[1];
			$numeric_precision 			= $arr[2];
			$numeric_scale 				= $arr[3];
		}
		$val = trim($val);
		
		switch($data_type) {
			case "bigint":
				$val = bigintval($val);
				break;
			case "boolean":
				if(strtolower($val)=='t') 	return 't';  // vs f
				if(strtolower($val)=='true')  return 't';  // vs false
				if(strtolower($val)=='1') 	return 't';  // vs 0
				if(strtolower($val)=='y') 	return 't';  // vs n
				if(strtolower($val)=='yes') 	return 't';  // vs no
				if(strtolower($val)=='on') 	return 't';  // vs off
				return 'f';
				break;
			case "character":
				$val = substr($val,0,$rs->fields['character_maximum_length']);
				break;
			case "character varying":
				$val = trim($val);
				if($rs->fields['character_maximum_length']!='') {
					$val = substr(trim($val),0,$rs->fields['character_maximum_length']);
				}
				break;
			case "date":
				$val = fnVerifyVariableIsDate($val);
				if($val=='')  return null;
				break;
			case "integer":
				$val = bigintval($val);
				break;
			case "numeric":
				if($numeric_scale==0) {
					$val = (int)$val;				
				} else {
					$val = sprintf("%01.".$numeric_scale."f", sprintf("%01.".$numeric_scale."f", $val)*1);
				}
				if(strlen($val)>$numeric_precision) return null;
				break;
		}
		return $val;
	}
?>
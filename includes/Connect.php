<?php	
class Connect {
	var $DATABASE;
	var $USERNAME;
	var $PASSWORD;
	var $HOSTNAME;
	
	var $DB_TYPE;
	
	var $MYSQL_DATABASE;
	var $MYSQL_USERNAME;
	var $MYSQL_HOSTNAME;
	var $MYSQL_PASSWORD;
	
	var $FILE_DIR;
	var $PROVIDER_URL;
	
	/*
	POSTGRESQL - HOST					||	DATABASE			||	USERNAME			||	PASSWORD
	sodium-pine-9.db.databaselabs.io	||	test_db				||	test_db				||	test_db
	sodium-pine-9.db.databaselabs.io	||	cowcalf				||	cowcalf_cattle		||	c0wc@lf
	sodium-pine-9.db.databaselabs.io	||	cps_swine			||	cps_swine			||	cp5_5w1n3
	sodium-pine-9.db.databaselabs.io	||	nsr_swine			||	nsr_swine			|| 	n@t10n@l_sw1ne
	sodium-pine-9.db.databaselabs.io	||	berkshire_swine		||	berkshire_swine		||	b3rk2h1r3_sw1n3
	sodium-pine-9.db.databaselabs.io	||	gelbvieh_american	||	gelbvieh			||	gelbv1eh
	sodium-pine-9.db.databaselabs.io	||	gelbvieh_canadian	||	gelbvieh_c			||	gelbv1eh_c
	sodium-pine-9.db.databaselabs.io	||	registry			||	registry			||	r&g1s+ry
	sodium-pine-9.db.databaselabs.io	||	american_maine_anjou||	maine-anjou			||	m@1ne-@nj0u
	sodium-pine-9.db.databaselabs.io	||	akaushi				||	akaushi				||	Ak@ush1
	sodium-pine-9.db.databaselabs.io	||	american_limousin	||	limousin			||	l1m0us1n
	sodium-pine-9.db.databaselabs.io	||	canadian_limousin	||	limousin_c			||	l1m0us1n_c
	sodium-pine-9.db.databaselabs.io	||	american_shorthorn	||	shorthorn			||	sh0r+h0rn
	sodium-pine-9.db.databaselabs.io	||	american_chianina	||	chianina			||	ch1@n1n@
	sodium-pine-9.db.databaselabs.io	||	abha_cattle			||	abha_cattle			||	4bh4_c4tt13
	sodium-pine-9.db.databaselabs.io	||	test_swine			||	test_swine			||	test_swine
	sodium-pine-9.db.databaselabs.io	||	greenacres			||	greenacres			||	gr33nacr3s

	MySQL - HOST						||	DATABASE			||	USERNAME			||	PASSWORD
	localhost							||	gelbvieh			||	gelbvieh			||	gelbv1eh
	localhost							||	gelbvieh_canadian	||	gelbvieh_c			||	gelbv1eh_c
	localhost							||	cowcalf				||	cowcalf				||	c0wc@lf
	localhost							||	akaushi				||	akaushi				||	Ak@ush1
	localhost							||	american_limousin	||	limousin			||	l1m0us1n
	localhost							||	canadian_limousin	||	limousin_c			||	l1m0us1n_c
	localhost							||	american_shorthorn	||	shorthorn			||	sh0r+h0rn
	localhost							||	american_maine_anjou||	maine-anjou			||	m@1ne-@nj0u
	localhost							||	american_chianina	||	chianina			||	ch1@n1n@
	*/
	
	function __construct($provider=NULL) {
		if(is_null($provider) || $provider == "") {
			die("cannot access webservice");
		} else {
			switch($provider) {
                case "DEV_CATTLE":	# DEVELOPER-CATTLE
                    $this->DATABASE = "developer_db";
                    $this->USERNAME = "developer";
                    $this->PASSWORD = "d3v310p3r";
                    $this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
                    $this->DB_TYPE	= "NEW";
                    $this->FILE_DIR	= "https://developer.digitalbeef.com/exports/";
                    $this->PROVIDER_URL = "https://developer.digitalbeef.com";
                    break;
                    break;
				case "TEST_DB":	# TEST-CATTLE
					$this->DATABASE = "test_db";
					$this->USERNAME = "test_db";
					$this->PASSWORD = "test_db";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "NEW";
					$this->FILE_DIR	= "https://test.digitalbeef.com/exports/";
					$this->PROVIDER_URL = "https://test.digitalbeef.com";
					break;
				case "TEST_SWINE":	# TEST-SWINE
					$this->DATABASE = "test_swine";
					$this->USERNAME = "test_swine";
					$this->PASSWORD = "test_swine";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "NEW";
					$this->FILE_DIR	= "https://test.digitalswine.com/exports/";
                    $this->PROVIDER_URL = "https://test.digitalswine.com";
					break;
                case "CWCF_CATTLE":	# COWCALF
                    $this->DATABASE = "cowcalf";
                    $this->USERNAME = "cowcalf_cattle";
                    $this->PASSWORD = "c0wc@lf";
                    $this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
                    $this->DB_TYPE	= "NEW";
                    $this->FILE_DIR	= "https://cowcalf.digitalbeef.com/exports/";
                    $this->PROVIDER_URL = "https://cowcalf.digitalbeef.com";
                    break;
				case "GREEN_CATTLE":# GREENACRES CATTLE
					$this->DATABASE = "greenacres";
					$this->USERNAME = "greenacres";
					$this->PASSWORD = "gr33nacr3s";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "NEW";
					$this->FILE_DIR	= "https://greenacres.digitalbeef.com/exports/";
                    $this->PROVIDER_URL = "https://greenacres.digitalbeef.com";
					break;
				case "CPS_SWINE":	# CERTIFIED PEDIGREE SWINE
					$this->DATABASE = "cps_swine";
					$this->USERNAME = "cps_swine";
					$this->PASSWORD = "cp5_5w1n3";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "NEW";
					$this->FILE_DIR	= "https://cps.digitalswine.com/exports/";
                    $this->PROVIDER_URL = "https://cps.digitalswine.com";
					break;
				case "NSR_SWINE":	# NATIONAL SWINE REGISTRY
					$this->DATABASE = "nsr_swine";
					$this->USERNAME = "nsr_swine";
					$this->PASSWORD = "n@t10n@l_sw1ne";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "NEW";
					$this->FILE_DIR	= "https://nsr.digitalswine.com/exports/";
                    $this->PROVIDER_URL = "https://nsr.digitalswine.com";
					break;
				case "BKS_SWINE":	# BERKSHIRE (NATIONAL SWINE REGISTRY)
					$this->DATABASE = "berkshire_swine";
					$this->USERNAME = "berkshire_swine";
					$this->PASSWORD = "b3rk2h1r3_sw1n3";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "NEW";
					$this->FILE_DIR	= "https://berkshire.digitalswine.com/exports/";
                    $this->PROVIDER_URL = "https://berkshire.digitalswine.com";
					break;
				case "ABHA_CATTLE":	# AMERICAN BLACK HEREFORD ASSOCIATION
					$this->DATABASE = "abha_cattle";
					$this->USERNAME = "abha_cattle";
					$this->PASSWORD = "4bh4_c4tt13";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "NEW";
					$this->FILE_DIR	= "https://abha.digitalbeef.com/exports/";
                    $this->PROVIDER_URL = "https://abha.digitalbeef.com";
					break;
				case "AAKA_CATTLE":	# AKAUSHI
					$this->DATABASE = "akaushi";
					$this->USERNAME = "akaushi";
					$this->PASSWORD = "Ak@ush1";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "OLD";
					$this->MYSQL_DATABASE = "akaushi";
					$this->MYSQL_USERNAME = "akaushi";
					$this->MYSQL_PASSWORD = "Ak@ush1";
					$this->MYSQL_HOSTNAME = "localhost";
					$this->FILE_DIR	= "https://akaushi.digitalbeef.com/exports/herd_reports/";
                    $this->PROVIDER_URL = "https://akaushi.digitalbeef.com";
					break;
				case "ACRS_CATTLE":	# CHIANINA
					$this->DATABASE = "american_chianina";
					$this->USERNAME = "chianina";
					$this->PASSWORD = "ch1@n1n@";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "OLD";
					$this->MYSQL_DATABASE = "american_chianina";
					$this->MYSQL_USERNAME = "chianina";
					$this->MYSQL_PASSWORD = "ch1@n1n@";
					$this->MYSQL_HOSTNAME = "localhost";
					$this->FILE_DIR	= "https://chianina.digitalbeef.com/exports/herd_reports/";
                    $this->PROVIDER_URL = "https://chianina.digitalbeef.com";
					break;
				case "AGVA_CATTLE":	# USA GELBVIEH
					$this->DATABASE = "gelbvieh_american";
					$this->USERNAME = "gelbvieh";
					$this->PASSWORD = "gelbv1eh";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "OLD";
					$this->MYSQL_DATABASE = "gelbvieh";
					$this->MYSQL_USERNAME = "gelbvieh";
					$this->MYSQL_PASSWORD = "gelbv1eh";
					$this->MYSQL_HOSTNAME = "localhost";
					$this->FILE_DIR	= "https://gelbvieh.digitalbeef.com/exports/herd_reports/";
                    $this->PROVIDER_URL = "https://gelbvieh.digitalbeef.com";
					break;
				case "CDGV_CATTLE":	# CAN GELBVIEH
					$this->DATABASE = "gelbvieh_canadian";
					$this->USERNAME = "gelbvieh_c";
					$this->PASSWORD = "gelbv1eh_c";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "OLD";
					$this->MYSQL_DATABASE = "gelbvieh_canadian";
					$this->MYSQL_USERNAME = "gelbvieh_c";
					$this->MYSQL_PASSWORD = "gelbv1eh_c";
					$this->MYSQL_HOSTNAME = "localhost";
					$this->FILE_DIR	= "https://cdgv.digitalbeef.com/exports/herd_reports/";
                    $this->PROVIDER_URL = "https://cdgv.digitalbeef.com";
					break;
				case "NALRS_CATTLE": # USA LIMOUSIN
					$this->DATABASE = "american_limousin";
					$this->USERNAME = "limousin";
					$this->PASSWORD = "l1m0us1n";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "OLD";
					$this->MYSQL_DATABASE = "american_limousin";
					$this->MYSQL_USERNAME = "limousin";
					$this->MYSQL_PASSWORD = "l1m0us1n";
					$this->MYSQL_HOSTNAME = "localhost";
					$this->FILE_DIR	= "https://limousin.digitalbeef.com/exports/herd_reports/";
                    $this->PROVIDER_URL = "https://limousin.digitalbeef.com";
					break;
				case "CLRS_CATTLE":	# CAN LIMOUSIN
					$this->DATABASE = "canadian_limousin";
					$this->USERNAME = "limousin_c";
					$this->PASSWORD = "l1m0us1n_c";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "OLD";
					$this->MYSQL_DATABASE = "canadian_limousin";
					$this->MYSQL_USERNAME = "limousin_c";
					$this->MYSQL_PASSWORD = "l1m0us1n_c";
					$this->MYSQL_HOSTNAME = "localhost";
					$this->FILE_DIR	= "https://cla.digitalbeef.com/exports/herd_reports/";
                    $this->PROVIDER_URL = "https://cla.digitalbeef.com";
					break;
				case "AMARS_CATTLE": # MAINE-ANJOU
					$this->DATABASE = "american_maine_anjou";
					$this->USERNAME = "maine-anjou";
					$this->PASSWORD = "m@1ne-@nj0u";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "OLD";
					$this->MYSQL_DATABASE = "american_maine_anjou";
					$this->MYSQL_USERNAME = "maine-anjou";
					$this->MYSQL_PASSWORD = "m@1ne-@nj0u";
					$this->MYSQL_HOSTNAME = "localhost";
					$this->FILE_DIR	= "https://maine-anjou.digitalbeef.com/exports/herd_reports/";
                    $this->PROVIDER_URL = "https://maine-anjou.digitalbeef.com";
					break;
				case "ASA_CATTLE": # USA SHORTHORN
					$this->DATABASE = "american_shorthorn";
					$this->USERNAME = "shorthorn";
					$this->PASSWORD = "sh0r+h0rn";
					$this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
					$this->DB_TYPE	= "OLD";
					$this->MYSQL_DATABASE = "american_shorthorn";
					$this->MYSQL_USERNAME = "shorthorn";
					$this->MYSQL_PASSWORD = "sh0r+h0rn";
					$this->MYSQL_HOSTNAME = "localhost";
					$this->FILE_DIR	= "https://shorthorn.digitalbeef.com/exports/herd_reports/";
                    $this->PROVIDER_URL = "https://shorthorn.digitalbeef.com";
					break;
                case "TEST_BMC":
                    $this->DATABASE = "test_bmc";
                    $this->USERNAME = "bcalabro";
                    $this->PASSWORD = "Kobraki25";
                    $this->HOSTNAME = "sodium-pine-9.db.databaselabs.io";
                    $this->DB_TYPE	= "NEW";
                    $this->FILE_DIR	= NULL;
                    $this->PROVIDER_URL = NULL;
                    break;
				default:
					die("cannot access webservice");
					break;	
			}
		}
	}
	
	############################## RECORD SET QUERY ##############################
	function recordSetQuery($sql, $params=NULL) {
		// make the connection
		$pg_conn = NewADOConnection('postgres');
		$pg_conn->SetFetchMode(ADODB_FETCH_BOTH);
		$pg_conn->Connect($this->HOSTNAME, $this->USERNAME, $this->PASSWORD, $this->DATABASE);
			
		// run the query
		$isSuccess = false;
		if(is_null($params) && trim($sql) != "") {
			// no params
			if($rsTemp = $pg_conn->Execute($sql)) {
				$isSuccess = true;
			} else {
				$isSuccess = false;
			}
		} else if(!is_null($params) && trim($sql) != "") {
			// with params
			if($rsTemp = $pg_conn->Execute($sql, $params)) {
				$isSuccess = true;
			} else {
				$isSuccess = false;
			}
		}
		
		// log any bad queries
		if (!$isSuccess) {
			$this->logBadQuery($sql, $params, $pg_conn->getError());
		}
		
		if($isSuccess) {
			// return the record set
			return $rsTemp;
		} else {
			// return false, error
			return false;
		}
	}
	
	############################## EXECUTE QUERY ##############################
	function executeQuery($sql, $params=NULL) {
		// make the connection
		$pg_conn = NewADOConnection('postgres');
		$pg_conn->SetFetchMode(ADODB_FETCH_BOTH);
		$pg_conn->Connect($this->HOSTNAME, $this->USERNAME, $this->PASSWORD, $this->DATABASE);
		
		// run the query
		$isSuccess = $pg_conn->Execute($sql, $params);
		
		// log any bad queries
		if (!$isSuccess) {
			$this->logBadQuery($sql, $params, $pg_conn->getError());
		}
		
		return $isSuccess;
	}
	
	############################## BAD QUERIES ##############################
	function logBadQuery($sql, $params=NULL, $error_message=NULL) {
		// make the connection
		$pg_conn = NewADOConnection('postgres');
		$pg_conn->SetFetchMode(ADODB_FETCH_BOTH);
		$pg_conn->Connect($this->HOSTNAME, $this->USERNAME, $this->PASSWORD, $this->DATABASE);
		
		// log any bad queries
		if(!is_null($params)) {
			$sql .= "\n(";
			for($i = 0; $i < count($params); $i++) {
				if($i > 0) {
					$sql .= ", ".$params[$i];
				} else {
					$sql .= $params[$i];
				}
			}
			$sql .= ")";
		}
		
		// log the query
		if($this->DB_TYPE === "NEW") {
			$arr 	= array();
			$arr[0] = $sql;
			$arr[1] = $_SERVER['SCRIPT_NAME'];
			$arr[2] = $_SERVER['REMOTE_ADDR'];
			$arr[3] = $error_message;
			$sql2 	= "INSERT INTO bad_query 
							(query, user_id, ip_address, error_message) 
						VALUES 
							(?, ?, ?, ?);";
			$pg_conn->Execute($sql2, $arr);
		} else if($this->DB_TYPE === "OLD") {
			$arr 	= array();
			$arr[0] = $sql;
			$arr[1] = $_SERVER['SCRIPT_NAME'];
			$arr[2] = $_SERVER['REMOTE_ADDR'];
			//$arr[3] = date("Y-m-d h:i:s");
			$sql2 	= "INSERT INTO tbl_bad_queries 
							(query_string, user_id, ip_address, query_date_time) 
						VALUES 
							(?, ?, ?);";
			$pg_conn->Execute($sql2, $arr);

		}
	}
	
	############################## MYSQL RECORD SET QUERY ##############################
	function recordSetQueryMySql($sql, $params=NULL) {
		if(trim($sql) == '') {
			return false;
		} else {
			$mysql_conn = NewADOConnection('mysql');
			$mysql_conn->SetFetchMode(ADODB_FETCH_BOTH);	
			$mysql_conn->Connect($this->MYSQL_HOSTNAME, $this->MYSQL_USERNAME, $this->MYSQL_PASSWORD, $this->MYSQL_DATABASE);
	
			return $mysql_conn->Execute($sql, $params);
		}
	}
	
	function executeQueryMySql($sql, $params=NULL) {
		if(trim($sql) == '') {
			return false;
		} else {
			$mysql_conn = NewADOConnection('mysql');
			$mysql_conn->SetFetchMode(ADODB_FETCH_BOTH);	
			$mysql_conn->Connect($this->MYSQL_HOSTNAME, $this->MYSQL_USERNAME, $this->MYSQL_PASSWORD, $this->MYSQL_DATABASE);

			return $mysql_conn->Execute($sql, $params);
		}
	}
}